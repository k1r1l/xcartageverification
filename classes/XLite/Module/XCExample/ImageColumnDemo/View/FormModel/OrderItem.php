<?php

namespace XLite\Module\XCExample\ImageColumnDemo\View\FormModel;

use \XLite\View\AView;
use \XLite\View\ItemsList\Model\Table;

/**
 * @ListChild (list="layout.footer", zone="admin", weight="1000")
 */

class OrderItem extends \XLite\View\ItemsList\Model\OrderItem implements \XLite\Base\IDecorator
{
    protected function defineColumns()
    {
        //todo: Добавляю колонку с Image
        $columns = parent::defineColumns();
        $columns = array(
            'name' => array(
                static::COLUMN_NAME => static::t('Order Item'),
                static::COLUMN_CREATE_CLASS => 'XLite\View\FormField\Inline\Select\Model\Product\OrderItem',
                static::COLUMN_PARAMS => array(
                    'required' => true,
                    \XLite\View\FormField\Select\Model\OrderItemSelector::PARAM_ORDER_ID => $this->getOrder()->getOrderId(),
                ),
                static::COLUMN_MAIN => true,
                static::COLUMN_ORDERBY => 100,
            ),
            'image' => array(
                static::COLUMN_NAME => static::t('Image'),
                static::COLUMN_TEMPLATE => 'modules/XCExample/ImageColumnDemo/html_code.twig',
                static::COLUMN_ORDERBY => 200,
            ),
            'price' => array(
                static::COLUMN_NAME => static::t('Price'),
                static::COLUMN_CLASS => 'XLite\View\FormField\Inline\Input\Text\Price\OrderItemPrice',
                static::COLUMN_PARAMS => array(
                    \XLite\View\FormField\Input\Text\Base\Numeric::PARAM_MIN => 0,
                    \XLite\View\FormField\Input\Text\Base\Numeric::PARAM_MOUSE_WHEEL_CTRL => false,
                ),
                static::COLUMN_ORDERBY => 300,
            ),
            'amount' => array(
                static::COLUMN_NAME => static::t('Qty'),
                static::COLUMN_CLASS => 'XLite\View\FormField\Inline\Input\Text\Integer\OrderItemAmount',
                static::COLUMN_PARAMS => array('required' => true),
                static::COLUMN_ORDERBY => 400,
            ),
            'total' => array(
                static::COLUMN_NAME => static::t('Total'),
                static::COLUMN_TEMPLATE => 'items_list/model/table/order_item/cell.total.twig',
                static::COLUMN_CREATE_TEMPLATE => 'items_list/model/table/order_item/cell.total.twig',
                static::COLUMN_ORDERBY => 500,
            ),
        );
        return $columns;
    }
}