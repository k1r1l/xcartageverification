<?php

namespace XLite\Module\XCExample\ImageColumnDemo\View;

class Image extends \XLite\View\Image implements \XLite\Base\IDecorator
{
    protected function defineWidgetParams()
    {
        parent::defineWidgetParams();

        $this->widgetParams[self::PARAM_USE_CACHE] = new \XLite\Model\WidgetParam\TypeBool('Use cache', 0);
    }
}