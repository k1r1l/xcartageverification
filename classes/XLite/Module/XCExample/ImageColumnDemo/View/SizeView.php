<?php

namespace XLite\Module\XCExample\ImageColumnDemo\View;
//    //todo: setting

abstract class SizeView extends \XLite\View\AView implements \XLite\Base\IDecorator
{
    /**
     * @return string
     */
//    protected function getDefaultTemplate()
//    {
//        return 'modules/XCExample/ImageColumnDemo/html_code.twig';
//    }

    /**
     * @return mixed
     */
    public function getHtmlCode()
    {
        return \XLite\Core\Config::getInstance()->XCExample->ImageColumnDemo->size_image;
    }
}