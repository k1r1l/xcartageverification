<?php
namespace XLite\Module\XCExample\ImageWidgetDemo\Model\DTO\Product;

/**
 * Abstract widget
 */
abstract class Info extends \XLite\Model\DTO\Product\Info implements \XLite\Base\IDecorator
{
    protected function init($object)
    {
        parent::init($object);

        $secondaryImages = $object->getSecondaryImages();

        $dtoImages = [
            0 => [
                'delete'   => false,
                'position' => '',
                'alt'      => '',
                'temp_id'  => '',
            ]
        ];

        foreach ($secondaryImages as $image) {
            $dtoImages[$image->getId()] = [
                'delete'   => false,
                'position' => '',
                'alt'      => '',
                'temp_id'  => '',
            ];
        }

        $this->default->secondary_images = $dtoImages;
    }

    public function populateTo($object, $rawData = null)
    {
        parent::populateTo($object, $rawData);

        $object->processFiles('secondaryImages', $this->default->secondary_images);
    }
}