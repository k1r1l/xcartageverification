<?php

namespace XLite\Module\XCExample\ImageWidgetDemo\Model\Image\Product;

/**
 * @Entity
 * @Table  (name="product_secondary_images")
 */
class SecondaryImage extends \XLite\Model\Base\Image
{
    /**
     * @Column (type="integer")
     */
    protected $orderby = 0;

    /**
     * @ManyToOne  (targetEntity="\XLite\Model\Product", inversedBy="secondary_images")
     * @JoinColumn (name="product_id", referencedColumnName="product_id")
     */
    protected $product;

    /**
     * @Column (type="string", length=255)
     */
    protected $alt = '';
}