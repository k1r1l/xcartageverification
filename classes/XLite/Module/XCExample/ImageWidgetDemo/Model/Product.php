<?php


namespace XLite\Module\XCExample\ImageWidgetDemo\Model;

abstract class Product extends \XLite\Model\Product implements \XLite\Base\IDecorator
{
    /**
     * @OneToMany (targetEntity="XLite\Module\XCExample\ImageWidgetDemo\Model\Image\Product\SecondaryImage", mappedBy="product", cascade={"all"})
     * @OrderBy   ({"orderby" = "ASC"})
     */
    protected $secondaryImages;

    public function __construct(array $data = array())
    {
        parent::__construct($data);

        $this->secondaryImages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getSecondaryImages()
    {
        return $this->secondaryImages;
    }

    public function addSecondaryImages($image)
    {
        $this->secondaryImages[] = $image;
        return $this;
    }
}