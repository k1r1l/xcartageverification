<?php

namespace XLite\Module\XCExample\ImageWidgetDemo\Model\Repo\Image\Product;

class SecondaryImage extends \XLite\Model\Repo\Base\Image
{
    /**
     * Returns the name of the directory within '<X-Cart>/images' where images are stored
     */
    public function getStorageName()
    {
        return 'product_secondary';
    }
}