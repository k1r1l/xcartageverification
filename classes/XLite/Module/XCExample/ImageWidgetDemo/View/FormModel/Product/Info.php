<?php

namespace XLite\Module\XCExample\ImageWidgetDemo\View\FormModel\Product;

/**
 * Product form model
 */
abstract class Info extends \XLite\View\FormModel\Product\Info implements \XLite\Base\IDecorator
{
    protected function defineFields()
    {
        $schema = parent::defineFields();

        $product = \XLite\Core\Database::getRepo('XLite\Model\Product')->find($this->getDataObject()->default->identity);

        $secondaryImages = [];

        if ($product) {
            $secondaryImages = $product->getSecondaryImages();
        }

        $schema[self::SECTION_DEFAULT]['secondary_images'] = [
            'label' => static::t('Secondary Images'),
            'type' => 'XLite\View\FormModel\Type\UploaderType',
            'imageClass' => 'XLite\Module\XCExample\ImageWidgetDemo\Model\Image\Product\SecondaryImage',
            'files' => $secondaryImages,
            'multiple' => true,
            'position' => 350,
        ];

        return $schema;
    }
}