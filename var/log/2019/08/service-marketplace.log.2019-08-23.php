<?php die(); ?>
Request to marketplace "XCart\Marketplace\Request\Set"

Time: 2019-08-23 16:50:15;
Channel: service-marketplace.INFO;
Runtime id: 21b6c28c8e759ec4940e6bce2142e498;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\Set
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "querySets": {
        "get_XC5_notifications": [
            0
        ]
    }
}

Time: 2019-08-23 16:50:15;
Channel: service-marketplace.DEBUG;
Runtime id: 21b6c28c8e759ec4940e6bce2142e498;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Fri, 23 Aug 2019 12:50:16 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "799"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"get_XC5_notifications\":[{\"type\":\"module\",\"module\":\"XC-FacebookMarketing\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/facebookinsta.png\",\"title\":\"Advertise your products on Facebook and Instagram and see your sales grow\",\"description\":\"Try the free Facebook Ads and Instagram Ads addon for X-Cart\",\"link\":\"\",\"date\":\"1558040400\"},{\"type\":\"news\",\"module\":\"\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/GA1.png\",\"title\":\"FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.\",\"description\":\"\",\"link\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup\",\"date\":\"1557262800\"}]}"
}

Time: 2019-08-23 16:50:16;
Channel: service-marketplace.DEBUG;
Runtime id: 21b6c28c8e759ec4940e6bce2142e498;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-08-23 16:50:16;
Channel: service-marketplace.DEBUG;
Runtime id: 21b6c28c8e759ec4940e6bce2142e498;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-08-23 16:50:16;
Channel: service-marketplace.DEBUG;
Runtime id: 21b6c28c8e759ec4940e6bce2142e498;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace correctly received "XCart\Marketplace\Request\Set"

Time: 2019-08-23 16:50:16;
Channel: service-marketplace.INFO;
Runtime id: 21b6c28c8e759ec4940e6bce2142e498;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (formatted)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "XCart\\Marketplace\\Request\\Notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-08-23 16:50:16;
Channel: service-marketplace.DEBUG;
Runtime id: 21b6c28c8e759ec4940e6bce2142e498;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

