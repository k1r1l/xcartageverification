<?php die(); ?>
2019-08-25 13:08:10	[scenario5d624ffa131e40.29068205]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 13:08:12	[scenario5d624ffa131e40.29068205]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    }
}

2019-08-25 13:08:12	[scenario5d624ffa131e40.29068205]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 35877454141591e0ab8f42a566c599b0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 13:08:12	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_first
2019-08-25 13:08:12	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_second
2019-08-25 13:08:18	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_third
2019-08-25 13:08:19	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_fourth
2019-08-25 13:08:22	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_fifth
2019-08-25 13:08:32	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_six
2019-08-25 13:08:34	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_seven
2019-08-25 13:08:37	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_nine
2019-08-25 13:08:39	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_ten
2019-08-25 13:08:40	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_eleven
2019-08-25 13:08:41	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_twelve
2019-08-25 13:08:48	[scenario5d624ffa131e40.29068205]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 13:08:48	[scenario5d624ffa131e40.29068205]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "XCExample-ImageWidgetDemo"
]
Context missing => []

2019-08-25 13:08:48	[scenario5d624ffa131e40.29068205]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 35877454141591e0ab8f42a566c599b0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 13:08:49	[scenario5d624ffa131e40.29068205]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 13:08:50	[scenario5d624ffa131e40.29068205]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 13:08:51	[scenario5d624ffa131e40.29068205]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 13:08:52	[scenario5d624ffa131e40.29068205]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 13:08:52	[scenario5d624ffa131e40.29068205]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 35877454141591e0ab8f42a566c599b0
Context transitions => [
    "step_thirteen"
]

2019-08-25 13:08:53	[scenario5d624ffa131e40.29068205]	Execute X-Cart step: step_thirteen
2019-08-25 13:08:55	[scenario5d624ffa131e40.29068205]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 13:09:23	[scenario5d624ffb5cc335.91266126]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:09:23	[scenario5d624ffb5cc335.91266126]	Update script transitions
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:09:23	[scenario5d624ffb5cc335.91266126]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:09:23	[scenario5d624ffb5cc335.91266126]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true
    }
}

2019-08-25 13:09:23	[scenario5d624ffb5cc335.91266126]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d02ee15cd3169027644cefb144b58bc8
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 13:09:23	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_first
2019-08-25 13:09:24	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_second
2019-08-25 13:09:28	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_third
2019-08-25 13:09:29	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_fourth
2019-08-25 13:09:31	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_fifth
2019-08-25 13:09:31	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_six
2019-08-25 13:09:32	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_seven
2019-08-25 13:09:33	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_nine
2019-08-25 13:09:33	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_ten
2019-08-25 13:09:34	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_eleven
2019-08-25 13:09:34	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_twelve
2019-08-25 13:09:39	[scenario5d624ffb5cc335.91266126]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:09:39	[scenario5d624ffb5cc335.91266126]	Data updated: XCExample-ImageWidgetDemo
2019-08-25 13:09:39	[scenario5d624ffb5cc335.91266126]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 13:09:39	[scenario5d624ffb5cc335.91266126]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d02ee15cd3169027644cefb144b58bc8
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 13:09:39	[scenario5d624ffb5cc335.91266126]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 13:09:40	[scenario5d624ffb5cc335.91266126]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 13:09:40	[scenario5d624ffb5cc335.91266126]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 13:09:40	[scenario5d624ffb5cc335.91266126]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 13:09:40	[scenario5d624ffb5cc335.91266126]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d02ee15cd3169027644cefb144b58bc8
Context transitions => [
    "step_thirteen"
]

2019-08-25 13:09:40	[scenario5d624ffb5cc335.91266126]	Execute X-Cart step: step_thirteen
2019-08-25 13:09:42	[scenario5d624ffb5cc335.91266126]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 13:24:11	[scenario5d6253bb6b4e86.43840118]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 13:24:13	[scenario5d6253bb6b4e86.43840118]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true
    }
}

2019-08-25 13:24:13	[scenario5d6253bb6b4e86.43840118]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 95a9f8141cd58a4aef1844bb1ff7ada8
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 13:24:13	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_first
2019-08-25 13:24:13	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_second
2019-08-25 13:24:18	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_third
2019-08-25 13:24:19	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_fourth
2019-08-25 13:24:22	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_fifth
2019-08-25 13:24:23	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_six
2019-08-25 13:24:23	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_seven
2019-08-25 13:24:25	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_nine
2019-08-25 13:24:25	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_ten
2019-08-25 13:24:25	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_eleven
2019-08-25 13:24:26	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_twelve
2019-08-25 13:24:32	[scenario5d6253bb6b4e86.43840118]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 13:24:32	[scenario5d6253bb6b4e86.43840118]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 13:24:32	[scenario5d6253bb6b4e86.43840118]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 95a9f8141cd58a4aef1844bb1ff7ada8
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 13:24:32	[scenario5d6253bb6b4e86.43840118]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 13:24:32	[scenario5d6253bb6b4e86.43840118]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 13:24:33	[scenario5d6253bb6b4e86.43840118]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 13:24:33	[scenario5d6253bb6b4e86.43840118]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 13:24:33	[scenario5d6253bb6b4e86.43840118]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 95a9f8141cd58a4aef1844bb1ff7ada8
Context transitions => [
    "step_thirteen"
]

2019-08-25 13:24:33	[scenario5d6253bb6b4e86.43840118]	Execute X-Cart step: step_thirteen
2019-08-25 13:24:35	[scenario5d6253bb6b4e86.43840118]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 13:31:22	[scenario5d62556a4658a6.37038633]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 13:31:23	[scenario5d62556a4658a6.37038633]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true
    }
}

2019-08-25 13:31:23	[scenario5d62556a4658a6.37038633]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5a1fc521eab81307dddd0934b4a5ab0d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 13:31:24	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_first
2019-08-25 13:31:24	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_second
2019-08-25 13:31:29	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_third
2019-08-25 13:31:30	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_fourth
2019-08-25 13:31:33	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_fifth
2019-08-25 13:31:33	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_six
2019-08-25 13:31:34	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_seven
2019-08-25 13:31:35	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_nine
2019-08-25 13:31:36	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_ten
2019-08-25 13:31:36	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_eleven
2019-08-25 13:31:36	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_twelve
2019-08-25 13:31:42	[scenario5d62556a4658a6.37038633]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 13:31:43	[scenario5d62556a4658a6.37038633]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 13:31:43	[scenario5d62556a4658a6.37038633]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5a1fc521eab81307dddd0934b4a5ab0d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 13:31:43	[scenario5d62556a4658a6.37038633]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 13:31:43	[scenario5d62556a4658a6.37038633]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 13:31:43	[scenario5d62556a4658a6.37038633]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 13:31:43	[scenario5d62556a4658a6.37038633]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 13:31:44	[scenario5d62556a4658a6.37038633]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5a1fc521eab81307dddd0934b4a5ab0d
Context transitions => [
    "step_thirteen"
]

2019-08-25 13:31:44	[scenario5d62556a4658a6.37038633]	Execute X-Cart step: step_thirteen
2019-08-25 13:31:46	[scenario5d62556a4658a6.37038633]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 13:32:56	[scenario5d625074bc80f3.80181429]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:32:56	[scenario5d625074bc80f3.80181429]	Update script transitions
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:32:56	[scenario5d625074bc80f3.80181429]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:32:56	[scenario5d625074bc80f3.80181429]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false
    }
}

2019-08-25 13:32:56	[scenario5d625074bc80f3.80181429]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bb2f27e8fa059fe44f325d3f0163d481
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 13:32:56	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_first
2019-08-25 13:32:57	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_second
2019-08-25 13:33:02	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_third
2019-08-25 13:33:03	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_fourth
2019-08-25 13:33:05	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_fifth
2019-08-25 13:33:05	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_six
2019-08-25 13:33:06	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_seven
2019-08-25 13:33:07	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_nine
2019-08-25 13:33:08	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_ten
2019-08-25 13:33:08	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_eleven
2019-08-25 13:33:09	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_twelve
2019-08-25 13:33:17	[scenario5d625074bc80f3.80181429]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:33:18	[scenario5d625074bc80f3.80181429]	Data updated: XCExample-ImageWidgetDemo
2019-08-25 13:33:18	[scenario5d625074bc80f3.80181429]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 13:33:18	[scenario5d625074bc80f3.80181429]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bb2f27e8fa059fe44f325d3f0163d481
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 13:33:18	[scenario5d625074bc80f3.80181429]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 13:33:19	[scenario5d625074bc80f3.80181429]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 13:33:19	[scenario5d625074bc80f3.80181429]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 13:33:19	[scenario5d625074bc80f3.80181429]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 13:33:19	[scenario5d625074bc80f3.80181429]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bb2f27e8fa059fe44f325d3f0163d481
Context transitions => [
    "step_thirteen"
]

2019-08-25 13:33:19	[scenario5d625074bc80f3.80181429]	Execute X-Cart step: step_thirteen
2019-08-25 13:33:21	[scenario5d625074bc80f3.80181429]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 13:36:17	[scenario5d625691b4aa24.53846041]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 13:36:19	[scenario5d625691b4aa24.53846041]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false
    }
}

2019-08-25 13:36:19	[scenario5d625691b4aa24.53846041]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f4f926e5bdc55b27f64043cee28e166b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 13:36:19	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_first
2019-08-25 13:36:20	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_second
2019-08-25 13:36:25	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_third
2019-08-25 13:36:25	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_fourth
2019-08-25 13:36:27	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_fifth
2019-08-25 13:36:28	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_six
2019-08-25 13:36:28	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_seven
2019-08-25 13:36:30	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_nine
2019-08-25 13:36:30	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_ten
2019-08-25 13:36:30	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_eleven
2019-08-25 13:36:31	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_twelve
2019-08-25 13:36:37	[scenario5d625691b4aa24.53846041]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 13:36:38	[scenario5d625691b4aa24.53846041]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 13:36:38	[scenario5d625691b4aa24.53846041]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f4f926e5bdc55b27f64043cee28e166b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 13:36:38	[scenario5d625691b4aa24.53846041]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 13:36:38	[scenario5d625691b4aa24.53846041]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 13:36:39	[scenario5d625691b4aa24.53846041]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 13:36:39	[scenario5d625691b4aa24.53846041]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 13:36:39	[scenario5d625691b4aa24.53846041]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f4f926e5bdc55b27f64043cee28e166b
Context transitions => [
    "step_thirteen"
]

2019-08-25 13:36:39	[scenario5d625691b4aa24.53846041]	Execute X-Cart step: step_thirteen
2019-08-25 13:36:41	[scenario5d625691b4aa24.53846041]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 13:38:08	[scenario5d625692758ba0.27105521]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:38:08	[scenario5d625692758ba0.27105521]	Update script transitions
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:38:08	[scenario5d625692758ba0.27105521]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:38:09	[scenario5d625692758ba0.27105521]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true
    }
}

2019-08-25 13:38:09	[scenario5d625692758ba0.27105521]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ee102a7760f78481f3ac339fc26a0d03
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 13:38:09	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_first
2019-08-25 13:38:09	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_second
2019-08-25 13:38:13	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_third
2019-08-25 13:38:14	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_fourth
2019-08-25 13:38:16	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_fifth
2019-08-25 13:38:16	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_six
2019-08-25 13:38:17	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_seven
2019-08-25 13:38:18	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_nine
2019-08-25 13:38:19	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_ten
2019-08-25 13:38:19	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_eleven
2019-08-25 13:38:19	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_twelve
2019-08-25 13:38:24	[scenario5d625692758ba0.27105521]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 13:38:24	[scenario5d625692758ba0.27105521]	Data updated: XCExample-ImageWidgetDemo
2019-08-25 13:38:25	[scenario5d625692758ba0.27105521]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 13:38:25	[scenario5d625692758ba0.27105521]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ee102a7760f78481f3ac339fc26a0d03
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 13:38:25	[scenario5d625692758ba0.27105521]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 13:38:25	[scenario5d625692758ba0.27105521]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 13:38:25	[scenario5d625692758ba0.27105521]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 13:38:25	[scenario5d625692758ba0.27105521]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 13:38:25	[scenario5d625692758ba0.27105521]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ee102a7760f78481f3ac339fc26a0d03
Context transitions => [
    "step_thirteen"
]

2019-08-25 13:38:26	[scenario5d625692758ba0.27105521]	Execute X-Cart step: step_thirteen
2019-08-25 13:38:27	[scenario5d625692758ba0.27105521]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 13:46:19	[scenario5d6258eb8d8801.47454993]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 13:46:21	[scenario5d6258eb8d8801.47454993]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true
    }
}

2019-08-25 13:46:21	[scenario5d6258eb8d8801.47454993]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 953d57a1c1c2bc94e31602e6df18f4e6
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 13:46:21	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_first
2019-08-25 13:46:21	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_second
2019-08-25 13:46:27	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_third
2019-08-25 13:46:27	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_fourth
2019-08-25 13:46:30	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_fifth
2019-08-25 13:46:30	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_six
2019-08-25 13:46:31	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_seven
2019-08-25 13:46:33	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_nine
2019-08-25 13:46:33	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_ten
2019-08-25 13:46:34	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_eleven
2019-08-25 13:46:34	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_twelve
2019-08-25 13:46:40	[scenario5d6258eb8d8801.47454993]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 13:46:41	[scenario5d6258eb8d8801.47454993]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 13:46:41	[scenario5d6258eb8d8801.47454993]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 953d57a1c1c2bc94e31602e6df18f4e6
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 13:46:41	[scenario5d6258eb8d8801.47454993]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 13:46:41	[scenario5d6258eb8d8801.47454993]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 13:46:41	[scenario5d6258eb8d8801.47454993]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 13:46:42	[scenario5d6258eb8d8801.47454993]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 13:46:42	[scenario5d6258eb8d8801.47454993]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 953d57a1c1c2bc94e31602e6df18f4e6
Context transitions => [
    "step_thirteen"
]

2019-08-25 13:46:42	[scenario5d6258eb8d8801.47454993]	Execute X-Cart step: step_thirteen
2019-08-25 13:46:44	[scenario5d6258eb8d8801.47454993]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 14:05:16	[scenario5d625d5cd28d12.46667047]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 14:05:19	[scenario5d625d5cd28d12.46667047]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true
    }
}

2019-08-25 14:05:19	[scenario5d625d5cd28d12.46667047]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9f844af2fac9384e63bd4dd2348389bb
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 14:05:19	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_first
2019-08-25 14:05:20	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_second
2019-08-25 14:05:26	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_third
2019-08-25 14:05:27	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_fourth
2019-08-25 14:05:30	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_fifth
2019-08-25 14:05:30	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_six
2019-08-25 14:05:31	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_seven
2019-08-25 14:05:34	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_nine
2019-08-25 14:05:34	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_ten
2019-08-25 14:05:35	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_eleven
2019-08-25 14:05:35	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_twelve
2019-08-25 14:05:42	[scenario5d625d5cd28d12.46667047]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 14:05:42	[scenario5d625d5cd28d12.46667047]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 14:05:42	[scenario5d625d5cd28d12.46667047]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9f844af2fac9384e63bd4dd2348389bb
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 14:05:43	[scenario5d625d5cd28d12.46667047]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 14:05:43	[scenario5d625d5cd28d12.46667047]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 14:05:43	[scenario5d625d5cd28d12.46667047]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 14:05:44	[scenario5d625d5cd28d12.46667047]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 14:05:44	[scenario5d625d5cd28d12.46667047]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9f844af2fac9384e63bd4dd2348389bb
Context transitions => [
    "step_thirteen"
]

2019-08-25 14:05:44	[scenario5d625d5cd28d12.46667047]	Execute X-Cart step: step_thirteen
2019-08-25 14:05:46	[scenario5d625d5cd28d12.46667047]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 14:15:46	[scenario5d625fd244bf25.67562838]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 14:15:48	[scenario5d625fd244bf25.67562838]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true
    }
}

2019-08-25 14:15:48	[scenario5d625fd244bf25.67562838]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bffa1a4239d82ff477ec45348bf90153
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 14:15:49	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_first
2019-08-25 14:15:49	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_second
2019-08-25 14:15:56	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_third
2019-08-25 14:15:56	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_fourth
2019-08-25 14:16:01	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_fifth
2019-08-25 14:16:02	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_six
2019-08-25 14:16:03	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_seven
2019-08-25 14:16:05	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_nine
2019-08-25 14:16:06	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_ten
2019-08-25 14:16:07	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_eleven
2019-08-25 14:16:07	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_twelve
2019-08-25 14:16:14	[scenario5d625fd244bf25.67562838]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 14:16:15	[scenario5d625fd244bf25.67562838]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 14:16:15	[scenario5d625fd244bf25.67562838]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bffa1a4239d82ff477ec45348bf90153
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 14:16:15	[scenario5d625fd244bf25.67562838]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 14:16:15	[scenario5d625fd244bf25.67562838]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 14:16:15	[scenario5d625fd244bf25.67562838]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 14:16:16	[scenario5d625fd244bf25.67562838]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 14:16:16	[scenario5d625fd244bf25.67562838]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bffa1a4239d82ff477ec45348bf90153
Context transitions => [
    "step_thirteen"
]

2019-08-25 14:16:16	[scenario5d625fd244bf25.67562838]	Execute X-Cart step: step_thirteen
2019-08-25 14:16:18	[scenario5d625fd244bf25.67562838]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 14:17:36	[scenario5d626040577eb8.61489084]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 14:17:38	[scenario5d626040577eb8.61489084]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true
    }
}

2019-08-25 14:17:38	[scenario5d626040577eb8.61489084]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 60043cfe382d910e8183da5cb5829b3b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 14:17:39	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_first
2019-08-25 14:17:39	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_second
2019-08-25 14:17:45	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_third
2019-08-25 14:17:45	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_fourth
2019-08-25 14:17:48	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_fifth
2019-08-25 14:17:48	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_six
2019-08-25 14:17:49	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_seven
2019-08-25 14:17:50	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_nine
2019-08-25 14:17:51	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_ten
2019-08-25 14:17:51	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_eleven
2019-08-25 14:17:51	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_twelve
2019-08-25 14:17:57	[scenario5d626040577eb8.61489084]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 14:17:58	[scenario5d626040577eb8.61489084]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "XCExample-ImageColumnDemo"
]
Context missing => []

2019-08-25 14:17:58	[scenario5d626040577eb8.61489084]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 60043cfe382d910e8183da5cb5829b3b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 14:17:58	[scenario5d626040577eb8.61489084]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 14:17:58	[scenario5d626040577eb8.61489084]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 14:17:58	[scenario5d626040577eb8.61489084]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 14:17:59	[scenario5d626040577eb8.61489084]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 14:17:59	[scenario5d626040577eb8.61489084]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 60043cfe382d910e8183da5cb5829b3b
Context transitions => [
    "step_thirteen"
]

2019-08-25 14:17:59	[scenario5d626040577eb8.61489084]	Execute X-Cart step: step_thirteen
2019-08-25 14:18:00	[scenario5d626040577eb8.61489084]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 14:18:21	[scenario5d6258ec527254.08129447]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 14:18:21	[scenario5d6258ec527254.08129447]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 14:18:22	[scenario5d6258ec527254.08129447]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 14:18:22	[scenario5d6258ec527254.08129447]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-25 14:18:22	[scenario5d6258ec527254.08129447]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b7cdc43d727f8ad103a6c890bd16de28
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 14:18:22	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_first
2019-08-25 14:18:22	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_second
2019-08-25 14:18:26	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_third
2019-08-25 14:18:27	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_fourth
2019-08-25 14:18:30	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_fifth
2019-08-25 14:18:30	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_six
2019-08-25 14:18:31	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_seven
2019-08-25 14:18:32	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_nine
2019-08-25 14:18:32	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_ten
2019-08-25 14:18:33	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_eleven
2019-08-25 14:18:33	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_twelve
2019-08-25 14:18:38	[scenario5d6258ec527254.08129447]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-25 14:18:38	[scenario5d6258ec527254.08129447]	Data updated: XCExample-ImageColumnDemo
2019-08-25 14:18:38	[scenario5d6258ec527254.08129447]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 14:18:38	[scenario5d6258ec527254.08129447]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b7cdc43d727f8ad103a6c890bd16de28
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 14:18:39	[scenario5d6258ec527254.08129447]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 14:18:39	[scenario5d6258ec527254.08129447]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 14:18:39	[scenario5d6258ec527254.08129447]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 14:18:39	[scenario5d6258ec527254.08129447]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 14:18:39	[scenario5d6258ec527254.08129447]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b7cdc43d727f8ad103a6c890bd16de28
Context transitions => [
    "step_thirteen"
]

2019-08-25 14:18:40	[scenario5d6258ec527254.08129447]	Execute X-Cart step: step_thirteen
2019-08-25 14:18:41	[scenario5d6258ec527254.08129447]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 14:34:48	[scenario5d626448ae3c89.81341282]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 14:34:50	[scenario5d626448ae3c89.81341282]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-25 14:34:50	[scenario5d626448ae3c89.81341282]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e723f8f2347a6dfa18bed09b585737cb
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 14:34:50	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_first
2019-08-25 14:34:50	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_second
2019-08-25 14:34:56	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_third
2019-08-25 14:34:56	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_fourth
2019-08-25 14:34:59	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_fifth
2019-08-25 14:34:59	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_six
2019-08-25 14:35:00	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_seven
2019-08-25 14:35:01	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_nine
2019-08-25 14:35:02	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_ten
2019-08-25 14:35:02	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_eleven
2019-08-25 14:35:02	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_twelve
2019-08-25 14:35:09	[scenario5d626448ae3c89.81341282]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 14:35:09	[scenario5d626448ae3c89.81341282]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 14:35:09	[scenario5d626448ae3c89.81341282]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e723f8f2347a6dfa18bed09b585737cb
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 14:35:09	[scenario5d626448ae3c89.81341282]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 14:35:10	[scenario5d626448ae3c89.81341282]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 14:35:10	[scenario5d626448ae3c89.81341282]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 14:35:10	[scenario5d626448ae3c89.81341282]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 14:35:10	[scenario5d626448ae3c89.81341282]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e723f8f2347a6dfa18bed09b585737cb
Context transitions => [
    "step_thirteen"
]

2019-08-25 14:35:10	[scenario5d626448ae3c89.81341282]	Execute X-Cart step: step_thirteen
2019-08-25 14:35:12	[scenario5d626448ae3c89.81341282]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 14:45:36	[scenario5d6266d01ea144.48675505]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 14:45:37	[scenario5d6266d01ea144.48675505]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-25 14:45:38	[scenario5d6266d01ea144.48675505]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1643d6d04e0a3bb5fff10d25515a07b6
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 14:45:38	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_first
2019-08-25 14:45:38	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_second
2019-08-25 14:45:44	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_third
2019-08-25 14:45:44	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_fourth
2019-08-25 14:45:47	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_fifth
2019-08-25 14:45:47	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_six
2019-08-25 14:45:48	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_seven
2019-08-25 14:45:49	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_nine
2019-08-25 14:45:50	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_ten
2019-08-25 14:45:50	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_eleven
2019-08-25 14:45:50	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_twelve
2019-08-25 14:45:57	[scenario5d6266d01ea144.48675505]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 14:45:57	[scenario5d6266d01ea144.48675505]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 14:45:57	[scenario5d6266d01ea144.48675505]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1643d6d04e0a3bb5fff10d25515a07b6
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 14:45:58	[scenario5d6266d01ea144.48675505]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 14:45:58	[scenario5d6266d01ea144.48675505]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 14:45:58	[scenario5d6266d01ea144.48675505]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 14:45:59	[scenario5d6266d01ea144.48675505]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 14:45:59	[scenario5d6266d01ea144.48675505]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1643d6d04e0a3bb5fff10d25515a07b6
Context transitions => [
    "step_thirteen"
]

2019-08-25 14:45:59	[scenario5d6266d01ea144.48675505]	Execute X-Cart step: step_thirteen
2019-08-25 14:46:01	[scenario5d6266d01ea144.48675505]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-25 14:47:19	[scenario5d6267375e28d3.87976923]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-25 14:47:21	[scenario5d6267375e28d3.87976923]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-25 14:47:21	[scenario5d6267375e28d3.87976923]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c800d02e0f8224a63da49a3fecbc64cb
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-25 14:47:21	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_first
2019-08-25 14:47:22	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_second
2019-08-25 14:47:28	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_third
2019-08-25 14:47:28	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_fourth
2019-08-25 14:47:32	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_fifth
2019-08-25 14:47:32	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_six
2019-08-25 14:47:33	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_seven
2019-08-25 14:47:35	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_nine
2019-08-25 14:47:36	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_ten
2019-08-25 14:47:37	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_eleven
2019-08-25 14:47:37	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_twelve
2019-08-25 14:47:45	[scenario5d6267375e28d3.87976923]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-25 14:47:46	[scenario5d6267375e28d3.87976923]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-25 14:47:46	[scenario5d6267375e28d3.87976923]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => c800d02e0f8224a63da49a3fecbc64cb
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-25 14:47:46	[scenario5d6267375e28d3.87976923]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-25 14:47:47	[scenario5d6267375e28d3.87976923]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-25 14:47:47	[scenario5d6267375e28d3.87976923]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-25 14:47:48	[scenario5d6267375e28d3.87976923]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-25 14:47:48	[scenario5d6267375e28d3.87976923]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c800d02e0f8224a63da49a3fecbc64cb
Context transitions => [
    "step_thirteen"
]

2019-08-25 14:47:48	[scenario5d6267375e28d3.87976923]	Execute X-Cart step: step_thirteen
2019-08-25 14:47:50	[scenario5d6267375e28d3.87976923]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

