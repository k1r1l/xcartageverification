<?php die(); ?>
2019-08-26 09:30:52	[scenario5d636e8cc23cc5.68672969]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 09:30:54	[scenario5d636e8cc23cc5.68672969]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 09:30:54	[scenario5d636e8cc23cc5.68672969]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5231a4115e41b65075cceb502e79058b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 09:30:54	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_first
2019-08-26 09:30:55	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_second
2019-08-26 09:31:00	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_third
2019-08-26 09:31:00	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_fourth
2019-08-26 09:31:03	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_fifth
2019-08-26 09:31:04	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_six
2019-08-26 09:31:04	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_seven
2019-08-26 09:31:05	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_nine
2019-08-26 09:31:06	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_ten
2019-08-26 09:31:06	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_eleven
2019-08-26 09:31:06	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_twelve
2019-08-26 09:31:12	[scenario5d636e8cc23cc5.68672969]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 09:31:12	[scenario5d636e8cc23cc5.68672969]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 09:31:12	[scenario5d636e8cc23cc5.68672969]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5231a4115e41b65075cceb502e79058b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 09:31:12	[scenario5d636e8cc23cc5.68672969]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 09:31:13	[scenario5d636e8cc23cc5.68672969]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 09:31:13	[scenario5d636e8cc23cc5.68672969]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 09:31:13	[scenario5d636e8cc23cc5.68672969]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 09:31:13	[scenario5d636e8cc23cc5.68672969]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5231a4115e41b65075cceb502e79058b
Context transitions => [
    "step_thirteen"
]

2019-08-26 09:31:13	[scenario5d636e8cc23cc5.68672969]	Execute X-Cart step: step_thirteen
2019-08-26 09:31:15	[scenario5d636e8cc23cc5.68672969]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 09:45:35	[scenario5d6371ff38a6d6.74266929]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 09:45:36	[scenario5d6371ff38a6d6.74266929]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 09:45:36	[scenario5d6371ff38a6d6.74266929]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 65249c735dcf73551f8b1911ee4ff36a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 09:45:36	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_first
2019-08-26 09:45:37	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_second
2019-08-26 09:45:42	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_third
2019-08-26 09:45:42	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_fourth
2019-08-26 09:45:44	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_fifth
2019-08-26 09:45:44	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_six
2019-08-26 09:45:45	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_seven
2019-08-26 09:45:46	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_nine
2019-08-26 09:45:47	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_ten
2019-08-26 09:45:47	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_eleven
2019-08-26 09:45:47	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_twelve
2019-08-26 09:45:53	[scenario5d6371ff38a6d6.74266929]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 09:45:53	[scenario5d6371ff38a6d6.74266929]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 09:45:53	[scenario5d6371ff38a6d6.74266929]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 65249c735dcf73551f8b1911ee4ff36a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 09:45:54	[scenario5d6371ff38a6d6.74266929]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 09:45:54	[scenario5d6371ff38a6d6.74266929]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 09:45:54	[scenario5d6371ff38a6d6.74266929]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 09:45:54	[scenario5d6371ff38a6d6.74266929]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 09:45:54	[scenario5d6371ff38a6d6.74266929]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 65249c735dcf73551f8b1911ee4ff36a
Context transitions => [
    "step_thirteen"
]

2019-08-26 09:45:54	[scenario5d6371ff38a6d6.74266929]	Execute X-Cart step: step_thirteen
2019-08-26 09:45:56	[scenario5d6371ff38a6d6.74266929]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 09:47:27	[scenario5d63726f0bf283.69270824]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 09:47:29	[scenario5d63726f0bf283.69270824]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 09:47:29	[scenario5d63726f0bf283.69270824]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b7528a5c76cbf16f14dcf003ca925ff1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 09:47:29	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_first
2019-08-26 09:47:30	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_second
2019-08-26 09:47:34	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_third
2019-08-26 09:47:34	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_fourth
2019-08-26 09:47:37	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_fifth
2019-08-26 09:47:37	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_six
2019-08-26 09:47:39	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_seven
2019-08-26 09:47:41	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_nine
2019-08-26 09:47:41	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_ten
2019-08-26 09:47:42	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_eleven
2019-08-26 09:47:42	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_twelve
2019-08-26 09:47:49	[scenario5d63726f0bf283.69270824]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 09:47:50	[scenario5d63726f0bf283.69270824]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 09:47:50	[scenario5d63726f0bf283.69270824]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b7528a5c76cbf16f14dcf003ca925ff1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 09:47:50	[scenario5d63726f0bf283.69270824]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 09:47:50	[scenario5d63726f0bf283.69270824]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 09:47:51	[scenario5d63726f0bf283.69270824]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 09:47:51	[scenario5d63726f0bf283.69270824]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 09:47:51	[scenario5d63726f0bf283.69270824]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b7528a5c76cbf16f14dcf003ca925ff1
Context transitions => [
    "step_thirteen"
]

2019-08-26 09:47:51	[scenario5d63726f0bf283.69270824]	Execute X-Cart step: step_thirteen
2019-08-26 09:47:52	[scenario5d63726f0bf283.69270824]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 09:52:54	[scenario5d6373b6223063.29776905]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 09:52:55	[scenario5d6373b6223063.29776905]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 09:52:55	[scenario5d6373b6223063.29776905]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ba3faa126b1c13cd10c25fbc87abbd17
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 09:52:55	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_first
2019-08-26 09:52:56	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_second
2019-08-26 09:52:59	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_third
2019-08-26 09:52:59	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_fourth
2019-08-26 09:53:01	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_fifth
2019-08-26 09:53:02	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_six
2019-08-26 09:53:02	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_seven
2019-08-26 09:53:03	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_nine
2019-08-26 09:53:04	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_ten
2019-08-26 09:53:04	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_eleven
2019-08-26 09:53:04	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_twelve
2019-08-26 09:53:09	[scenario5d6373b6223063.29776905]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 09:53:10	[scenario5d6373b6223063.29776905]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 09:53:10	[scenario5d6373b6223063.29776905]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ba3faa126b1c13cd10c25fbc87abbd17
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 09:53:10	[scenario5d6373b6223063.29776905]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 09:53:10	[scenario5d6373b6223063.29776905]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 09:53:10	[scenario5d6373b6223063.29776905]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 09:53:10	[scenario5d6373b6223063.29776905]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 09:53:10	[scenario5d6373b6223063.29776905]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ba3faa126b1c13cd10c25fbc87abbd17
Context transitions => [
    "step_thirteen"
]

2019-08-26 09:53:11	[scenario5d6373b6223063.29776905]	Execute X-Cart step: step_thirteen
2019-08-26 09:53:12	[scenario5d6373b6223063.29776905]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 09:57:46	[scenario5d6374da923065.43733706]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 09:57:48	[scenario5d6374da923065.43733706]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 09:57:48	[scenario5d6374da923065.43733706]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b2681f1423bbcad4fb711e188a7d12c2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 09:57:48	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_first
2019-08-26 09:57:48	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_second
2019-08-26 09:57:51	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_third
2019-08-26 09:57:52	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_fourth
2019-08-26 09:57:54	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_fifth
2019-08-26 09:57:54	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_six
2019-08-26 09:57:55	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_seven
2019-08-26 09:57:56	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_nine
2019-08-26 09:57:57	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_ten
2019-08-26 09:57:57	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_eleven
2019-08-26 09:57:57	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_twelve
2019-08-26 09:58:02	[scenario5d6374da923065.43733706]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 09:58:02	[scenario5d6374da923065.43733706]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 09:58:02	[scenario5d6374da923065.43733706]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b2681f1423bbcad4fb711e188a7d12c2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 09:58:02	[scenario5d6374da923065.43733706]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 09:58:03	[scenario5d6374da923065.43733706]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 09:58:03	[scenario5d6374da923065.43733706]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 09:58:03	[scenario5d6374da923065.43733706]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 09:58:03	[scenario5d6374da923065.43733706]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b2681f1423bbcad4fb711e188a7d12c2
Context transitions => [
    "step_thirteen"
]

2019-08-26 09:58:03	[scenario5d6374da923065.43733706]	Execute X-Cart step: step_thirteen
2019-08-26 09:58:04	[scenario5d6374da923065.43733706]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 09:59:18	[scenario5d637536581604.11278628]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 09:59:20	[scenario5d637536581604.11278628]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 09:59:20	[scenario5d637536581604.11278628]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ee675bf53831cac2c5548a34cea12cca
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 09:59:21	[scenario5d637536581604.11278628]	Execute X-Cart step: step_first
2019-08-26 09:59:21	[scenario5d637536581604.11278628]	Execute X-Cart step: step_second
2019-08-26 09:59:25	[scenario5d637536581604.11278628]	Execute X-Cart step: step_third
2019-08-26 09:59:26	[scenario5d637536581604.11278628]	Execute X-Cart step: step_fourth
2019-08-26 09:59:29	[scenario5d637536581604.11278628]	Execute X-Cart step: step_fifth
2019-08-26 09:59:29	[scenario5d637536581604.11278628]	Execute X-Cart step: step_six
2019-08-26 09:59:30	[scenario5d637536581604.11278628]	Execute X-Cart step: step_seven
2019-08-26 09:59:32	[scenario5d637536581604.11278628]	Execute X-Cart step: step_nine
2019-08-26 09:59:32	[scenario5d637536581604.11278628]	Execute X-Cart step: step_ten
2019-08-26 09:59:32	[scenario5d637536581604.11278628]	Execute X-Cart step: step_eleven
2019-08-26 09:59:33	[scenario5d637536581604.11278628]	Execute X-Cart step: step_twelve
2019-08-26 09:59:38	[scenario5d637536581604.11278628]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 09:59:39	[scenario5d637536581604.11278628]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 09:59:39	[scenario5d637536581604.11278628]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ee675bf53831cac2c5548a34cea12cca
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 09:59:39	[scenario5d637536581604.11278628]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 09:59:40	[scenario5d637536581604.11278628]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 09:59:40	[scenario5d637536581604.11278628]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 09:59:40	[scenario5d637536581604.11278628]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 09:59:40	[scenario5d637536581604.11278628]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ee675bf53831cac2c5548a34cea12cca
Context transitions => [
    "step_thirteen"
]

2019-08-26 09:59:40	[scenario5d637536581604.11278628]	Execute X-Cart step: step_thirteen
2019-08-26 09:59:42	[scenario5d637536581604.11278628]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:07:21	[scenario5d6377192ac960.62176787]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:07:22	[scenario5d6377192ac960.62176787]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:07:22	[scenario5d6377192ac960.62176787]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 08ef91ab97bc19a681697a910f628fa0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:07:22	[scenario5d6377192ac960.62176787]	Execute X-Cart step: step_first
2019-08-26 10:07:23	[scenario5d6377192ac960.62176787]	Execute X-Cart step: step_second
2019-08-26 10:07:28	[scenario5d6377192ac960.62176787]	Execute X-Cart step: step_third
2019-08-26 10:07:28	[scenario5d6377192ac960.62176787]	Execute X-Cart step: step_fourth
2019-08-26 10:07:30	[scenario5d6377192ac960.62176787]	Execute X-Cart step: step_fifth
2019-08-26 10:07:30	[scenario5d6377192ac960.62176787]	Execute X-Cart step: step_six
2019-08-26 10:07:31	[scenario5d6377192ac960.62176787]	Execute X-Cart step: step_seven
2019-08-26 10:07:44	[scenario5d6377192ac960.62176787]	XCart\Bus\Rebuild\Executor\Step\Rollback\EditionChange::initialize
Context stepData => {
    "editionNameBefore": ""
}

2019-08-26 10:07:51	[scenario5d63773770f585.65178325]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:07:53	[scenario5d63773770f585.65178325]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:07:53	[scenario5d63773770f585.65178325]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 32e9a0457296fa3b7bdbec4dd55a0b3a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:07:53	[scenario5d63773770f585.65178325]	Execute X-Cart step: step_first
2019-08-26 10:07:55	[scenario5d63773770f585.65178325]	Execute X-Cart step: step_second
2019-08-26 10:07:57	[scenario5d63773770f585.65178325]	Execute X-Cart step: step_third
2019-08-26 10:07:58	[scenario5d63773770f585.65178325]	Execute X-Cart step: step_fourth
2019-08-26 10:07:59	[scenario5d63773770f585.65178325]	Execute X-Cart step: step_fifth
2019-08-26 10:08:00	[scenario5d63773770f585.65178325]	Execute X-Cart step: step_six
2019-08-26 10:08:00	[scenario5d63773770f585.65178325]	Execute X-Cart step: step_seven
2019-08-26 10:08:06	[scenario5d63773770f585.65178325]	XCart\Bus\Rebuild\Executor\Step\Rollback\EditionChange::initialize
Context stepData => {
    "editionNameBefore": ""
}

2019-08-26 10:08:59	[scenario5d63777b06bb15.22113492]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:09:00	[scenario5d63777b06bb15.22113492]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:09:00	[scenario5d63777b06bb15.22113492]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 36ffb25b63b8b49fb4ad58b9f9201ef2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:09:00	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_first
2019-08-26 10:09:01	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_second
2019-08-26 10:09:04	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_third
2019-08-26 10:09:04	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_fourth
2019-08-26 10:09:06	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_fifth
2019-08-26 10:09:06	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_six
2019-08-26 10:09:07	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_seven
2019-08-26 10:09:09	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_nine
2019-08-26 10:09:09	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_ten
2019-08-26 10:09:09	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_eleven
2019-08-26 10:09:10	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_twelve
2019-08-26 10:09:16	[scenario5d63777b06bb15.22113492]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:09:17	[scenario5d63777b06bb15.22113492]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:09:17	[scenario5d63777b06bb15.22113492]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 36ffb25b63b8b49fb4ad58b9f9201ef2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:09:18	[scenario5d63777b06bb15.22113492]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:09:19	[scenario5d63777b06bb15.22113492]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:09:20	[scenario5d63777b06bb15.22113492]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:09:21	[scenario5d63777b06bb15.22113492]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:09:21	[scenario5d63777b06bb15.22113492]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 36ffb25b63b8b49fb4ad58b9f9201ef2
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:09:22	[scenario5d63777b06bb15.22113492]	Execute X-Cart step: step_thirteen
2019-08-26 10:09:24	[scenario5d63777b06bb15.22113492]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:10:34	[scenario5d6377da8f6d01.82638759]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:10:36	[scenario5d6377da8f6d01.82638759]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:10:37	[scenario5d6377da8f6d01.82638759]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fee8323a7b3d9bb203d75c0efb652161
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:10:37	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_first
2019-08-26 10:10:38	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_second
2019-08-26 10:10:43	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_third
2019-08-26 10:10:44	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_fourth
2019-08-26 10:10:47	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_fifth
2019-08-26 10:10:47	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_six
2019-08-26 10:10:48	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_seven
2019-08-26 10:10:49	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_nine
2019-08-26 10:10:50	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_ten
2019-08-26 10:10:51	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_eleven
2019-08-26 10:10:51	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_twelve
2019-08-26 10:10:59	[scenario5d6377da8f6d01.82638759]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:11:00	[scenario5d6377da8f6d01.82638759]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:11:00	[scenario5d6377da8f6d01.82638759]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => fee8323a7b3d9bb203d75c0efb652161
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:11:00	[scenario5d6377da8f6d01.82638759]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:11:01	[scenario5d6377da8f6d01.82638759]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:11:01	[scenario5d6377da8f6d01.82638759]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:11:01	[scenario5d6377da8f6d01.82638759]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:11:01	[scenario5d6377da8f6d01.82638759]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fee8323a7b3d9bb203d75c0efb652161
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:11:01	[scenario5d6377da8f6d01.82638759]	Execute X-Cart step: step_thirteen
2019-08-26 10:11:03	[scenario5d6377da8f6d01.82638759]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:12:16	[scenario5d637840d58c54.78702150]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:12:18	[scenario5d637840d58c54.78702150]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:12:18	[scenario5d637840d58c54.78702150]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cc0e8a6951dc38912f9ba84b74d6ff93
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:12:18	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_first
2019-08-26 10:12:19	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_second
2019-08-26 10:12:22	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_third
2019-08-26 10:12:22	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_fourth
2019-08-26 10:12:25	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_fifth
2019-08-26 10:12:25	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_six
2019-08-26 10:12:26	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_seven
2019-08-26 10:12:28	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_nine
2019-08-26 10:12:29	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_ten
2019-08-26 10:12:29	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_eleven
2019-08-26 10:12:29	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_twelve
2019-08-26 10:12:36	[scenario5d637840d58c54.78702150]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:12:37	[scenario5d637840d58c54.78702150]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:12:37	[scenario5d637840d58c54.78702150]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => cc0e8a6951dc38912f9ba84b74d6ff93
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:12:37	[scenario5d637840d58c54.78702150]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:12:38	[scenario5d637840d58c54.78702150]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:12:38	[scenario5d637840d58c54.78702150]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:12:38	[scenario5d637840d58c54.78702150]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:12:38	[scenario5d637840d58c54.78702150]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cc0e8a6951dc38912f9ba84b74d6ff93
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:12:38	[scenario5d637840d58c54.78702150]	Execute X-Cart step: step_thirteen
2019-08-26 10:12:40	[scenario5d637840d58c54.78702150]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:16:35	[scenario5d6379431768e4.78548137]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:16:36	[scenario5d6379431768e4.78548137]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:16:36	[scenario5d6379431768e4.78548137]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bd724dc0ce5204e400b1cea349ab7f0f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:16:37	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_first
2019-08-26 10:16:38	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_second
2019-08-26 10:16:45	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_third
2019-08-26 10:16:46	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_fourth
2019-08-26 10:16:50	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_fifth
2019-08-26 10:16:51	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_six
2019-08-26 10:16:52	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_seven
2019-08-26 10:16:54	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_nine
2019-08-26 10:16:55	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_ten
2019-08-26 10:16:56	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_eleven
2019-08-26 10:16:57	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_twelve
2019-08-26 10:17:03	[scenario5d6379431768e4.78548137]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:17:03	[scenario5d6379431768e4.78548137]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:17:03	[scenario5d6379431768e4.78548137]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bd724dc0ce5204e400b1cea349ab7f0f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:17:04	[scenario5d6379431768e4.78548137]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:17:04	[scenario5d6379431768e4.78548137]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:17:04	[scenario5d6379431768e4.78548137]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:17:04	[scenario5d6379431768e4.78548137]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:17:04	[scenario5d6379431768e4.78548137]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bd724dc0ce5204e400b1cea349ab7f0f
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:17:05	[scenario5d6379431768e4.78548137]	Execute X-Cart step: step_thirteen
2019-08-26 10:17:06	[scenario5d6379431768e4.78548137]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:19:11	[scenario5d6379dfde2714.93691143]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:19:14	[scenario5d6379dfde2714.93691143]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:19:14	[scenario5d6379dfde2714.93691143]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f05e1c8d9c3f7cb1752f47595d2ca4a4
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:19:15	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_first
2019-08-26 10:19:16	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_second
2019-08-26 10:19:21	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_third
2019-08-26 10:19:22	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_fourth
2019-08-26 10:19:24	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_fifth
2019-08-26 10:19:24	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_six
2019-08-26 10:19:25	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_seven
2019-08-26 10:19:26	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_nine
2019-08-26 10:19:27	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_ten
2019-08-26 10:19:27	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_eleven
2019-08-26 10:19:28	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_twelve
2019-08-26 10:19:33	[scenario5d6379dfde2714.93691143]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:19:34	[scenario5d6379dfde2714.93691143]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:19:34	[scenario5d6379dfde2714.93691143]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f05e1c8d9c3f7cb1752f47595d2ca4a4
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:19:34	[scenario5d6379dfde2714.93691143]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:19:34	[scenario5d6379dfde2714.93691143]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:19:34	[scenario5d6379dfde2714.93691143]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:19:35	[scenario5d6379dfde2714.93691143]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:19:35	[scenario5d6379dfde2714.93691143]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f05e1c8d9c3f7cb1752f47595d2ca4a4
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:19:35	[scenario5d6379dfde2714.93691143]	Execute X-Cart step: step_thirteen
2019-08-26 10:19:37	[scenario5d6379dfde2714.93691143]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:23:20	[scenario5d636e8ddf35b1.43706140]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 10:23:20	[scenario5d636e8ddf35b1.43706140]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 10:23:21	[scenario5d636e8ddf35b1.43706140]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 10:23:21	[scenario5d636e8ddf35b1.43706140]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-26 10:23:21	[scenario5d636e8ddf35b1.43706140]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 324cb68ae2d909487b1ec93afc7d0255
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:23:21	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_first
2019-08-26 10:23:22	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_second
2019-08-26 10:23:26	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_third
2019-08-26 10:23:27	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_fourth
2019-08-26 10:23:29	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_fifth
2019-08-26 10:23:29	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_six
2019-08-26 10:23:30	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_seven
2019-08-26 10:23:31	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_nine
2019-08-26 10:23:32	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_ten
2019-08-26 10:23:32	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_eleven
2019-08-26 10:23:32	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_twelve
2019-08-26 10:23:38	[scenario5d636e8ddf35b1.43706140]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 10:23:39	[scenario5d636e8ddf35b1.43706140]	Data updated: XCExample-ImageColumnDemo
2019-08-26 10:23:39	[scenario5d636e8ddf35b1.43706140]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:23:39	[scenario5d636e8ddf35b1.43706140]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 324cb68ae2d909487b1ec93afc7d0255
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:23:39	[scenario5d636e8ddf35b1.43706140]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:23:39	[scenario5d636e8ddf35b1.43706140]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:23:40	[scenario5d636e8ddf35b1.43706140]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:23:40	[scenario5d636e8ddf35b1.43706140]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:23:40	[scenario5d636e8ddf35b1.43706140]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 324cb68ae2d909487b1ec93afc7d0255
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:23:40	[scenario5d636e8ddf35b1.43706140]	Execute X-Cart step: step_thirteen
2019-08-26 10:23:42	[scenario5d636e8ddf35b1.43706140]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:24:20	[scenario5d637b0ad26b03.12482851]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 10:24:20	[scenario5d637b0ad26b03.12482851]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 10:24:21	[scenario5d637b0ad26b03.12482851]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 10:24:21	[scenario5d637b0ad26b03.12482851]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:24:21	[scenario5d637b0ad26b03.12482851]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8e9f7645b650526f4ed08606166fdec9
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:24:21	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_first
2019-08-26 10:24:21	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_second
2019-08-26 10:24:27	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_third
2019-08-26 10:24:28	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_fourth
2019-08-26 10:24:31	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_fifth
2019-08-26 10:24:31	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_six
2019-08-26 10:24:32	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_seven
2019-08-26 10:24:34	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_nine
2019-08-26 10:24:35	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_ten
2019-08-26 10:24:35	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_eleven
2019-08-26 10:24:35	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_twelve
2019-08-26 10:24:43	[scenario5d637b0ad26b03.12482851]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 10:24:44	[scenario5d637b0ad26b03.12482851]	Data updated: XCExample-ImageColumnDemo
2019-08-26 10:24:44	[scenario5d637b0ad26b03.12482851]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:24:44	[scenario5d637b0ad26b03.12482851]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8e9f7645b650526f4ed08606166fdec9
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:24:44	[scenario5d637b0ad26b03.12482851]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:24:45	[scenario5d637b0ad26b03.12482851]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:24:45	[scenario5d637b0ad26b03.12482851]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:24:45	[scenario5d637b0ad26b03.12482851]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:24:45	[scenario5d637b0ad26b03.12482851]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8e9f7645b650526f4ed08606166fdec9
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:24:46	[scenario5d637b0ad26b03.12482851]	Execute X-Cart step: step_thirteen
2019-08-26 10:24:48	[scenario5d637b0ad26b03.12482851]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:25:32	[scenario5d637b5c0442d0.21789270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:25:33	[scenario5d637b5c0442d0.21789270]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:25:33	[scenario5d637b5c0442d0.21789270]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d46e96ce0f7f14e8542b9a8a5cab43f3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:25:33	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_first
2019-08-26 10:25:34	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_second
2019-08-26 10:25:39	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_third
2019-08-26 10:25:39	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_fourth
2019-08-26 10:25:41	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_fifth
2019-08-26 10:25:41	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_six
2019-08-26 10:25:42	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_seven
2019-08-26 10:25:43	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_nine
2019-08-26 10:25:44	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_ten
2019-08-26 10:25:44	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_eleven
2019-08-26 10:25:44	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_twelve
2019-08-26 10:25:51	[scenario5d637b5c0442d0.21789270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:25:52	[scenario5d637b5c0442d0.21789270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:25:52	[scenario5d637b5c0442d0.21789270]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d46e96ce0f7f14e8542b9a8a5cab43f3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:25:53	[scenario5d637b5c0442d0.21789270]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:25:54	[scenario5d637b5c0442d0.21789270]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:25:55	[scenario5d637b5c0442d0.21789270]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:25:56	[scenario5d637b5c0442d0.21789270]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:25:56	[scenario5d637b5c0442d0.21789270]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d46e96ce0f7f14e8542b9a8a5cab43f3
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:25:57	[scenario5d637b5c0442d0.21789270]	Execute X-Cart step: step_thirteen
2019-08-26 10:25:59	[scenario5d637b5c0442d0.21789270]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:26:43	[scenario5d637ba3342344.76028262]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:26:46	[scenario5d637ba3342344.76028262]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:26:46	[scenario5d637ba3342344.76028262]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cefbad483f9990f9fa801cac6047b45f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:26:47	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_first
2019-08-26 10:26:48	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_second
2019-08-26 10:26:54	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_third
2019-08-26 10:26:56	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_fourth
2019-08-26 10:26:59	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_fifth
2019-08-26 10:27:00	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_six
2019-08-26 10:27:01	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_seven
2019-08-26 10:27:03	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_nine
2019-08-26 10:27:04	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_ten
2019-08-26 10:27:05	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_eleven
2019-08-26 10:27:06	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_twelve
2019-08-26 10:27:14	[scenario5d637ba3342344.76028262]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:27:16	[scenario5d637ba3342344.76028262]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:27:16	[scenario5d637ba3342344.76028262]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => cefbad483f9990f9fa801cac6047b45f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:27:16	[scenario5d637ba3342344.76028262]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:27:17	[scenario5d637ba3342344.76028262]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:27:18	[scenario5d637ba3342344.76028262]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:27:19	[scenario5d637ba3342344.76028262]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:27:19	[scenario5d637ba3342344.76028262]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cefbad483f9990f9fa801cac6047b45f
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:27:20	[scenario5d637ba3342344.76028262]	Execute X-Cart step: step_thirteen
2019-08-26 10:27:22	[scenario5d637ba3342344.76028262]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:28:34	[scenario5d637c12619b94.96391027]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:28:38	[scenario5d637c12619b94.96391027]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:28:38	[scenario5d637c12619b94.96391027]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2e6365606a41f23d264f1492bc1c3df2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:28:39	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_first
2019-08-26 10:28:41	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_second
2019-08-26 10:28:47	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_third
2019-08-26 10:28:48	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_fourth
2019-08-26 10:28:52	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_fifth
2019-08-26 10:28:53	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_six
2019-08-26 10:28:54	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_seven
2019-08-26 10:28:55	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_nine
2019-08-26 10:28:56	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_ten
2019-08-26 10:28:56	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_eleven
2019-08-26 10:28:56	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_twelve
2019-08-26 10:29:02	[scenario5d637c12619b94.96391027]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:29:03	[scenario5d637c12619b94.96391027]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:29:03	[scenario5d637c12619b94.96391027]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2e6365606a41f23d264f1492bc1c3df2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:29:04	[scenario5d637c12619b94.96391027]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:29:04	[scenario5d637c12619b94.96391027]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:29:04	[scenario5d637c12619b94.96391027]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:29:04	[scenario5d637c12619b94.96391027]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:29:04	[scenario5d637c12619b94.96391027]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2e6365606a41f23d264f1492bc1c3df2
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:29:05	[scenario5d637c12619b94.96391027]	Execute X-Cart step: step_thirteen
2019-08-26 10:29:07	[scenario5d637c12619b94.96391027]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:34:07	[scenario5d637d5f52e7f6.81308375]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:34:08	[scenario5d637d5f52e7f6.81308375]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:34:08	[scenario5d637d5f52e7f6.81308375]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => eaa0bbc2343334e094c665ee04521e15
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:34:09	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_first
2019-08-26 10:34:09	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_second
2019-08-26 10:34:14	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_third
2019-08-26 10:34:16	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_fourth
2019-08-26 10:34:19	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_fifth
2019-08-26 10:34:20	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_six
2019-08-26 10:34:21	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_seven
2019-08-26 10:34:23	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_nine
2019-08-26 10:34:24	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_ten
2019-08-26 10:34:25	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_eleven
2019-08-26 10:34:26	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_twelve
2019-08-26 10:34:32	[scenario5d637d5f52e7f6.81308375]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:34:33	[scenario5d637d5f52e7f6.81308375]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:34:33	[scenario5d637d5f52e7f6.81308375]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => eaa0bbc2343334e094c665ee04521e15
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:34:34	[scenario5d637d5f52e7f6.81308375]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:34:35	[scenario5d637d5f52e7f6.81308375]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:34:35	[scenario5d637d5f52e7f6.81308375]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:34:36	[scenario5d637d5f52e7f6.81308375]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:34:36	[scenario5d637d5f52e7f6.81308375]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => eaa0bbc2343334e094c665ee04521e15
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:34:36	[scenario5d637d5f52e7f6.81308375]	Execute X-Cart step: step_thirteen
2019-08-26 10:34:38	[scenario5d637d5f52e7f6.81308375]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:35:30	[scenario5d637db2a5db42.99031938]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:35:33	[scenario5d637db2a5db42.99031938]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:35:33	[scenario5d637db2a5db42.99031938]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b8eae5b7ae799d82cd40681d9bca8dec
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:35:33	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_first
2019-08-26 10:35:33	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_second
2019-08-26 10:35:39	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_third
2019-08-26 10:35:40	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_fourth
2019-08-26 10:35:43	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_fifth
2019-08-26 10:35:43	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_six
2019-08-26 10:35:44	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_seven
2019-08-26 10:35:46	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_nine
2019-08-26 10:35:47	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_ten
2019-08-26 10:35:47	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_eleven
2019-08-26 10:35:47	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_twelve
2019-08-26 10:35:55	[scenario5d637db2a5db42.99031938]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:35:56	[scenario5d637db2a5db42.99031938]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:35:56	[scenario5d637db2a5db42.99031938]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b8eae5b7ae799d82cd40681d9bca8dec
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:35:56	[scenario5d637db2a5db42.99031938]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:35:57	[scenario5d637db2a5db42.99031938]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:35:57	[scenario5d637db2a5db42.99031938]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:35:57	[scenario5d637db2a5db42.99031938]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:35:58	[scenario5d637db2a5db42.99031938]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b8eae5b7ae799d82cd40681d9bca8dec
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:35:58	[scenario5d637db2a5db42.99031938]	Execute X-Cart step: step_thirteen
2019-08-26 10:36:00	[scenario5d637db2a5db42.99031938]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:37:09	[scenario5d637e15f02187.76950504]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:37:12	[scenario5d637e15f02187.76950504]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:37:12	[scenario5d637e15f02187.76950504]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a462369f7cdfd1f66a1d27a1e004beb2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:37:12	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_first
2019-08-26 10:37:13	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_second
2019-08-26 10:37:19	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_third
2019-08-26 10:37:19	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_fourth
2019-08-26 10:37:23	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_fifth
2019-08-26 10:37:23	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_six
2019-08-26 10:37:24	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_seven
2019-08-26 10:37:26	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_nine
2019-08-26 10:37:27	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_ten
2019-08-26 10:37:28	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_eleven
2019-08-26 10:37:28	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_twelve
2019-08-26 10:37:34	[scenario5d637e15f02187.76950504]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:37:34	[scenario5d637e15f02187.76950504]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:37:34	[scenario5d637e15f02187.76950504]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a462369f7cdfd1f66a1d27a1e004beb2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:37:35	[scenario5d637e15f02187.76950504]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:37:35	[scenario5d637e15f02187.76950504]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:37:35	[scenario5d637e15f02187.76950504]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:37:35	[scenario5d637e15f02187.76950504]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:37:35	[scenario5d637e15f02187.76950504]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a462369f7cdfd1f66a1d27a1e004beb2
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:37:36	[scenario5d637e15f02187.76950504]	Execute X-Cart step: step_thirteen
2019-08-26 10:37:38	[scenario5d637e15f02187.76950504]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:47:05	[scenario5d638069159559.40292073]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:47:06	[scenario5d638069159559.40292073]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:47:06	[scenario5d638069159559.40292073]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => dfb3615421ea244a08740eafd2f3b02e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:47:06	[scenario5d638069159559.40292073]	Execute X-Cart step: step_first
2019-08-26 10:47:07	[scenario5d638069159559.40292073]	Execute X-Cart step: step_second
2019-08-26 10:47:13	[scenario5d638069159559.40292073]	Execute X-Cart step: step_third
2019-08-26 10:47:14	[scenario5d638069159559.40292073]	Execute X-Cart step: step_fourth
2019-08-26 10:47:17	[scenario5d638069159559.40292073]	Execute X-Cart step: step_fifth
2019-08-26 10:47:17	[scenario5d638069159559.40292073]	Execute X-Cart step: step_six
2019-08-26 10:47:18	[scenario5d638069159559.40292073]	Execute X-Cart step: step_seven
2019-08-26 10:47:20	[scenario5d638069159559.40292073]	Execute X-Cart step: step_nine
2019-08-26 10:47:21	[scenario5d638069159559.40292073]	Execute X-Cart step: step_ten
2019-08-26 10:47:22	[scenario5d638069159559.40292073]	Execute X-Cart step: step_eleven
2019-08-26 10:47:22	[scenario5d638069159559.40292073]	Execute X-Cart step: step_twelve
2019-08-26 10:47:30	[scenario5d638069159559.40292073]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:47:31	[scenario5d638069159559.40292073]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:47:31	[scenario5d638069159559.40292073]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => dfb3615421ea244a08740eafd2f3b02e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:47:31	[scenario5d638069159559.40292073]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:47:31	[scenario5d638069159559.40292073]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:47:32	[scenario5d638069159559.40292073]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:47:32	[scenario5d638069159559.40292073]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:47:32	[scenario5d638069159559.40292073]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => dfb3615421ea244a08740eafd2f3b02e
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:47:32	[scenario5d638069159559.40292073]	Execute X-Cart step: step_thirteen
2019-08-26 10:47:34	[scenario5d638069159559.40292073]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:48:09	[scenario5d6380a91702c8.55818371]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:48:11	[scenario5d6380a91702c8.55818371]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:48:11	[scenario5d6380a91702c8.55818371]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e1c5beb13a94f19747767ebeee4a1cea
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:48:11	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_first
2019-08-26 10:48:12	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_second
2019-08-26 10:48:17	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_third
2019-08-26 10:48:17	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_fourth
2019-08-26 10:48:21	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_fifth
2019-08-26 10:48:21	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_six
2019-08-26 10:48:23	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_seven
2019-08-26 10:48:25	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_nine
2019-08-26 10:48:25	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_ten
2019-08-26 10:48:26	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_eleven
2019-08-26 10:48:26	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_twelve
2019-08-26 10:48:33	[scenario5d6380a91702c8.55818371]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:48:34	[scenario5d6380a91702c8.55818371]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:48:34	[scenario5d6380a91702c8.55818371]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e1c5beb13a94f19747767ebeee4a1cea
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:48:34	[scenario5d6380a91702c8.55818371]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:48:35	[scenario5d6380a91702c8.55818371]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:48:35	[scenario5d6380a91702c8.55818371]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:48:35	[scenario5d6380a91702c8.55818371]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:48:35	[scenario5d6380a91702c8.55818371]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e1c5beb13a94f19747767ebeee4a1cea
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:48:35	[scenario5d6380a91702c8.55818371]	Execute X-Cart step: step_thirteen
2019-08-26 10:48:37	[scenario5d6380a91702c8.55818371]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 10:49:15	[scenario5d6380eb2288b1.27465230]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 10:49:17	[scenario5d6380eb2288b1.27465230]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 10:49:17	[scenario5d6380eb2288b1.27465230]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 69655514075e64832f6c03b6fe4f95e2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 10:49:17	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_first
2019-08-26 10:49:18	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_second
2019-08-26 10:49:22	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_third
2019-08-26 10:49:22	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_fourth
2019-08-26 10:49:26	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_fifth
2019-08-26 10:49:26	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_six
2019-08-26 10:49:27	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_seven
2019-08-26 10:49:29	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_nine
2019-08-26 10:49:30	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_ten
2019-08-26 10:49:30	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_eleven
2019-08-26 10:49:31	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_twelve
2019-08-26 10:49:38	[scenario5d6380eb2288b1.27465230]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 10:49:38	[scenario5d6380eb2288b1.27465230]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 10:49:38	[scenario5d6380eb2288b1.27465230]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 69655514075e64832f6c03b6fe4f95e2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 10:49:39	[scenario5d6380eb2288b1.27465230]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 10:49:39	[scenario5d6380eb2288b1.27465230]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 10:49:39	[scenario5d6380eb2288b1.27465230]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 10:49:40	[scenario5d6380eb2288b1.27465230]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 10:49:40	[scenario5d6380eb2288b1.27465230]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 69655514075e64832f6c03b6fe4f95e2
Context transitions => [
    "step_thirteen"
]

2019-08-26 10:49:40	[scenario5d6380eb2288b1.27465230]	Execute X-Cart step: step_thirteen
2019-08-26 10:49:42	[scenario5d6380eb2288b1.27465230]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 12:04:49	[scenario5d6392a1b8bce3.76977275]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 12:04:51	[scenario5d6392a1b8bce3.76977275]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 12:04:51	[scenario5d6392a1b8bce3.76977275]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e33057f9fb58db57ab1dc720398c50ec
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 12:04:51	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_first
2019-08-26 12:04:52	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_second
2019-08-26 12:04:57	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_third
2019-08-26 12:04:58	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_fourth
2019-08-26 12:05:00	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_fifth
2019-08-26 12:05:01	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_six
2019-08-26 12:05:01	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_seven
2019-08-26 12:05:03	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_nine
2019-08-26 12:05:04	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_ten
2019-08-26 12:05:04	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_eleven
2019-08-26 12:05:04	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_twelve
2019-08-26 12:05:10	[scenario5d6392a1b8bce3.76977275]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 12:05:11	[scenario5d6392a1b8bce3.76977275]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 12:05:11	[scenario5d6392a1b8bce3.76977275]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e33057f9fb58db57ab1dc720398c50ec
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 12:05:11	[scenario5d6392a1b8bce3.76977275]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 12:05:11	[scenario5d6392a1b8bce3.76977275]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 12:05:11	[scenario5d6392a1b8bce3.76977275]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 12:05:11	[scenario5d6392a1b8bce3.76977275]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 12:05:12	[scenario5d6392a1b8bce3.76977275]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e33057f9fb58db57ab1dc720398c50ec
Context transitions => [
    "step_thirteen"
]

2019-08-26 12:05:12	[scenario5d6392a1b8bce3.76977275]	Execute X-Cart step: step_thirteen
2019-08-26 12:05:14	[scenario5d6392a1b8bce3.76977275]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 12:08:35	[scenario5d63938386f977.91524601]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 12:08:37	[scenario5d63938386f977.91524601]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 12:08:38	[scenario5d63938386f977.91524601]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 844c83942c01b8a0cf5c26f7ffd40b1f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 12:08:38	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_first
2019-08-26 12:08:39	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_second
2019-08-26 12:08:45	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_third
2019-08-26 12:08:47	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_fourth
2019-08-26 12:08:50	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_fifth
2019-08-26 12:08:50	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_six
2019-08-26 12:08:51	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_seven
2019-08-26 12:08:53	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_nine
2019-08-26 12:08:54	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_ten
2019-08-26 12:08:55	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_eleven
2019-08-26 12:08:56	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_twelve
2019-08-26 12:09:04	[scenario5d63938386f977.91524601]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 12:09:05	[scenario5d63938386f977.91524601]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 12:09:05	[scenario5d63938386f977.91524601]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 844c83942c01b8a0cf5c26f7ffd40b1f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 12:09:05	[scenario5d63938386f977.91524601]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 12:09:06	[scenario5d63938386f977.91524601]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 12:09:07	[scenario5d63938386f977.91524601]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 12:09:08	[scenario5d63938386f977.91524601]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 12:09:08	[scenario5d63938386f977.91524601]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 844c83942c01b8a0cf5c26f7ffd40b1f
Context transitions => [
    "step_thirteen"
]

2019-08-26 12:09:09	[scenario5d63938386f977.91524601]	Execute X-Cart step: step_thirteen
2019-08-26 12:09:11	[scenario5d63938386f977.91524601]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 12:12:18	[scenario5d639462b46c77.57523160]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 12:12:20	[scenario5d639462b46c77.57523160]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 12:12:20	[scenario5d639462b46c77.57523160]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a901da1e252894cc51340d50a821380e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 12:12:20	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_first
2019-08-26 12:12:20	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_second
2019-08-26 12:12:25	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_third
2019-08-26 12:12:26	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_fourth
2019-08-26 12:12:28	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_fifth
2019-08-26 12:12:28	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_six
2019-08-26 12:12:29	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_seven
2019-08-26 12:12:30	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_nine
2019-08-26 12:12:31	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_ten
2019-08-26 12:12:31	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_eleven
2019-08-26 12:12:31	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_twelve
2019-08-26 12:12:37	[scenario5d639462b46c77.57523160]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 12:12:37	[scenario5d639462b46c77.57523160]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 12:12:37	[scenario5d639462b46c77.57523160]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a901da1e252894cc51340d50a821380e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 12:12:38	[scenario5d639462b46c77.57523160]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 12:12:38	[scenario5d639462b46c77.57523160]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 12:12:38	[scenario5d639462b46c77.57523160]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 12:12:38	[scenario5d639462b46c77.57523160]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 12:12:38	[scenario5d639462b46c77.57523160]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a901da1e252894cc51340d50a821380e
Context transitions => [
    "step_thirteen"
]

2019-08-26 12:12:38	[scenario5d639462b46c77.57523160]	Execute X-Cart step: step_thirteen
2019-08-26 12:12:40	[scenario5d639462b46c77.57523160]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 12:17:00	[scenario5d637b5cc0c555.54207066]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 12:17:00	[scenario5d637b5cc0c555.54207066]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 12:17:00	[scenario5d637b5cc0c555.54207066]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 12:17:00	[scenario5d637b5cc0c555.54207066]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-26 12:17:00	[scenario5d637b5cc0c555.54207066]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8279ba128cff25c4e152ca3e154ef6da
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 12:17:01	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_first
2019-08-26 12:17:01	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_second
2019-08-26 12:17:06	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_third
2019-08-26 12:17:06	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_fourth
2019-08-26 12:17:08	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_fifth
2019-08-26 12:17:09	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_six
2019-08-26 12:17:09	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_seven
2019-08-26 12:17:11	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_nine
2019-08-26 12:17:11	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_ten
2019-08-26 12:17:11	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_eleven
2019-08-26 12:17:12	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_twelve
2019-08-26 12:17:17	[scenario5d637b5cc0c555.54207066]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 12:17:18	[scenario5d637b5cc0c555.54207066]	Data updated: XCExample-ImageColumnDemo
2019-08-26 12:17:18	[scenario5d637b5cc0c555.54207066]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 12:17:18	[scenario5d637b5cc0c555.54207066]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8279ba128cff25c4e152ca3e154ef6da
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 12:17:18	[scenario5d637b5cc0c555.54207066]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 12:17:18	[scenario5d637b5cc0c555.54207066]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 12:17:19	[scenario5d637b5cc0c555.54207066]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 12:17:19	[scenario5d637b5cc0c555.54207066]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 12:17:19	[scenario5d637b5cc0c555.54207066]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8279ba128cff25c4e152ca3e154ef6da
Context transitions => [
    "step_thirteen"
]

2019-08-26 12:17:19	[scenario5d637b5cc0c555.54207066]	Execute X-Cart step: step_thirteen
2019-08-26 12:17:21	[scenario5d637b5cc0c555.54207066]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 12:25:30	[scenario5d6397686364b4.86674665]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 12:25:30	[scenario5d6397686364b4.86674665]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 12:25:31	[scenario5d6397686364b4.86674665]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 12:25:31	[scenario5d6397686364b4.86674665]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 12:25:31	[scenario5d6397686364b4.86674665]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 426446999a78e5c8ba51708860e566ab
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 12:25:31	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_first
2019-08-26 12:25:32	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_second
2019-08-26 12:25:37	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_third
2019-08-26 12:25:37	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_fourth
2019-08-26 12:25:39	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_fifth
2019-08-26 12:25:40	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_six
2019-08-26 12:25:41	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_seven
2019-08-26 12:25:42	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_nine
2019-08-26 12:25:43	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_ten
2019-08-26 12:25:48	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_eleven
2019-08-26 12:25:54	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_twelve
2019-08-26 12:26:02	[scenario5d6397686364b4.86674665]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 12:26:02	[scenario5d6397686364b4.86674665]	Data updated: XCExample-ImageColumnDemo
2019-08-26 12:26:03	[scenario5d6397686364b4.86674665]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 12:26:03	[scenario5d6397686364b4.86674665]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 426446999a78e5c8ba51708860e566ab
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 12:26:03	[scenario5d6397686364b4.86674665]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 12:26:03	[scenario5d6397686364b4.86674665]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 12:26:04	[scenario5d6397686364b4.86674665]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 12:26:04	[scenario5d6397686364b4.86674665]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 12:26:04	[scenario5d6397686364b4.86674665]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 426446999a78e5c8ba51708860e566ab
Context transitions => [
    "step_thirteen"
]

2019-08-26 12:26:04	[scenario5d6397686364b4.86674665]	Execute X-Cart step: step_thirteen
2019-08-26 12:26:07	[scenario5d6397686364b4.86674665]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 12:53:08	[scenario5d639df46aaf66.85350884]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 12:53:10	[scenario5d639df46aaf66.85350884]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 12:53:10	[scenario5d639df46aaf66.85350884]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d8f84b4ce08e208ec095dc71337427b3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 12:53:10	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_first
2019-08-26 12:53:10	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_second
2019-08-26 12:53:16	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_third
2019-08-26 12:53:17	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_fourth
2019-08-26 12:53:20	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_fifth
2019-08-26 12:53:21	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_six
2019-08-26 12:53:22	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_seven
2019-08-26 12:53:24	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_nine
2019-08-26 12:53:25	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_ten
2019-08-26 12:53:26	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_eleven
2019-08-26 12:53:27	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_twelve
2019-08-26 12:53:32	[scenario5d639df46aaf66.85350884]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 12:53:33	[scenario5d639df46aaf66.85350884]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 12:53:33	[scenario5d639df46aaf66.85350884]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d8f84b4ce08e208ec095dc71337427b3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 12:53:34	[scenario5d639df46aaf66.85350884]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 12:53:35	[scenario5d639df46aaf66.85350884]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 12:53:36	[scenario5d639df46aaf66.85350884]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 12:53:37	[scenario5d639df46aaf66.85350884]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 12:53:37	[scenario5d639df46aaf66.85350884]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d8f84b4ce08e208ec095dc71337427b3
Context transitions => [
    "step_thirteen"
]

2019-08-26 12:53:38	[scenario5d639df46aaf66.85350884]	Execute X-Cart step: step_thirteen
2019-08-26 12:53:39	[scenario5d639df46aaf66.85350884]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 12:57:35	[scenario5d639eff673de7.71522927]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 12:57:36	[scenario5d639eff673de7.71522927]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 12:57:36	[scenario5d639eff673de7.71522927]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1242e07764958d8143aac83791b71cd7
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 12:57:37	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_first
2019-08-26 12:57:37	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_second
2019-08-26 12:57:41	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_third
2019-08-26 12:57:42	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_fourth
2019-08-26 12:57:44	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_fifth
2019-08-26 12:57:45	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_six
2019-08-26 12:57:46	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_seven
2019-08-26 12:57:47	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_nine
2019-08-26 12:57:48	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_ten
2019-08-26 12:57:48	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_eleven
2019-08-26 12:57:48	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_twelve
2019-08-26 12:57:54	[scenario5d639eff673de7.71522927]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 12:57:54	[scenario5d639eff673de7.71522927]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 12:57:54	[scenario5d639eff673de7.71522927]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1242e07764958d8143aac83791b71cd7
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 12:57:54	[scenario5d639eff673de7.71522927]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 12:57:55	[scenario5d639eff673de7.71522927]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 12:57:55	[scenario5d639eff673de7.71522927]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 12:57:55	[scenario5d639eff673de7.71522927]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 12:57:55	[scenario5d639eff673de7.71522927]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1242e07764958d8143aac83791b71cd7
Context transitions => [
    "step_thirteen"
]

2019-08-26 12:57:55	[scenario5d639eff673de7.71522927]	Execute X-Cart step: step_thirteen
2019-08-26 12:57:57	[scenario5d639eff673de7.71522927]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 13:09:18	[scenario5d6397bcb4cf94.37022270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 13:09:18	[scenario5d6397bcb4cf94.37022270]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 13:09:18	[scenario5d6397bcb4cf94.37022270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 13:09:18	[scenario5d6397bcb4cf94.37022270]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-26 13:09:18	[scenario5d6397bcb4cf94.37022270]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e6603157258c46c5d87d40d3cd287124
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 13:09:18	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_first
2019-08-26 13:09:19	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_second
2019-08-26 13:09:23	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_third
2019-08-26 13:09:23	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_fourth
2019-08-26 13:09:25	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_fifth
2019-08-26 13:09:25	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_six
2019-08-26 13:09:26	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_seven
2019-08-26 13:09:27	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_nine
2019-08-26 13:09:28	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_ten
2019-08-26 13:09:28	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_eleven
2019-08-26 13:09:28	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_twelve
2019-08-26 13:09:34	[scenario5d6397bcb4cf94.37022270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 13:09:34	[scenario5d6397bcb4cf94.37022270]	Data updated: XCExample-ImageColumnDemo
2019-08-26 13:09:34	[scenario5d6397bcb4cf94.37022270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 13:09:34	[scenario5d6397bcb4cf94.37022270]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e6603157258c46c5d87d40d3cd287124
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 13:09:34	[scenario5d6397bcb4cf94.37022270]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 13:09:35	[scenario5d6397bcb4cf94.37022270]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 13:09:35	[scenario5d6397bcb4cf94.37022270]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 13:09:35	[scenario5d6397bcb4cf94.37022270]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 13:09:35	[scenario5d6397bcb4cf94.37022270]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e6603157258c46c5d87d40d3cd287124
Context transitions => [
    "step_thirteen"
]

2019-08-26 13:09:35	[scenario5d6397bcb4cf94.37022270]	Execute X-Cart step: step_thirteen
2019-08-26 13:09:37	[scenario5d6397bcb4cf94.37022270]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 13:09:58	[scenario5d63a1e6d756b0.24515578]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 13:10:00	[scenario5d63a1e6d756b0.24515578]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-26 13:10:00	[scenario5d63a1e6d756b0.24515578]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8b2a9064deabeeb0921594bb53ac6411
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 13:10:00	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_first
2019-08-26 13:10:00	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_second
2019-08-26 13:10:03	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_third
2019-08-26 13:10:03	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_fourth
2019-08-26 13:10:05	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_fifth
2019-08-26 13:10:06	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_six
2019-08-26 13:10:07	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_seven
2019-08-26 13:10:08	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_nine
2019-08-26 13:10:08	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_ten
2019-08-26 13:10:08	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_eleven
2019-08-26 13:10:09	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_twelve
2019-08-26 13:10:13	[scenario5d63a1e6d756b0.24515578]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 13:10:14	[scenario5d63a1e6d756b0.24515578]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 13:10:14	[scenario5d63a1e6d756b0.24515578]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8b2a9064deabeeb0921594bb53ac6411
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 13:10:14	[scenario5d63a1e6d756b0.24515578]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 13:10:14	[scenario5d63a1e6d756b0.24515578]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 13:10:15	[scenario5d63a1e6d756b0.24515578]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 13:10:15	[scenario5d63a1e6d756b0.24515578]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 13:10:15	[scenario5d63a1e6d756b0.24515578]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8b2a9064deabeeb0921594bb53ac6411
Context transitions => [
    "step_thirteen"
]

2019-08-26 13:10:15	[scenario5d63a1e6d756b0.24515578]	Execute X-Cart step: step_thirteen
2019-08-26 13:10:16	[scenario5d63a1e6d756b0.24515578]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 13:10:39	[scenario5d63a1e78e1f35.48957978]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 13:10:39	[scenario5d63a1e78e1f35.48957978]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 13:10:39	[scenario5d63a1e78e1f35.48957978]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 13:10:40	[scenario5d63a1e78e1f35.48957978]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 13:10:40	[scenario5d63a1e78e1f35.48957978]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 437151b4241064ce00fb66949d30a06d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 13:10:40	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_first
2019-08-26 13:10:40	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_second
2019-08-26 13:10:43	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_third
2019-08-26 13:10:43	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_fourth
2019-08-26 13:10:45	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_fifth
2019-08-26 13:10:45	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_six
2019-08-26 13:10:46	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_seven
2019-08-26 13:10:47	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_nine
2019-08-26 13:10:48	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_ten
2019-08-26 13:10:48	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_eleven
2019-08-26 13:10:48	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_twelve
2019-08-26 13:10:53	[scenario5d63a1e78e1f35.48957978]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 13:10:54	[scenario5d63a1e78e1f35.48957978]	Data updated: XCExample-ImageColumnDemo
2019-08-26 13:10:54	[scenario5d63a1e78e1f35.48957978]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 13:10:54	[scenario5d63a1e78e1f35.48957978]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 437151b4241064ce00fb66949d30a06d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 13:10:55	[scenario5d63a1e78e1f35.48957978]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 13:10:56	[scenario5d63a1e78e1f35.48957978]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 13:10:57	[scenario5d63a1e78e1f35.48957978]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 13:10:58	[scenario5d63a1e78e1f35.48957978]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 13:10:58	[scenario5d63a1e78e1f35.48957978]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 437151b4241064ce00fb66949d30a06d
Context transitions => [
    "step_thirteen"
]

2019-08-26 13:10:59	[scenario5d63a1e78e1f35.48957978]	Execute X-Cart step: step_thirteen
2019-08-26 13:11:00	[scenario5d63a1e78e1f35.48957978]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 13:13:34	[scenario5d63a2beaba679.07555398]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 13:13:36	[scenario5d63a2beaba679.07555398]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 13:13:36	[scenario5d63a2beaba679.07555398]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 726c4e4fee626a8a334a33a566229062
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 13:13:36	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_first
2019-08-26 13:13:36	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_second
2019-08-26 13:13:39	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_third
2019-08-26 13:13:39	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_fourth
2019-08-26 13:13:41	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_fifth
2019-08-26 13:13:41	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_six
2019-08-26 13:13:42	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_seven
2019-08-26 13:13:43	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_nine
2019-08-26 13:13:44	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_ten
2019-08-26 13:13:44	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_eleven
2019-08-26 13:13:44	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_twelve
2019-08-26 13:13:50	[scenario5d63a2beaba679.07555398]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 13:13:50	[scenario5d63a2beaba679.07555398]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 13:13:50	[scenario5d63a2beaba679.07555398]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 726c4e4fee626a8a334a33a566229062
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 13:13:50	[scenario5d63a2beaba679.07555398]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 13:13:51	[scenario5d63a2beaba679.07555398]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 13:13:51	[scenario5d63a2beaba679.07555398]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 13:13:51	[scenario5d63a2beaba679.07555398]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 13:13:52	[scenario5d63a2beaba679.07555398]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 726c4e4fee626a8a334a33a566229062
Context transitions => [
    "step_thirteen"
]

2019-08-26 13:13:52	[scenario5d63a2beaba679.07555398]	Execute X-Cart step: step_thirteen
2019-08-26 13:13:53	[scenario5d63a2beaba679.07555398]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:00:45	[scenario5d63adcd42d701.85502218]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:01:46	[scenario5d63adcd42d701.85502218]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:01:46	[scenario5d63adcd42d701.85502218]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 16b0172f447bf69520c40221e7cce2e5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:01:46	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_first
2019-08-26 14:01:47	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_second
2019-08-26 14:01:53	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_third
2019-08-26 14:01:54	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_fourth
2019-08-26 14:01:57	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_fifth
2019-08-26 14:01:58	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_six
2019-08-26 14:01:59	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_seven
2019-08-26 14:02:01	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_nine
2019-08-26 14:02:02	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_ten
2019-08-26 14:02:03	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_eleven
2019-08-26 14:02:04	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_twelve
2019-08-26 14:02:10	[scenario5d63adcd42d701.85502218]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:02:11	[scenario5d63adcd42d701.85502218]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:02:11	[scenario5d63adcd42d701.85502218]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 16b0172f447bf69520c40221e7cce2e5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:02:12	[scenario5d63adcd42d701.85502218]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:02:12	[scenario5d63adcd42d701.85502218]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:02:12	[scenario5d63adcd42d701.85502218]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:02:13	[scenario5d63adcd42d701.85502218]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:02:13	[scenario5d63adcd42d701.85502218]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 16b0172f447bf69520c40221e7cce2e5
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:02:13	[scenario5d63adcd42d701.85502218]	Execute X-Cart step: step_thirteen
2019-08-26 14:02:16	[scenario5d63adcd42d701.85502218]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:04:57	[scenario5d63aec9beb2c8.14003163]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:04:58	[scenario5d63aec9beb2c8.14003163]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:04:59	[scenario5d63aec9beb2c8.14003163]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3d5562ad73f40f7d31c3c59722c2959d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:04:59	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_first
2019-08-26 14:04:59	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_second
2019-08-26 14:05:04	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_third
2019-08-26 14:05:05	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_fourth
2019-08-26 14:05:07	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_fifth
2019-08-26 14:05:07	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_six
2019-08-26 14:05:08	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_seven
2019-08-26 14:05:09	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_nine
2019-08-26 14:05:10	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_ten
2019-08-26 14:05:10	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_eleven
2019-08-26 14:05:10	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_twelve
2019-08-26 14:05:16	[scenario5d63aec9beb2c8.14003163]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:05:17	[scenario5d63aec9beb2c8.14003163]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:05:17	[scenario5d63aec9beb2c8.14003163]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 3d5562ad73f40f7d31c3c59722c2959d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:05:17	[scenario5d63aec9beb2c8.14003163]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:05:17	[scenario5d63aec9beb2c8.14003163]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:05:18	[scenario5d63aec9beb2c8.14003163]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:05:18	[scenario5d63aec9beb2c8.14003163]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:05:18	[scenario5d63aec9beb2c8.14003163]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3d5562ad73f40f7d31c3c59722c2959d
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:05:18	[scenario5d63aec9beb2c8.14003163]	Execute X-Cart step: step_thirteen
2019-08-26 14:05:20	[scenario5d63aec9beb2c8.14003163]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:07:04	[scenario5d63af4832cee3.67513139]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:07:06	[scenario5d63af4832cee3.67513139]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:07:06	[scenario5d63af4832cee3.67513139]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 587ed66fed035fb7d9168d1db977e681
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:07:06	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_first
2019-08-26 14:07:07	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_second
2019-08-26 14:07:12	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_third
2019-08-26 14:07:13	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_fourth
2019-08-26 14:07:16	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_fifth
2019-08-26 14:07:16	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_six
2019-08-26 14:07:18	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_seven
2019-08-26 14:07:20	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_nine
2019-08-26 14:07:20	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_ten
2019-08-26 14:07:21	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_eleven
2019-08-26 14:07:21	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_twelve
2019-08-26 14:07:29	[scenario5d63af4832cee3.67513139]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:07:30	[scenario5d63af4832cee3.67513139]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:07:30	[scenario5d63af4832cee3.67513139]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 587ed66fed035fb7d9168d1db977e681
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:07:30	[scenario5d63af4832cee3.67513139]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:07:31	[scenario5d63af4832cee3.67513139]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:07:31	[scenario5d63af4832cee3.67513139]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:07:32	[scenario5d63af4832cee3.67513139]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:07:32	[scenario5d63af4832cee3.67513139]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 587ed66fed035fb7d9168d1db977e681
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:07:32	[scenario5d63af4832cee3.67513139]	Execute X-Cart step: step_thirteen
2019-08-26 14:07:34	[scenario5d63af4832cee3.67513139]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:14:35	[scenario5d63b10b898630.34417870]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:14:36	[scenario5d63b10b898630.34417870]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:14:36	[scenario5d63b10b898630.34417870]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a5ff69d37a2f9038f1275ccd2b965c8b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:14:36	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_first
2019-08-26 14:14:37	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_second
2019-08-26 14:14:42	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_third
2019-08-26 14:14:42	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_fourth
2019-08-26 14:14:44	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_fifth
2019-08-26 14:14:44	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_six
2019-08-26 14:14:45	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_seven
2019-08-26 14:14:47	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_nine
2019-08-26 14:14:47	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_ten
2019-08-26 14:14:48	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_eleven
2019-08-26 14:14:48	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_twelve
2019-08-26 14:14:54	[scenario5d63b10b898630.34417870]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:14:54	[scenario5d63b10b898630.34417870]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:14:54	[scenario5d63b10b898630.34417870]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a5ff69d37a2f9038f1275ccd2b965c8b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:14:54	[scenario5d63b10b898630.34417870]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:14:55	[scenario5d63b10b898630.34417870]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:14:55	[scenario5d63b10b898630.34417870]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:14:55	[scenario5d63b10b898630.34417870]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:14:55	[scenario5d63b10b898630.34417870]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a5ff69d37a2f9038f1275ccd2b965c8b
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:14:55	[scenario5d63b10b898630.34417870]	Execute X-Cart step: step_thirteen
2019-08-26 14:14:57	[scenario5d63b10b898630.34417870]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:18:54	[scenario5d63b20e232b34.18988383]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:18:55	[scenario5d63b20e232b34.18988383]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:18:55	[scenario5d63b20e232b34.18988383]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6278b2245f72181d0fe9ffcfd41d5dd1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:18:55	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_first
2019-08-26 14:18:56	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_second
2019-08-26 14:19:00	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_third
2019-08-26 14:19:00	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_fourth
2019-08-26 14:19:03	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_fifth
2019-08-26 14:19:03	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_six
2019-08-26 14:19:04	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_seven
2019-08-26 14:19:05	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_nine
2019-08-26 14:19:05	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_ten
2019-08-26 14:19:06	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_eleven
2019-08-26 14:19:06	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_twelve
2019-08-26 14:19:12	[scenario5d63b20e232b34.18988383]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:19:12	[scenario5d63b20e232b34.18988383]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:19:12	[scenario5d63b20e232b34.18988383]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6278b2245f72181d0fe9ffcfd41d5dd1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:19:12	[scenario5d63b20e232b34.18988383]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:19:13	[scenario5d63b20e232b34.18988383]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:19:13	[scenario5d63b20e232b34.18988383]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:19:13	[scenario5d63b20e232b34.18988383]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:19:13	[scenario5d63b20e232b34.18988383]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6278b2245f72181d0fe9ffcfd41d5dd1
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:19:13	[scenario5d63b20e232b34.18988383]	Execute X-Cart step: step_thirteen
2019-08-26 14:19:15	[scenario5d63b20e232b34.18988383]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:20:22	[scenario5d63b26685b0a8.29898428]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:20:24	[scenario5d63b26685b0a8.29898428]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:20:24	[scenario5d63b26685b0a8.29898428]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cb76995f177667ea346df13fb6d62f0f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:20:24	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_first
2019-08-26 14:20:25	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_second
2019-08-26 14:20:29	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_third
2019-08-26 14:20:29	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_fourth
2019-08-26 14:20:32	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_fifth
2019-08-26 14:20:32	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_six
2019-08-26 14:20:33	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_seven
2019-08-26 14:20:34	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_nine
2019-08-26 14:20:35	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_ten
2019-08-26 14:20:35	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_eleven
2019-08-26 14:20:35	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_twelve
2019-08-26 14:20:40	[scenario5d63b26685b0a8.29898428]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:20:41	[scenario5d63b26685b0a8.29898428]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:20:41	[scenario5d63b26685b0a8.29898428]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => cb76995f177667ea346df13fb6d62f0f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:20:41	[scenario5d63b26685b0a8.29898428]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:20:42	[scenario5d63b26685b0a8.29898428]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:20:42	[scenario5d63b26685b0a8.29898428]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:20:42	[scenario5d63b26685b0a8.29898428]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:20:42	[scenario5d63b26685b0a8.29898428]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cb76995f177667ea346df13fb6d62f0f
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:20:42	[scenario5d63b26685b0a8.29898428]	Execute X-Cart step: step_thirteen
2019-08-26 14:20:44	[scenario5d63b26685b0a8.29898428]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:21:54	[scenario5d63b2c2d70ed9.99150660]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:21:56	[scenario5d63b2c2d70ed9.99150660]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:21:57	[scenario5d63b2c2d70ed9.99150660]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5ce976c6784935477091c534b5885fc1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:21:57	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_first
2019-08-26 14:21:57	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_second
2019-08-26 14:22:01	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_third
2019-08-26 14:22:01	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_fourth
2019-08-26 14:22:03	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_fifth
2019-08-26 14:22:03	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_six
2019-08-26 14:22:04	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_seven
2019-08-26 14:22:06	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_nine
2019-08-26 14:22:06	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_ten
2019-08-26 14:22:06	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_eleven
2019-08-26 14:22:07	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_twelve
2019-08-26 14:22:12	[scenario5d63b2c2d70ed9.99150660]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:22:13	[scenario5d63b2c2d70ed9.99150660]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:22:13	[scenario5d63b2c2d70ed9.99150660]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5ce976c6784935477091c534b5885fc1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:22:13	[scenario5d63b2c2d70ed9.99150660]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:22:13	[scenario5d63b2c2d70ed9.99150660]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:22:13	[scenario5d63b2c2d70ed9.99150660]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:22:14	[scenario5d63b2c2d70ed9.99150660]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:22:14	[scenario5d63b2c2d70ed9.99150660]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5ce976c6784935477091c534b5885fc1
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:22:14	[scenario5d63b2c2d70ed9.99150660]	Execute X-Cart step: step_thirteen
2019-08-26 14:22:15	[scenario5d63b2c2d70ed9.99150660]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:24:31	[scenario5d63b35f5a2185.57011627]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:24:33	[scenario5d63b35f5a2185.57011627]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:24:33	[scenario5d63b35f5a2185.57011627]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 982363161265f4ded4246d0da1bd7e5d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:24:33	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_first
2019-08-26 14:24:34	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_second
2019-08-26 14:24:38	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_third
2019-08-26 14:24:38	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_fourth
2019-08-26 14:24:42	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_fifth
2019-08-26 14:24:42	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_six
2019-08-26 14:24:43	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_seven
2019-08-26 14:24:45	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_nine
2019-08-26 14:24:46	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_ten
2019-08-26 14:24:46	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_eleven
2019-08-26 14:24:47	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_twelve
2019-08-26 14:24:52	[scenario5d63b35f5a2185.57011627]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:24:52	[scenario5d63b35f5a2185.57011627]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:24:52	[scenario5d63b35f5a2185.57011627]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 982363161265f4ded4246d0da1bd7e5d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:24:53	[scenario5d63b35f5a2185.57011627]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:24:53	[scenario5d63b35f5a2185.57011627]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:24:53	[scenario5d63b35f5a2185.57011627]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:24:53	[scenario5d63b35f5a2185.57011627]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:24:54	[scenario5d63b35f5a2185.57011627]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 982363161265f4ded4246d0da1bd7e5d
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:24:54	[scenario5d63b35f5a2185.57011627]	Execute X-Cart step: step_thirteen
2019-08-26 14:24:55	[scenario5d63b35f5a2185.57011627]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:36:53	[scenario5d63b645ce1df4.40045263]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:36:55	[scenario5d63b645ce1df4.40045263]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:36:55	[scenario5d63b645ce1df4.40045263]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 308dabc7580a4d0ad468439ead3979cf
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:36:55	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_first
2019-08-26 14:36:56	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_second
2019-08-26 14:37:01	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_third
2019-08-26 14:37:01	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_fourth
2019-08-26 14:37:04	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_fifth
2019-08-26 14:37:04	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_six
2019-08-26 14:37:05	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_seven
2019-08-26 14:37:06	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_nine
2019-08-26 14:37:07	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_ten
2019-08-26 14:37:07	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_eleven
2019-08-26 14:37:07	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_twelve
2019-08-26 14:37:13	[scenario5d63b645ce1df4.40045263]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:37:14	[scenario5d63b645ce1df4.40045263]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:37:14	[scenario5d63b645ce1df4.40045263]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 308dabc7580a4d0ad468439ead3979cf
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:37:14	[scenario5d63b645ce1df4.40045263]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:37:14	[scenario5d63b645ce1df4.40045263]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:37:14	[scenario5d63b645ce1df4.40045263]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:37:15	[scenario5d63b645ce1df4.40045263]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:37:15	[scenario5d63b645ce1df4.40045263]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 308dabc7580a4d0ad468439ead3979cf
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:37:15	[scenario5d63b645ce1df4.40045263]	Execute X-Cart step: step_thirteen
2019-08-26 14:37:17	[scenario5d63b645ce1df4.40045263]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:37:46	[scenario5d63b67ad0f297.67142109]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:37:49	[scenario5d63b67ad0f297.67142109]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:37:49	[scenario5d63b67ad0f297.67142109]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b67d65e750f90842ae307354a9e25d7f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:37:49	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_first
2019-08-26 14:37:50	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_second
2019-08-26 14:37:55	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_third
2019-08-26 14:37:56	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_fourth
2019-08-26 14:38:00	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_fifth
2019-08-26 14:38:01	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_six
2019-08-26 14:38:02	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_seven
2019-08-26 14:38:04	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_nine
2019-08-26 14:38:05	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_ten
2019-08-26 14:38:06	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_eleven
2019-08-26 14:38:07	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_twelve
2019-08-26 14:38:13	[scenario5d63b67ad0f297.67142109]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:38:15	[scenario5d63b67ad0f297.67142109]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:38:15	[scenario5d63b67ad0f297.67142109]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b67d65e750f90842ae307354a9e25d7f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:38:16	[scenario5d63b67ad0f297.67142109]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:38:17	[scenario5d63b67ad0f297.67142109]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:38:17	[scenario5d63b67ad0f297.67142109]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:38:17	[scenario5d63b67ad0f297.67142109]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:38:17	[scenario5d63b67ad0f297.67142109]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b67d65e750f90842ae307354a9e25d7f
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:38:17	[scenario5d63b67ad0f297.67142109]	Execute X-Cart step: step_thirteen
2019-08-26 14:38:19	[scenario5d63b67ad0f297.67142109]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:42:29	[scenario5d63b795e9ee34.82120026]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:42:31	[scenario5d63b795e9ee34.82120026]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:42:31	[scenario5d63b795e9ee34.82120026]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b8e0de78473179806a6e474aa5e89e0d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:42:31	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_first
2019-08-26 14:42:32	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_second
2019-08-26 14:42:38	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_third
2019-08-26 14:42:39	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_fourth
2019-08-26 14:42:41	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_fifth
2019-08-26 14:42:42	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_six
2019-08-26 14:42:42	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_seven
2019-08-26 14:42:44	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_nine
2019-08-26 14:42:44	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_ten
2019-08-26 14:42:44	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_eleven
2019-08-26 14:42:45	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_twelve
2019-08-26 14:42:51	[scenario5d63b795e9ee34.82120026]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:42:52	[scenario5d63b795e9ee34.82120026]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:42:52	[scenario5d63b795e9ee34.82120026]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b8e0de78473179806a6e474aa5e89e0d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:42:52	[scenario5d63b795e9ee34.82120026]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:42:52	[scenario5d63b795e9ee34.82120026]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:42:52	[scenario5d63b795e9ee34.82120026]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:42:53	[scenario5d63b795e9ee34.82120026]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:42:53	[scenario5d63b795e9ee34.82120026]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b8e0de78473179806a6e474aa5e89e0d
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:42:53	[scenario5d63b795e9ee34.82120026]	Execute X-Cart step: step_thirteen
2019-08-26 14:42:55	[scenario5d63b795e9ee34.82120026]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:43:39	[scenario5d63b7dba62164.81208779]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:43:41	[scenario5d63b7dba62164.81208779]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:43:41	[scenario5d63b7dba62164.81208779]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 81487ac82b3f49ee2bced2ed090c515e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:43:42	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_first
2019-08-26 14:43:42	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_second
2019-08-26 14:43:47	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_third
2019-08-26 14:43:48	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_fourth
2019-08-26 14:43:51	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_fifth
2019-08-26 14:43:51	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_six
2019-08-26 14:43:52	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_seven
2019-08-26 14:43:54	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_nine
2019-08-26 14:43:55	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_ten
2019-08-26 14:43:55	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_eleven
2019-08-26 14:43:56	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_twelve
2019-08-26 14:44:02	[scenario5d63b7dba62164.81208779]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:44:03	[scenario5d63b7dba62164.81208779]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:44:03	[scenario5d63b7dba62164.81208779]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 81487ac82b3f49ee2bced2ed090c515e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:44:03	[scenario5d63b7dba62164.81208779]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:44:04	[scenario5d63b7dba62164.81208779]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:44:04	[scenario5d63b7dba62164.81208779]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:44:04	[scenario5d63b7dba62164.81208779]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:44:04	[scenario5d63b7dba62164.81208779]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 81487ac82b3f49ee2bced2ed090c515e
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:44:04	[scenario5d63b7dba62164.81208779]	Execute X-Cart step: step_thirteen
2019-08-26 14:44:07	[scenario5d63b7dba62164.81208779]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:44:50	[scenario5d63b822d84ba7.03010377]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:44:53	[scenario5d63b822d84ba7.03010377]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:44:53	[scenario5d63b822d84ba7.03010377]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ec46223d9db3548f6e6c79c0df34249e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:44:53	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_first
2019-08-26 14:44:54	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_second
2019-08-26 14:45:01	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_third
2019-08-26 14:45:02	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_fourth
2019-08-26 14:45:06	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_fifth
2019-08-26 14:45:07	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_six
2019-08-26 14:45:08	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_seven
2019-08-26 14:45:11	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_nine
2019-08-26 14:45:12	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_ten
2019-08-26 14:45:12	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_eleven
2019-08-26 14:45:12	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_twelve
2019-08-26 14:45:21	[scenario5d63b822d84ba7.03010377]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:45:22	[scenario5d63b822d84ba7.03010377]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:45:22	[scenario5d63b822d84ba7.03010377]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ec46223d9db3548f6e6c79c0df34249e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:45:22	[scenario5d63b822d84ba7.03010377]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:45:23	[scenario5d63b822d84ba7.03010377]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:45:23	[scenario5d63b822d84ba7.03010377]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:45:24	[scenario5d63b822d84ba7.03010377]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:45:24	[scenario5d63b822d84ba7.03010377]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ec46223d9db3548f6e6c79c0df34249e
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:45:24	[scenario5d63b822d84ba7.03010377]	Execute X-Cart step: step_thirteen
2019-08-26 14:45:26	[scenario5d63b822d84ba7.03010377]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:46:51	[scenario5d63b89b0885f3.27489470]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:46:53	[scenario5d63b89b0885f3.27489470]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:46:53	[scenario5d63b89b0885f3.27489470]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 607121b1aa5e1f6d1025257d759b4f3a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:46:53	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_first
2019-08-26 14:46:54	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_second
2019-08-26 14:47:00	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_third
2019-08-26 14:47:01	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_fourth
2019-08-26 14:47:06	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_fifth
2019-08-26 14:47:07	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_six
2019-08-26 14:47:09	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_seven
2019-08-26 14:47:11	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_nine
2019-08-26 14:47:12	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_ten
2019-08-26 14:47:13	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_eleven
2019-08-26 14:47:13	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_twelve
2019-08-26 14:47:21	[scenario5d63b89b0885f3.27489470]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:47:22	[scenario5d63b89b0885f3.27489470]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:47:22	[scenario5d63b89b0885f3.27489470]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 607121b1aa5e1f6d1025257d759b4f3a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:47:23	[scenario5d63b89b0885f3.27489470]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:47:23	[scenario5d63b89b0885f3.27489470]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:47:23	[scenario5d63b89b0885f3.27489470]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:47:24	[scenario5d63b89b0885f3.27489470]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:47:24	[scenario5d63b89b0885f3.27489470]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 607121b1aa5e1f6d1025257d759b4f3a
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:47:24	[scenario5d63b89b0885f3.27489470]	Execute X-Cart step: step_thirteen
2019-08-26 14:47:26	[scenario5d63b89b0885f3.27489470]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:49:28	[scenario5d63b9383b0da7.01902379]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:49:29	[scenario5d63b9383b0da7.01902379]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:49:29	[scenario5d63b9383b0da7.01902379]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bacb3bd0c581df90d242ccc685aac5e8
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:49:29	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_first
2019-08-26 14:49:30	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_second
2019-08-26 14:49:35	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_third
2019-08-26 14:49:35	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_fourth
2019-08-26 14:49:37	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_fifth
2019-08-26 14:49:38	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_six
2019-08-26 14:49:39	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_seven
2019-08-26 14:49:40	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_nine
2019-08-26 14:49:41	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_ten
2019-08-26 14:49:41	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_eleven
2019-08-26 14:49:41	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_twelve
2019-08-26 14:49:48	[scenario5d63b9383b0da7.01902379]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:49:49	[scenario5d63b9383b0da7.01902379]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:49:49	[scenario5d63b9383b0da7.01902379]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bacb3bd0c581df90d242ccc685aac5e8
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:49:49	[scenario5d63b9383b0da7.01902379]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:49:49	[scenario5d63b9383b0da7.01902379]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:49:49	[scenario5d63b9383b0da7.01902379]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:49:50	[scenario5d63b9383b0da7.01902379]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:49:50	[scenario5d63b9383b0da7.01902379]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bacb3bd0c581df90d242ccc685aac5e8
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:49:50	[scenario5d63b9383b0da7.01902379]	Execute X-Cart step: step_thirteen
2019-08-26 14:49:52	[scenario5d63b9383b0da7.01902379]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:50:46	[scenario5d63b9867e25c1.60425136]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 14:50:48	[scenario5d63b9867e25c1.60425136]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 14:50:48	[scenario5d63b9867e25c1.60425136]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a4d86edcc794d8efc762fc5edc3462f2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:50:48	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_first
2019-08-26 14:50:49	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_second
2019-08-26 14:50:55	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_third
2019-08-26 14:50:55	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_fourth
2019-08-26 14:50:59	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_fifth
2019-08-26 14:50:59	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_six
2019-08-26 14:51:00	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_seven
2019-08-26 14:51:02	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_nine
2019-08-26 14:51:03	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_ten
2019-08-26 14:51:03	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_eleven
2019-08-26 14:51:04	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_twelve
2019-08-26 14:51:12	[scenario5d63b9867e25c1.60425136]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 14:51:13	[scenario5d63b9867e25c1.60425136]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:51:13	[scenario5d63b9867e25c1.60425136]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a4d86edcc794d8efc762fc5edc3462f2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:51:13	[scenario5d63b9867e25c1.60425136]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:51:14	[scenario5d63b9867e25c1.60425136]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:51:14	[scenario5d63b9867e25c1.60425136]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:51:14	[scenario5d63b9867e25c1.60425136]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:51:14	[scenario5d63b9867e25c1.60425136]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a4d86edcc794d8efc762fc5edc3462f2
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:51:15	[scenario5d63b9867e25c1.60425136]	Execute X-Cart step: step_thirteen
2019-08-26 14:51:17	[scenario5d63b9867e25c1.60425136]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 14:57:25	[scenario5d63a24402f0f0.52829584]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 14:57:25	[scenario5d63a24402f0f0.52829584]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 14:57:26	[scenario5d63a24402f0f0.52829584]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 14:57:26	[scenario5d63a24402f0f0.52829584]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-26 14:57:26	[scenario5d63a24402f0f0.52829584]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9ff0d17fa3eed602498d10ca43a8827f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 14:57:26	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_first
2019-08-26 14:57:27	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_second
2019-08-26 14:57:33	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_third
2019-08-26 14:57:34	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_fourth
2019-08-26 14:57:36	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_fifth
2019-08-26 14:57:37	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_six
2019-08-26 14:57:37	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_seven
2019-08-26 14:57:39	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_nine
2019-08-26 14:57:39	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_ten
2019-08-26 14:57:40	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_eleven
2019-08-26 14:57:40	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_twelve
2019-08-26 14:57:47	[scenario5d63a24402f0f0.52829584]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 14:57:47	[scenario5d63a24402f0f0.52829584]	Data updated: XCExample-ImageColumnDemo
2019-08-26 14:57:47	[scenario5d63a24402f0f0.52829584]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 14:57:47	[scenario5d63a24402f0f0.52829584]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9ff0d17fa3eed602498d10ca43a8827f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 14:57:47	[scenario5d63a24402f0f0.52829584]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 14:57:48	[scenario5d63a24402f0f0.52829584]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 14:57:48	[scenario5d63a24402f0f0.52829584]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 14:57:48	[scenario5d63a24402f0f0.52829584]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 14:57:48	[scenario5d63a24402f0f0.52829584]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9ff0d17fa3eed602498d10ca43a8827f
Context transitions => [
    "step_thirteen"
]

2019-08-26 14:57:48	[scenario5d63a24402f0f0.52829584]	Execute X-Cart step: step_thirteen
2019-08-26 14:57:51	[scenario5d63a24402f0f0.52829584]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 15:11:18	[scenario5d63be464ea6d1.99952063]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 15:11:18	[scenario5d63be464ea6d1.99952063]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 15:11:18	[scenario5d63be464ea6d1.99952063]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 15:11:19	[scenario5d63be464ea6d1.99952063]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 15:11:19	[scenario5d63be464ea6d1.99952063]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 71754cc96f35431906cd51d383524399
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 15:11:19	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_first
2019-08-26 15:11:20	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_second
2019-08-26 15:11:25	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_third
2019-08-26 15:11:26	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_fourth
2019-08-26 15:11:29	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_fifth
2019-08-26 15:11:29	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_six
2019-08-26 15:11:31	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_seven
2019-08-26 15:11:33	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_nine
2019-08-26 15:11:34	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_ten
2019-08-26 15:11:35	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_eleven
2019-08-26 15:11:36	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_twelve
2019-08-26 15:11:42	[scenario5d63be464ea6d1.99952063]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-26 15:11:43	[scenario5d63be464ea6d1.99952063]	Data updated: XCExample-ImageColumnDemo
2019-08-26 15:11:43	[scenario5d63be464ea6d1.99952063]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 15:11:43	[scenario5d63be464ea6d1.99952063]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 71754cc96f35431906cd51d383524399
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 15:11:44	[scenario5d63be464ea6d1.99952063]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 15:11:45	[scenario5d63be464ea6d1.99952063]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 15:11:46	[scenario5d63be464ea6d1.99952063]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 15:11:47	[scenario5d63be464ea6d1.99952063]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 15:11:47	[scenario5d63be464ea6d1.99952063]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 71754cc96f35431906cd51d383524399
Context transitions => [
    "step_thirteen"
]

2019-08-26 15:11:48	[scenario5d63be464ea6d1.99952063]	Execute X-Cart step: step_thirteen
2019-08-26 15:11:50	[scenario5d63be464ea6d1.99952063]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 15:17:13	[scenario5d63bfb9066832.06863277]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 15:17:15	[scenario5d63bfb9066832.06863277]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 15:17:15	[scenario5d63bfb9066832.06863277]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9739d8c0cf41fe8725eebbb69f796ccf
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 15:17:15	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_first
2019-08-26 15:17:16	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_second
2019-08-26 15:17:22	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_third
2019-08-26 15:17:23	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_fourth
2019-08-26 15:17:26	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_fifth
2019-08-26 15:17:26	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_six
2019-08-26 15:17:27	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_seven
2019-08-26 15:17:29	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_nine
2019-08-26 15:17:30	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_ten
2019-08-26 15:17:30	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_eleven
2019-08-26 15:17:31	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_twelve
2019-08-26 15:17:38	[scenario5d63bfb9066832.06863277]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 15:17:39	[scenario5d63bfb9066832.06863277]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 15:17:39	[scenario5d63bfb9066832.06863277]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9739d8c0cf41fe8725eebbb69f796ccf
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 15:17:39	[scenario5d63bfb9066832.06863277]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 15:17:40	[scenario5d63bfb9066832.06863277]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 15:17:40	[scenario5d63bfb9066832.06863277]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 15:17:40	[scenario5d63bfb9066832.06863277]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 15:17:40	[scenario5d63bfb9066832.06863277]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9739d8c0cf41fe8725eebbb69f796ccf
Context transitions => [
    "step_thirteen"
]

2019-08-26 15:17:40	[scenario5d63bfb9066832.06863277]	Execute X-Cart step: step_thirteen
2019-08-26 15:17:43	[scenario5d63bfb9066832.06863277]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 15:19:46	[scenario5d63c052e1fc78.11369733]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 15:19:50	[scenario5d63c052e1fc78.11369733]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 15:19:50	[scenario5d63c052e1fc78.11369733]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2fb02f561704534eeb40568e2fa49316
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 15:19:51	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_first
2019-08-26 15:19:52	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_second
2019-08-26 15:19:59	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_third
2019-08-26 15:20:00	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_fourth
2019-08-26 15:20:03	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_fifth
2019-08-26 15:20:04	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_six
2019-08-26 15:20:05	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_seven
2019-08-26 15:20:07	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_nine
2019-08-26 15:20:08	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_ten
2019-08-26 15:20:09	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_eleven
2019-08-26 15:20:10	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_twelve
2019-08-26 15:20:16	[scenario5d63c052e1fc78.11369733]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 15:20:17	[scenario5d63c052e1fc78.11369733]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 15:20:17	[scenario5d63c052e1fc78.11369733]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2fb02f561704534eeb40568e2fa49316
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 15:20:18	[scenario5d63c052e1fc78.11369733]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 15:20:19	[scenario5d63c052e1fc78.11369733]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 15:20:20	[scenario5d63c052e1fc78.11369733]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 15:20:21	[scenario5d63c052e1fc78.11369733]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 15:20:21	[scenario5d63c052e1fc78.11369733]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2fb02f561704534eeb40568e2fa49316
Context transitions => [
    "step_thirteen"
]

2019-08-26 15:20:22	[scenario5d63c052e1fc78.11369733]	Execute X-Cart step: step_thirteen
2019-08-26 15:20:23	[scenario5d63c052e1fc78.11369733]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 15:24:01	[scenario5d63c1519fac77.72375521]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 15:24:03	[scenario5d63c1519fac77.72375521]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 15:24:03	[scenario5d63c1519fac77.72375521]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 291f3ae2cd5b3a007fa4664fef8afb8e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 15:24:03	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_first
2019-08-26 15:24:04	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_second
2019-08-26 15:24:10	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_third
2019-08-26 15:24:11	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_fourth
2019-08-26 15:24:14	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_fifth
2019-08-26 15:24:14	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_six
2019-08-26 15:24:15	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_seven
2019-08-26 15:24:16	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_nine
2019-08-26 15:24:18	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_ten
2019-08-26 15:24:18	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_eleven
2019-08-26 15:24:18	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_twelve
2019-08-26 15:24:25	[scenario5d63c1519fac77.72375521]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 15:24:26	[scenario5d63c1519fac77.72375521]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 15:24:26	[scenario5d63c1519fac77.72375521]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 291f3ae2cd5b3a007fa4664fef8afb8e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 15:24:26	[scenario5d63c1519fac77.72375521]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 15:24:27	[scenario5d63c1519fac77.72375521]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 15:24:27	[scenario5d63c1519fac77.72375521]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 15:24:27	[scenario5d63c1519fac77.72375521]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 15:24:27	[scenario5d63c1519fac77.72375521]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 291f3ae2cd5b3a007fa4664fef8afb8e
Context transitions => [
    "step_thirteen"
]

2019-08-26 15:24:27	[scenario5d63c1519fac77.72375521]	Execute X-Cart step: step_thirteen
2019-08-26 15:24:29	[scenario5d63c1519fac77.72375521]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 15:30:19	[scenario5d63c2cbb0fb55.02939903]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 15:30:21	[scenario5d63c2cbb0fb55.02939903]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 15:30:21	[scenario5d63c2cbb0fb55.02939903]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a4fffe62200699e75374981046de0997
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 15:30:21	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_first
2019-08-26 15:30:22	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_second
2019-08-26 15:30:30	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_third
2019-08-26 15:30:31	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_fourth
2019-08-26 15:30:35	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_fifth
2019-08-26 15:30:36	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_six
2019-08-26 15:30:37	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_seven
2019-08-26 15:30:39	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_nine
2019-08-26 15:30:40	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_ten
2019-08-26 15:30:41	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_eleven
2019-08-26 15:30:42	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_twelve
2019-08-26 15:30:48	[scenario5d63c2cbb0fb55.02939903]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 15:30:50	[scenario5d63c2cbb0fb55.02939903]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 15:30:50	[scenario5d63c2cbb0fb55.02939903]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a4fffe62200699e75374981046de0997
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 15:30:51	[scenario5d63c2cbb0fb55.02939903]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 15:30:52	[scenario5d63c2cbb0fb55.02939903]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 15:30:53	[scenario5d63c2cbb0fb55.02939903]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 15:30:54	[scenario5d63c2cbb0fb55.02939903]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 15:30:54	[scenario5d63c2cbb0fb55.02939903]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a4fffe62200699e75374981046de0997
Context transitions => [
    "step_thirteen"
]

2019-08-26 15:30:55	[scenario5d63c2cbb0fb55.02939903]	Execute X-Cart step: step_thirteen
2019-08-26 15:30:56	[scenario5d63c2cbb0fb55.02939903]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 15:33:44	[scenario5d63c3983bc621.18583645]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 15:33:46	[scenario5d63c3983bc621.18583645]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 15:33:46	[scenario5d63c3983bc621.18583645]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 841c223735358d5cb08381cc0e713b49
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 15:33:46	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_first
2019-08-26 15:33:46	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_second
2019-08-26 15:33:53	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_third
2019-08-26 15:33:53	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_fourth
2019-08-26 15:33:56	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_fifth
2019-08-26 15:33:56	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_six
2019-08-26 15:33:57	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_seven
2019-08-26 15:33:58	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_nine
2019-08-26 15:33:59	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_ten
2019-08-26 15:33:59	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_eleven
2019-08-26 15:33:59	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_twelve
2019-08-26 15:34:06	[scenario5d63c3983bc621.18583645]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 15:34:06	[scenario5d63c3983bc621.18583645]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 15:34:06	[scenario5d63c3983bc621.18583645]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 841c223735358d5cb08381cc0e713b49
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 15:34:07	[scenario5d63c3983bc621.18583645]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 15:34:07	[scenario5d63c3983bc621.18583645]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 15:34:07	[scenario5d63c3983bc621.18583645]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 15:34:08	[scenario5d63c3983bc621.18583645]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 15:34:08	[scenario5d63c3983bc621.18583645]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 841c223735358d5cb08381cc0e713b49
Context transitions => [
    "step_thirteen"
]

2019-08-26 15:34:08	[scenario5d63c3983bc621.18583645]	Execute X-Cart step: step_thirteen
2019-08-26 15:34:10	[scenario5d63c3983bc621.18583645]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 15:34:53	[scenario5d63c3dd037353.04984728]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 15:34:56	[scenario5d63c3dd037353.04984728]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 15:34:56	[scenario5d63c3dd037353.04984728]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 699786e930ebfb50c9c3b225c2ff52ad
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 15:34:56	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_first
2019-08-26 15:34:57	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_second
2019-08-26 15:35:05	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_third
2019-08-26 15:35:06	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_fourth
2019-08-26 15:35:11	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_fifth
2019-08-26 15:35:12	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_six
2019-08-26 15:35:14	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_seven
2019-08-26 15:35:17	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_nine
2019-08-26 15:35:18	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_ten
2019-08-26 15:35:19	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_eleven
2019-08-26 15:35:20	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_twelve
2019-08-26 15:35:28	[scenario5d63c3dd037353.04984728]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 15:35:29	[scenario5d63c3dd037353.04984728]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 15:35:29	[scenario5d63c3dd037353.04984728]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 699786e930ebfb50c9c3b225c2ff52ad
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 15:35:29	[scenario5d63c3dd037353.04984728]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 15:35:31	[scenario5d63c3dd037353.04984728]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 15:35:32	[scenario5d63c3dd037353.04984728]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 15:35:33	[scenario5d63c3dd037353.04984728]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 15:35:33	[scenario5d63c3dd037353.04984728]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 699786e930ebfb50c9c3b225c2ff52ad
Context transitions => [
    "step_thirteen"
]

2019-08-26 15:35:34	[scenario5d63c3dd037353.04984728]	Execute X-Cart step: step_thirteen
2019-08-26 15:35:36	[scenario5d63c3dd037353.04984728]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-26 15:44:19	[scenario5d63c61326af23.76404527]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-26 15:44:20	[scenario5d63c61326af23.76404527]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-26 15:44:20	[scenario5d63c61326af23.76404527]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 157666652f1fb283b2849e4a04489e4c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-26 15:44:21	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_first
2019-08-26 15:44:21	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_second
2019-08-26 15:44:27	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_third
2019-08-26 15:44:27	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_fourth
2019-08-26 15:44:30	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_fifth
2019-08-26 15:44:31	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_six
2019-08-26 15:44:32	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_seven
2019-08-26 15:44:33	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_nine
2019-08-26 15:44:34	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_ten
2019-08-26 15:44:34	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_eleven
2019-08-26 15:44:34	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_twelve
2019-08-26 15:44:41	[scenario5d63c61326af23.76404527]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-26 15:44:41	[scenario5d63c61326af23.76404527]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-26 15:44:41	[scenario5d63c61326af23.76404527]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 157666652f1fb283b2849e4a04489e4c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-26 15:44:41	[scenario5d63c61326af23.76404527]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-26 15:44:42	[scenario5d63c61326af23.76404527]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-26 15:44:42	[scenario5d63c61326af23.76404527]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-26 15:44:42	[scenario5d63c61326af23.76404527]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-26 15:44:42	[scenario5d63c61326af23.76404527]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 157666652f1fb283b2849e4a04489e4c
Context transitions => [
    "step_thirteen"
]

2019-08-26 15:44:42	[scenario5d63c61326af23.76404527]	Execute X-Cart step: step_thirteen
2019-08-26 15:44:44	[scenario5d63c61326af23.76404527]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

