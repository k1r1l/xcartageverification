<?php die(); ?>
2019-08-28 11:48:21	[scenario5d6631c557fc51.92901238]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 11:48:23	[scenario5d6631c557fc51.92901238]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 11:48:23	[scenario5d6631c557fc51.92901238]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bc8a22e296b445659afc41c84868b1a0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 11:48:23	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_first
2019-08-28 11:48:24	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_second
2019-08-28 11:48:28	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_third
2019-08-28 11:48:29	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_fourth
2019-08-28 11:48:32	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_fifth
2019-08-28 11:48:33	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_six
2019-08-28 11:48:34	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_seven
2019-08-28 11:48:35	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_nine
2019-08-28 11:48:36	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_ten
2019-08-28 11:48:37	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_eleven
2019-08-28 11:48:38	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_twelve
2019-08-28 11:48:44	[scenario5d6631c557fc51.92901238]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 11:48:46	[scenario5d6631c557fc51.92901238]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 11:48:46	[scenario5d6631c557fc51.92901238]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bc8a22e296b445659afc41c84868b1a0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 11:48:46	[scenario5d6631c557fc51.92901238]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 11:48:47	[scenario5d6631c557fc51.92901238]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 11:48:48	[scenario5d6631c557fc51.92901238]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 11:48:49	[scenario5d6631c557fc51.92901238]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 11:48:49	[scenario5d6631c557fc51.92901238]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bc8a22e296b445659afc41c84868b1a0
Context transitions => [
    "step_thirteen"
]

2019-08-28 11:48:50	[scenario5d6631c557fc51.92901238]	Execute X-Cart step: step_thirteen
2019-08-28 11:48:51	[scenario5d6631c557fc51.92901238]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 11:52:20	[scenario5d6632b4b1ab81.95576745]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 11:52:22	[scenario5d6632b4b1ab81.95576745]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 11:52:22	[scenario5d6632b4b1ab81.95576745]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 755526948b1526b64ace7909628e4cd2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 11:52:22	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_first
2019-08-28 11:52:22	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_second
2019-08-28 11:52:25	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_third
2019-08-28 11:52:25	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_fourth
2019-08-28 11:52:27	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_fifth
2019-08-28 11:52:27	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_six
2019-08-28 11:52:28	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_seven
2019-08-28 11:52:29	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_nine
2019-08-28 11:52:30	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_ten
2019-08-28 11:52:30	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_eleven
2019-08-28 11:52:30	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_twelve
2019-08-28 11:52:35	[scenario5d6632b4b1ab81.95576745]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 11:52:36	[scenario5d6632b4b1ab81.95576745]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 11:52:36	[scenario5d6632b4b1ab81.95576745]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 755526948b1526b64ace7909628e4cd2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 11:52:36	[scenario5d6632b4b1ab81.95576745]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 11:52:36	[scenario5d6632b4b1ab81.95576745]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 11:52:36	[scenario5d6632b4b1ab81.95576745]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 11:52:37	[scenario5d6632b4b1ab81.95576745]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 11:52:37	[scenario5d6632b4b1ab81.95576745]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 755526948b1526b64ace7909628e4cd2
Context transitions => [
    "step_thirteen"
]

2019-08-28 11:52:37	[scenario5d6632b4b1ab81.95576745]	Execute X-Cart step: step_thirteen
2019-08-28 11:52:39	[scenario5d6632b4b1ab81.95576745]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 11:56:23	[scenario5d6633a78c6f39.65025703]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 11:56:26	[scenario5d6633a78c6f39.65025703]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 11:56:26	[scenario5d6633a78c6f39.65025703]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9d79e6e08f530305ba1f9b4cf960c28d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 11:56:27	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_first
2019-08-28 11:56:29	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_second
2019-08-28 11:56:32	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_third
2019-08-28 11:56:33	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_fourth
2019-08-28 11:56:36	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_fifth
2019-08-28 11:56:37	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_six
2019-08-28 11:56:38	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_seven
2019-08-28 11:56:40	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_nine
2019-08-28 11:56:41	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_ten
2019-08-28 11:56:42	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_eleven
2019-08-28 11:56:43	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_twelve
2019-08-28 11:56:49	[scenario5d6633a78c6f39.65025703]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 11:56:50	[scenario5d6633a78c6f39.65025703]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 11:56:50	[scenario5d6633a78c6f39.65025703]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9d79e6e08f530305ba1f9b4cf960c28d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 11:56:50	[scenario5d6633a78c6f39.65025703]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 11:56:51	[scenario5d6633a78c6f39.65025703]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 11:56:52	[scenario5d6633a78c6f39.65025703]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 11:56:53	[scenario5d6633a78c6f39.65025703]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 11:56:53	[scenario5d6633a78c6f39.65025703]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9d79e6e08f530305ba1f9b4cf960c28d
Context transitions => [
    "step_thirteen"
]

2019-08-28 11:56:54	[scenario5d6633a78c6f39.65025703]	Execute X-Cart step: step_thirteen
2019-08-28 11:56:56	[scenario5d6633a78c6f39.65025703]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 11:59:37	[scenario5d663469c3b002.25913260]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 11:59:40	[scenario5d663469c3b002.25913260]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 11:59:40	[scenario5d663469c3b002.25913260]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ee319d1642ed6bbcefabf09b76dd85de
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 11:59:40	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_first
2019-08-28 11:59:40	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_second
2019-08-28 11:59:44	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_third
2019-08-28 11:59:46	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_fourth
2019-08-28 11:59:49	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_fifth
2019-08-28 11:59:50	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_six
2019-08-28 11:59:51	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_seven
2019-08-28 11:59:53	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_nine
2019-08-28 11:59:54	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_ten
2019-08-28 11:59:55	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_eleven
2019-08-28 11:59:56	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_twelve
2019-08-28 12:00:03	[scenario5d663469c3b002.25913260]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 12:00:04	[scenario5d663469c3b002.25913260]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 12:00:04	[scenario5d663469c3b002.25913260]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ee319d1642ed6bbcefabf09b76dd85de
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 12:00:04	[scenario5d663469c3b002.25913260]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 12:00:05	[scenario5d663469c3b002.25913260]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 12:00:06	[scenario5d663469c3b002.25913260]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 12:00:07	[scenario5d663469c3b002.25913260]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 12:00:07	[scenario5d663469c3b002.25913260]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ee319d1642ed6bbcefabf09b76dd85de
Context transitions => [
    "step_thirteen"
]

2019-08-28 12:00:08	[scenario5d663469c3b002.25913260]	Execute X-Cart step: step_thirteen
2019-08-28 12:00:10	[scenario5d663469c3b002.25913260]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 12:09:47	[scenario5d6631c67556c6.15604584]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-28 12:09:47	[scenario5d6631c67556c6.15604584]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-28 12:09:47	[scenario5d6631c67556c6.15604584]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-28 12:09:48	[scenario5d6631c67556c6.15604584]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-28 12:09:48	[scenario5d6631c67556c6.15604584]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 43413a342e79eda7b7afd92268d5b85e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 12:09:48	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_first
2019-08-28 12:09:50	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_second
2019-08-28 12:09:55	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_third
2019-08-28 12:09:57	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_fourth
2019-08-28 12:09:59	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_fifth
2019-08-28 12:10:00	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_six
2019-08-28 12:10:01	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_seven
2019-08-28 12:10:03	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_nine
2019-08-28 12:10:04	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_ten
2019-08-28 12:10:05	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_eleven
2019-08-28 12:10:06	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_twelve
2019-08-28 12:10:11	[scenario5d6631c67556c6.15604584]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-28 12:10:12	[scenario5d6631c67556c6.15604584]	Data updated: XCExample-ImageColumnDemo
2019-08-28 12:10:13	[scenario5d6631c67556c6.15604584]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 12:10:13	[scenario5d6631c67556c6.15604584]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 43413a342e79eda7b7afd92268d5b85e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 12:10:13	[scenario5d6631c67556c6.15604584]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 12:10:14	[scenario5d6631c67556c6.15604584]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 12:10:15	[scenario5d6631c67556c6.15604584]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 12:10:16	[scenario5d6631c67556c6.15604584]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 12:10:16	[scenario5d6631c67556c6.15604584]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 43413a342e79eda7b7afd92268d5b85e
Context transitions => [
    "step_thirteen"
]

2019-08-28 12:10:17	[scenario5d6631c67556c6.15604584]	Execute X-Cart step: step_thirteen
2019-08-28 12:10:19	[scenario5d6631c67556c6.15604584]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 12:20:34	[scenario5d6639527229b8.95684594]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 12:20:36	[scenario5d6639527229b8.95684594]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-28 12:20:36	[scenario5d6639527229b8.95684594]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 86832ec9f078c373ce4cbaf82c82b994
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 12:20:36	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_first
2019-08-28 12:20:36	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_second
2019-08-28 12:20:41	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_third
2019-08-28 12:20:41	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_fourth
2019-08-28 12:20:44	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_fifth
2019-08-28 12:20:45	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_six
2019-08-28 12:20:46	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_seven
2019-08-28 12:20:48	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_nine
2019-08-28 12:20:49	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_ten
2019-08-28 12:20:50	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_eleven
2019-08-28 12:20:51	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_twelve
2019-08-28 12:20:56	[scenario5d6639527229b8.95684594]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 12:20:57	[scenario5d6639527229b8.95684594]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 12:20:57	[scenario5d6639527229b8.95684594]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 86832ec9f078c373ce4cbaf82c82b994
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 12:20:58	[scenario5d6639527229b8.95684594]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 12:20:59	[scenario5d6639527229b8.95684594]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 12:21:00	[scenario5d6639527229b8.95684594]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 12:21:01	[scenario5d6639527229b8.95684594]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 12:21:01	[scenario5d6639527229b8.95684594]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 86832ec9f078c373ce4cbaf82c82b994
Context transitions => [
    "step_thirteen"
]

2019-08-28 12:21:02	[scenario5d6639527229b8.95684594]	Execute X-Cart step: step_thirteen
2019-08-28 12:21:04	[scenario5d6639527229b8.95684594]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 12:27:59	[scenario5d663b0fe3ee15.96981675]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 12:28:01	[scenario5d663b0fe3ee15.96981675]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-28 12:28:01	[scenario5d663b0fe3ee15.96981675]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 52a93adc43773711fc0b40e559ce263a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 12:28:01	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_first
2019-08-28 12:28:02	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_second
2019-08-28 12:28:07	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_third
2019-08-28 12:28:09	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_fourth
2019-08-28 12:28:12	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_fifth
2019-08-28 12:28:13	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_six
2019-08-28 12:28:14	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_seven
2019-08-28 12:28:16	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_nine
2019-08-28 12:28:17	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_ten
2019-08-28 12:28:18	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_eleven
2019-08-28 12:28:19	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_twelve
2019-08-28 12:28:27	[scenario5d663b0fe3ee15.96981675]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 12:28:28	[scenario5d663b0fe3ee15.96981675]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 12:28:28	[scenario5d663b0fe3ee15.96981675]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 52a93adc43773711fc0b40e559ce263a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 12:28:28	[scenario5d663b0fe3ee15.96981675]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 12:28:29	[scenario5d663b0fe3ee15.96981675]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 12:28:29	[scenario5d663b0fe3ee15.96981675]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 12:28:30	[scenario5d663b0fe3ee15.96981675]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 12:28:30	[scenario5d663b0fe3ee15.96981675]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 52a93adc43773711fc0b40e559ce263a
Context transitions => [
    "step_thirteen"
]

2019-08-28 12:28:30	[scenario5d663b0fe3ee15.96981675]	Execute X-Cart step: step_thirteen
2019-08-28 12:28:32	[scenario5d663b0fe3ee15.96981675]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 12:33:56	[scenario5d663c749cd8b9.79691529]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 12:33:58	[scenario5d663c749cd8b9.79691529]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-28 12:33:58	[scenario5d663c749cd8b9.79691529]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 21fcc79c2f3edb06cc0b29d2b1adc97f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 12:33:58	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_first
2019-08-28 12:33:58	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_second
2019-08-28 12:34:04	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_third
2019-08-28 12:34:06	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_fourth
2019-08-28 12:34:08	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_fifth
2019-08-28 12:34:09	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_six
2019-08-28 12:34:10	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_seven
2019-08-28 12:34:12	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_nine
2019-08-28 12:34:13	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_ten
2019-08-28 12:34:14	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_eleven
2019-08-28 12:34:15	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_twelve
2019-08-28 12:34:21	[scenario5d663c749cd8b9.79691529]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 12:34:22	[scenario5d663c749cd8b9.79691529]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 12:34:22	[scenario5d663c749cd8b9.79691529]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 21fcc79c2f3edb06cc0b29d2b1adc97f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 12:34:22	[scenario5d663c749cd8b9.79691529]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 12:34:23	[scenario5d663c749cd8b9.79691529]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 12:34:24	[scenario5d663c749cd8b9.79691529]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 12:34:25	[scenario5d663c749cd8b9.79691529]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 12:34:25	[scenario5d663c749cd8b9.79691529]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 21fcc79c2f3edb06cc0b29d2b1adc97f
Context transitions => [
    "step_thirteen"
]

2019-08-28 12:34:26	[scenario5d663c749cd8b9.79691529]	Execute X-Cart step: step_thirteen
2019-08-28 12:34:28	[scenario5d663c749cd8b9.79691529]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 12:49:34	[scenario5d66401e1ecdf3.32425427]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 12:49:35	[scenario5d66401e1ecdf3.32425427]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-28 12:49:35	[scenario5d66401e1ecdf3.32425427]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 326a1ca343d1155cc761b2234a26daea
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 12:49:35	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_first
2019-08-28 12:49:36	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_second
2019-08-28 12:49:41	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_third
2019-08-28 12:49:42	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_fourth
2019-08-28 12:49:45	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_fifth
2019-08-28 12:49:45	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_six
2019-08-28 12:49:46	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_seven
2019-08-28 12:49:48	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_nine
2019-08-28 12:49:48	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_ten
2019-08-28 12:49:49	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_eleven
2019-08-28 12:49:49	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_twelve
2019-08-28 12:49:55	[scenario5d66401e1ecdf3.32425427]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 12:49:55	[scenario5d66401e1ecdf3.32425427]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 12:49:55	[scenario5d66401e1ecdf3.32425427]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 326a1ca343d1155cc761b2234a26daea
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 12:49:56	[scenario5d66401e1ecdf3.32425427]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 12:49:56	[scenario5d66401e1ecdf3.32425427]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 12:49:57	[scenario5d66401e1ecdf3.32425427]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 12:49:58	[scenario5d66401e1ecdf3.32425427]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 12:49:58	[scenario5d66401e1ecdf3.32425427]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 326a1ca343d1155cc761b2234a26daea
Context transitions => [
    "step_thirteen"
]

2019-08-28 12:49:59	[scenario5d66401e1ecdf3.32425427]	Execute X-Cart step: step_thirteen
2019-08-28 12:50:01	[scenario5d66401e1ecdf3.32425427]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 13:40:27	[scenario5d664bfbe2ad42.88730445]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-28 13:40:27	[scenario5d664bfbe2ad42.88730445]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-28 13:40:28	[scenario5d664bfbe2ad42.88730445]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-28 13:40:28	[scenario5d664bfbe2ad42.88730445]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 13:40:28	[scenario5d664bfbe2ad42.88730445]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4afa2a9e807eda646999e9be42900fd2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 13:40:28	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_first
2019-08-28 13:40:29	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_second
2019-08-28 13:40:33	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_third
2019-08-28 13:40:33	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_fourth
2019-08-28 13:40:36	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_fifth
2019-08-28 13:40:37	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_six
2019-08-28 13:40:39	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_seven
2019-08-28 13:40:42	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_nine
2019-08-28 13:40:43	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_ten
2019-08-28 13:40:44	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_eleven
2019-08-28 13:40:45	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_twelve
2019-08-28 13:40:52	[scenario5d664bfbe2ad42.88730445]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-28 13:40:53	[scenario5d664bfbe2ad42.88730445]	Data updated: XCExample-ImageColumnDemo
2019-08-28 13:40:53	[scenario5d664bfbe2ad42.88730445]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 13:40:53	[scenario5d664bfbe2ad42.88730445]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4afa2a9e807eda646999e9be42900fd2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 13:40:53	[scenario5d664bfbe2ad42.88730445]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 13:40:54	[scenario5d664bfbe2ad42.88730445]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 13:40:54	[scenario5d664bfbe2ad42.88730445]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 13:40:55	[scenario5d664bfbe2ad42.88730445]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 13:40:55	[scenario5d664bfbe2ad42.88730445]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4afa2a9e807eda646999e9be42900fd2
Context transitions => [
    "step_thirteen"
]

2019-08-28 13:40:56	[scenario5d664bfbe2ad42.88730445]	Execute X-Cart step: step_thirteen
2019-08-28 13:40:59	[scenario5d664bfbe2ad42.88730445]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 13:42:23	[scenario5d664c7fb52759.74414578]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 13:42:28	[scenario5d664c7fb52759.74414578]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 13:42:28	[scenario5d664c7fb52759.74414578]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d92142734a7c728d7875a09ab0ee969a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 13:42:29	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_first
2019-08-28 13:42:31	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_second
2019-08-28 13:42:36	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_third
2019-08-28 13:42:38	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_fourth
2019-08-28 13:42:41	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_fifth
2019-08-28 13:42:42	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_six
2019-08-28 13:42:43	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_seven
2019-08-28 13:42:45	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_nine
2019-08-28 13:42:46	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_ten
2019-08-28 13:42:47	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_eleven
2019-08-28 13:42:48	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_twelve
2019-08-28 13:42:56	[scenario5d664c7fb52759.74414578]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 13:42:57	[scenario5d664c7fb52759.74414578]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 13:42:57	[scenario5d664c7fb52759.74414578]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d92142734a7c728d7875a09ab0ee969a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 13:42:57	[scenario5d664c7fb52759.74414578]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 13:42:58	[scenario5d664c7fb52759.74414578]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 13:42:59	[scenario5d664c7fb52759.74414578]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 13:43:00	[scenario5d664c7fb52759.74414578]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 13:43:00	[scenario5d664c7fb52759.74414578]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d92142734a7c728d7875a09ab0ee969a
Context transitions => [
    "step_thirteen"
]

2019-08-28 13:43:01	[scenario5d664c7fb52759.74414578]	Execute X-Cart step: step_thirteen
2019-08-28 13:43:03	[scenario5d664c7fb52759.74414578]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 14:18:41	[scenario5d6655014ca721.73644956]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 14:18:43	[scenario5d6655014ca721.73644956]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 14:18:43	[scenario5d6655014ca721.73644956]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cdbdac07f154e7bfe9ff6a5f1ac413c9
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 14:18:43	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_first
2019-08-28 14:18:44	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_second
2019-08-28 14:18:50	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_third
2019-08-28 14:18:50	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_fourth
2019-08-28 14:18:54	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_fifth
2019-08-28 14:18:54	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_six
2019-08-28 14:18:55	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_seven
2019-08-28 14:18:57	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_nine
2019-08-28 14:18:58	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_ten
2019-08-28 14:18:58	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_eleven
2019-08-28 14:18:59	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_twelve
2019-08-28 14:19:05	[scenario5d6655014ca721.73644956]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 14:19:06	[scenario5d6655014ca721.73644956]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 14:19:06	[scenario5d6655014ca721.73644956]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => cdbdac07f154e7bfe9ff6a5f1ac413c9
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 14:19:06	[scenario5d6655014ca721.73644956]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 14:19:06	[scenario5d6655014ca721.73644956]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 14:19:06	[scenario5d6655014ca721.73644956]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 14:19:07	[scenario5d6655014ca721.73644956]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 14:19:07	[scenario5d6655014ca721.73644956]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cdbdac07f154e7bfe9ff6a5f1ac413c9
Context transitions => [
    "step_thirteen"
]

2019-08-28 14:19:07	[scenario5d6655014ca721.73644956]	Execute X-Cart step: step_thirteen
2019-08-28 14:19:09	[scenario5d6655014ca721.73644956]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 14:20:33	[scenario5d66557136c2e5.04365561]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 14:20:34	[scenario5d66557136c2e5.04365561]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 14:20:34	[scenario5d66557136c2e5.04365561]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 493755889ae6c2820eb95f72eb25dcba
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 14:20:34	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_first
2019-08-28 14:20:35	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_second
2019-08-28 14:20:42	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_third
2019-08-28 14:20:42	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_fourth
2019-08-28 14:20:46	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_fifth
2019-08-28 14:20:46	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_six
2019-08-28 14:20:48	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_seven
2019-08-28 14:20:51	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_nine
2019-08-28 14:20:52	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_ten
2019-08-28 14:20:52	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_eleven
2019-08-28 14:20:52	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_twelve
2019-08-28 14:21:01	[scenario5d66557136c2e5.04365561]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 14:21:03	[scenario5d66557136c2e5.04365561]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 14:21:03	[scenario5d66557136c2e5.04365561]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 493755889ae6c2820eb95f72eb25dcba
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 14:21:03	[scenario5d66557136c2e5.04365561]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 14:21:04	[scenario5d66557136c2e5.04365561]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 14:21:04	[scenario5d66557136c2e5.04365561]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 14:21:04	[scenario5d66557136c2e5.04365561]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 14:21:04	[scenario5d66557136c2e5.04365561]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 493755889ae6c2820eb95f72eb25dcba
Context transitions => [
    "step_thirteen"
]

2019-08-28 14:21:05	[scenario5d66557136c2e5.04365561]	Execute X-Cart step: step_thirteen
2019-08-28 14:21:07	[scenario5d66557136c2e5.04365561]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 14:22:05	[scenario5d6655cdf31052.17352867]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 14:22:08	[scenario5d6655cdf31052.17352867]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 14:22:08	[scenario5d6655cdf31052.17352867]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 76ba6ef06dbaf371644f38540248cd7c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 14:22:08	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_first
2019-08-28 14:22:09	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_second
2019-08-28 14:22:15	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_third
2019-08-28 14:22:15	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_fourth
2019-08-28 14:22:19	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_fifth
2019-08-28 14:22:19	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_six
2019-08-28 14:22:20	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_seven
2019-08-28 14:22:22	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_nine
2019-08-28 14:22:23	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_ten
2019-08-28 14:22:23	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_eleven
2019-08-28 14:22:24	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_twelve
2019-08-28 14:22:33	[scenario5d6655cdf31052.17352867]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 14:22:34	[scenario5d6655cdf31052.17352867]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 14:22:34	[scenario5d6655cdf31052.17352867]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 76ba6ef06dbaf371644f38540248cd7c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 14:22:34	[scenario5d6655cdf31052.17352867]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 14:22:35	[scenario5d6655cdf31052.17352867]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 14:22:35	[scenario5d6655cdf31052.17352867]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 14:22:35	[scenario5d6655cdf31052.17352867]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 14:22:35	[scenario5d6655cdf31052.17352867]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 76ba6ef06dbaf371644f38540248cd7c
Context transitions => [
    "step_thirteen"
]

2019-08-28 14:22:35	[scenario5d6655cdf31052.17352867]	Execute X-Cart step: step_thirteen
2019-08-28 14:22:38	[scenario5d6655cdf31052.17352867]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 14:32:28	[scenario5d66583c3600e9.09319263]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 14:32:30	[scenario5d66583c3600e9.09319263]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 14:32:30	[scenario5d66583c3600e9.09319263]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c72aa7e0f93a3e35229cc1e2985aad7a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 14:32:30	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_first
2019-08-28 14:32:31	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_second
2019-08-28 14:32:38	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_third
2019-08-28 14:32:38	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_fourth
2019-08-28 14:32:42	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_fifth
2019-08-28 14:32:42	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_six
2019-08-28 14:32:44	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_seven
2019-08-28 14:32:46	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_nine
2019-08-28 14:32:47	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_ten
2019-08-28 14:32:48	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_eleven
2019-08-28 14:32:49	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_twelve
2019-08-28 14:32:55	[scenario5d66583c3600e9.09319263]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 14:32:56	[scenario5d66583c3600e9.09319263]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 14:32:56	[scenario5d66583c3600e9.09319263]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => c72aa7e0f93a3e35229cc1e2985aad7a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 14:32:56	[scenario5d66583c3600e9.09319263]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 14:32:57	[scenario5d66583c3600e9.09319263]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 14:32:58	[scenario5d66583c3600e9.09319263]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 14:32:59	[scenario5d66583c3600e9.09319263]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 14:32:59	[scenario5d66583c3600e9.09319263]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c72aa7e0f93a3e35229cc1e2985aad7a
Context transitions => [
    "step_thirteen"
]

2019-08-28 14:33:00	[scenario5d66583c3600e9.09319263]	Execute X-Cart step: step_thirteen
2019-08-28 14:33:02	[scenario5d66583c3600e9.09319263]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 14:38:49	[scenario5d6659b91e2230.34133535]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 14:38:50	[scenario5d6659b91e2230.34133535]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 14:38:50	[scenario5d6659b91e2230.34133535]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 052c448a6131b85337d11e4c4011d8b8
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 14:38:50	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_first
2019-08-28 14:38:51	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_second
2019-08-28 14:38:57	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_third
2019-08-28 14:38:57	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_fourth
2019-08-28 14:39:00	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_fifth
2019-08-28 14:39:00	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_six
2019-08-28 14:39:01	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_seven
2019-08-28 14:39:02	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_nine
2019-08-28 14:39:03	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_ten
2019-08-28 14:39:03	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_eleven
2019-08-28 14:39:03	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_twelve
2019-08-28 14:39:10	[scenario5d6659b91e2230.34133535]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 14:39:10	[scenario5d6659b91e2230.34133535]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 14:39:10	[scenario5d6659b91e2230.34133535]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 052c448a6131b85337d11e4c4011d8b8
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 14:39:10	[scenario5d6659b91e2230.34133535]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 14:39:11	[scenario5d6659b91e2230.34133535]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 14:39:11	[scenario5d6659b91e2230.34133535]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 14:39:11	[scenario5d6659b91e2230.34133535]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 14:39:11	[scenario5d6659b91e2230.34133535]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 052c448a6131b85337d11e4c4011d8b8
Context transitions => [
    "step_thirteen"
]

2019-08-28 14:39:11	[scenario5d6659b91e2230.34133535]	Execute X-Cart step: step_thirteen
2019-08-28 14:39:14	[scenario5d6659b91e2230.34133535]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-28 14:40:40	[scenario5d665a28abee85.31128181]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-28 14:40:42	[scenario5d665a28abee85.31128181]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-28 14:40:42	[scenario5d665a28abee85.31128181]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a0afd8728ab418602908bbd3fcd87e66
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-28 14:40:42	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_first
2019-08-28 14:40:43	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_second
2019-08-28 14:40:49	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_third
2019-08-28 14:40:50	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_fourth
2019-08-28 14:40:53	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_fifth
2019-08-28 14:40:53	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_six
2019-08-28 14:40:55	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_seven
2019-08-28 14:40:57	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_nine
2019-08-28 14:40:57	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_ten
2019-08-28 14:40:58	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_eleven
2019-08-28 14:40:58	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_twelve
2019-08-28 14:41:06	[scenario5d665a28abee85.31128181]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-28 14:41:07	[scenario5d665a28abee85.31128181]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-28 14:41:07	[scenario5d665a28abee85.31128181]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a0afd8728ab418602908bbd3fcd87e66
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-28 14:41:07	[scenario5d665a28abee85.31128181]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-28 14:41:08	[scenario5d665a28abee85.31128181]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-28 14:41:08	[scenario5d665a28abee85.31128181]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-28 14:41:09	[scenario5d665a28abee85.31128181]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-28 14:41:09	[scenario5d665a28abee85.31128181]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a0afd8728ab418602908bbd3fcd87e66
Context transitions => [
    "step_thirteen"
]

2019-08-28 14:41:09	[scenario5d665a28abee85.31128181]	Execute X-Cart step: step_thirteen
2019-08-28 14:41:12	[scenario5d665a28abee85.31128181]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

