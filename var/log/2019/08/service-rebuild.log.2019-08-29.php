<?php die(); ?>
2019-08-29 10:31:18	[scenario5d6771369a3a61.90907115]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-29 10:31:20	[scenario5d6771369a3a61.90907115]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-29 10:31:20	[scenario5d6771369a3a61.90907115]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cdb972e685ef5715ae2b16919f515425
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 10:31:20	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_first
2019-08-29 10:31:20	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_second
2019-08-29 10:31:27	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_third
2019-08-29 10:31:27	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_fourth
2019-08-29 10:31:30	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_fifth
2019-08-29 10:31:30	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_six
2019-08-29 10:31:31	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_seven
2019-08-29 10:31:32	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_nine
2019-08-29 10:31:33	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_ten
2019-08-29 10:31:33	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_eleven
2019-08-29 10:31:33	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_twelve
2019-08-29 10:31:39	[scenario5d6771369a3a61.90907115]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-29 10:31:40	[scenario5d6771369a3a61.90907115]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 10:31:40	[scenario5d6771369a3a61.90907115]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => cdb972e685ef5715ae2b16919f515425
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 10:31:40	[scenario5d6771369a3a61.90907115]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 10:31:40	[scenario5d6771369a3a61.90907115]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 10:31:41	[scenario5d6771369a3a61.90907115]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 10:31:41	[scenario5d6771369a3a61.90907115]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 10:31:41	[scenario5d6771369a3a61.90907115]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cdb972e685ef5715ae2b16919f515425
Context transitions => [
    "step_thirteen"
]

2019-08-29 10:31:41	[scenario5d6771369a3a61.90907115]	Execute X-Cart step: step_thirteen
2019-08-29 10:31:43	[scenario5d6771369a3a61.90907115]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-29 11:24:20	[scenario5d677da4b0fd71.07626433]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-29 11:24:25	[scenario5d677da4b0fd71.07626433]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-29 11:24:25	[scenario5d677da4b0fd71.07626433]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a9d1b7f9df7e0b47689da3fe7dfcf0fd
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 11:24:25	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_first
2019-08-29 11:24:25	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_second
2019-08-29 11:24:30	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_third
2019-08-29 11:24:31	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_fourth
2019-08-29 11:24:33	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_fifth
2019-08-29 11:24:33	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_six
2019-08-29 11:24:34	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_seven
2019-08-29 11:24:35	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_nine
2019-08-29 11:24:36	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_ten
2019-08-29 11:24:40	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_eleven
2019-08-29 11:24:43	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_twelve
2019-08-29 11:24:49	[scenario5d677da4b0fd71.07626433]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-29 11:24:50	[scenario5d677da4b0fd71.07626433]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 11:24:50	[scenario5d677da4b0fd71.07626433]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a9d1b7f9df7e0b47689da3fe7dfcf0fd
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 11:24:50	[scenario5d677da4b0fd71.07626433]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 11:24:51	[scenario5d677da4b0fd71.07626433]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 11:24:51	[scenario5d677da4b0fd71.07626433]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 11:24:51	[scenario5d677da4b0fd71.07626433]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 11:24:51	[scenario5d677da4b0fd71.07626433]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a9d1b7f9df7e0b47689da3fe7dfcf0fd
Context transitions => [
    "step_thirteen"
]

2019-08-29 11:24:51	[scenario5d677da4b0fd71.07626433]	Execute X-Cart step: step_thirteen
2019-08-29 11:24:53	[scenario5d677da4b0fd71.07626433]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-29 11:25:38	[scenario5d677df27419a3.31281148]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-29 11:25:41	[scenario5d677df27419a3.31281148]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-29 11:25:41	[scenario5d677df27419a3.31281148]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 24749a7327b0c670bf81b7448ada82f5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 11:25:42	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_first
2019-08-29 11:25:44	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_second
2019-08-29 11:25:48	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_third
2019-08-29 11:25:49	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_fourth
2019-08-29 11:25:52	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_fifth
2019-08-29 11:25:53	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_six
2019-08-29 11:25:54	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_seven
2019-08-29 11:25:56	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_nine
2019-08-29 11:25:57	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_ten
2019-08-29 11:25:58	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_eleven
2019-08-29 11:25:59	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_twelve
2019-08-29 11:26:05	[scenario5d677df27419a3.31281148]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-29 11:26:07	[scenario5d677df27419a3.31281148]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 11:26:07	[scenario5d677df27419a3.31281148]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 24749a7327b0c670bf81b7448ada82f5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 11:26:07	[scenario5d677df27419a3.31281148]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 11:26:08	[scenario5d677df27419a3.31281148]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 11:26:09	[scenario5d677df27419a3.31281148]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 11:26:10	[scenario5d677df27419a3.31281148]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 11:26:10	[scenario5d677df27419a3.31281148]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 24749a7327b0c670bf81b7448ada82f5
Context transitions => [
    "step_thirteen"
]

2019-08-29 11:26:11	[scenario5d677df27419a3.31281148]	Execute X-Cart step: step_thirteen
2019-08-29 11:26:13	[scenario5d677df27419a3.31281148]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-29 11:29:21	[scenario5d6770400727e6.39986689]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-29 11:29:21	[scenario5d6770400727e6.39986689]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-29 11:29:22	[scenario5d6770400727e6.39986689]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-29 11:29:22	[scenario5d6770400727e6.39986689]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": false
    }
}

2019-08-29 11:29:22	[scenario5d6770400727e6.39986689]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f8c594abf91410cdddc3d97d8049d296
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 11:29:23	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_first
2019-08-29 11:29:24	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_second
2019-08-29 11:29:30	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_third
2019-08-29 11:29:31	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_fourth
2019-08-29 11:29:34	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_fifth
2019-08-29 11:29:34	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_six
2019-08-29 11:29:36	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_seven
2019-08-29 11:29:38	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_nine
2019-08-29 11:29:38	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_ten
2019-08-29 11:29:39	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_eleven
2019-08-29 11:29:39	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_twelve
2019-08-29 11:29:47	[scenario5d6770400727e6.39986689]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-29 11:29:48	[scenario5d6770400727e6.39986689]	Data updated: XCExample-ImageColumnDemo
2019-08-29 11:29:48	[scenario5d6770400727e6.39986689]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 11:29:48	[scenario5d6770400727e6.39986689]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f8c594abf91410cdddc3d97d8049d296
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 11:29:49	[scenario5d6770400727e6.39986689]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 11:29:49	[scenario5d6770400727e6.39986689]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 11:29:50	[scenario5d6770400727e6.39986689]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 11:29:50	[scenario5d6770400727e6.39986689]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 11:29:50	[scenario5d6770400727e6.39986689]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f8c594abf91410cdddc3d97d8049d296
Context transitions => [
    "step_thirteen"
]

2019-08-29 11:29:50	[scenario5d6770400727e6.39986689]	Execute X-Cart step: step_thirteen
2019-08-29 11:29:53	[scenario5d6770400727e6.39986689]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-29 11:30:30	[scenario5d677f0bd05015.84748364]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-29 11:30:30	[scenario5d677f0bd05015.84748364]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-29 11:30:30	[scenario5d677f0bd05015.84748364]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-29 11:30:30	[scenario5d677f0bd05015.84748364]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-29 11:30:30	[scenario5d677f0bd05015.84748364]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 32e6031e78f24e3bc2a6f30a4c11e1a2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 11:30:31	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_first
2019-08-29 11:30:31	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_second
2019-08-29 11:30:36	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_third
2019-08-29 11:30:37	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_fourth
2019-08-29 11:30:39	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_fifth
2019-08-29 11:30:39	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_six
2019-08-29 11:30:40	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_seven
2019-08-29 11:30:41	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_nine
2019-08-29 11:30:42	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_ten
2019-08-29 11:30:42	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_eleven
2019-08-29 11:30:42	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_twelve
2019-08-29 11:30:49	[scenario5d677f0bd05015.84748364]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-29 11:30:50	[scenario5d677f0bd05015.84748364]	Data updated: XCExample-ImageColumnDemo
2019-08-29 11:30:50	[scenario5d677f0bd05015.84748364]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 11:30:50	[scenario5d677f0bd05015.84748364]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 32e6031e78f24e3bc2a6f30a4c11e1a2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 11:30:50	[scenario5d677f0bd05015.84748364]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 11:30:51	[scenario5d677f0bd05015.84748364]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 11:30:52	[scenario5d677f0bd05015.84748364]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 11:30:53	[scenario5d677f0bd05015.84748364]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 11:30:53	[scenario5d677f0bd05015.84748364]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 32e6031e78f24e3bc2a6f30a4c11e1a2
Context transitions => [
    "step_thirteen"
]

2019-08-29 11:30:54	[scenario5d677f0bd05015.84748364]	Execute X-Cart step: step_thirteen
2019-08-29 11:30:56	[scenario5d677f0bd05015.84748364]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-29 11:32:07	[scenario5d677f77ada377.89348806]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-29 11:32:10	[scenario5d677f77ada377.89348806]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-29 11:32:10	[scenario5d677f77ada377.89348806]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 21019df5b62f92928b35ee37e9c816f1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 11:32:10	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_first
2019-08-29 11:32:11	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_second
2019-08-29 11:32:17	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_third
2019-08-29 11:32:17	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_fourth
2019-08-29 11:32:20	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_fifth
2019-08-29 11:32:21	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_six
2019-08-29 11:32:22	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_seven
2019-08-29 11:32:24	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_nine
2019-08-29 11:32:25	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_ten
2019-08-29 11:32:25	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_eleven
2019-08-29 11:32:25	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_twelve
2019-08-29 11:32:34	[scenario5d677f77ada377.89348806]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-29 11:32:35	[scenario5d677f77ada377.89348806]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 11:32:35	[scenario5d677f77ada377.89348806]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 21019df5b62f92928b35ee37e9c816f1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 11:32:35	[scenario5d677f77ada377.89348806]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 11:32:35	[scenario5d677f77ada377.89348806]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 11:32:36	[scenario5d677f77ada377.89348806]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 11:32:36	[scenario5d677f77ada377.89348806]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 11:32:36	[scenario5d677f77ada377.89348806]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 21019df5b62f92928b35ee37e9c816f1
Context transitions => [
    "step_thirteen"
]

2019-08-29 11:32:36	[scenario5d677f77ada377.89348806]	Execute X-Cart step: step_thirteen
2019-08-29 11:32:38	[scenario5d677f77ada377.89348806]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-29 11:33:48	[scenario5d677fdc422783.38624131]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-29 11:33:50	[scenario5d677fdc422783.38624131]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-29 11:33:50	[scenario5d677fdc422783.38624131]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cd085ef6dfaace755ff9b8adee1ebfd2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 11:33:50	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_first
2019-08-29 11:33:51	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_second
2019-08-29 11:33:57	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_third
2019-08-29 11:33:57	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_fourth
2019-08-29 11:34:00	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_fifth
2019-08-29 11:34:00	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_six
2019-08-29 11:34:02	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_seven
2019-08-29 11:34:04	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_nine
2019-08-29 11:34:04	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_ten
2019-08-29 11:34:05	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_eleven
2019-08-29 11:34:05	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_twelve
2019-08-29 11:34:13	[scenario5d677fdc422783.38624131]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-29 11:34:14	[scenario5d677fdc422783.38624131]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 11:34:14	[scenario5d677fdc422783.38624131]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => cd085ef6dfaace755ff9b8adee1ebfd2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 11:34:14	[scenario5d677fdc422783.38624131]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 11:34:14	[scenario5d677fdc422783.38624131]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 11:34:15	[scenario5d677fdc422783.38624131]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 11:34:15	[scenario5d677fdc422783.38624131]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 11:34:15	[scenario5d677fdc422783.38624131]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cd085ef6dfaace755ff9b8adee1ebfd2
Context transitions => [
    "step_thirteen"
]

2019-08-29 11:34:15	[scenario5d677fdc422783.38624131]	Execute X-Cart step: step_thirteen
2019-08-29 11:34:18	[scenario5d677fdc422783.38624131]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-29 11:52:41	[scenario5d6784499884f9.06282839]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-29 11:52:43	[scenario5d6784499884f9.06282839]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-29 11:52:43	[scenario5d6784499884f9.06282839]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b9eeaff4ffc78d19bbd81944e6ad18f5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 11:52:43	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_first
2019-08-29 11:52:43	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_second
2019-08-29 11:52:50	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_third
2019-08-29 11:52:51	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_fourth
2019-08-29 11:52:53	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_fifth
2019-08-29 11:52:54	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_six
2019-08-29 11:52:55	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_seven
2019-08-29 11:52:56	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_nine
2019-08-29 11:52:57	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_ten
2019-08-29 11:52:57	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_eleven
2019-08-29 11:52:58	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_twelve
2019-08-29 11:53:05	[scenario5d6784499884f9.06282839]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-29 11:53:05	[scenario5d6784499884f9.06282839]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 11:53:05	[scenario5d6784499884f9.06282839]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b9eeaff4ffc78d19bbd81944e6ad18f5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 11:53:06	[scenario5d6784499884f9.06282839]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 11:53:06	[scenario5d6784499884f9.06282839]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 11:53:06	[scenario5d6784499884f9.06282839]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 11:53:06	[scenario5d6784499884f9.06282839]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 11:53:07	[scenario5d6784499884f9.06282839]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b9eeaff4ffc78d19bbd81944e6ad18f5
Context transitions => [
    "step_thirteen"
]

2019-08-29 11:53:07	[scenario5d6784499884f9.06282839]	Execute X-Cart step: step_thirteen
2019-08-29 11:53:09	[scenario5d6784499884f9.06282839]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-29 11:53:20	[scenario5d67847016b215.20829646]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-29 11:53:21	[scenario5d67847016b215.20829646]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-29 11:53:22	[scenario5d67847016b215.20829646]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e995c7e04252ef7115ed657f4980871f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 11:53:22	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_first
2019-08-29 11:53:22	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_second
2019-08-29 11:53:28	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_third
2019-08-29 11:53:29	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_fourth
2019-08-29 11:53:32	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_fifth
2019-08-29 11:53:32	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_six
2019-08-29 11:53:34	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_seven
2019-08-29 11:53:36	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_nine
2019-08-29 11:53:36	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_ten
2019-08-29 11:53:37	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_eleven
2019-08-29 11:53:37	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_twelve
2019-08-29 11:53:45	[scenario5d67847016b215.20829646]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-29 11:53:46	[scenario5d67847016b215.20829646]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 11:53:46	[scenario5d67847016b215.20829646]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e995c7e04252ef7115ed657f4980871f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 11:53:46	[scenario5d67847016b215.20829646]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 11:53:46	[scenario5d67847016b215.20829646]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 11:53:47	[scenario5d67847016b215.20829646]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 11:53:47	[scenario5d67847016b215.20829646]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 11:53:47	[scenario5d67847016b215.20829646]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e995c7e04252ef7115ed657f4980871f
Context transitions => [
    "step_thirteen"
]

2019-08-29 11:53:48	[scenario5d67847016b215.20829646]	Execute X-Cart step: step_thirteen
2019-08-29 11:53:50	[scenario5d67847016b215.20829646]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-29 12:18:10	[scenario5d678a428f4603.15519910]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-29 12:18:12	[scenario5d678a428f4603.15519910]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": true,
        "ImageColumnDemo": true
    }
}

2019-08-29 12:18:12	[scenario5d678a428f4603.15519910]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 87e7d6bd839e7a30ee5b3a7b885a5fa5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-29 12:18:12	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_first
2019-08-29 12:18:13	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_second
2019-08-29 12:18:20	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_third
2019-08-29 12:18:20	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_fourth
2019-08-29 12:18:24	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_fifth
2019-08-29 12:18:25	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_six
2019-08-29 12:18:26	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_seven
2019-08-29 12:18:28	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_nine
2019-08-29 12:18:29	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_ten
2019-08-29 12:18:29	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_eleven
2019-08-29 12:18:29	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_twelve
2019-08-29 12:18:37	[scenario5d678a428f4603.15519910]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-29 12:18:38	[scenario5d678a428f4603.15519910]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-29 12:18:38	[scenario5d678a428f4603.15519910]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 87e7d6bd839e7a30ee5b3a7b885a5fa5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-29 12:18:38	[scenario5d678a428f4603.15519910]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-29 12:18:39	[scenario5d678a428f4603.15519910]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-29 12:18:39	[scenario5d678a428f4603.15519910]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-29 12:18:39	[scenario5d678a428f4603.15519910]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-29 12:18:39	[scenario5d678a428f4603.15519910]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 87e7d6bd839e7a30ee5b3a7b885a5fa5
Context transitions => [
    "step_thirteen"
]

2019-08-29 12:18:39	[scenario5d678a428f4603.15519910]	Execute X-Cart step: step_thirteen
2019-08-29 12:18:41	[scenario5d678a428f4603.15519910]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

