<?php die(); ?>
2019-08-30 11:56:32	[scenario5d68d6a4db28e1.34539293]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    },
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 11:56:32	[scenario5d68d6a4db28e1.34539293]	Update script transitions
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    },
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 11:56:32	[scenario5d68d6a4db28e1.34539293]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    },
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 11:56:32	[scenario5d68d6a4db28e1.34539293]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-08-30 11:56:33	[scenario5d68d6a4db28e1.34539293]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b8331fb7153f8b4109389e6b9b8ec948
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 11:56:33	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_first
2019-08-30 11:56:33	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_second
2019-08-30 11:56:39	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_third
2019-08-30 11:56:40	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_fourth
2019-08-30 11:56:44	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_fifth
2019-08-30 11:56:45	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_six
2019-08-30 11:56:46	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_seven
2019-08-30 11:56:48	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_nine
2019-08-30 11:56:49	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_ten
2019-08-30 11:56:50	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_eleven
2019-08-30 11:56:51	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_twelve
2019-08-30 11:56:56	[scenario5d68d6a4db28e1.34539293]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-ImageColumnDemo": {
        "id": "XCExample-ImageColumnDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageColumnDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageColumnDemo",
            "authorName": "XCExample",
            "moduleName": "ImageColumnDemo",
            "description": "Update table order list",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/admin/modules/XCExample/ImageColumnDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageColumnDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    },
    "XCExample-ImageWidgetDemo": {
        "id": "XCExample-ImageWidgetDemo",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-ImageWidgetDemo",
            "version": "5.4.0.4",
            "type": "shipping",
            "author": "XCExample",
            "name": "ImageWidgetDemo",
            "authorName": "XCExample",
            "moduleName": "ImageWidgetDemo",
            "description": "Product list. Add secondary images.",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/ImageWidgetDemo/",
                "/home/kirill/domen/xcart/skins/customer/modules/XCExample/ImageWidgetDemo/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 11:56:57	[scenario5d68d6a4db28e1.34539293]	Data updated: XCExample-ImageColumnDemo
2019-08-30 11:56:57	[scenario5d68d6a4db28e1.34539293]	Data updated: XCExample-ImageWidgetDemo
2019-08-30 11:56:57	[scenario5d68d6a4db28e1.34539293]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-30 11:56:57	[scenario5d68d6a4db28e1.34539293]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b8331fb7153f8b4109389e6b9b8ec948
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 11:56:58	[scenario5d68d6a4db28e1.34539293]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 11:56:59	[scenario5d68d6a4db28e1.34539293]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 11:57:00	[scenario5d68d6a4db28e1.34539293]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 11:57:01	[scenario5d68d6a4db28e1.34539293]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 11:57:01	[scenario5d68d6a4db28e1.34539293]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b8331fb7153f8b4109389e6b9b8ec948
Context transitions => [
    "step_thirteen"
]

2019-08-30 11:57:02	[scenario5d68d6a4db28e1.34539293]	Execute X-Cart step: step_thirteen
2019-08-30 11:57:03	[scenario5d68d6a4db28e1.34539293]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-30 12:12:03	[scenario5d68da53e5abc9.47770349]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-30 12:12:05	[scenario5d68da53e5abc9.47770349]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-08-30 12:12:05	[scenario5d68da53e5abc9.47770349]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 37cbfadc2c5d8690e96f424db035d852
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 12:12:05	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_first
2019-08-30 12:12:06	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_second
2019-08-30 12:12:12	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_third
2019-08-30 12:12:13	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_fourth
2019-08-30 12:12:17	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_fifth
2019-08-30 12:12:18	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_six
2019-08-30 12:12:19	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_seven
2019-08-30 12:12:21	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_nine
2019-08-30 12:12:22	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_ten
2019-08-30 12:12:23	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_eleven
2019-08-30 12:12:24	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_twelve
2019-08-30 12:12:29	[scenario5d68da53e5abc9.47770349]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-30 12:12:30	[scenario5d68da53e5abc9.47770349]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "XCExample-AgeVerification"
]
Context missing => []

2019-08-30 12:12:30	[scenario5d68da53e5abc9.47770349]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 37cbfadc2c5d8690e96f424db035d852
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 12:12:31	[scenario5d68da53e5abc9.47770349]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 12:12:32	[scenario5d68da53e5abc9.47770349]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 12:12:33	[scenario5d68da53e5abc9.47770349]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 12:12:34	[scenario5d68da53e5abc9.47770349]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 12:12:34	[scenario5d68da53e5abc9.47770349]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 37cbfadc2c5d8690e96f424db035d852
Context transitions => [
    "step_thirteen"
]

2019-08-30 12:12:35	[scenario5d68da53e5abc9.47770349]	Execute X-Cart step: step_thirteen
2019-08-30 12:12:36	[scenario5d68da53e5abc9.47770349]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-30 12:13:14	[scenario5d68d70bd51d69.63509103]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 12:13:14	[scenario5d68d70bd51d69.63509103]	Update script transitions
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 12:13:14	[scenario5d68d70bd51d69.63509103]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 12:13:14	[scenario5d68d70bd51d69.63509103]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false,
        "AgeVerification": true
    }
}

2019-08-30 12:13:14	[scenario5d68d70bd51d69.63509103]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ff5b5ffa011174baa1ccf968d05bce6f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 12:13:14	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_first
2019-08-30 12:13:15	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_second
2019-08-30 12:13:21	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_third
2019-08-30 12:13:22	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_fourth
2019-08-30 12:13:25	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_fifth
2019-08-30 12:13:26	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_six
2019-08-30 12:13:27	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_seven
2019-08-30 12:13:29	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_nine
2019-08-30 12:13:30	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_ten
2019-08-30 12:13:31	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_eleven
2019-08-30 12:13:32	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_twelve
2019-08-30 12:13:37	[scenario5d68d70bd51d69.63509103]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 12:13:38	[scenario5d68d70bd51d69.63509103]	Data updated: XCExample-AgeVerification
2019-08-30 12:13:38	[scenario5d68d70bd51d69.63509103]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-30 12:13:38	[scenario5d68d70bd51d69.63509103]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ff5b5ffa011174baa1ccf968d05bce6f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 12:13:39	[scenario5d68d70bd51d69.63509103]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 12:13:40	[scenario5d68d70bd51d69.63509103]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 12:13:41	[scenario5d68d70bd51d69.63509103]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 12:13:42	[scenario5d68d70bd51d69.63509103]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 12:13:42	[scenario5d68d70bd51d69.63509103]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ff5b5ffa011174baa1ccf968d05bce6f
Context transitions => [
    "step_thirteen"
]

2019-08-30 12:13:43	[scenario5d68d70bd51d69.63509103]	Execute X-Cart step: step_thirteen
2019-08-30 12:13:44	[scenario5d68d70bd51d69.63509103]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-30 12:18:05	[scenario5d68dbbda7aad5.14831807]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-30 12:18:09	[scenario5d68dbbda7aad5.14831807]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false,
        "AgeVerification": true
    }
}

2019-08-30 12:18:09	[scenario5d68dbbda7aad5.14831807]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5b17bde4dcabebfe5e77a38e2dc01892
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 12:18:10	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_first
2019-08-30 12:18:11	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_second
2019-08-30 12:18:17	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_third
2019-08-30 12:18:18	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_fourth
2019-08-30 12:18:21	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_fifth
2019-08-30 12:18:22	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_six
2019-08-30 12:18:23	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_seven
2019-08-30 12:18:25	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_nine
2019-08-30 12:18:26	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_ten
2019-08-30 12:18:27	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_eleven
2019-08-30 12:18:28	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_twelve
2019-08-30 12:18:33	[scenario5d68dbbda7aad5.14831807]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-30 12:18:34	[scenario5d68dbbda7aad5.14831807]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => [
    "XCExample-AgeVerification"
]

2019-08-30 12:18:34	[scenario5d68dbbda7aad5.14831807]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5b17bde4dcabebfe5e77a38e2dc01892
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 12:18:35	[scenario5d68dbbda7aad5.14831807]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 12:18:36	[scenario5d68dbbda7aad5.14831807]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 12:18:37	[scenario5d68dbbda7aad5.14831807]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 12:18:38	[scenario5d68dbbda7aad5.14831807]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 12:18:38	[scenario5d68dbbda7aad5.14831807]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5b17bde4dcabebfe5e77a38e2dc01892
Context transitions => [
    "step_thirteen"
]

2019-08-30 12:18:39	[scenario5d68dbbda7aad5.14831807]	Execute X-Cart step: step_thirteen
2019-08-30 12:18:41	[scenario5d68dbbda7aad5.14831807]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-30 12:20:41	[scenario5d68db2b239e03.64537918]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 12:20:41	[scenario5d68db2b239e03.64537918]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 12:20:41	[scenario5d68db2b239e03.64537918]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 12:20:41	[scenario5d68db2b239e03.64537918]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-08-30 12:20:41	[scenario5d68db2b239e03.64537918]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e35f67befcbc905370b2e368532720c0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 12:20:41	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_first
2019-08-30 12:20:42	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_second
2019-08-30 12:20:46	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_third
2019-08-30 12:20:47	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_fourth
2019-08-30 12:20:49	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_fifth
2019-08-30 12:20:49	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_six
2019-08-30 12:20:50	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_seven
2019-08-30 12:20:52	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_nine
2019-08-30 12:20:53	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_ten
2019-08-30 12:20:54	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_eleven
2019-08-30 12:20:55	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_twelve
2019-08-30 12:21:00	[scenario5d68db2b239e03.64537918]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-08-30 12:21:01	[scenario5d68db2b239e03.64537918]	Data updated: TrueMachine-AgeVerification
2019-08-30 12:21:01	[scenario5d68db2b239e03.64537918]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-30 12:21:01	[scenario5d68db2b239e03.64537918]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e35f67befcbc905370b2e368532720c0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 12:21:02	[scenario5d68db2b239e03.64537918]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 12:21:03	[scenario5d68db2b239e03.64537918]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 12:21:04	[scenario5d68db2b239e03.64537918]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 12:21:05	[scenario5d68db2b239e03.64537918]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 12:21:05	[scenario5d68db2b239e03.64537918]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e35f67befcbc905370b2e368532720c0
Context transitions => [
    "step_thirteen"
]

2019-08-30 12:21:06	[scenario5d68db2b239e03.64537918]	Execute X-Cart step: step_thirteen
2019-08-30 12:21:07	[scenario5d68db2b239e03.64537918]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-30 12:31:32	[scenario5d68dee4595941.85439282]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-30 12:31:34	[scenario5d68dee4595941.85439282]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-08-30 12:31:34	[scenario5d68dee4595941.85439282]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 676bdfe2b7132a02c0aa10e4f7d2eb10
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 12:31:35	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_first
2019-08-30 12:31:36	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_second
2019-08-30 12:31:42	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_third
2019-08-30 12:31:43	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_fourth
2019-08-30 12:31:46	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_fifth
2019-08-30 12:31:47	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_six
2019-08-30 12:31:48	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_seven
2019-08-30 12:31:50	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_nine
2019-08-30 12:31:51	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_ten
2019-08-30 12:31:52	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_eleven
2019-08-30 12:31:53	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_twelve
2019-08-30 12:31:58	[scenario5d68dee4595941.85439282]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-30 12:31:59	[scenario5d68dee4595941.85439282]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-30 12:31:59	[scenario5d68dee4595941.85439282]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 676bdfe2b7132a02c0aa10e4f7d2eb10
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 12:32:00	[scenario5d68dee4595941.85439282]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 12:32:01	[scenario5d68dee4595941.85439282]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 12:32:02	[scenario5d68dee4595941.85439282]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 12:32:03	[scenario5d68dee4595941.85439282]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 12:32:03	[scenario5d68dee4595941.85439282]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 676bdfe2b7132a02c0aa10e4f7d2eb10
Context transitions => [
    "step_thirteen"
]

2019-08-30 12:32:04	[scenario5d68dee4595941.85439282]	Execute X-Cart step: step_thirteen
2019-08-30 12:32:06	[scenario5d68dee4595941.85439282]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-30 12:40:23	[scenario5d68e0f78a4ff5.30367567]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-30 12:40:25	[scenario5d68e0f78a4ff5.30367567]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-08-30 12:40:25	[scenario5d68e0f78a4ff5.30367567]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ecf7e0e673673ddde64fdf25b758f918
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 12:40:25	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_first
2019-08-30 12:40:26	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_second
2019-08-30 12:40:31	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_third
2019-08-30 12:40:32	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_fourth
2019-08-30 12:40:34	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_fifth
2019-08-30 12:40:34	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_six
2019-08-30 12:40:35	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_seven
2019-08-30 12:40:37	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_nine
2019-08-30 12:40:37	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_ten
2019-08-30 12:40:38	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_eleven
2019-08-30 12:40:38	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_twelve
2019-08-30 12:40:44	[scenario5d68e0f78a4ff5.30367567]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-30 12:40:44	[scenario5d68e0f78a4ff5.30367567]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-30 12:40:44	[scenario5d68e0f78a4ff5.30367567]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ecf7e0e673673ddde64fdf25b758f918
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 12:40:45	[scenario5d68e0f78a4ff5.30367567]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 12:40:45	[scenario5d68e0f78a4ff5.30367567]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 12:40:45	[scenario5d68e0f78a4ff5.30367567]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 12:40:45	[scenario5d68e0f78a4ff5.30367567]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 12:40:46	[scenario5d68e0f78a4ff5.30367567]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ecf7e0e673673ddde64fdf25b758f918
Context transitions => [
    "step_thirteen"
]

2019-08-30 12:40:46	[scenario5d68e0f78a4ff5.30367567]	Execute X-Cart step: step_thirteen
2019-08-30 12:40:48	[scenario5d68e0f78a4ff5.30367567]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-30 14:05:38	[scenario5d68f4f2059142.50804219]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-30 14:05:40	[scenario5d68f4f2059142.50804219]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-08-30 14:05:40	[scenario5d68f4f2059142.50804219]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8b5e9e64d92218e45fce67913e2b3632
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 14:05:41	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_first
2019-08-30 14:05:41	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_second
2019-08-30 14:05:48	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_third
2019-08-30 14:05:49	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_fourth
2019-08-30 14:05:52	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_fifth
2019-08-30 14:05:53	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_six
2019-08-30 14:05:54	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_seven
2019-08-30 14:05:57	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_nine
2019-08-30 14:05:57	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_ten
2019-08-30 14:05:58	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_eleven
2019-08-30 14:05:58	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_twelve
2019-08-30 14:06:06	[scenario5d68f4f2059142.50804219]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-30 14:06:07	[scenario5d68f4f2059142.50804219]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-30 14:06:07	[scenario5d68f4f2059142.50804219]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8b5e9e64d92218e45fce67913e2b3632
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 14:06:07	[scenario5d68f4f2059142.50804219]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 14:06:08	[scenario5d68f4f2059142.50804219]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 14:06:08	[scenario5d68f4f2059142.50804219]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 14:06:08	[scenario5d68f4f2059142.50804219]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 14:06:08	[scenario5d68f4f2059142.50804219]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8b5e9e64d92218e45fce67913e2b3632
Context transitions => [
    "step_thirteen"
]

2019-08-30 14:06:09	[scenario5d68f4f2059142.50804219]	Execute X-Cart step: step_thirteen
2019-08-30 14:06:11	[scenario5d68f4f2059142.50804219]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-30 14:32:40	[scenario5d68fb487df6b5.16754532]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-30 14:32:42	[scenario5d68fb487df6b5.16754532]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-08-30 14:32:42	[scenario5d68fb487df6b5.16754532]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 02101213a22e79d7cd808d00e90a733c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 14:32:42	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_first
2019-08-30 14:32:43	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_second
2019-08-30 14:32:49	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_third
2019-08-30 14:32:49	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_fourth
2019-08-30 14:32:52	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_fifth
2019-08-30 14:32:52	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_six
2019-08-30 14:32:53	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_seven
2019-08-30 14:32:55	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_nine
2019-08-30 14:32:55	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_ten
2019-08-30 14:32:56	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_eleven
2019-08-30 14:32:56	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_twelve
2019-08-30 14:33:03	[scenario5d68fb487df6b5.16754532]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-30 14:33:03	[scenario5d68fb487df6b5.16754532]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-30 14:33:03	[scenario5d68fb487df6b5.16754532]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 02101213a22e79d7cd808d00e90a733c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 14:33:03	[scenario5d68fb487df6b5.16754532]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 14:33:04	[scenario5d68fb487df6b5.16754532]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 14:33:04	[scenario5d68fb487df6b5.16754532]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 14:33:04	[scenario5d68fb487df6b5.16754532]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 14:33:04	[scenario5d68fb487df6b5.16754532]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 02101213a22e79d7cd808d00e90a733c
Context transitions => [
    "step_thirteen"
]

2019-08-30 14:33:05	[scenario5d68fb487df6b5.16754532]	Execute X-Cart step: step_thirteen
2019-08-30 14:33:07	[scenario5d68fb487df6b5.16754532]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-08-30 15:02:08	[scenario5d690230b8b250.11329828]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-08-30 15:02:10	[scenario5d690230b8b250.11329828]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-08-30 15:02:10	[scenario5d690230b8b250.11329828]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3723879127ef49181dc766f8f8ec202a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-08-30 15:02:10	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_first
2019-08-30 15:02:11	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_second
2019-08-30 15:02:16	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_third
2019-08-30 15:02:17	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_fourth
2019-08-30 15:02:20	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_fifth
2019-08-30 15:02:20	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_six
2019-08-30 15:02:21	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_seven
2019-08-30 15:02:22	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_nine
2019-08-30 15:02:23	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_ten
2019-08-30 15:02:23	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_eleven
2019-08-30 15:02:24	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_twelve
2019-08-30 15:02:31	[scenario5d690230b8b250.11329828]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-08-30 15:02:31	[scenario5d690230b8b250.11329828]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-08-30 15:02:31	[scenario5d690230b8b250.11329828]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 3723879127ef49181dc766f8f8ec202a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-08-30 15:02:32	[scenario5d690230b8b250.11329828]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-08-30 15:02:32	[scenario5d690230b8b250.11329828]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-08-30 15:02:32	[scenario5d690230b8b250.11329828]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-08-30 15:02:32	[scenario5d690230b8b250.11329828]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-08-30 15:02:33	[scenario5d690230b8b250.11329828]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3723879127ef49181dc766f8f8ec202a
Context transitions => [
    "step_thirteen"
]

2019-08-30 15:02:33	[scenario5d690230b8b250.11329828]	Execute X-Cart step: step_thirteen
2019-08-30 15:02:35	[scenario5d690230b8b250.11329828]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

