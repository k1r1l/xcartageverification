<?php die(); ?>
Request to marketplace "XCart\Marketplace\Request\Set"

Time: 2019-09-05 07:55:06;
Channel: service-marketplace.INFO;
Runtime id: 1c454a4eb573fbc2620e464d0a2191be;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\Set
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "querySets": {
        "get_XC5_notifications": [
            0
        ]
    }
}

Time: 2019-09-05 07:55:06;
Channel: service-marketplace.DEBUG;
Runtime id: 1c454a4eb573fbc2620e464d0a2191be;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 03:55:06 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "799"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"get_XC5_notifications\":[{\"type\":\"module\",\"module\":\"XC-FacebookMarketing\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/facebookinsta.png\",\"title\":\"Advertise your products on Facebook and Instagram and see your sales grow\",\"description\":\"Try the free Facebook Ads and Instagram Ads addon for X-Cart\",\"link\":\"\",\"date\":\"1558040400\"},{\"type\":\"news\",\"module\":\"\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/GA1.png\",\"title\":\"FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.\",\"description\":\"\",\"link\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup\",\"date\":\"1557262800\"}]}"
}

Time: 2019-09-05 07:55:07;
Channel: service-marketplace.DEBUG;
Runtime id: 1c454a4eb573fbc2620e464d0a2191be;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 07:55:07;
Channel: service-marketplace.DEBUG;
Runtime id: 1c454a4eb573fbc2620e464d0a2191be;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 07:55:07;
Channel: service-marketplace.DEBUG;
Runtime id: 1c454a4eb573fbc2620e464d0a2191be;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace correctly received "XCart\Marketplace\Request\Set"

Time: 2019-09-05 07:55:07;
Channel: service-marketplace.INFO;
Runtime id: 1c454a4eb573fbc2620e464d0a2191be;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (formatted)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "XCart\\Marketplace\\Request\\Notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 07:55:07;
Channel: service-marketplace.DEBUG;
Runtime id: 1c454a4eb573fbc2620e464d0a2191be;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 07:56:19;
Channel: service-marketplace.INFO;
Runtime id: ad4aaa8d7bfa4529e88318ca3d3122dd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 07:56:19;
Channel: service-marketplace.DEBUG;
Runtime id: ad4aaa8d7bfa4529e88318ca3d3122dd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 03:56:19 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 07:56:20;
Channel: service-marketplace.DEBUG;
Runtime id: ad4aaa8d7bfa4529e88318ca3d3122dd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:56:20;
Channel: service-marketplace.DEBUG;
Runtime id: ad4aaa8d7bfa4529e88318ca3d3122dd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:56:20;
Channel: service-marketplace.DEBUG;
Runtime id: ad4aaa8d7bfa4529e88318ca3d3122dd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:56:20;
Channel: service-marketplace.WARNING;
Runtime id: ad4aaa8d7bfa4529e88318ca3d3122dd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 07:56:20;
Channel: service-marketplace.EMERGENCY;
Runtime id: ad4aaa8d7bfa4529e88318ca3d3122dd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 07:57:00;
Channel: service-marketplace.INFO;
Runtime id: 793f0f606bb3dd6b9dab1ed3076b201b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 07:57:00;
Channel: service-marketplace.DEBUG;
Runtime id: 793f0f606bb3dd6b9dab1ed3076b201b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 03:57:00 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 07:57:00;
Channel: service-marketplace.DEBUG;
Runtime id: 793f0f606bb3dd6b9dab1ed3076b201b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:57:00;
Channel: service-marketplace.DEBUG;
Runtime id: 793f0f606bb3dd6b9dab1ed3076b201b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:57:00;
Channel: service-marketplace.DEBUG;
Runtime id: 793f0f606bb3dd6b9dab1ed3076b201b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:57:00;
Channel: service-marketplace.WARNING;
Runtime id: 793f0f606bb3dd6b9dab1ed3076b201b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 07:57:00;
Channel: service-marketplace.EMERGENCY;
Runtime id: 793f0f606bb3dd6b9dab1ed3076b201b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 07:57:29;
Channel: service-marketplace.INFO;
Runtime id: 539272cc9d050934a0860d0203738c74;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 07:57:29;
Channel: service-marketplace.DEBUG;
Runtime id: 539272cc9d050934a0860d0203738c74;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 03:57:29 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 07:57:29;
Channel: service-marketplace.DEBUG;
Runtime id: 539272cc9d050934a0860d0203738c74;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:57:29;
Channel: service-marketplace.DEBUG;
Runtime id: 539272cc9d050934a0860d0203738c74;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:57:29;
Channel: service-marketplace.DEBUG;
Runtime id: 539272cc9d050934a0860d0203738c74;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:57:29;
Channel: service-marketplace.WARNING;
Runtime id: 539272cc9d050934a0860d0203738c74;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 07:57:29;
Channel: service-marketplace.EMERGENCY;
Runtime id: 539272cc9d050934a0860d0203738c74;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 07:58:09;
Channel: service-marketplace.INFO;
Runtime id: d0b779d29b2706b5aad3d5be6697ea36;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 07:58:09;
Channel: service-marketplace.DEBUG;
Runtime id: d0b779d29b2706b5aad3d5be6697ea36;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 03:58:10 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 07:58:10;
Channel: service-marketplace.DEBUG;
Runtime id: d0b779d29b2706b5aad3d5be6697ea36;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:58:10;
Channel: service-marketplace.DEBUG;
Runtime id: d0b779d29b2706b5aad3d5be6697ea36;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:58:10;
Channel: service-marketplace.DEBUG;
Runtime id: d0b779d29b2706b5aad3d5be6697ea36;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 07:58:10;
Channel: service-marketplace.WARNING;
Runtime id: d0b779d29b2706b5aad3d5be6697ea36;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 07:58:10;
Channel: service-marketplace.EMERGENCY;
Runtime id: d0b779d29b2706b5aad3d5be6697ea36;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:03:09;
Channel: service-marketplace.INFO;
Runtime id: 27d734116c774234067aacfdebd95671;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:03:09;
Channel: service-marketplace.DEBUG;
Runtime id: 27d734116c774234067aacfdebd95671;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:03:09 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:03:10;
Channel: service-marketplace.DEBUG;
Runtime id: 27d734116c774234067aacfdebd95671;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:03:10;
Channel: service-marketplace.DEBUG;
Runtime id: 27d734116c774234067aacfdebd95671;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:03:10;
Channel: service-marketplace.DEBUG;
Runtime id: 27d734116c774234067aacfdebd95671;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:03:10;
Channel: service-marketplace.WARNING;
Runtime id: 27d734116c774234067aacfdebd95671;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:03:10;
Channel: service-marketplace.EMERGENCY;
Runtime id: 27d734116c774234067aacfdebd95671;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:03:38;
Channel: service-marketplace.INFO;
Runtime id: 06afba8bd613e737b49b51c55b9239aa;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:03:38;
Channel: service-marketplace.DEBUG;
Runtime id: 06afba8bd613e737b49b51c55b9239aa;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:03:38 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:03:38;
Channel: service-marketplace.DEBUG;
Runtime id: 06afba8bd613e737b49b51c55b9239aa;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:03:38;
Channel: service-marketplace.DEBUG;
Runtime id: 06afba8bd613e737b49b51c55b9239aa;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:03:38;
Channel: service-marketplace.DEBUG;
Runtime id: 06afba8bd613e737b49b51c55b9239aa;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:03:38;
Channel: service-marketplace.WARNING;
Runtime id: 06afba8bd613e737b49b51c55b9239aa;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:03:38;
Channel: service-marketplace.EMERGENCY;
Runtime id: 06afba8bd613e737b49b51c55b9239aa;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:19:47;
Channel: service-marketplace.INFO;
Runtime id: ee694bd48fafe54d8e9389654a468465;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:19:47;
Channel: service-marketplace.DEBUG;
Runtime id: ee694bd48fafe54d8e9389654a468465;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:19:47 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:19:48;
Channel: service-marketplace.DEBUG;
Runtime id: ee694bd48fafe54d8e9389654a468465;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:19:48;
Channel: service-marketplace.DEBUG;
Runtime id: ee694bd48fafe54d8e9389654a468465;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:19:48;
Channel: service-marketplace.DEBUG;
Runtime id: ee694bd48fafe54d8e9389654a468465;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:19:48;
Channel: service-marketplace.WARNING;
Runtime id: ee694bd48fafe54d8e9389654a468465;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:19:48;
Channel: service-marketplace.EMERGENCY;
Runtime id: ee694bd48fafe54d8e9389654a468465;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:20:23;
Channel: service-marketplace.INFO;
Runtime id: b243e08784819f3beecb8d8373cd8913;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:20:23;
Channel: service-marketplace.DEBUG;
Runtime id: b243e08784819f3beecb8d8373cd8913;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:20:24 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:20:24;
Channel: service-marketplace.DEBUG;
Runtime id: b243e08784819f3beecb8d8373cd8913;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:20:24;
Channel: service-marketplace.DEBUG;
Runtime id: b243e08784819f3beecb8d8373cd8913;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:20:24;
Channel: service-marketplace.DEBUG;
Runtime id: b243e08784819f3beecb8d8373cd8913;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:20:24;
Channel: service-marketplace.WARNING;
Runtime id: b243e08784819f3beecb8d8373cd8913;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:20:24;
Channel: service-marketplace.EMERGENCY;
Runtime id: b243e08784819f3beecb8d8373cd8913;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:24:12;
Channel: service-marketplace.INFO;
Runtime id: 3d04a78c8fd732608ffc37a735e2a602;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:24:12;
Channel: service-marketplace.DEBUG;
Runtime id: 3d04a78c8fd732608ffc37a735e2a602;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:24:12 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:24:12;
Channel: service-marketplace.DEBUG;
Runtime id: 3d04a78c8fd732608ffc37a735e2a602;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:24:12;
Channel: service-marketplace.DEBUG;
Runtime id: 3d04a78c8fd732608ffc37a735e2a602;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:24:12;
Channel: service-marketplace.DEBUG;
Runtime id: 3d04a78c8fd732608ffc37a735e2a602;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:24:12;
Channel: service-marketplace.WARNING;
Runtime id: 3d04a78c8fd732608ffc37a735e2a602;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:24:12;
Channel: service-marketplace.EMERGENCY;
Runtime id: 3d04a78c8fd732608ffc37a735e2a602;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:24:39;
Channel: service-marketplace.INFO;
Runtime id: a98bce3d072528b9706c2dea824b1eb6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:24:39;
Channel: service-marketplace.DEBUG;
Runtime id: a98bce3d072528b9706c2dea824b1eb6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:24:39 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:24:40;
Channel: service-marketplace.DEBUG;
Runtime id: a98bce3d072528b9706c2dea824b1eb6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:24:40;
Channel: service-marketplace.DEBUG;
Runtime id: a98bce3d072528b9706c2dea824b1eb6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:24:40;
Channel: service-marketplace.DEBUG;
Runtime id: a98bce3d072528b9706c2dea824b1eb6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:24:40;
Channel: service-marketplace.WARNING;
Runtime id: a98bce3d072528b9706c2dea824b1eb6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:24:40;
Channel: service-marketplace.EMERGENCY;
Runtime id: a98bce3d072528b9706c2dea824b1eb6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:30:37;
Channel: service-marketplace.INFO;
Runtime id: 44a78b4b6cdf402aeaa7bad483d718d0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:30:37;
Channel: service-marketplace.DEBUG;
Runtime id: 44a78b4b6cdf402aeaa7bad483d718d0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:30:38 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:30:38;
Channel: service-marketplace.DEBUG;
Runtime id: 44a78b4b6cdf402aeaa7bad483d718d0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:30:38;
Channel: service-marketplace.DEBUG;
Runtime id: 44a78b4b6cdf402aeaa7bad483d718d0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:30:38;
Channel: service-marketplace.DEBUG;
Runtime id: 44a78b4b6cdf402aeaa7bad483d718d0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:30:38;
Channel: service-marketplace.WARNING;
Runtime id: 44a78b4b6cdf402aeaa7bad483d718d0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:30:38;
Channel: service-marketplace.EMERGENCY;
Runtime id: 44a78b4b6cdf402aeaa7bad483d718d0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:31:04;
Channel: service-marketplace.INFO;
Runtime id: 4a335b03a1b5b21baf1cdec787aee957;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:31:04;
Channel: service-marketplace.DEBUG;
Runtime id: 4a335b03a1b5b21baf1cdec787aee957;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:31:04 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:31:04;
Channel: service-marketplace.DEBUG;
Runtime id: 4a335b03a1b5b21baf1cdec787aee957;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:31:04;
Channel: service-marketplace.DEBUG;
Runtime id: 4a335b03a1b5b21baf1cdec787aee957;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:31:04;
Channel: service-marketplace.DEBUG;
Runtime id: 4a335b03a1b5b21baf1cdec787aee957;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:31:04;
Channel: service-marketplace.WARNING;
Runtime id: 4a335b03a1b5b21baf1cdec787aee957;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:31:04;
Channel: service-marketplace.EMERGENCY;
Runtime id: 4a335b03a1b5b21baf1cdec787aee957;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:52:09;
Channel: service-marketplace.INFO;
Runtime id: 8f23ce0f17fc8ce114e08b7b861b07c0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:52:09;
Channel: service-marketplace.DEBUG;
Runtime id: 8f23ce0f17fc8ce114e08b7b861b07c0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:52:09 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:52:10;
Channel: service-marketplace.DEBUG;
Runtime id: 8f23ce0f17fc8ce114e08b7b861b07c0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:52:10;
Channel: service-marketplace.DEBUG;
Runtime id: 8f23ce0f17fc8ce114e08b7b861b07c0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:52:10;
Channel: service-marketplace.DEBUG;
Runtime id: 8f23ce0f17fc8ce114e08b7b861b07c0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:52:10;
Channel: service-marketplace.WARNING;
Runtime id: 8f23ce0f17fc8ce114e08b7b861b07c0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:52:10;
Channel: service-marketplace.EMERGENCY;
Runtime id: 8f23ce0f17fc8ce114e08b7b861b07c0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:52:36;
Channel: service-marketplace.INFO;
Runtime id: cc7a8cce229d684be85411223d8a3d6a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:52:36;
Channel: service-marketplace.DEBUG;
Runtime id: cc7a8cce229d684be85411223d8a3d6a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:52:37 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:52:37;
Channel: service-marketplace.DEBUG;
Runtime id: cc7a8cce229d684be85411223d8a3d6a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:52:37;
Channel: service-marketplace.DEBUG;
Runtime id: cc7a8cce229d684be85411223d8a3d6a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:52:37;
Channel: service-marketplace.DEBUG;
Runtime id: cc7a8cce229d684be85411223d8a3d6a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:52:37;
Channel: service-marketplace.WARNING;
Runtime id: cc7a8cce229d684be85411223d8a3d6a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:52:37;
Channel: service-marketplace.EMERGENCY;
Runtime id: cc7a8cce229d684be85411223d8a3d6a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\Set"

Time: 2019-09-05 08:56:14;
Channel: service-marketplace.INFO;
Runtime id: 0b6459ca04431af75170bc0d6ba46b2f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\Set
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "querySets": {
        "get_XC5_notifications": [
            0
        ]
    }
}

Time: 2019-09-05 08:56:14;
Channel: service-marketplace.DEBUG;
Runtime id: 0b6459ca04431af75170bc0d6ba46b2f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:56:15 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "799"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"get_XC5_notifications\":[{\"type\":\"module\",\"module\":\"XC-FacebookMarketing\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/facebookinsta.png\",\"title\":\"Advertise your products on Facebook and Instagram and see your sales grow\",\"description\":\"Try the free Facebook Ads and Instagram Ads addon for X-Cart\",\"link\":\"\",\"date\":\"1558040400\"},{\"type\":\"news\",\"module\":\"\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/GA1.png\",\"title\":\"FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.\",\"description\":\"\",\"link\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup\",\"date\":\"1557262800\"}]}"
}

Time: 2019-09-05 08:56:15;
Channel: service-marketplace.DEBUG;
Runtime id: 0b6459ca04431af75170bc0d6ba46b2f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 08:56:15;
Channel: service-marketplace.DEBUG;
Runtime id: 0b6459ca04431af75170bc0d6ba46b2f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 08:56:15;
Channel: service-marketplace.DEBUG;
Runtime id: 0b6459ca04431af75170bc0d6ba46b2f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace correctly received "XCart\Marketplace\Request\Set"

Time: 2019-09-05 08:56:15;
Channel: service-marketplace.INFO;
Runtime id: 0b6459ca04431af75170bc0d6ba46b2f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (formatted)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "XCart\\Marketplace\\Request\\Notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 08:56:15;
Channel: service-marketplace.DEBUG;
Runtime id: 0b6459ca04431af75170bc0d6ba46b2f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:59:01;
Channel: service-marketplace.INFO;
Runtime id: 614356b68437fb12f436004ed364a301;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:59:01;
Channel: service-marketplace.DEBUG;
Runtime id: 614356b68437fb12f436004ed364a301;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:59:01 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:59:02;
Channel: service-marketplace.DEBUG;
Runtime id: 614356b68437fb12f436004ed364a301;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:59:02;
Channel: service-marketplace.DEBUG;
Runtime id: 614356b68437fb12f436004ed364a301;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:59:02;
Channel: service-marketplace.DEBUG;
Runtime id: 614356b68437fb12f436004ed364a301;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:59:02;
Channel: service-marketplace.WARNING;
Runtime id: 614356b68437fb12f436004ed364a301;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:59:02;
Channel: service-marketplace.EMERGENCY;
Runtime id: 614356b68437fb12f436004ed364a301;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 08:59:37;
Channel: service-marketplace.INFO;
Runtime id: ab0b11284436f9f23fad15c6aefbb683;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 08:59:37;
Channel: service-marketplace.DEBUG;
Runtime id: ab0b11284436f9f23fad15c6aefbb683;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 04:59:38 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 08:59:38;
Channel: service-marketplace.DEBUG;
Runtime id: ab0b11284436f9f23fad15c6aefbb683;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:59:38;
Channel: service-marketplace.DEBUG;
Runtime id: ab0b11284436f9f23fad15c6aefbb683;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:59:38;
Channel: service-marketplace.DEBUG;
Runtime id: ab0b11284436f9f23fad15c6aefbb683;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 08:59:38;
Channel: service-marketplace.WARNING;
Runtime id: ab0b11284436f9f23fad15c6aefbb683;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 08:59:38;
Channel: service-marketplace.EMERGENCY;
Runtime id: ab0b11284436f9f23fad15c6aefbb683;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 09:06:40;
Channel: service-marketplace.INFO;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 09:06:40;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 05:06:41 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.WARNING;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.EMERGENCY;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\Set"

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.INFO;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\Set
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "querySets": {
        "get_all_tags": {
            "lng": "all",
            "0": 0
        }
    }
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 05:06:41 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "13896"
        ],
        "connection": [
            "close"
        ],
        "vary": [
            "Accept-Encoding"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"get_all_tags\":{\"1\":{\"tag_name\":\"Marketing\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Banner%2520Facebook%2520and%2520Instagram%2520Ads%2520on%2520tags.jpg\",\"tag_module_banner\":\"XC-FacebookMarketing\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041c\\u0430\\u0440\\u043a\\u0435\\u0442\\u0438\\u043d\\u0433\"}}},\"2\":{\"tag_name\":\"User experience\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0418\\u043d\\u0442\\u0435\\u0440\\u0444\\u0435\\u0439\\u0441 \\u043f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u044f\"}}},\"3\":{\"tag_name\":\"Design tweaks\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Advanced%2520Contact%2520us%2520form.jpg\",\"tag_module_banner\":\"QSL-AdvancedContactUs\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d\"}}},\"4\":{\"tag_name\":\"Administration\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0410\\u0434\\u043c\\u0438\\u043d\\u0438\\u0441\\u0442\\u0440\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435\"}}},\"5\":{\"tag_name\":\"Taxes\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/TaxJar-banner-tag.png\",\"tag_module_banner\":\"XC-TaxJar\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041d\\u0430\\u043b\\u043e\\u0433\\u0438\"}}},\"6\":{\"tag_name\":\"Fulfillment\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041e\\u0431\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430 \\u0437\\u0430\\u043a\\u0430\\u0437\\u043e\\u0432\"}}},\"7\":{\"tag_name\":\"Payment processing\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Square_tag_banner.jpg\",\"tag_module_banner\":\"XPay-Square\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041e\\u0431\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430 \\u043f\\u043b\\u0430\\u0442\\u0435\\u0436\\u0435\\u0439\"}}},\"8\":{\"tag_name\":\"Translation\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041f\\u0435\\u0440\\u0435\\u0432\\u043e\\u0434\"}}},\"9\":{\"tag_name\":\"Shipping\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/71lbs%2520banner-2-v2-web-ready%2520%25281%2529.jpg\",\"tag_module_banner\":\"AutomatedShippingRefunds71LBS-SeventyOnePounds\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430\"}}},\"11\":{\"tag_name\":\"Development\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0420\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430\"}}},\"12\":{\"tag_name\":\"Statistics\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0441\\u0442\\u0438\\u043a\\u0430\"}}},\"13\":{\"tag_name\":\"Mobile Commerce\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u043b\\u0430\\u0442\\u0444\\u043e\\u0440\\u043c\\u044b\"}}},\"15\":{\"tag_name\":\"SEO\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/SEO%2520Health%2520Check_1.jpg\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"https:\\/\\/www.x-cart.com\\/seo-consulting.html\",\"category\":\"M\",\"translations\":[]},\"16\":{\"tag_name\":\"Search and Navigation\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Cloud_Search.jpg\",\"tag_module_banner\":\"QSL-CloudSearch\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041f\\u043e\\u0438\\u0441\\u043a \\u0438 \\u043d\\u0430\\u0432\\u0438\\u0433\\u0430\\u0446\\u0438\\u044f\"}}},\"17\":{\"tag_name\":\"Social\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/livechat.jpg\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"https:\\/\\/market.x-cart.com\\/addons\\/livechat.html\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0421\\u043e\\u0446\\u0438\\u0430\\u043b\\u0438\\u0437\\u0430\\u0446\\u0438\\u044f\"}}},\"18\":{\"tag_name\":\"Catalog Management\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Shop_by_Brand.jpg\",\"tag_module_banner\":\"QSL-ShopByBrand\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433 \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432\"}}},\"21\":{\"tag_name\":\"Blog\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0411\\u043b\\u043e\\u0433\"}}},\"22\":{\"tag_name\":\"Accounting\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0424\\u0438\\u043d\\u0430\\u043d\\u0441\\u043e\\u0432\\u044b\\u0439 \\u0443\\u0447\\u0451\\u0442\"}}},\"23\":{\"tag_name\":\"Orders\",\"tag_banner_expiration_date\":\"Sun, 12 May 2019 00:00:00 +0400\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Xcart%2520Marketplace%2520Skinny%2520Banner.png\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"https:\\/\\/market.x-cart.com\\/addons\\/integration-nchannel.html?utm_source=marketplace&utm_medium=tag-banner\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0417\\u0430\\u043a\\u0430\\u0437\\u044b\"}}},\"24\":{\"tag_name\":\"Security\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0411\\u0435\\u0437\\u043e\\u043f\\u0430\\u0441\\u043d\\u043e\\u0441\\u0442\\u044c\"}}},\"26\":{\"tag_name\":\"Miscellaneous\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041f\\u0440\\u043e\\u0447\\u0438\\u0435\"}}},\"27\":{\"tag_name\":\"Products\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0422\\u043e\\u0432\\u0430\\u0440\\u044b\"}}},\"29\":{\"tag_name\":\"Migration\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041f\\u0435\\u0440\\u0435\\u043d\\u043e\\u0441 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445\"}}},\"30\":{\"tag_name\":\"Fraud prevention\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0417\\u0430\\u0449\\u0438\\u0442\\u0430 \\u043e\\u0442 \\u043c\\u043e\\u0448\\u0435\\u043d\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430\"}}},\"31\":{\"tag_name\":\"Price modifiers\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041c\\u043e\\u0434\\u0438\\u0444\\u0438\\u043a\\u0430\\u0446\\u0438\\u044f \\u0446\\u0435\\u043d\"}}},\"32\":{\"tag_name\":\"Sales Channels\",\"tag_banner_expiration_date\":\"Wed, 15 May 2019 00:00:00 +0400\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/xcart_thinbanner.png\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&utm_medium=tag-banner\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041a\\u0430\\u043d\\u0430\\u043b\\u044b \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\"}}},\"33\":{\"tag_name\":\"Templates\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0428\\u0430\\u0431\\u043b\\u043e\\u043d\\u044b \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0430\"}}},\"34\":{\"tag_name\":\"Multi-vendor friendly\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0414\\u043b\\u044f \\u041c\\u0443\\u043b\\u044c\\u0442\\u0438\\u0432\\u0435\\u043d\\u0434\\u043e\\u0440\"}}},\"35\":{\"tag_name\":\"Checkout\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Square_tag_banner.jpg\",\"tag_module_banner\":\"XPay-Square\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":[]},\"36\":{\"tag_name\":\"Jewelry &amp; Accessories\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"37\":{\"tag_name\":\"Pets\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"38\":{\"tag_name\":\"Art &amp; Photography\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"39\":{\"tag_name\":\"Health &amp; Beauty\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"40\":{\"tag_name\":\"Books\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"41\":{\"tag_name\":\"Business\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"42\":{\"tag_name\":\"Cars\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"43\":{\"tag_name\":\"T-Shirts\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"44\":{\"tag_name\":\"Computer\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"45\":{\"tag_name\":\"Education\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"46\":{\"tag_name\":\"Electronics\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"47\":{\"tag_name\":\"Entertainment\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"48\":{\"tag_name\":\"Popular\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"49\":{\"tag_name\":\"Fitness\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"50\":{\"tag_name\":\"Flowers\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"51\":{\"tag_name\":\"Food &amp; Drink\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"52\":{\"tag_name\":\"Gifts\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"53\":{\"tag_name\":\"Crafts &amp; Hobbies\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"54\":{\"tag_name\":\"Industrial\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"55\":{\"tag_name\":\"Furniture &amp; Interior\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"56\":{\"tag_name\":\"Media\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"57\":{\"tag_name\":\"Medical\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"58\":{\"tag_name\":\"Military\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"59\":{\"tag_name\":\"Music\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"60\":{\"tag_name\":\"Nutritional supplements\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"61\":{\"tag_name\":\"Sports\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"62\":{\"tag_name\":\"Supermarket\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"63\":{\"tag_name\":\"Equipment &amp; Tools\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"64\":{\"tag_name\":\"Watches\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"65\":{\"tag_name\":\"Wedding\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"66\":{\"tag_name\":\"Minimal\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"67\":{\"tag_name\":\"Apparel &amp; Clothing\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]}}}"
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "get_all_tags": {
        "1": {
            "tag_name": "Marketing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Banner%2520Facebook%2520and%2520Instagram%2520Ads%2520on%2520tags.jpg",
            "tag_module_banner": "XC-FacebookMarketing",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Маркетинг"
                }
            }
        },
        "2": {
            "tag_name": "User experience",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Интерфейс покупателя"
                }
            }
        },
        "3": {
            "tag_name": "Design tweaks",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Advanced%2520Contact%2520us%2520form.jpg",
            "tag_module_banner": "QSL-AdvancedContactUs",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Дизайн"
                }
            }
        },
        "4": {
            "tag_name": "Administration",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Администрирование"
                }
            }
        },
        "5": {
            "tag_name": "Taxes",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/TaxJar-banner-tag.png",
            "tag_module_banner": "XC-TaxJar",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Налоги"
                }
            }
        },
        "6": {
            "tag_name": "Fulfillment",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Обработка заказов"
                }
            }
        },
        "7": {
            "tag_name": "Payment processing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "tag_module_banner": "XPay-Square",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Обработка платежей"
                }
            }
        },
        "8": {
            "tag_name": "Translation",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Перевод"
                }
            }
        },
        "9": {
            "tag_name": "Shipping",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/71lbs%2520banner-2-v2-web-ready%2520%25281%2529.jpg",
            "tag_module_banner": "AutomatedShippingRefunds71LBS-SeventyOnePounds",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Доставка"
                }
            }
        },
        "11": {
            "tag_name": "Development",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Разработка"
                }
            }
        },
        "12": {
            "tag_name": "Statistics",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Статистика"
                }
            }
        },
        "13": {
            "tag_name": "Mobile Commerce",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Мобильные платформы"
                }
            }
        },
        "15": {
            "tag_name": "SEO",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/SEO%2520Health%2520Check_1.jpg",
            "tag_module_banner": "",
            "tag_banner_url": "https://www.x-cart.com/seo-consulting.html",
            "category": "M",
            "translations": []
        },
        "16": {
            "tag_name": "Search and Navigation",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Cloud_Search.jpg",
            "tag_module_banner": "QSL-CloudSearch",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Поиск и навигация"
                }
            }
        },
        "17": {
            "tag_name": "Social",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/livechat.jpg",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/livechat.html",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Социализация"
                }
            }
        },
        "18": {
            "tag_name": "Catalog Management",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Shop_by_Brand.jpg",
            "tag_module_banner": "QSL-ShopByBrand",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Каталог товаров"
                }
            }
        },
        "21": {
            "tag_name": "Blog",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Блог"
                }
            }
        },
        "22": {
            "tag_name": "Accounting",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Финансовый учёт"
                }
            }
        },
        "23": {
            "tag_name": "Orders",
            "tag_banner_expiration_date": "Sun, 12 May 2019 00:00:00 +0400",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Xcart%2520Marketplace%2520Skinny%2520Banner.png",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/integration-nchannel.html?utm_source=marketplace&utm_medium=tag-banner",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Заказы"
                }
            }
        },
        "24": {
            "tag_name": "Security",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Безопасность"
                }
            }
        },
        "26": {
            "tag_name": "Miscellaneous",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Прочие"
                }
            }
        },
        "27": {
            "tag_name": "Products",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Товары"
                }
            }
        },
        "29": {
            "tag_name": "Migration",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Перенос данных"
                }
            }
        },
        "30": {
            "tag_name": "Fraud prevention",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Защита от мошенничества"
                }
            }
        },
        "31": {
            "tag_name": "Price modifiers",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Модификация цен"
                }
            }
        },
        "32": {
            "tag_name": "Sales Channels",
            "tag_banner_expiration_date": "Wed, 15 May 2019 00:00:00 +0400",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/xcart_thinbanner.png",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&utm_medium=tag-banner",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Каналы продаж"
                }
            }
        },
        "33": {
            "tag_name": "Templates",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Шаблоны дизайна"
                }
            }
        },
        "34": {
            "tag_name": "Multi-vendor friendly",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Для Мультивендор"
                }
            }
        },
        "35": {
            "tag_name": "Checkout",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "tag_module_banner": "XPay-Square",
            "tag_banner_url": "",
            "category": "M",
            "translations": []
        },
        "36": {
            "tag_name": "Jewelry &amp; Accessories",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "37": {
            "tag_name": "Pets",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "38": {
            "tag_name": "Art &amp; Photography",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "39": {
            "tag_name": "Health &amp; Beauty",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "40": {
            "tag_name": "Books",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "41": {
            "tag_name": "Business",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "42": {
            "tag_name": "Cars",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "43": {
            "tag_name": "T-Shirts",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "44": {
            "tag_name": "Computer",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "45": {
            "tag_name": "Education",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "46": {
            "tag_name": "Electronics",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "47": {
            "tag_name": "Entertainment",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "48": {
            "tag_name": "Popular",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "49": {
            "tag_name": "Fitness",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "50": {
            "tag_name": "Flowers",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "51": {
            "tag_name": "Food &amp; Drink",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "52": {
            "tag_name": "Gifts",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "53": {
            "tag_name": "Crafts &amp; Hobbies",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "54": {
            "tag_name": "Industrial",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "55": {
            "tag_name": "Furniture &amp; Interior",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "56": {
            "tag_name": "Media",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "57": {
            "tag_name": "Medical",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "58": {
            "tag_name": "Military",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "59": {
            "tag_name": "Music",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "60": {
            "tag_name": "Nutritional supplements",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "61": {
            "tag_name": "Sports",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "62": {
            "tag_name": "Supermarket",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "63": {
            "tag_name": "Equipment &amp; Tools",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "64": {
            "tag_name": "Watches",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "65": {
            "tag_name": "Wedding",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "66": {
            "tag_name": "Minimal",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "67": {
            "tag_name": "Apparel &amp; Clothing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        }
    }
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "get_all_tags": {
        "1": {
            "tag_name": "Marketing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Banner%2520Facebook%2520and%2520Instagram%2520Ads%2520on%2520tags.jpg",
            "tag_module_banner": "XC-FacebookMarketing",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Маркетинг"
                }
            }
        },
        "2": {
            "tag_name": "User experience",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Интерфейс покупателя"
                }
            }
        },
        "3": {
            "tag_name": "Design tweaks",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Advanced%2520Contact%2520us%2520form.jpg",
            "tag_module_banner": "QSL-AdvancedContactUs",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Дизайн"
                }
            }
        },
        "4": {
            "tag_name": "Administration",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Администрирование"
                }
            }
        },
        "5": {
            "tag_name": "Taxes",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/TaxJar-banner-tag.png",
            "tag_module_banner": "XC-TaxJar",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Налоги"
                }
            }
        },
        "6": {
            "tag_name": "Fulfillment",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Обработка заказов"
                }
            }
        },
        "7": {
            "tag_name": "Payment processing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "tag_module_banner": "XPay-Square",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Обработка платежей"
                }
            }
        },
        "8": {
            "tag_name": "Translation",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Перевод"
                }
            }
        },
        "9": {
            "tag_name": "Shipping",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/71lbs%2520banner-2-v2-web-ready%2520%25281%2529.jpg",
            "tag_module_banner": "AutomatedShippingRefunds71LBS-SeventyOnePounds",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Доставка"
                }
            }
        },
        "11": {
            "tag_name": "Development",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Разработка"
                }
            }
        },
        "12": {
            "tag_name": "Statistics",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Статистика"
                }
            }
        },
        "13": {
            "tag_name": "Mobile Commerce",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Мобильные платформы"
                }
            }
        },
        "15": {
            "tag_name": "SEO",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/SEO%2520Health%2520Check_1.jpg",
            "tag_module_banner": "",
            "tag_banner_url": "https://www.x-cart.com/seo-consulting.html",
            "category": "M",
            "translations": []
        },
        "16": {
            "tag_name": "Search and Navigation",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Cloud_Search.jpg",
            "tag_module_banner": "QSL-CloudSearch",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Поиск и навигация"
                }
            }
        },
        "17": {
            "tag_name": "Social",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/livechat.jpg",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/livechat.html",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Социализация"
                }
            }
        },
        "18": {
            "tag_name": "Catalog Management",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Shop_by_Brand.jpg",
            "tag_module_banner": "QSL-ShopByBrand",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Каталог товаров"
                }
            }
        },
        "21": {
            "tag_name": "Blog",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Блог"
                }
            }
        },
        "22": {
            "tag_name": "Accounting",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Финансовый учёт"
                }
            }
        },
        "23": {
            "tag_name": "Orders",
            "tag_banner_expiration_date": "Sun, 12 May 2019 00:00:00 +0400",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Xcart%2520Marketplace%2520Skinny%2520Banner.png",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/integration-nchannel.html?utm_source=marketplace&utm_medium=tag-banner",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Заказы"
                }
            }
        },
        "24": {
            "tag_name": "Security",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Безопасность"
                }
            }
        },
        "26": {
            "tag_name": "Miscellaneous",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Прочие"
                }
            }
        },
        "27": {
            "tag_name": "Products",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Товары"
                }
            }
        },
        "29": {
            "tag_name": "Migration",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Перенос данных"
                }
            }
        },
        "30": {
            "tag_name": "Fraud prevention",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Защита от мошенничества"
                }
            }
        },
        "31": {
            "tag_name": "Price modifiers",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Модификация цен"
                }
            }
        },
        "32": {
            "tag_name": "Sales Channels",
            "tag_banner_expiration_date": "Wed, 15 May 2019 00:00:00 +0400",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/xcart_thinbanner.png",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&utm_medium=tag-banner",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Каналы продаж"
                }
            }
        },
        "33": {
            "tag_name": "Templates",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Шаблоны дизайна"
                }
            }
        },
        "34": {
            "tag_name": "Multi-vendor friendly",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Для Мультивендор"
                }
            }
        },
        "35": {
            "tag_name": "Checkout",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "tag_module_banner": "XPay-Square",
            "tag_banner_url": "",
            "category": "M",
            "translations": []
        },
        "36": {
            "tag_name": "Jewelry &amp; Accessories",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "37": {
            "tag_name": "Pets",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "38": {
            "tag_name": "Art &amp; Photography",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "39": {
            "tag_name": "Health &amp; Beauty",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "40": {
            "tag_name": "Books",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "41": {
            "tag_name": "Business",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "42": {
            "tag_name": "Cars",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "43": {
            "tag_name": "T-Shirts",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "44": {
            "tag_name": "Computer",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "45": {
            "tag_name": "Education",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "46": {
            "tag_name": "Electronics",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "47": {
            "tag_name": "Entertainment",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "48": {
            "tag_name": "Popular",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "49": {
            "tag_name": "Fitness",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "50": {
            "tag_name": "Flowers",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "51": {
            "tag_name": "Food &amp; Drink",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "52": {
            "tag_name": "Gifts",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "53": {
            "tag_name": "Crafts &amp; Hobbies",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "54": {
            "tag_name": "Industrial",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "55": {
            "tag_name": "Furniture &amp; Interior",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "56": {
            "tag_name": "Media",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "57": {
            "tag_name": "Medical",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "58": {
            "tag_name": "Military",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "59": {
            "tag_name": "Music",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "60": {
            "tag_name": "Nutritional supplements",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "61": {
            "tag_name": "Sports",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "62": {
            "tag_name": "Supermarket",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "63": {
            "tag_name": "Equipment &amp; Tools",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "64": {
            "tag_name": "Watches",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "65": {
            "tag_name": "Wedding",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "66": {
            "tag_name": "Minimal",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "67": {
            "tag_name": "Apparel &amp; Clothing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        }
    }
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace correctly received "XCart\Marketplace\Request\Set"

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.INFO;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (formatted)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "XCart\\Marketplace\\Request\\Tags": {
        "1": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Banner%2520Facebook%2520and%2520Instagram%2520Ads%2520on%2520tags.jpg",
            "module": "XC-FacebookMarketing",
            "expires": "",
            "url": "",
            "name": "Marketing",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Маркетинг"
                }
            ]
        },
        "2": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "User experience",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Интерфейс покупателя"
                }
            ]
        },
        "3": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Advanced%2520Contact%2520us%2520form.jpg",
            "module": "QSL-AdvancedContactUs",
            "expires": "",
            "url": "",
            "name": "Design tweaks",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Дизайн"
                }
            ]
        },
        "4": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Administration",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Администрирование"
                }
            ]
        },
        "5": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/TaxJar-banner-tag.png",
            "module": "XC-TaxJar",
            "expires": "",
            "url": "",
            "name": "Taxes",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Налоги"
                }
            ]
        },
        "6": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Fulfillment",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Обработка заказов"
                }
            ]
        },
        "7": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "module": "XPay-Square",
            "expires": "",
            "url": "",
            "name": "Payment processing",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Обработка платежей"
                }
            ]
        },
        "8": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Translation",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Перевод"
                }
            ]
        },
        "9": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/71lbs%2520banner-2-v2-web-ready%2520%25281%2529.jpg",
            "module": "AutomatedShippingRefunds71LBS-SeventyOnePounds",
            "expires": "",
            "url": "",
            "name": "Shipping",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Доставка"
                }
            ]
        },
        "11": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Development",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Разработка"
                }
            ]
        },
        "12": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Statistics",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Статистика"
                }
            ]
        },
        "13": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Mobile Commerce",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Мобильные платформы"
                }
            ]
        },
        "15": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/SEO%2520Health%2520Check_1.jpg",
            "module": "",
            "expires": "",
            "url": "https://www.x-cart.com/seo-consulting.html",
            "name": "SEO",
            "category": "M",
            "tarnslations": []
        },
        "16": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Cloud_Search.jpg",
            "module": "QSL-CloudSearch",
            "expires": "",
            "url": "",
            "name": "Search and Navigation",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Поиск и навигация"
                }
            ]
        },
        "17": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/livechat.jpg",
            "module": "",
            "expires": "",
            "url": "https://market.x-cart.com/addons/livechat.html",
            "name": "Social",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Социализация"
                }
            ]
        },
        "18": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Shop_by_Brand.jpg",
            "module": "QSL-ShopByBrand",
            "expires": "",
            "url": "",
            "name": "Catalog Management",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Каталог товаров"
                }
            ]
        },
        "21": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Blog",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Блог"
                }
            ]
        },
        "22": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Accounting",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Финансовый учёт"
                }
            ]
        },
        "23": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Xcart%2520Marketplace%2520Skinny%2520Banner.png",
            "module": "",
            "expires": "Sun, 12 May 2019 00:00:00 +0400",
            "url": "https://market.x-cart.com/addons/integration-nchannel.html?utm_source=marketplace&utm_medium=tag-banner",
            "name": "Orders",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Заказы"
                }
            ]
        },
        "24": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Security",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Безопасность"
                }
            ]
        },
        "26": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Miscellaneous",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Прочие"
                }
            ]
        },
        "27": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Products",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Товары"
                }
            ]
        },
        "29": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Migration",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Перенос данных"
                }
            ]
        },
        "30": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Fraud prevention",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Защита от мошенничества"
                }
            ]
        },
        "31": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Price modifiers",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Модификация цен"
                }
            ]
        },
        "32": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/xcart_thinbanner.png",
            "module": "",
            "expires": "Wed, 15 May 2019 00:00:00 +0400",
            "url": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&utm_medium=tag-banner",
            "name": "Sales Channels",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Каналы продаж"
                }
            ]
        },
        "33": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Templates",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Шаблоны дизайна"
                }
            ]
        },
        "34": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Multi-vendor friendly",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Для Мультивендор"
                }
            ]
        },
        "35": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "module": "XPay-Square",
            "expires": "",
            "url": "",
            "name": "Checkout",
            "category": "M",
            "tarnslations": []
        },
        "36": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Jewelry &amp; Accessories",
            "category": "T",
            "tarnslations": []
        },
        "37": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Pets",
            "category": "T",
            "tarnslations": []
        },
        "38": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Art &amp; Photography",
            "category": "T",
            "tarnslations": []
        },
        "39": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Health &amp; Beauty",
            "category": "T",
            "tarnslations": []
        },
        "40": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Books",
            "category": "T",
            "tarnslations": []
        },
        "41": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Business",
            "category": "T",
            "tarnslations": []
        },
        "42": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Cars",
            "category": "T",
            "tarnslations": []
        },
        "43": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "T-Shirts",
            "category": "T",
            "tarnslations": []
        },
        "44": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Computer",
            "category": "T",
            "tarnslations": []
        },
        "45": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Education",
            "category": "T",
            "tarnslations": []
        },
        "46": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Electronics",
            "category": "T",
            "tarnslations": []
        },
        "47": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Entertainment",
            "category": "T",
            "tarnslations": []
        },
        "48": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Popular",
            "category": "T",
            "tarnslations": []
        },
        "49": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Fitness",
            "category": "T",
            "tarnslations": []
        },
        "50": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Flowers",
            "category": "T",
            "tarnslations": []
        },
        "51": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Food &amp; Drink",
            "category": "T",
            "tarnslations": []
        },
        "52": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Gifts",
            "category": "T",
            "tarnslations": []
        },
        "53": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Crafts &amp; Hobbies",
            "category": "T",
            "tarnslations": []
        },
        "54": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Industrial",
            "category": "T",
            "tarnslations": []
        },
        "55": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Furniture &amp; Interior",
            "category": "T",
            "tarnslations": []
        },
        "56": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Media",
            "category": "T",
            "tarnslations": []
        },
        "57": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Medical",
            "category": "T",
            "tarnslations": []
        },
        "58": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Military",
            "category": "T",
            "tarnslations": []
        },
        "59": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Music",
            "category": "T",
            "tarnslations": []
        },
        "60": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Nutritional supplements",
            "category": "T",
            "tarnslations": []
        },
        "61": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Sports",
            "category": "T",
            "tarnslations": []
        },
        "62": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Supermarket",
            "category": "T",
            "tarnslations": []
        },
        "63": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Equipment &amp; Tools",
            "category": "T",
            "tarnslations": []
        },
        "64": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Watches",
            "category": "T",
            "tarnslations": []
        },
        "65": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Wedding",
            "category": "T",
            "tarnslations": []
        },
        "66": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Minimal",
            "category": "T",
            "tarnslations": []
        },
        "67": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Apparel &amp; Clothing",
            "category": "T",
            "tarnslations": []
        }
    }
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\Set"

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.INFO;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\Set
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "querySets": {
        "get_all_tags": {
            "lng": "all",
            "0": 0
        },
        "get_all_banners": [
            0
        ]
    }
}

Time: 2019-09-05 09:06:41;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 05:06:42 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "15193"
        ],
        "connection": [
            "close"
        ],
        "vary": [
            "Accept-Encoding"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"get_all_tags\":{\"1\":{\"tag_name\":\"Marketing\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Banner%2520Facebook%2520and%2520Instagram%2520Ads%2520on%2520tags.jpg\",\"tag_module_banner\":\"XC-FacebookMarketing\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041c\\u0430\\u0440\\u043a\\u0435\\u0442\\u0438\\u043d\\u0433\"}}},\"2\":{\"tag_name\":\"User experience\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0418\\u043d\\u0442\\u0435\\u0440\\u0444\\u0435\\u0439\\u0441 \\u043f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u044f\"}}},\"3\":{\"tag_name\":\"Design tweaks\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Advanced%2520Contact%2520us%2520form.jpg\",\"tag_module_banner\":\"QSL-AdvancedContactUs\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d\"}}},\"4\":{\"tag_name\":\"Administration\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0410\\u0434\\u043c\\u0438\\u043d\\u0438\\u0441\\u0442\\u0440\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435\"}}},\"5\":{\"tag_name\":\"Taxes\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/TaxJar-banner-tag.png\",\"tag_module_banner\":\"XC-TaxJar\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041d\\u0430\\u043b\\u043e\\u0433\\u0438\"}}},\"6\":{\"tag_name\":\"Fulfillment\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041e\\u0431\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430 \\u0437\\u0430\\u043a\\u0430\\u0437\\u043e\\u0432\"}}},\"7\":{\"tag_name\":\"Payment processing\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Square_tag_banner.jpg\",\"tag_module_banner\":\"XPay-Square\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041e\\u0431\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430 \\u043f\\u043b\\u0430\\u0442\\u0435\\u0436\\u0435\\u0439\"}}},\"8\":{\"tag_name\":\"Translation\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041f\\u0435\\u0440\\u0435\\u0432\\u043e\\u0434\"}}},\"9\":{\"tag_name\":\"Shipping\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/71lbs%2520banner-2-v2-web-ready%2520%25281%2529.jpg\",\"tag_module_banner\":\"AutomatedShippingRefunds71LBS-SeventyOnePounds\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430\"}}},\"11\":{\"tag_name\":\"Development\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0420\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430\"}}},\"12\":{\"tag_name\":\"Statistics\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0441\\u0442\\u0438\\u043a\\u0430\"}}},\"13\":{\"tag_name\":\"Mobile Commerce\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u043b\\u0430\\u0442\\u0444\\u043e\\u0440\\u043c\\u044b\"}}},\"15\":{\"tag_name\":\"SEO\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/SEO%2520Health%2520Check_1.jpg\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"https:\\/\\/www.x-cart.com\\/seo-consulting.html\",\"category\":\"M\",\"translations\":[]},\"16\":{\"tag_name\":\"Search and Navigation\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Cloud_Search.jpg\",\"tag_module_banner\":\"QSL-CloudSearch\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041f\\u043e\\u0438\\u0441\\u043a \\u0438 \\u043d\\u0430\\u0432\\u0438\\u0433\\u0430\\u0446\\u0438\\u044f\"}}},\"17\":{\"tag_name\":\"Social\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/livechat.jpg\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"https:\\/\\/market.x-cart.com\\/addons\\/livechat.html\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0421\\u043e\\u0446\\u0438\\u0430\\u043b\\u0438\\u0437\\u0430\\u0446\\u0438\\u044f\"}}},\"18\":{\"tag_name\":\"Catalog Management\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Shop_by_Brand.jpg\",\"tag_module_banner\":\"QSL-ShopByBrand\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433 \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432\"}}},\"21\":{\"tag_name\":\"Blog\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0411\\u043b\\u043e\\u0433\"}}},\"22\":{\"tag_name\":\"Accounting\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0424\\u0438\\u043d\\u0430\\u043d\\u0441\\u043e\\u0432\\u044b\\u0439 \\u0443\\u0447\\u0451\\u0442\"}}},\"23\":{\"tag_name\":\"Orders\",\"tag_banner_expiration_date\":\"Sun, 12 May 2019 00:00:00 +0400\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Xcart%2520Marketplace%2520Skinny%2520Banner.png\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"https:\\/\\/market.x-cart.com\\/addons\\/integration-nchannel.html?utm_source=marketplace&utm_medium=tag-banner\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0417\\u0430\\u043a\\u0430\\u0437\\u044b\"}}},\"24\":{\"tag_name\":\"Security\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0411\\u0435\\u0437\\u043e\\u043f\\u0430\\u0441\\u043d\\u043e\\u0441\\u0442\\u044c\"}}},\"26\":{\"tag_name\":\"Miscellaneous\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041f\\u0440\\u043e\\u0447\\u0438\\u0435\"}}},\"27\":{\"tag_name\":\"Products\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0422\\u043e\\u0432\\u0430\\u0440\\u044b\"}}},\"29\":{\"tag_name\":\"Migration\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041f\\u0435\\u0440\\u0435\\u043d\\u043e\\u0441 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445\"}}},\"30\":{\"tag_name\":\"Fraud prevention\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0417\\u0430\\u0449\\u0438\\u0442\\u0430 \\u043e\\u0442 \\u043c\\u043e\\u0448\\u0435\\u043d\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430\"}}},\"31\":{\"tag_name\":\"Price modifiers\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041c\\u043e\\u0434\\u0438\\u0444\\u0438\\u043a\\u0430\\u0446\\u0438\\u044f \\u0446\\u0435\\u043d\"}}},\"32\":{\"tag_name\":\"Sales Channels\",\"tag_banner_expiration_date\":\"Wed, 15 May 2019 00:00:00 +0400\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/xcart_thinbanner.png\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&utm_medium=tag-banner\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u041a\\u0430\\u043d\\u0430\\u043b\\u044b \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\"}}},\"33\":{\"tag_name\":\"Templates\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0428\\u0430\\u0431\\u043b\\u043e\\u043d\\u044b \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0430\"}}},\"34\":{\"tag_name\":\"Multi-vendor friendly\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":{\"ru\":{\"tag_name\":\"\\u0414\\u043b\\u044f \\u041c\\u0443\\u043b\\u044c\\u0442\\u0438\\u0432\\u0435\\u043d\\u0434\\u043e\\u0440\"}}},\"35\":{\"tag_name\":\"Checkout\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/tags\\/banners\\/Square_tag_banner.jpg\",\"tag_module_banner\":\"XPay-Square\",\"tag_banner_url\":\"\",\"category\":\"M\",\"translations\":[]},\"36\":{\"tag_name\":\"Jewelry &amp; Accessories\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"37\":{\"tag_name\":\"Pets\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"38\":{\"tag_name\":\"Art &amp; Photography\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"39\":{\"tag_name\":\"Health &amp; Beauty\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"40\":{\"tag_name\":\"Books\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"41\":{\"tag_name\":\"Business\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"42\":{\"tag_name\":\"Cars\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"43\":{\"tag_name\":\"T-Shirts\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"44\":{\"tag_name\":\"Computer\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"45\":{\"tag_name\":\"Education\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"46\":{\"tag_name\":\"Electronics\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"47\":{\"tag_name\":\"Entertainment\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"48\":{\"tag_name\":\"Popular\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"49\":{\"tag_name\":\"Fitness\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"50\":{\"tag_name\":\"Flowers\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"51\":{\"tag_name\":\"Food &amp; Drink\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"52\":{\"tag_name\":\"Gifts\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"53\":{\"tag_name\":\"Crafts &amp; Hobbies\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"54\":{\"tag_name\":\"Industrial\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"55\":{\"tag_name\":\"Furniture &amp; Interior\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"56\":{\"tag_name\":\"Media\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"57\":{\"tag_name\":\"Medical\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"58\":{\"tag_name\":\"Military\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"59\":{\"tag_name\":\"Music\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"60\":{\"tag_name\":\"Nutritional supplements\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"61\":{\"tag_name\":\"Sports\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"62\":{\"tag_name\":\"Supermarket\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"63\":{\"tag_name\":\"Equipment &amp; Tools\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"64\":{\"tag_name\":\"Watches\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"65\":{\"tag_name\":\"Wedding\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"66\":{\"tag_name\":\"Minimal\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]},\"67\":{\"tag_name\":\"Apparel &amp; Clothing\",\"tag_banner_expiration_date\":\"\",\"tag_banner_img\":\"\",\"tag_module_banner\":\"\",\"tag_banner_url\":\"\",\"category\":\"T\",\"translations\":[]}},\"get_all_banners\":[{\"banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/addons\\/banners2\\/xcart_banner_large_15.png\",\"banner_module\":null,\"banner_url\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=big-banner\",\"banner_section\":\"landing\"},{\"banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/addons\\/banners2\\/Banner%2520Facebook%2520and%2520Instagram%2520Ads.jpg\",\"banner_module\":\"XC-FacebookMarketing\",\"banner_url\":\"\",\"banner_section\":\"landing\"},{\"banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/addons\\/banners2\\/71lbs%2520Banner-1-v2-web-ready.jpg\",\"banner_module\":\"AutomatedShippingRefunds71LBS-SeventyOnePounds\",\"banner_url\":\"\",\"banner_section\":\"landing\"},{\"banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/addons\\/banners2\\/Square_payment_admin_area.jpg\",\"banner_module\":\"XPay-Square\",\"banner_url\":\"\",\"banner_section\":\"landing\"},{\"banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/addons\\/banners2\\/TaxJar.jpg\",\"banner_module\":\"XC-TaxJar\",\"banner_url\":\"\",\"banner_section\":\"landing\"},{\"banner_img\":\"http:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/addons\\/banners2\\/ANET%2520Vertical%2520Banner.png\",\"banner_module\":\"QSL-AuthorizenetAcceptjs\",\"banner_url\":\"\",\"banner_section\":\"payment\"}]}"
}

Time: 2019-09-05 09:06:42;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "get_all_tags": {
        "1": {
            "tag_name": "Marketing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Banner%2520Facebook%2520and%2520Instagram%2520Ads%2520on%2520tags.jpg",
            "tag_module_banner": "XC-FacebookMarketing",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Маркетинг"
                }
            }
        },
        "2": {
            "tag_name": "User experience",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Интерфейс покупателя"
                }
            }
        },
        "3": {
            "tag_name": "Design tweaks",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Advanced%2520Contact%2520us%2520form.jpg",
            "tag_module_banner": "QSL-AdvancedContactUs",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Дизайн"
                }
            }
        },
        "4": {
            "tag_name": "Administration",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Администрирование"
                }
            }
        },
        "5": {
            "tag_name": "Taxes",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/TaxJar-banner-tag.png",
            "tag_module_banner": "XC-TaxJar",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Налоги"
                }
            }
        },
        "6": {
            "tag_name": "Fulfillment",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Обработка заказов"
                }
            }
        },
        "7": {
            "tag_name": "Payment processing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "tag_module_banner": "XPay-Square",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Обработка платежей"
                }
            }
        },
        "8": {
            "tag_name": "Translation",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Перевод"
                }
            }
        },
        "9": {
            "tag_name": "Shipping",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/71lbs%2520banner-2-v2-web-ready%2520%25281%2529.jpg",
            "tag_module_banner": "AutomatedShippingRefunds71LBS-SeventyOnePounds",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Доставка"
                }
            }
        },
        "11": {
            "tag_name": "Development",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Разработка"
                }
            }
        },
        "12": {
            "tag_name": "Statistics",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Статистика"
                }
            }
        },
        "13": {
            "tag_name": "Mobile Commerce",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Мобильные платформы"
                }
            }
        },
        "15": {
            "tag_name": "SEO",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/SEO%2520Health%2520Check_1.jpg",
            "tag_module_banner": "",
            "tag_banner_url": "https://www.x-cart.com/seo-consulting.html",
            "category": "M",
            "translations": []
        },
        "16": {
            "tag_name": "Search and Navigation",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Cloud_Search.jpg",
            "tag_module_banner": "QSL-CloudSearch",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Поиск и навигация"
                }
            }
        },
        "17": {
            "tag_name": "Social",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/livechat.jpg",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/livechat.html",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Социализация"
                }
            }
        },
        "18": {
            "tag_name": "Catalog Management",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Shop_by_Brand.jpg",
            "tag_module_banner": "QSL-ShopByBrand",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Каталог товаров"
                }
            }
        },
        "21": {
            "tag_name": "Blog",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Блог"
                }
            }
        },
        "22": {
            "tag_name": "Accounting",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Финансовый учёт"
                }
            }
        },
        "23": {
            "tag_name": "Orders",
            "tag_banner_expiration_date": "Sun, 12 May 2019 00:00:00 +0400",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Xcart%2520Marketplace%2520Skinny%2520Banner.png",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/integration-nchannel.html?utm_source=marketplace&utm_medium=tag-banner",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Заказы"
                }
            }
        },
        "24": {
            "tag_name": "Security",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Безопасность"
                }
            }
        },
        "26": {
            "tag_name": "Miscellaneous",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Прочие"
                }
            }
        },
        "27": {
            "tag_name": "Products",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Товары"
                }
            }
        },
        "29": {
            "tag_name": "Migration",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Перенос данных"
                }
            }
        },
        "30": {
            "tag_name": "Fraud prevention",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Защита от мошенничества"
                }
            }
        },
        "31": {
            "tag_name": "Price modifiers",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Модификация цен"
                }
            }
        },
        "32": {
            "tag_name": "Sales Channels",
            "tag_banner_expiration_date": "Wed, 15 May 2019 00:00:00 +0400",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/xcart_thinbanner.png",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&utm_medium=tag-banner",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Каналы продаж"
                }
            }
        },
        "33": {
            "tag_name": "Templates",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Шаблоны дизайна"
                }
            }
        },
        "34": {
            "tag_name": "Multi-vendor friendly",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Для Мультивендор"
                }
            }
        },
        "35": {
            "tag_name": "Checkout",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "tag_module_banner": "XPay-Square",
            "tag_banner_url": "",
            "category": "M",
            "translations": []
        },
        "36": {
            "tag_name": "Jewelry &amp; Accessories",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "37": {
            "tag_name": "Pets",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "38": {
            "tag_name": "Art &amp; Photography",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "39": {
            "tag_name": "Health &amp; Beauty",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "40": {
            "tag_name": "Books",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "41": {
            "tag_name": "Business",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "42": {
            "tag_name": "Cars",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "43": {
            "tag_name": "T-Shirts",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "44": {
            "tag_name": "Computer",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "45": {
            "tag_name": "Education",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "46": {
            "tag_name": "Electronics",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "47": {
            "tag_name": "Entertainment",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "48": {
            "tag_name": "Popular",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "49": {
            "tag_name": "Fitness",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "50": {
            "tag_name": "Flowers",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "51": {
            "tag_name": "Food &amp; Drink",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "52": {
            "tag_name": "Gifts",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "53": {
            "tag_name": "Crafts &amp; Hobbies",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "54": {
            "tag_name": "Industrial",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "55": {
            "tag_name": "Furniture &amp; Interior",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "56": {
            "tag_name": "Media",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "57": {
            "tag_name": "Medical",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "58": {
            "tag_name": "Military",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "59": {
            "tag_name": "Music",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "60": {
            "tag_name": "Nutritional supplements",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "61": {
            "tag_name": "Sports",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "62": {
            "tag_name": "Supermarket",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "63": {
            "tag_name": "Equipment &amp; Tools",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "64": {
            "tag_name": "Watches",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "65": {
            "tag_name": "Wedding",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "66": {
            "tag_name": "Minimal",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "67": {
            "tag_name": "Apparel &amp; Clothing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        }
    },
    "get_all_banners": [
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/xcart_banner_large_15.png",
            "banner_module": null,
            "banner_url": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=big-banner",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/Banner%2520Facebook%2520and%2520Instagram%2520Ads.jpg",
            "banner_module": "XC-FacebookMarketing",
            "banner_url": "",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/71lbs%2520Banner-1-v2-web-ready.jpg",
            "banner_module": "AutomatedShippingRefunds71LBS-SeventyOnePounds",
            "banner_url": "",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/Square_payment_admin_area.jpg",
            "banner_module": "XPay-Square",
            "banner_url": "",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/TaxJar.jpg",
            "banner_module": "XC-TaxJar",
            "banner_url": "",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/ANET%2520Vertical%2520Banner.png",
            "banner_module": "QSL-AuthorizenetAcceptjs",
            "banner_url": "",
            "banner_section": "payment"
        }
    ]
}

Time: 2019-09-05 09:06:42;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "get_all_tags": {
        "1": {
            "tag_name": "Marketing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Banner%2520Facebook%2520and%2520Instagram%2520Ads%2520on%2520tags.jpg",
            "tag_module_banner": "XC-FacebookMarketing",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Маркетинг"
                }
            }
        },
        "2": {
            "tag_name": "User experience",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Интерфейс покупателя"
                }
            }
        },
        "3": {
            "tag_name": "Design tweaks",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Advanced%2520Contact%2520us%2520form.jpg",
            "tag_module_banner": "QSL-AdvancedContactUs",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Дизайн"
                }
            }
        },
        "4": {
            "tag_name": "Administration",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Администрирование"
                }
            }
        },
        "5": {
            "tag_name": "Taxes",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/TaxJar-banner-tag.png",
            "tag_module_banner": "XC-TaxJar",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Налоги"
                }
            }
        },
        "6": {
            "tag_name": "Fulfillment",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Обработка заказов"
                }
            }
        },
        "7": {
            "tag_name": "Payment processing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "tag_module_banner": "XPay-Square",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Обработка платежей"
                }
            }
        },
        "8": {
            "tag_name": "Translation",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Перевод"
                }
            }
        },
        "9": {
            "tag_name": "Shipping",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/71lbs%2520banner-2-v2-web-ready%2520%25281%2529.jpg",
            "tag_module_banner": "AutomatedShippingRefunds71LBS-SeventyOnePounds",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Доставка"
                }
            }
        },
        "11": {
            "tag_name": "Development",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Разработка"
                }
            }
        },
        "12": {
            "tag_name": "Statistics",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Статистика"
                }
            }
        },
        "13": {
            "tag_name": "Mobile Commerce",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Мобильные платформы"
                }
            }
        },
        "15": {
            "tag_name": "SEO",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/SEO%2520Health%2520Check_1.jpg",
            "tag_module_banner": "",
            "tag_banner_url": "https://www.x-cart.com/seo-consulting.html",
            "category": "M",
            "translations": []
        },
        "16": {
            "tag_name": "Search and Navigation",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Cloud_Search.jpg",
            "tag_module_banner": "QSL-CloudSearch",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Поиск и навигация"
                }
            }
        },
        "17": {
            "tag_name": "Social",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/livechat.jpg",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/livechat.html",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Социализация"
                }
            }
        },
        "18": {
            "tag_name": "Catalog Management",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Shop_by_Brand.jpg",
            "tag_module_banner": "QSL-ShopByBrand",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Каталог товаров"
                }
            }
        },
        "21": {
            "tag_name": "Blog",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Блог"
                }
            }
        },
        "22": {
            "tag_name": "Accounting",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Финансовый учёт"
                }
            }
        },
        "23": {
            "tag_name": "Orders",
            "tag_banner_expiration_date": "Sun, 12 May 2019 00:00:00 +0400",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Xcart%2520Marketplace%2520Skinny%2520Banner.png",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/integration-nchannel.html?utm_source=marketplace&utm_medium=tag-banner",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Заказы"
                }
            }
        },
        "24": {
            "tag_name": "Security",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Безопасность"
                }
            }
        },
        "26": {
            "tag_name": "Miscellaneous",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Прочие"
                }
            }
        },
        "27": {
            "tag_name": "Products",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Товары"
                }
            }
        },
        "29": {
            "tag_name": "Migration",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Перенос данных"
                }
            }
        },
        "30": {
            "tag_name": "Fraud prevention",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Защита от мошенничества"
                }
            }
        },
        "31": {
            "tag_name": "Price modifiers",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Модификация цен"
                }
            }
        },
        "32": {
            "tag_name": "Sales Channels",
            "tag_banner_expiration_date": "Wed, 15 May 2019 00:00:00 +0400",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/xcart_thinbanner.png",
            "tag_module_banner": "",
            "tag_banner_url": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&utm_medium=tag-banner",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Каналы продаж"
                }
            }
        },
        "33": {
            "tag_name": "Templates",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Шаблоны дизайна"
                }
            }
        },
        "34": {
            "tag_name": "Multi-vendor friendly",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "M",
            "translations": {
                "ru": {
                    "tag_name": "Для Мультивендор"
                }
            }
        },
        "35": {
            "tag_name": "Checkout",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "tag_module_banner": "XPay-Square",
            "tag_banner_url": "",
            "category": "M",
            "translations": []
        },
        "36": {
            "tag_name": "Jewelry &amp; Accessories",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "37": {
            "tag_name": "Pets",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "38": {
            "tag_name": "Art &amp; Photography",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "39": {
            "tag_name": "Health &amp; Beauty",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "40": {
            "tag_name": "Books",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "41": {
            "tag_name": "Business",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "42": {
            "tag_name": "Cars",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "43": {
            "tag_name": "T-Shirts",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "44": {
            "tag_name": "Computer",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "45": {
            "tag_name": "Education",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "46": {
            "tag_name": "Electronics",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "47": {
            "tag_name": "Entertainment",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "48": {
            "tag_name": "Popular",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "49": {
            "tag_name": "Fitness",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "50": {
            "tag_name": "Flowers",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "51": {
            "tag_name": "Food &amp; Drink",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "52": {
            "tag_name": "Gifts",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "53": {
            "tag_name": "Crafts &amp; Hobbies",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "54": {
            "tag_name": "Industrial",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "55": {
            "tag_name": "Furniture &amp; Interior",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "56": {
            "tag_name": "Media",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "57": {
            "tag_name": "Medical",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "58": {
            "tag_name": "Military",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "59": {
            "tag_name": "Music",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "60": {
            "tag_name": "Nutritional supplements",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "61": {
            "tag_name": "Sports",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "62": {
            "tag_name": "Supermarket",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "63": {
            "tag_name": "Equipment &amp; Tools",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "64": {
            "tag_name": "Watches",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "65": {
            "tag_name": "Wedding",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "66": {
            "tag_name": "Minimal",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        },
        "67": {
            "tag_name": "Apparel &amp; Clothing",
            "tag_banner_expiration_date": "",
            "tag_banner_img": "",
            "tag_module_banner": "",
            "tag_banner_url": "",
            "category": "T",
            "translations": []
        }
    },
    "get_all_banners": [
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/xcart_banner_large_15.png",
            "banner_module": null,
            "banner_url": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=big-banner",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/Banner%2520Facebook%2520and%2520Instagram%2520Ads.jpg",
            "banner_module": "XC-FacebookMarketing",
            "banner_url": "",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/71lbs%2520Banner-1-v2-web-ready.jpg",
            "banner_module": "AutomatedShippingRefunds71LBS-SeventyOnePounds",
            "banner_url": "",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/Square_payment_admin_area.jpg",
            "banner_module": "XPay-Square",
            "banner_url": "",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/TaxJar.jpg",
            "banner_module": "XC-TaxJar",
            "banner_url": "",
            "banner_section": "landing"
        },
        {
            "banner_img": "http://my.x-cart.com/sites/default/files/addons/banners2/ANET%2520Vertical%2520Banner.png",
            "banner_module": "QSL-AuthorizenetAcceptjs",
            "banner_url": "",
            "banner_section": "payment"
        }
    ]
}

Time: 2019-09-05 09:06:42;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace correctly received "XCart\Marketplace\Request\Set"

Time: 2019-09-05 09:06:42;
Channel: service-marketplace.INFO;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (formatted)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "XCart\\Marketplace\\Request\\Tags": {
        "1": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Banner%2520Facebook%2520and%2520Instagram%2520Ads%2520on%2520tags.jpg",
            "module": "XC-FacebookMarketing",
            "expires": "",
            "url": "",
            "name": "Marketing",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Маркетинг"
                }
            ]
        },
        "2": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "User experience",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Интерфейс покупателя"
                }
            ]
        },
        "3": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Advanced%2520Contact%2520us%2520form.jpg",
            "module": "QSL-AdvancedContactUs",
            "expires": "",
            "url": "",
            "name": "Design tweaks",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Дизайн"
                }
            ]
        },
        "4": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Administration",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Администрирование"
                }
            ]
        },
        "5": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/TaxJar-banner-tag.png",
            "module": "XC-TaxJar",
            "expires": "",
            "url": "",
            "name": "Taxes",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Налоги"
                }
            ]
        },
        "6": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Fulfillment",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Обработка заказов"
                }
            ]
        },
        "7": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "module": "XPay-Square",
            "expires": "",
            "url": "",
            "name": "Payment processing",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Обработка платежей"
                }
            ]
        },
        "8": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Translation",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Перевод"
                }
            ]
        },
        "9": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/71lbs%2520banner-2-v2-web-ready%2520%25281%2529.jpg",
            "module": "AutomatedShippingRefunds71LBS-SeventyOnePounds",
            "expires": "",
            "url": "",
            "name": "Shipping",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Доставка"
                }
            ]
        },
        "11": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Development",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Разработка"
                }
            ]
        },
        "12": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Statistics",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Статистика"
                }
            ]
        },
        "13": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Mobile Commerce",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Мобильные платформы"
                }
            ]
        },
        "15": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/SEO%2520Health%2520Check_1.jpg",
            "module": "",
            "expires": "",
            "url": "https://www.x-cart.com/seo-consulting.html",
            "name": "SEO",
            "category": "M",
            "tarnslations": []
        },
        "16": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Cloud_Search.jpg",
            "module": "QSL-CloudSearch",
            "expires": "",
            "url": "",
            "name": "Search and Navigation",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Поиск и навигация"
                }
            ]
        },
        "17": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/livechat.jpg",
            "module": "",
            "expires": "",
            "url": "https://market.x-cart.com/addons/livechat.html",
            "name": "Social",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Социализация"
                }
            ]
        },
        "18": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Shop_by_Brand.jpg",
            "module": "QSL-ShopByBrand",
            "expires": "",
            "url": "",
            "name": "Catalog Management",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Каталог товаров"
                }
            ]
        },
        "21": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Blog",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Блог"
                }
            ]
        },
        "22": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Accounting",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Финансовый учёт"
                }
            ]
        },
        "23": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Xcart%2520Marketplace%2520Skinny%2520Banner.png",
            "module": "",
            "expires": "Sun, 12 May 2019 00:00:00 +0400",
            "url": "https://market.x-cart.com/addons/integration-nchannel.html?utm_source=marketplace&utm_medium=tag-banner",
            "name": "Orders",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Заказы"
                }
            ]
        },
        "24": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Security",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Безопасность"
                }
            ]
        },
        "26": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Miscellaneous",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Прочие"
                }
            ]
        },
        "27": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Products",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Товары"
                }
            ]
        },
        "29": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Migration",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Перенос данных"
                }
            ]
        },
        "30": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Fraud prevention",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Защита от мошенничества"
                }
            ]
        },
        "31": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Price modifiers",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Модификация цен"
                }
            ]
        },
        "32": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/xcart_thinbanner.png",
            "module": "",
            "expires": "Wed, 15 May 2019 00:00:00 +0400",
            "url": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&utm_medium=tag-banner",
            "name": "Sales Channels",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Каналы продаж"
                }
            ]
        },
        "33": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Templates",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Шаблоны дизайна"
                }
            ]
        },
        "34": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Multi-vendor friendly",
            "category": "M",
            "tarnslations": [
                {
                    "code": "ru",
                    "tag_name": "Для Мультивендор"
                }
            ]
        },
        "35": {
            "image": "http://my.x-cart.com/sites/default/files/tags/banners/Square_tag_banner.jpg",
            "module": "XPay-Square",
            "expires": "",
            "url": "",
            "name": "Checkout",
            "category": "M",
            "tarnslations": []
        },
        "36": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Jewelry &amp; Accessories",
            "category": "T",
            "tarnslations": []
        },
        "37": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Pets",
            "category": "T",
            "tarnslations": []
        },
        "38": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Art &amp; Photography",
            "category": "T",
            "tarnslations": []
        },
        "39": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Health &amp; Beauty",
            "category": "T",
            "tarnslations": []
        },
        "40": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Books",
            "category": "T",
            "tarnslations": []
        },
        "41": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Business",
            "category": "T",
            "tarnslations": []
        },
        "42": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Cars",
            "category": "T",
            "tarnslations": []
        },
        "43": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "T-Shirts",
            "category": "T",
            "tarnslations": []
        },
        "44": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Computer",
            "category": "T",
            "tarnslations": []
        },
        "45": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Education",
            "category": "T",
            "tarnslations": []
        },
        "46": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Electronics",
            "category": "T",
            "tarnslations": []
        },
        "47": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Entertainment",
            "category": "T",
            "tarnslations": []
        },
        "48": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Popular",
            "category": "T",
            "tarnslations": []
        },
        "49": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Fitness",
            "category": "T",
            "tarnslations": []
        },
        "50": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Flowers",
            "category": "T",
            "tarnslations": []
        },
        "51": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Food &amp; Drink",
            "category": "T",
            "tarnslations": []
        },
        "52": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Gifts",
            "category": "T",
            "tarnslations": []
        },
        "53": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Crafts &amp; Hobbies",
            "category": "T",
            "tarnslations": []
        },
        "54": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Industrial",
            "category": "T",
            "tarnslations": []
        },
        "55": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Furniture &amp; Interior",
            "category": "T",
            "tarnslations": []
        },
        "56": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Media",
            "category": "T",
            "tarnslations": []
        },
        "57": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Medical",
            "category": "T",
            "tarnslations": []
        },
        "58": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Military",
            "category": "T",
            "tarnslations": []
        },
        "59": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Music",
            "category": "T",
            "tarnslations": []
        },
        "60": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Nutritional supplements",
            "category": "T",
            "tarnslations": []
        },
        "61": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Sports",
            "category": "T",
            "tarnslations": []
        },
        "62": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Supermarket",
            "category": "T",
            "tarnslations": []
        },
        "63": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Equipment &amp; Tools",
            "category": "T",
            "tarnslations": []
        },
        "64": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Watches",
            "category": "T",
            "tarnslations": []
        },
        "65": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Wedding",
            "category": "T",
            "tarnslations": []
        },
        "66": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Minimal",
            "category": "T",
            "tarnslations": []
        },
        "67": {
            "image": "",
            "module": "",
            "expires": "",
            "url": "",
            "name": "Apparel &amp; Clothing",
            "category": "T",
            "tarnslations": []
        }
    },
    "XCart\\Marketplace\\Request\\Banners": [
        {
            "image": "http://my.x-cart.com/sites/default/files/addons/banners2/xcart_banner_large_15.png",
            "module": null,
            "url": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&utm_medium=big-banner",
            "section": "landing"
        },
        {
            "image": "http://my.x-cart.com/sites/default/files/addons/banners2/Banner%2520Facebook%2520and%2520Instagram%2520Ads.jpg",
            "module": "XC-FacebookMarketing",
            "url": "",
            "section": "landing"
        },
        {
            "image": "http://my.x-cart.com/sites/default/files/addons/banners2/71lbs%2520Banner-1-v2-web-ready.jpg",
            "module": "AutomatedShippingRefunds71LBS-SeventyOnePounds",
            "url": "",
            "section": "landing"
        },
        {
            "image": "http://my.x-cart.com/sites/default/files/addons/banners2/Square_payment_admin_area.jpg",
            "module": "XPay-Square",
            "url": "",
            "section": "landing"
        },
        {
            "image": "http://my.x-cart.com/sites/default/files/addons/banners2/TaxJar.jpg",
            "module": "XC-TaxJar",
            "url": "",
            "section": "landing"
        },
        {
            "image": "http://my.x-cart.com/sites/default/files/addons/banners2/ANET%2520Vertical%2520Banner.png",
            "module": "QSL-AuthorizenetAcceptjs",
            "url": "",
            "section": "payment"
        }
    ]
}

Time: 2019-09-05 09:06:42;
Channel: service-marketplace.DEBUG;
Runtime id: 83f249c76bef5042267186f3c2ce5370;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 09:06:43;
Channel: service-marketplace.INFO;
Runtime id: b05de714ae61945c6cdbc56c7562e165;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 09:06:43;
Channel: service-marketplace.DEBUG;
Runtime id: b05de714ae61945c6cdbc56c7562e165;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 05:06:43 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 09:06:44;
Channel: service-marketplace.DEBUG;
Runtime id: b05de714ae61945c6cdbc56c7562e165;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 09:06:44;
Channel: service-marketplace.DEBUG;
Runtime id: b05de714ae61945c6cdbc56c7562e165;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 09:06:44;
Channel: service-marketplace.DEBUG;
Runtime id: b05de714ae61945c6cdbc56c7562e165;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 09:06:44;
Channel: service-marketplace.WARNING;
Runtime id: b05de714ae61945c6cdbc56c7562e165;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 09:06:44;
Channel: service-marketplace.EMERGENCY;
Runtime id: b05de714ae61945c6cdbc56c7562e165;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\Set"

Time: 2019-09-05 13:45:57;
Channel: service-marketplace.INFO;
Runtime id: 82ad89fdb0fa6ea865047627435b8dd0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\Set
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "querySets": {
        "get_XC5_notifications": [
            0
        ]
    }
}

Time: 2019-09-05 13:45:57;
Channel: service-marketplace.DEBUG;
Runtime id: 82ad89fdb0fa6ea865047627435b8dd0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 09:45:58 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "799"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"get_XC5_notifications\":[{\"type\":\"module\",\"module\":\"XC-FacebookMarketing\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/facebookinsta.png\",\"title\":\"Advertise your products on Facebook and Instagram and see your sales grow\",\"description\":\"Try the free Facebook Ads and Instagram Ads addon for X-Cart\",\"link\":\"\",\"date\":\"1558040400\"},{\"type\":\"news\",\"module\":\"\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/GA1.png\",\"title\":\"FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.\",\"description\":\"\",\"link\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup\",\"date\":\"1557262800\"}]}"
}

Time: 2019-09-05 13:45:58;
Channel: service-marketplace.DEBUG;
Runtime id: 82ad89fdb0fa6ea865047627435b8dd0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 13:45:58;
Channel: service-marketplace.DEBUG;
Runtime id: 82ad89fdb0fa6ea865047627435b8dd0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 13:45:58;
Channel: service-marketplace.DEBUG;
Runtime id: 82ad89fdb0fa6ea865047627435b8dd0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace correctly received "XCart\Marketplace\Request\Set"

Time: 2019-09-05 13:45:58;
Channel: service-marketplace.INFO;
Runtime id: 82ad89fdb0fa6ea865047627435b8dd0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (formatted)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "XCart\\Marketplace\\Request\\Notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 13:45:58;
Channel: service-marketplace.DEBUG;
Runtime id: 82ad89fdb0fa6ea865047627435b8dd0;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 13:58:56;
Channel: service-marketplace.INFO;
Runtime id: 38f11546864226ab1ad9f87ae8544b06;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 13:58:56;
Channel: service-marketplace.DEBUG;
Runtime id: 38f11546864226ab1ad9f87ae8544b06;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 09:58:56 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 13:58:57;
Channel: service-marketplace.DEBUG;
Runtime id: 38f11546864226ab1ad9f87ae8544b06;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 13:58:57;
Channel: service-marketplace.DEBUG;
Runtime id: 38f11546864226ab1ad9f87ae8544b06;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 13:58:57;
Channel: service-marketplace.DEBUG;
Runtime id: 38f11546864226ab1ad9f87ae8544b06;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 13:58:57;
Channel: service-marketplace.WARNING;
Runtime id: 38f11546864226ab1ad9f87ae8544b06;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 13:58:57;
Channel: service-marketplace.EMERGENCY;
Runtime id: 38f11546864226ab1ad9f87ae8544b06;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 13:58:57;
Channel: service-marketplace.INFO;
Runtime id: 12554493571e14032c10d05ca8fa62de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 13:58:57;
Channel: service-marketplace.DEBUG;
Runtime id: 12554493571e14032c10d05ca8fa62de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 09:58:58 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 13:58:58;
Channel: service-marketplace.DEBUG;
Runtime id: 12554493571e14032c10d05ca8fa62de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 13:58:58;
Channel: service-marketplace.DEBUG;
Runtime id: 12554493571e14032c10d05ca8fa62de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 13:58:58;
Channel: service-marketplace.DEBUG;
Runtime id: 12554493571e14032c10d05ca8fa62de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 13:58:58;
Channel: service-marketplace.WARNING;
Runtime id: 12554493571e14032c10d05ca8fa62de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 13:58:58;
Channel: service-marketplace.EMERGENCY;
Runtime id: 12554493571e14032c10d05ca8fa62de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:02:39;
Channel: service-marketplace.INFO;
Runtime id: bc5ee592cc23ebf46faa65c7a8887837;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:02:39;
Channel: service-marketplace.DEBUG;
Runtime id: bc5ee592cc23ebf46faa65c7a8887837;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:02:39 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:02:39;
Channel: service-marketplace.DEBUG;
Runtime id: bc5ee592cc23ebf46faa65c7a8887837;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:02:39;
Channel: service-marketplace.DEBUG;
Runtime id: bc5ee592cc23ebf46faa65c7a8887837;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:02:39;
Channel: service-marketplace.DEBUG;
Runtime id: bc5ee592cc23ebf46faa65c7a8887837;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:02:39;
Channel: service-marketplace.WARNING;
Runtime id: bc5ee592cc23ebf46faa65c7a8887837;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:02:39;
Channel: service-marketplace.EMERGENCY;
Runtime id: bc5ee592cc23ebf46faa65c7a8887837;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:03:06;
Channel: service-marketplace.INFO;
Runtime id: a65814dbf350ba04aaae24eacc318c18;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:03:06;
Channel: service-marketplace.DEBUG;
Runtime id: a65814dbf350ba04aaae24eacc318c18;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:03:07 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:03:07;
Channel: service-marketplace.DEBUG;
Runtime id: a65814dbf350ba04aaae24eacc318c18;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:03:07;
Channel: service-marketplace.DEBUG;
Runtime id: a65814dbf350ba04aaae24eacc318c18;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:03:07;
Channel: service-marketplace.DEBUG;
Runtime id: a65814dbf350ba04aaae24eacc318c18;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:03:07;
Channel: service-marketplace.WARNING;
Runtime id: a65814dbf350ba04aaae24eacc318c18;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:03:07;
Channel: service-marketplace.EMERGENCY;
Runtime id: a65814dbf350ba04aaae24eacc318c18;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:28:21;
Channel: service-marketplace.INFO;
Runtime id: 59927337b476e80eac9dcf42b1fdb551;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:28:21;
Channel: service-marketplace.DEBUG;
Runtime id: 59927337b476e80eac9dcf42b1fdb551;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:28:21 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:28:21;
Channel: service-marketplace.DEBUG;
Runtime id: 59927337b476e80eac9dcf42b1fdb551;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:28:21;
Channel: service-marketplace.DEBUG;
Runtime id: 59927337b476e80eac9dcf42b1fdb551;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:28:21;
Channel: service-marketplace.DEBUG;
Runtime id: 59927337b476e80eac9dcf42b1fdb551;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:28:21;
Channel: service-marketplace.WARNING;
Runtime id: 59927337b476e80eac9dcf42b1fdb551;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:28:21;
Channel: service-marketplace.EMERGENCY;
Runtime id: 59927337b476e80eac9dcf42b1fdb551;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:28:52;
Channel: service-marketplace.INFO;
Runtime id: d832dad09bed7fc829e19344cafcfd56;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:28:52;
Channel: service-marketplace.DEBUG;
Runtime id: d832dad09bed7fc829e19344cafcfd56;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:28:52 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:28:52;
Channel: service-marketplace.DEBUG;
Runtime id: d832dad09bed7fc829e19344cafcfd56;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:28:52;
Channel: service-marketplace.DEBUG;
Runtime id: d832dad09bed7fc829e19344cafcfd56;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:28:52;
Channel: service-marketplace.DEBUG;
Runtime id: d832dad09bed7fc829e19344cafcfd56;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:28:52;
Channel: service-marketplace.WARNING;
Runtime id: d832dad09bed7fc829e19344cafcfd56;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:28:52;
Channel: service-marketplace.EMERGENCY;
Runtime id: d832dad09bed7fc829e19344cafcfd56;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:31:24;
Channel: service-marketplace.INFO;
Runtime id: 461732c3041155b4beb6dd7c5f06d2e6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:31:24;
Channel: service-marketplace.DEBUG;
Runtime id: 461732c3041155b4beb6dd7c5f06d2e6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:31:24 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:31:24;
Channel: service-marketplace.DEBUG;
Runtime id: 461732c3041155b4beb6dd7c5f06d2e6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:31:24;
Channel: service-marketplace.DEBUG;
Runtime id: 461732c3041155b4beb6dd7c5f06d2e6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:31:24;
Channel: service-marketplace.DEBUG;
Runtime id: 461732c3041155b4beb6dd7c5f06d2e6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:31:24;
Channel: service-marketplace.WARNING;
Runtime id: 461732c3041155b4beb6dd7c5f06d2e6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:31:24;
Channel: service-marketplace.EMERGENCY;
Runtime id: 461732c3041155b4beb6dd7c5f06d2e6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:31:54;
Channel: service-marketplace.INFO;
Runtime id: 1e5f2d0a60a679add428ffce86531a29;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:31:54;
Channel: service-marketplace.DEBUG;
Runtime id: 1e5f2d0a60a679add428ffce86531a29;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:31:55 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:31:55;
Channel: service-marketplace.DEBUG;
Runtime id: 1e5f2d0a60a679add428ffce86531a29;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:31:55;
Channel: service-marketplace.DEBUG;
Runtime id: 1e5f2d0a60a679add428ffce86531a29;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:31:55;
Channel: service-marketplace.DEBUG;
Runtime id: 1e5f2d0a60a679add428ffce86531a29;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:31:55;
Channel: service-marketplace.WARNING;
Runtime id: 1e5f2d0a60a679add428ffce86531a29;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:31:55;
Channel: service-marketplace.EMERGENCY;
Runtime id: 1e5f2d0a60a679add428ffce86531a29;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:36:20;
Channel: service-marketplace.INFO;
Runtime id: 448396881178eae83fe477e31091e70f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:36:20;
Channel: service-marketplace.DEBUG;
Runtime id: 448396881178eae83fe477e31091e70f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:36:20 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:36:20;
Channel: service-marketplace.DEBUG;
Runtime id: 448396881178eae83fe477e31091e70f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:36:20;
Channel: service-marketplace.DEBUG;
Runtime id: 448396881178eae83fe477e31091e70f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:36:20;
Channel: service-marketplace.DEBUG;
Runtime id: 448396881178eae83fe477e31091e70f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:36:20;
Channel: service-marketplace.WARNING;
Runtime id: 448396881178eae83fe477e31091e70f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:36:20;
Channel: service-marketplace.EMERGENCY;
Runtime id: 448396881178eae83fe477e31091e70f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:36:55;
Channel: service-marketplace.INFO;
Runtime id: 2f7c7b3cc558f08a8f3bbdefd21692f5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:36:55;
Channel: service-marketplace.DEBUG;
Runtime id: 2f7c7b3cc558f08a8f3bbdefd21692f5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:36:55 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:36:55;
Channel: service-marketplace.DEBUG;
Runtime id: 2f7c7b3cc558f08a8f3bbdefd21692f5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:36:55;
Channel: service-marketplace.DEBUG;
Runtime id: 2f7c7b3cc558f08a8f3bbdefd21692f5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:36:55;
Channel: service-marketplace.DEBUG;
Runtime id: 2f7c7b3cc558f08a8f3bbdefd21692f5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:36:55;
Channel: service-marketplace.WARNING;
Runtime id: 2f7c7b3cc558f08a8f3bbdefd21692f5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:36:55;
Channel: service-marketplace.EMERGENCY;
Runtime id: 2f7c7b3cc558f08a8f3bbdefd21692f5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:38:53;
Channel: service-marketplace.INFO;
Runtime id: a4f817b3615aa7e085ee07e256296cbe;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:38:53;
Channel: service-marketplace.DEBUG;
Runtime id: a4f817b3615aa7e085ee07e256296cbe;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:38:54 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:38:54;
Channel: service-marketplace.DEBUG;
Runtime id: a4f817b3615aa7e085ee07e256296cbe;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:38:54;
Channel: service-marketplace.DEBUG;
Runtime id: a4f817b3615aa7e085ee07e256296cbe;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:38:54;
Channel: service-marketplace.DEBUG;
Runtime id: a4f817b3615aa7e085ee07e256296cbe;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:38:54;
Channel: service-marketplace.WARNING;
Runtime id: a4f817b3615aa7e085ee07e256296cbe;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:38:54;
Channel: service-marketplace.EMERGENCY;
Runtime id: a4f817b3615aa7e085ee07e256296cbe;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:39:36;
Channel: service-marketplace.INFO;
Runtime id: fbd195cd5e9c5d9ebcbc025c0f1c6e7e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:39:36;
Channel: service-marketplace.DEBUG;
Runtime id: fbd195cd5e9c5d9ebcbc025c0f1c6e7e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:39:36 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:39:36;
Channel: service-marketplace.DEBUG;
Runtime id: fbd195cd5e9c5d9ebcbc025c0f1c6e7e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:39:36;
Channel: service-marketplace.DEBUG;
Runtime id: fbd195cd5e9c5d9ebcbc025c0f1c6e7e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:39:36;
Channel: service-marketplace.DEBUG;
Runtime id: fbd195cd5e9c5d9ebcbc025c0f1c6e7e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:39:36;
Channel: service-marketplace.WARNING;
Runtime id: fbd195cd5e9c5d9ebcbc025c0f1c6e7e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:39:36;
Channel: service-marketplace.EMERGENCY;
Runtime id: fbd195cd5e9c5d9ebcbc025c0f1c6e7e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:42:57;
Channel: service-marketplace.INFO;
Runtime id: c2f811be24a1223fcf16f13dc03c7bd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:42:57;
Channel: service-marketplace.DEBUG;
Runtime id: c2f811be24a1223fcf16f13dc03c7bd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:42:58 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:42:58;
Channel: service-marketplace.DEBUG;
Runtime id: c2f811be24a1223fcf16f13dc03c7bd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:42:58;
Channel: service-marketplace.DEBUG;
Runtime id: c2f811be24a1223fcf16f13dc03c7bd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:42:58;
Channel: service-marketplace.DEBUG;
Runtime id: c2f811be24a1223fcf16f13dc03c7bd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:42:58;
Channel: service-marketplace.WARNING;
Runtime id: c2f811be24a1223fcf16f13dc03c7bd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:42:58;
Channel: service-marketplace.EMERGENCY;
Runtime id: c2f811be24a1223fcf16f13dc03c7bd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:43:23;
Channel: service-marketplace.INFO;
Runtime id: 12f33f91b48fefe0798e13c073aa4aee;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:43:23;
Channel: service-marketplace.DEBUG;
Runtime id: 12f33f91b48fefe0798e13c073aa4aee;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:43:24 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:43:24;
Channel: service-marketplace.DEBUG;
Runtime id: 12f33f91b48fefe0798e13c073aa4aee;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:43:24;
Channel: service-marketplace.DEBUG;
Runtime id: 12f33f91b48fefe0798e13c073aa4aee;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:43:24;
Channel: service-marketplace.DEBUG;
Runtime id: 12f33f91b48fefe0798e13c073aa4aee;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:43:24;
Channel: service-marketplace.WARNING;
Runtime id: 12f33f91b48fefe0798e13c073aa4aee;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:43:24;
Channel: service-marketplace.EMERGENCY;
Runtime id: 12f33f91b48fefe0798e13c073aa4aee;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:44:55;
Channel: service-marketplace.INFO;
Runtime id: f95ad33c1441936c6d2915355de7ecfd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:44:55;
Channel: service-marketplace.DEBUG;
Runtime id: f95ad33c1441936c6d2915355de7ecfd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:44:55 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:44:55;
Channel: service-marketplace.DEBUG;
Runtime id: f95ad33c1441936c6d2915355de7ecfd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:44:55;
Channel: service-marketplace.DEBUG;
Runtime id: f95ad33c1441936c6d2915355de7ecfd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:44:55;
Channel: service-marketplace.DEBUG;
Runtime id: f95ad33c1441936c6d2915355de7ecfd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:44:55;
Channel: service-marketplace.WARNING;
Runtime id: f95ad33c1441936c6d2915355de7ecfd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:44:55;
Channel: service-marketplace.EMERGENCY;
Runtime id: f95ad33c1441936c6d2915355de7ecfd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:45:26;
Channel: service-marketplace.INFO;
Runtime id: 81c5d117819a541b72b917b7c3c90fdb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:45:26;
Channel: service-marketplace.DEBUG;
Runtime id: 81c5d117819a541b72b917b7c3c90fdb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:45:27 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:45:27;
Channel: service-marketplace.DEBUG;
Runtime id: 81c5d117819a541b72b917b7c3c90fdb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:45:27;
Channel: service-marketplace.DEBUG;
Runtime id: 81c5d117819a541b72b917b7c3c90fdb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:45:27;
Channel: service-marketplace.DEBUG;
Runtime id: 81c5d117819a541b72b917b7c3c90fdb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:45:27;
Channel: service-marketplace.WARNING;
Runtime id: 81c5d117819a541b72b917b7c3c90fdb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:45:27;
Channel: service-marketplace.EMERGENCY;
Runtime id: 81c5d117819a541b72b917b7c3c90fdb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:47:15;
Channel: service-marketplace.INFO;
Runtime id: dbe4de37644d77c46034548dae49706b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:47:15;
Channel: service-marketplace.DEBUG;
Runtime id: dbe4de37644d77c46034548dae49706b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:47:16 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:47:16;
Channel: service-marketplace.DEBUG;
Runtime id: dbe4de37644d77c46034548dae49706b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:47:16;
Channel: service-marketplace.DEBUG;
Runtime id: dbe4de37644d77c46034548dae49706b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:47:16;
Channel: service-marketplace.DEBUG;
Runtime id: dbe4de37644d77c46034548dae49706b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:47:16;
Channel: service-marketplace.WARNING;
Runtime id: dbe4de37644d77c46034548dae49706b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:47:16;
Channel: service-marketplace.EMERGENCY;
Runtime id: dbe4de37644d77c46034548dae49706b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:47:40;
Channel: service-marketplace.INFO;
Runtime id: f14f5712ce451a0ece62bfcfaff8fb01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:47:40;
Channel: service-marketplace.DEBUG;
Runtime id: f14f5712ce451a0ece62bfcfaff8fb01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:47:40 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 14:47:40;
Channel: service-marketplace.DEBUG;
Runtime id: f14f5712ce451a0ece62bfcfaff8fb01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:47:40;
Channel: service-marketplace.DEBUG;
Runtime id: f14f5712ce451a0ece62bfcfaff8fb01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:47:40;
Channel: service-marketplace.DEBUG;
Runtime id: f14f5712ce451a0ece62bfcfaff8fb01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 14:47:40;
Channel: service-marketplace.WARNING;
Runtime id: f14f5712ce451a0ece62bfcfaff8fb01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 14:47:40;
Channel: service-marketplace.EMERGENCY;
Runtime id: f14f5712ce451a0ece62bfcfaff8fb01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\Set"

Time: 2019-09-05 14:47:40;
Channel: service-marketplace.INFO;
Runtime id: 5c81dac4e51144f7957b887a812d01c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\Set
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "querySets": {
        "get_XC5_notifications": [
            0
        ]
    }
}

Time: 2019-09-05 14:47:40;
Channel: service-marketplace.DEBUG;
Runtime id: 5c81dac4e51144f7957b887a812d01c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 10:47:41 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "799"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"get_XC5_notifications\":[{\"type\":\"module\",\"module\":\"XC-FacebookMarketing\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/facebookinsta.png\",\"title\":\"Advertise your products on Facebook and Instagram and see your sales grow\",\"description\":\"Try the free Facebook Ads and Instagram Ads addon for X-Cart\",\"link\":\"\",\"date\":\"1558040400\"},{\"type\":\"news\",\"module\":\"\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/GA1.png\",\"title\":\"FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.\",\"description\":\"\",\"link\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup\",\"date\":\"1557262800\"}]}"
}

Time: 2019-09-05 14:47:41;
Channel: service-marketplace.DEBUG;
Runtime id: 5c81dac4e51144f7957b887a812d01c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 14:47:41;
Channel: service-marketplace.DEBUG;
Runtime id: 5c81dac4e51144f7957b887a812d01c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 14:47:41;
Channel: service-marketplace.DEBUG;
Runtime id: 5c81dac4e51144f7957b887a812d01c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace correctly received "XCart\Marketplace\Request\Set"

Time: 2019-09-05 14:47:41;
Channel: service-marketplace.INFO;
Runtime id: 5c81dac4e51144f7957b887a812d01c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (formatted)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "XCart\\Marketplace\\Request\\Notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 14:47:41;
Channel: service-marketplace.DEBUG;
Runtime id: 5c81dac4e51144f7957b887a812d01c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 14:49:34;
Channel: service-marketplace.INFO;
Runtime id: 7687ce4032855e609500b99a44b10995;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 14:49:34;
Channel: service-marketplace.DEBUG;
Runtime id: 7687ce4032855e609500b99a44b10995;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Set marketplace lock

Context:
request => XCart\Marketplace\Request\VersionInfo
code => 0
message => Error creating resource: [message] fopen(): php_network_getaddresses: getaddrinfo failed: Temporary failure in name resolution
[file] /home/kirill/domen/xcart/service/vendor/guzzlehttp/ringphp/src/Client/StreamHandler.php
[line] 406
[message] fopen(http://my.x-cart.com/index.php?q=api%2Fget_version_info): failed to open stream: php_network_getaddresses: getaddrinfo failed: Temporary failure in name resolution
[file] /home/kirill/domen/xcart/service/vendor/guzzlehttp/ringphp/src/Client/StreamHandler.php
[line] 406
[message] Undefined variable: http_response_header
[file] /home/kirill/domen/xcart/service/vendor/guzzlehttp/ringphp/src/Client/StreamHandler.php
[line] 407

Time: 2019-09-05 14:49:34;
Channel: service-marketplace.INFO;
Runtime id: 7687ce4032855e609500b99a44b10995;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 14:50:01;
Channel: service-marketplace.INFO;
Runtime id: c4a9d1769cb76bbeedd266df05c7c41f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 14:52:13;
Channel: service-marketplace.INFO;
Runtime id: f11069b86d7370b8d14a9c9b569c4e25;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 14:52:47;
Channel: service-marketplace.INFO;
Runtime id: 173af160f8d9c814da2ff12a4ef9326d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 14:54:46;
Channel: service-marketplace.INFO;
Runtime id: 123f5539d8ba18f6be141deb79dda730;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 14:55:12;
Channel: service-marketplace.INFO;
Runtime id: 315fe6d72679f41e23c0db208d625aea;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:07:51;
Channel: service-marketplace.INFO;
Runtime id: c2167233f5828fe3f163bf629770ffde;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:08:17;
Channel: service-marketplace.INFO;
Runtime id: 04ee031d340c933623b2ecb1fcf23421;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:21:38;
Channel: service-marketplace.INFO;
Runtime id: b3e58eb85f6ddeab8805a2feb644b1ea;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:22:08;
Channel: service-marketplace.INFO;
Runtime id: 5e0888835a81408ba9f3ad5119d8507b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:31:17;
Channel: service-marketplace.INFO;
Runtime id: f65bb30f0168f7b5437b07072821e268;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:31:44;
Channel: service-marketplace.INFO;
Runtime id: 78aba877738e5cefbc62b5beabbc19dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:38:12;
Channel: service-marketplace.INFO;
Runtime id: fa213b078ed53a0b2a131bcd93c987af;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:38:38;
Channel: service-marketplace.INFO;
Runtime id: 33e40c1023f13c80c943350dc67f20a1;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:45:36;
Channel: service-marketplace.INFO;
Runtime id: 2ee097b9094c7b0287d13192794ee43c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:46:09;
Channel: service-marketplace.INFO;
Runtime id: ae0c08885ab6753ea294b1ee4d9c094f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:48:19;
Channel: service-marketplace.INFO;
Runtime id: 5cbd0d3ae22fc40e1bf358cf6b4edba2;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\VersionInfo

Time: 2019-09-05 15:48:53;
Channel: service-marketplace.INFO;
Runtime id: 46cecbc966fb55acab67770524a8aedb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Marketplace is locked

Context:
request => XCart\Marketplace\Request\Set

Time: 2019-09-05 15:48:53;
Channel: service-marketplace.INFO;
Runtime id: 33aac8e6c633178b74197dc5ef98a1c6;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:49:41;
Channel: service-marketplace.INFO;
Runtime id: d71c4af30d5f7ff12335f5a0b010fe37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:49:41;
Channel: service-marketplace.DEBUG;
Runtime id: d71c4af30d5f7ff12335f5a0b010fe37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:49:42 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:49:42;
Channel: service-marketplace.DEBUG;
Runtime id: d71c4af30d5f7ff12335f5a0b010fe37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:49:42;
Channel: service-marketplace.DEBUG;
Runtime id: d71c4af30d5f7ff12335f5a0b010fe37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:49:42;
Channel: service-marketplace.DEBUG;
Runtime id: d71c4af30d5f7ff12335f5a0b010fe37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:49:42;
Channel: service-marketplace.WARNING;
Runtime id: d71c4af30d5f7ff12335f5a0b010fe37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:49:42;
Channel: service-marketplace.EMERGENCY;
Runtime id: d71c4af30d5f7ff12335f5a0b010fe37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:50:15;
Channel: service-marketplace.INFO;
Runtime id: 909fecddc2aaf636eeef45318dc63e37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:50:15;
Channel: service-marketplace.DEBUG;
Runtime id: 909fecddc2aaf636eeef45318dc63e37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:50:16 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:50:16;
Channel: service-marketplace.DEBUG;
Runtime id: 909fecddc2aaf636eeef45318dc63e37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:50:16;
Channel: service-marketplace.DEBUG;
Runtime id: 909fecddc2aaf636eeef45318dc63e37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:50:16;
Channel: service-marketplace.DEBUG;
Runtime id: 909fecddc2aaf636eeef45318dc63e37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:50:16;
Channel: service-marketplace.WARNING;
Runtime id: 909fecddc2aaf636eeef45318dc63e37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:50:16;
Channel: service-marketplace.EMERGENCY;
Runtime id: 909fecddc2aaf636eeef45318dc63e37;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:54:20;
Channel: service-marketplace.INFO;
Runtime id: 17db26983a1425e90144e6102213942d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:54:20;
Channel: service-marketplace.DEBUG;
Runtime id: 17db26983a1425e90144e6102213942d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:54:20 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:54:20;
Channel: service-marketplace.DEBUG;
Runtime id: 17db26983a1425e90144e6102213942d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:54:20;
Channel: service-marketplace.DEBUG;
Runtime id: 17db26983a1425e90144e6102213942d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:54:20;
Channel: service-marketplace.DEBUG;
Runtime id: 17db26983a1425e90144e6102213942d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:54:20;
Channel: service-marketplace.WARNING;
Runtime id: 17db26983a1425e90144e6102213942d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:54:20;
Channel: service-marketplace.EMERGENCY;
Runtime id: 17db26983a1425e90144e6102213942d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:54:47;
Channel: service-marketplace.INFO;
Runtime id: 9831ab3bd84918700f81c9a2c55b8e73;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:54:47;
Channel: service-marketplace.DEBUG;
Runtime id: 9831ab3bd84918700f81c9a2c55b8e73;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:54:47 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:54:47;
Channel: service-marketplace.DEBUG;
Runtime id: 9831ab3bd84918700f81c9a2c55b8e73;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:54:47;
Channel: service-marketplace.DEBUG;
Runtime id: 9831ab3bd84918700f81c9a2c55b8e73;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:54:47;
Channel: service-marketplace.DEBUG;
Runtime id: 9831ab3bd84918700f81c9a2c55b8e73;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:54:47;
Channel: service-marketplace.WARNING;
Runtime id: 9831ab3bd84918700f81c9a2c55b8e73;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:54:47;
Channel: service-marketplace.EMERGENCY;
Runtime id: 9831ab3bd84918700f81c9a2c55b8e73;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:55:29;
Channel: service-marketplace.INFO;
Runtime id: f7c4e6e7266125f05966dc49183b0a69;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:55:29;
Channel: service-marketplace.DEBUG;
Runtime id: f7c4e6e7266125f05966dc49183b0a69;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:55:29 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:55:29;
Channel: service-marketplace.DEBUG;
Runtime id: f7c4e6e7266125f05966dc49183b0a69;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:55:29;
Channel: service-marketplace.DEBUG;
Runtime id: f7c4e6e7266125f05966dc49183b0a69;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:55:29;
Channel: service-marketplace.DEBUG;
Runtime id: f7c4e6e7266125f05966dc49183b0a69;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:55:29;
Channel: service-marketplace.WARNING;
Runtime id: f7c4e6e7266125f05966dc49183b0a69;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:55:29;
Channel: service-marketplace.EMERGENCY;
Runtime id: f7c4e6e7266125f05966dc49183b0a69;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:56:02;
Channel: service-marketplace.INFO;
Runtime id: 80f73b8300841be7b101873db6b47fcf;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:56:02;
Channel: service-marketplace.DEBUG;
Runtime id: 80f73b8300841be7b101873db6b47fcf;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:56:03 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:56:03;
Channel: service-marketplace.DEBUG;
Runtime id: 80f73b8300841be7b101873db6b47fcf;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:56:03;
Channel: service-marketplace.DEBUG;
Runtime id: 80f73b8300841be7b101873db6b47fcf;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:56:03;
Channel: service-marketplace.DEBUG;
Runtime id: 80f73b8300841be7b101873db6b47fcf;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:56:03;
Channel: service-marketplace.WARNING;
Runtime id: 80f73b8300841be7b101873db6b47fcf;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:56:03;
Channel: service-marketplace.EMERGENCY;
Runtime id: 80f73b8300841be7b101873db6b47fcf;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:57:28;
Channel: service-marketplace.INFO;
Runtime id: 048c4f4eabc8fd1fb7b1890bc53cd33f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:57:28;
Channel: service-marketplace.DEBUG;
Runtime id: 048c4f4eabc8fd1fb7b1890bc53cd33f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:57:28 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:57:28;
Channel: service-marketplace.DEBUG;
Runtime id: 048c4f4eabc8fd1fb7b1890bc53cd33f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:57:28;
Channel: service-marketplace.DEBUG;
Runtime id: 048c4f4eabc8fd1fb7b1890bc53cd33f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:57:28;
Channel: service-marketplace.DEBUG;
Runtime id: 048c4f4eabc8fd1fb7b1890bc53cd33f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:57:28;
Channel: service-marketplace.WARNING;
Runtime id: 048c4f4eabc8fd1fb7b1890bc53cd33f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:57:28;
Channel: service-marketplace.EMERGENCY;
Runtime id: 048c4f4eabc8fd1fb7b1890bc53cd33f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:58:02;
Channel: service-marketplace.INFO;
Runtime id: c1b196205aa03ff031affe2599f95090;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:58:02;
Channel: service-marketplace.DEBUG;
Runtime id: c1b196205aa03ff031affe2599f95090;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:58:02 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:58:02;
Channel: service-marketplace.DEBUG;
Runtime id: c1b196205aa03ff031affe2599f95090;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:58:02;
Channel: service-marketplace.DEBUG;
Runtime id: c1b196205aa03ff031affe2599f95090;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:58:02;
Channel: service-marketplace.DEBUG;
Runtime id: c1b196205aa03ff031affe2599f95090;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:58:02;
Channel: service-marketplace.WARNING;
Runtime id: c1b196205aa03ff031affe2599f95090;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:58:02;
Channel: service-marketplace.EMERGENCY;
Runtime id: c1b196205aa03ff031affe2599f95090;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:58:58;
Channel: service-marketplace.INFO;
Runtime id: ca5b245fbac22818f38d5f162ed0c77c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:58:58;
Channel: service-marketplace.DEBUG;
Runtime id: ca5b245fbac22818f38d5f162ed0c77c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:58:58 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:58:59;
Channel: service-marketplace.DEBUG;
Runtime id: ca5b245fbac22818f38d5f162ed0c77c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:58:59;
Channel: service-marketplace.DEBUG;
Runtime id: ca5b245fbac22818f38d5f162ed0c77c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:58:59;
Channel: service-marketplace.DEBUG;
Runtime id: ca5b245fbac22818f38d5f162ed0c77c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:58:59;
Channel: service-marketplace.WARNING;
Runtime id: ca5b245fbac22818f38d5f162ed0c77c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:58:59;
Channel: service-marketplace.EMERGENCY;
Runtime id: ca5b245fbac22818f38d5f162ed0c77c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:59:32;
Channel: service-marketplace.INFO;
Runtime id: 159562a0451e72348285522d6d70a6b4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:59:32;
Channel: service-marketplace.DEBUG;
Runtime id: 159562a0451e72348285522d6d70a6b4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:59:32 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:59:32;
Channel: service-marketplace.DEBUG;
Runtime id: 159562a0451e72348285522d6d70a6b4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:59:32;
Channel: service-marketplace.DEBUG;
Runtime id: 159562a0451e72348285522d6d70a6b4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:59:32;
Channel: service-marketplace.DEBUG;
Runtime id: 159562a0451e72348285522d6d70a6b4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:59:32;
Channel: service-marketplace.WARNING;
Runtime id: 159562a0451e72348285522d6d70a6b4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:59:32;
Channel: service-marketplace.EMERGENCY;
Runtime id: 159562a0451e72348285522d6d70a6b4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 15:59:47;
Channel: service-marketplace.INFO;
Runtime id: b719afca622c65ada83005207619d143;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 15:59:47;
Channel: service-marketplace.DEBUG;
Runtime id: b719afca622c65ada83005207619d143;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 11:59:48 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 15:59:48;
Channel: service-marketplace.DEBUG;
Runtime id: b719afca622c65ada83005207619d143;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:59:48;
Channel: service-marketplace.DEBUG;
Runtime id: b719afca622c65ada83005207619d143;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:59:48;
Channel: service-marketplace.DEBUG;
Runtime id: b719afca622c65ada83005207619d143;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 15:59:48;
Channel: service-marketplace.WARNING;
Runtime id: b719afca622c65ada83005207619d143;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 15:59:48;
Channel: service-marketplace.EMERGENCY;
Runtime id: b719afca622c65ada83005207619d143;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:00:12;
Channel: service-marketplace.INFO;
Runtime id: 6bb266f197b5f90b44aefc306ca9d29b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:00:12;
Channel: service-marketplace.DEBUG;
Runtime id: 6bb266f197b5f90b44aefc306ca9d29b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:00:13 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:00:13;
Channel: service-marketplace.DEBUG;
Runtime id: 6bb266f197b5f90b44aefc306ca9d29b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:00:13;
Channel: service-marketplace.DEBUG;
Runtime id: 6bb266f197b5f90b44aefc306ca9d29b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:00:13;
Channel: service-marketplace.DEBUG;
Runtime id: 6bb266f197b5f90b44aefc306ca9d29b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:00:13;
Channel: service-marketplace.WARNING;
Runtime id: 6bb266f197b5f90b44aefc306ca9d29b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:00:13;
Channel: service-marketplace.EMERGENCY;
Runtime id: 6bb266f197b5f90b44aefc306ca9d29b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:01:29;
Channel: service-marketplace.INFO;
Runtime id: 18b8d4a61418a6b54e77d12188cd0adb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:01:29;
Channel: service-marketplace.DEBUG;
Runtime id: 18b8d4a61418a6b54e77d12188cd0adb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:01:29 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:01:29;
Channel: service-marketplace.DEBUG;
Runtime id: 18b8d4a61418a6b54e77d12188cd0adb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:01:29;
Channel: service-marketplace.DEBUG;
Runtime id: 18b8d4a61418a6b54e77d12188cd0adb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:01:29;
Channel: service-marketplace.DEBUG;
Runtime id: 18b8d4a61418a6b54e77d12188cd0adb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:01:29;
Channel: service-marketplace.WARNING;
Runtime id: 18b8d4a61418a6b54e77d12188cd0adb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:01:29;
Channel: service-marketplace.EMERGENCY;
Runtime id: 18b8d4a61418a6b54e77d12188cd0adb;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:02:03;
Channel: service-marketplace.INFO;
Runtime id: 7effca1a648349943d5621d47292ab81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:02:03;
Channel: service-marketplace.DEBUG;
Runtime id: 7effca1a648349943d5621d47292ab81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:02:04 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:02:04;
Channel: service-marketplace.DEBUG;
Runtime id: 7effca1a648349943d5621d47292ab81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:02:04;
Channel: service-marketplace.DEBUG;
Runtime id: 7effca1a648349943d5621d47292ab81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:02:04;
Channel: service-marketplace.DEBUG;
Runtime id: 7effca1a648349943d5621d47292ab81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:02:04;
Channel: service-marketplace.WARNING;
Runtime id: 7effca1a648349943d5621d47292ab81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:02:04;
Channel: service-marketplace.EMERGENCY;
Runtime id: 7effca1a648349943d5621d47292ab81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:02:52;
Channel: service-marketplace.INFO;
Runtime id: 377e7e569ce6d11169c3e443c4e94a2a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:02:52;
Channel: service-marketplace.DEBUG;
Runtime id: 377e7e569ce6d11169c3e443c4e94a2a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:02:53 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:02:53;
Channel: service-marketplace.DEBUG;
Runtime id: 377e7e569ce6d11169c3e443c4e94a2a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:02:53;
Channel: service-marketplace.DEBUG;
Runtime id: 377e7e569ce6d11169c3e443c4e94a2a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:02:53;
Channel: service-marketplace.DEBUG;
Runtime id: 377e7e569ce6d11169c3e443c4e94a2a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:02:53;
Channel: service-marketplace.WARNING;
Runtime id: 377e7e569ce6d11169c3e443c4e94a2a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:02:53;
Channel: service-marketplace.EMERGENCY;
Runtime id: 377e7e569ce6d11169c3e443c4e94a2a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:03:26;
Channel: service-marketplace.INFO;
Runtime id: 1fe0f631462c3449a7ad68786079025e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:03:26;
Channel: service-marketplace.DEBUG;
Runtime id: 1fe0f631462c3449a7ad68786079025e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:03:28 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:03:29;
Channel: service-marketplace.DEBUG;
Runtime id: 1fe0f631462c3449a7ad68786079025e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:03:29;
Channel: service-marketplace.DEBUG;
Runtime id: 1fe0f631462c3449a7ad68786079025e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:03:29;
Channel: service-marketplace.DEBUG;
Runtime id: 1fe0f631462c3449a7ad68786079025e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:03:29;
Channel: service-marketplace.WARNING;
Runtime id: 1fe0f631462c3449a7ad68786079025e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:03:29;
Channel: service-marketplace.EMERGENCY;
Runtime id: 1fe0f631462c3449a7ad68786079025e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:39:01;
Channel: service-marketplace.INFO;
Runtime id: 2f8739bd576ea5f2c60636126589863b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:39:01;
Channel: service-marketplace.DEBUG;
Runtime id: 2f8739bd576ea5f2c60636126589863b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:39:01 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:39:01;
Channel: service-marketplace.DEBUG;
Runtime id: 2f8739bd576ea5f2c60636126589863b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:39:01;
Channel: service-marketplace.DEBUG;
Runtime id: 2f8739bd576ea5f2c60636126589863b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:39:01;
Channel: service-marketplace.DEBUG;
Runtime id: 2f8739bd576ea5f2c60636126589863b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:39:01;
Channel: service-marketplace.WARNING;
Runtime id: 2f8739bd576ea5f2c60636126589863b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:39:01;
Channel: service-marketplace.EMERGENCY;
Runtime id: 2f8739bd576ea5f2c60636126589863b;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:39:33;
Channel: service-marketplace.INFO;
Runtime id: af9515c9b22476d766a84729d0fb935a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:39:33;
Channel: service-marketplace.DEBUG;
Runtime id: af9515c9b22476d766a84729d0fb935a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:39:33 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:39:33;
Channel: service-marketplace.DEBUG;
Runtime id: af9515c9b22476d766a84729d0fb935a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:39:33;
Channel: service-marketplace.DEBUG;
Runtime id: af9515c9b22476d766a84729d0fb935a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:39:33;
Channel: service-marketplace.DEBUG;
Runtime id: af9515c9b22476d766a84729d0fb935a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:39:33;
Channel: service-marketplace.WARNING;
Runtime id: af9515c9b22476d766a84729d0fb935a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:39:33;
Channel: service-marketplace.EMERGENCY;
Runtime id: af9515c9b22476d766a84729d0fb935a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:41:39;
Channel: service-marketplace.INFO;
Runtime id: f728e3d4afddc23dd1815167e67ac5d4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:41:39;
Channel: service-marketplace.DEBUG;
Runtime id: f728e3d4afddc23dd1815167e67ac5d4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:41:39 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:41:39;
Channel: service-marketplace.DEBUG;
Runtime id: f728e3d4afddc23dd1815167e67ac5d4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:41:39;
Channel: service-marketplace.DEBUG;
Runtime id: f728e3d4afddc23dd1815167e67ac5d4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:41:39;
Channel: service-marketplace.DEBUG;
Runtime id: f728e3d4afddc23dd1815167e67ac5d4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:41:39;
Channel: service-marketplace.WARNING;
Runtime id: f728e3d4afddc23dd1815167e67ac5d4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:41:39;
Channel: service-marketplace.EMERGENCY;
Runtime id: f728e3d4afddc23dd1815167e67ac5d4;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:42:08;
Channel: service-marketplace.INFO;
Runtime id: 4a3aaeac00f00bb64f8452aa9796632d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:42:08;
Channel: service-marketplace.DEBUG;
Runtime id: 4a3aaeac00f00bb64f8452aa9796632d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:42:09 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:42:09;
Channel: service-marketplace.DEBUG;
Runtime id: 4a3aaeac00f00bb64f8452aa9796632d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:42:09;
Channel: service-marketplace.DEBUG;
Runtime id: 4a3aaeac00f00bb64f8452aa9796632d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:42:09;
Channel: service-marketplace.DEBUG;
Runtime id: 4a3aaeac00f00bb64f8452aa9796632d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:42:09;
Channel: service-marketplace.WARNING;
Runtime id: 4a3aaeac00f00bb64f8452aa9796632d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:42:09;
Channel: service-marketplace.EMERGENCY;
Runtime id: 4a3aaeac00f00bb64f8452aa9796632d;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:55:10;
Channel: service-marketplace.INFO;
Runtime id: c398c14b2ef617543c82719075cb6b81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:55:10;
Channel: service-marketplace.DEBUG;
Runtime id: c398c14b2ef617543c82719075cb6b81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:55:11 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:55:11;
Channel: service-marketplace.DEBUG;
Runtime id: c398c14b2ef617543c82719075cb6b81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:55:11;
Channel: service-marketplace.DEBUG;
Runtime id: c398c14b2ef617543c82719075cb6b81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:55:11;
Channel: service-marketplace.DEBUG;
Runtime id: c398c14b2ef617543c82719075cb6b81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:55:11;
Channel: service-marketplace.WARNING;
Runtime id: c398c14b2ef617543c82719075cb6b81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:55:11;
Channel: service-marketplace.EMERGENCY;
Runtime id: c398c14b2ef617543c82719075cb6b81;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:55:41;
Channel: service-marketplace.INFO;
Runtime id: e9070d152d8778a9dde7ac214f3ef511;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.DEBUG;
Runtime id: e9070d152d8778a9dde7ac214f3ef511;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:55:42 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.DEBUG;
Runtime id: e9070d152d8778a9dde7ac214f3ef511;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.DEBUG;
Runtime id: e9070d152d8778a9dde7ac214f3ef511;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.DEBUG;
Runtime id: e9070d152d8778a9dde7ac214f3ef511;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.WARNING;
Runtime id: e9070d152d8778a9dde7ac214f3ef511;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.EMERGENCY;
Runtime id: e9070d152d8778a9dde7ac214f3ef511;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\Set"

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.INFO;
Runtime id: 8144b670ab899c8da8d548e06f4265c2;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\Set
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "querySets": {
        "get_XC5_notifications": [
            0
        ]
    }
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.DEBUG;
Runtime id: 8144b670ab899c8da8d548e06f4265c2;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:55:42 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "799"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"get_XC5_notifications\":[{\"type\":\"module\",\"module\":\"XC-FacebookMarketing\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/facebookinsta.png\",\"title\":\"Advertise your products on Facebook and Instagram and see your sales grow\",\"description\":\"Try the free Facebook Ads and Instagram Ads addon for X-Cart\",\"link\":\"\",\"date\":\"1558040400\"},{\"type\":\"news\",\"module\":\"\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/GA1.png\",\"title\":\"FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.\",\"description\":\"\",\"link\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup\",\"date\":\"1557262800\"}]}"
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.DEBUG;
Runtime id: 8144b670ab899c8da8d548e06f4265c2;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.DEBUG;
Runtime id: 8144b670ab899c8da8d548e06f4265c2;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.DEBUG;
Runtime id: 8144b670ab899c8da8d548e06f4265c2;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace correctly received "XCart\Marketplace\Request\Set"

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.INFO;
Runtime id: 8144b670ab899c8da8d548e06f4265c2;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (formatted)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "XCart\\Marketplace\\Request\\Notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 16:55:42;
Channel: service-marketplace.DEBUG;
Runtime id: 8144b670ab899c8da8d548e06f4265c2;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 16:56:48;
Channel: service-marketplace.INFO;
Runtime id: c57b252e8b5324c11dee149ae7704c61;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 16:56:48;
Channel: service-marketplace.DEBUG;
Runtime id: c57b252e8b5324c11dee149ae7704c61;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 12:56:48 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 16:56:48;
Channel: service-marketplace.DEBUG;
Runtime id: c57b252e8b5324c11dee149ae7704c61;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:56:48;
Channel: service-marketplace.DEBUG;
Runtime id: c57b252e8b5324c11dee149ae7704c61;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:56:48;
Channel: service-marketplace.DEBUG;
Runtime id: c57b252e8b5324c11dee149ae7704c61;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 16:56:48;
Channel: service-marketplace.WARNING;
Runtime id: c57b252e8b5324c11dee149ae7704c61;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 16:56:48;
Channel: service-marketplace.EMERGENCY;
Runtime id: c57b252e8b5324c11dee149ae7704c61;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:04:54;
Channel: service-marketplace.INFO;
Runtime id: 3cf27aaa94c148104992b7c0a69c63d5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:04:54;
Channel: service-marketplace.DEBUG;
Runtime id: 3cf27aaa94c148104992b7c0a69c63d5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:04:54 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:04:54;
Channel: service-marketplace.DEBUG;
Runtime id: 3cf27aaa94c148104992b7c0a69c63d5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:04:54;
Channel: service-marketplace.DEBUG;
Runtime id: 3cf27aaa94c148104992b7c0a69c63d5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:04:54;
Channel: service-marketplace.DEBUG;
Runtime id: 3cf27aaa94c148104992b7c0a69c63d5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:04:54;
Channel: service-marketplace.WARNING;
Runtime id: 3cf27aaa94c148104992b7c0a69c63d5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:04:54;
Channel: service-marketplace.EMERGENCY;
Runtime id: 3cf27aaa94c148104992b7c0a69c63d5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:05:07;
Channel: service-marketplace.INFO;
Runtime id: 8eaf0fac13116f2d75a1b971db5b1fd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:05:07;
Channel: service-marketplace.DEBUG;
Runtime id: 8eaf0fac13116f2d75a1b971db5b1fd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:05:08 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:05:08;
Channel: service-marketplace.DEBUG;
Runtime id: 8eaf0fac13116f2d75a1b971db5b1fd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:05:08;
Channel: service-marketplace.DEBUG;
Runtime id: 8eaf0fac13116f2d75a1b971db5b1fd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:05:08;
Channel: service-marketplace.DEBUG;
Runtime id: 8eaf0fac13116f2d75a1b971db5b1fd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:05:08;
Channel: service-marketplace.WARNING;
Runtime id: 8eaf0fac13116f2d75a1b971db5b1fd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:05:08;
Channel: service-marketplace.EMERGENCY;
Runtime id: 8eaf0fac13116f2d75a1b971db5b1fd7;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:05:34;
Channel: service-marketplace.INFO;
Runtime id: 3f6f68e9cfba04116b42359b34644c89;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:05:34;
Channel: service-marketplace.DEBUG;
Runtime id: 3f6f68e9cfba04116b42359b34644c89;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:05:34 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:05:34;
Channel: service-marketplace.DEBUG;
Runtime id: 3f6f68e9cfba04116b42359b34644c89;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:05:34;
Channel: service-marketplace.DEBUG;
Runtime id: 3f6f68e9cfba04116b42359b34644c89;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:05:34;
Channel: service-marketplace.DEBUG;
Runtime id: 3f6f68e9cfba04116b42359b34644c89;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:05:34;
Channel: service-marketplace.WARNING;
Runtime id: 3f6f68e9cfba04116b42359b34644c89;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:05:34;
Channel: service-marketplace.EMERGENCY;
Runtime id: 3f6f68e9cfba04116b42359b34644c89;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:08:39;
Channel: service-marketplace.INFO;
Runtime id: 12e8b7a23f83cd28ad6d249b11e93ffd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:08:39;
Channel: service-marketplace.DEBUG;
Runtime id: 12e8b7a23f83cd28ad6d249b11e93ffd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:08:40 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:08:40;
Channel: service-marketplace.DEBUG;
Runtime id: 12e8b7a23f83cd28ad6d249b11e93ffd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:08:40;
Channel: service-marketplace.DEBUG;
Runtime id: 12e8b7a23f83cd28ad6d249b11e93ffd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:08:40;
Channel: service-marketplace.DEBUG;
Runtime id: 12e8b7a23f83cd28ad6d249b11e93ffd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:08:40;
Channel: service-marketplace.WARNING;
Runtime id: 12e8b7a23f83cd28ad6d249b11e93ffd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:08:40;
Channel: service-marketplace.EMERGENCY;
Runtime id: 12e8b7a23f83cd28ad6d249b11e93ffd;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:09:04;
Channel: service-marketplace.INFO;
Runtime id: 14c44a43003002811a3735c289fb2fd5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:09:05;
Channel: service-marketplace.DEBUG;
Runtime id: 14c44a43003002811a3735c289fb2fd5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:09:05 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:09:05;
Channel: service-marketplace.DEBUG;
Runtime id: 14c44a43003002811a3735c289fb2fd5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:09:05;
Channel: service-marketplace.DEBUG;
Runtime id: 14c44a43003002811a3735c289fb2fd5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:09:05;
Channel: service-marketplace.DEBUG;
Runtime id: 14c44a43003002811a3735c289fb2fd5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:09:05;
Channel: service-marketplace.WARNING;
Runtime id: 14c44a43003002811a3735c289fb2fd5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:09:05;
Channel: service-marketplace.EMERGENCY;
Runtime id: 14c44a43003002811a3735c289fb2fd5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:10:11;
Channel: service-marketplace.INFO;
Runtime id: f2618013756a6c4777d2d55e5517d70a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:10:11;
Channel: service-marketplace.DEBUG;
Runtime id: f2618013756a6c4777d2d55e5517d70a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:10:11 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:10:11;
Channel: service-marketplace.DEBUG;
Runtime id: f2618013756a6c4777d2d55e5517d70a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:10:11;
Channel: service-marketplace.DEBUG;
Runtime id: f2618013756a6c4777d2d55e5517d70a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:10:11;
Channel: service-marketplace.DEBUG;
Runtime id: f2618013756a6c4777d2d55e5517d70a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:10:11;
Channel: service-marketplace.WARNING;
Runtime id: f2618013756a6c4777d2d55e5517d70a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:10:11;
Channel: service-marketplace.EMERGENCY;
Runtime id: f2618013756a6c4777d2d55e5517d70a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:10:44;
Channel: service-marketplace.INFO;
Runtime id: e01305f27e8106af71a4ac65f0ba73dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:10:44;
Channel: service-marketplace.DEBUG;
Runtime id: e01305f27e8106af71a4ac65f0ba73dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:10:45 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:10:45;
Channel: service-marketplace.DEBUG;
Runtime id: e01305f27e8106af71a4ac65f0ba73dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:10:45;
Channel: service-marketplace.DEBUG;
Runtime id: e01305f27e8106af71a4ac65f0ba73dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:10:45;
Channel: service-marketplace.DEBUG;
Runtime id: e01305f27e8106af71a4ac65f0ba73dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:10:45;
Channel: service-marketplace.WARNING;
Runtime id: e01305f27e8106af71a4ac65f0ba73dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:10:45;
Channel: service-marketplace.EMERGENCY;
Runtime id: e01305f27e8106af71a4ac65f0ba73dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:13:23;
Channel: service-marketplace.INFO;
Runtime id: 0bf57b9cdf0bd743b8b09c39c6f6155a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:13:23;
Channel: service-marketplace.DEBUG;
Runtime id: 0bf57b9cdf0bd743b8b09c39c6f6155a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:13:23 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:13:23;
Channel: service-marketplace.DEBUG;
Runtime id: 0bf57b9cdf0bd743b8b09c39c6f6155a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:13:23;
Channel: service-marketplace.DEBUG;
Runtime id: 0bf57b9cdf0bd743b8b09c39c6f6155a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:13:23;
Channel: service-marketplace.DEBUG;
Runtime id: 0bf57b9cdf0bd743b8b09c39c6f6155a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:13:23;
Channel: service-marketplace.WARNING;
Runtime id: 0bf57b9cdf0bd743b8b09c39c6f6155a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:13:23;
Channel: service-marketplace.EMERGENCY;
Runtime id: 0bf57b9cdf0bd743b8b09c39c6f6155a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:13:48;
Channel: service-marketplace.INFO;
Runtime id: b622bd3e0a6f1777039af57677f2b71c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:13:48;
Channel: service-marketplace.DEBUG;
Runtime id: b622bd3e0a6f1777039af57677f2b71c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:13:49 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:13:49;
Channel: service-marketplace.DEBUG;
Runtime id: b622bd3e0a6f1777039af57677f2b71c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:13:49;
Channel: service-marketplace.DEBUG;
Runtime id: b622bd3e0a6f1777039af57677f2b71c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:13:49;
Channel: service-marketplace.DEBUG;
Runtime id: b622bd3e0a6f1777039af57677f2b71c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:13:49;
Channel: service-marketplace.WARNING;
Runtime id: b622bd3e0a6f1777039af57677f2b71c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:13:49;
Channel: service-marketplace.EMERGENCY;
Runtime id: b622bd3e0a6f1777039af57677f2b71c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:13:56;
Channel: service-marketplace.INFO;
Runtime id: e8c97d7e8a14a9c042cfcf92e0651d01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:13:56;
Channel: service-marketplace.DEBUG;
Runtime id: e8c97d7e8a14a9c042cfcf92e0651d01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:13:56 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:13:56;
Channel: service-marketplace.DEBUG;
Runtime id: e8c97d7e8a14a9c042cfcf92e0651d01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:13:56;
Channel: service-marketplace.DEBUG;
Runtime id: e8c97d7e8a14a9c042cfcf92e0651d01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:13:56;
Channel: service-marketplace.DEBUG;
Runtime id: e8c97d7e8a14a9c042cfcf92e0651d01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:13:56;
Channel: service-marketplace.WARNING;
Runtime id: e8c97d7e8a14a9c042cfcf92e0651d01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:13:56;
Channel: service-marketplace.EMERGENCY;
Runtime id: e8c97d7e8a14a9c042cfcf92e0651d01;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:15:23;
Channel: service-marketplace.INFO;
Runtime id: 4b00923e2a88a9ca659d9fc958a8424a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:15:23;
Channel: service-marketplace.DEBUG;
Runtime id: 4b00923e2a88a9ca659d9fc958a8424a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:15:24 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:15:24;
Channel: service-marketplace.DEBUG;
Runtime id: 4b00923e2a88a9ca659d9fc958a8424a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:15:24;
Channel: service-marketplace.DEBUG;
Runtime id: 4b00923e2a88a9ca659d9fc958a8424a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:15:24;
Channel: service-marketplace.DEBUG;
Runtime id: 4b00923e2a88a9ca659d9fc958a8424a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:15:24;
Channel: service-marketplace.WARNING;
Runtime id: 4b00923e2a88a9ca659d9fc958a8424a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:15:24;
Channel: service-marketplace.EMERGENCY;
Runtime id: 4b00923e2a88a9ca659d9fc958a8424a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:17:14;
Channel: service-marketplace.INFO;
Runtime id: 97b328e1b36583adb23111af8d6b01b5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:17:14;
Channel: service-marketplace.DEBUG;
Runtime id: 97b328e1b36583adb23111af8d6b01b5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:17:14 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:17:14;
Channel: service-marketplace.DEBUG;
Runtime id: 97b328e1b36583adb23111af8d6b01b5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:17:14;
Channel: service-marketplace.DEBUG;
Runtime id: 97b328e1b36583adb23111af8d6b01b5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:17:14;
Channel: service-marketplace.DEBUG;
Runtime id: 97b328e1b36583adb23111af8d6b01b5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:17:14;
Channel: service-marketplace.WARNING;
Runtime id: 97b328e1b36583adb23111af8d6b01b5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:17:14;
Channel: service-marketplace.EMERGENCY;
Runtime id: 97b328e1b36583adb23111af8d6b01b5;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:17:40;
Channel: service-marketplace.INFO;
Runtime id: c9ae7c98dd1fd5340983087300f7110c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:17:40;
Channel: service-marketplace.DEBUG;
Runtime id: c9ae7c98dd1fd5340983087300f7110c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:17:40 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:17:41;
Channel: service-marketplace.DEBUG;
Runtime id: c9ae7c98dd1fd5340983087300f7110c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:17:41;
Channel: service-marketplace.DEBUG;
Runtime id: c9ae7c98dd1fd5340983087300f7110c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:17:41;
Channel: service-marketplace.DEBUG;
Runtime id: c9ae7c98dd1fd5340983087300f7110c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:17:41;
Channel: service-marketplace.WARNING;
Runtime id: c9ae7c98dd1fd5340983087300f7110c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:17:41;
Channel: service-marketplace.EMERGENCY;
Runtime id: c9ae7c98dd1fd5340983087300f7110c;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:19:59;
Channel: service-marketplace.INFO;
Runtime id: 58d0ec620ee2f68d2dc6d3badb08f0dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:19:59;
Channel: service-marketplace.DEBUG;
Runtime id: 58d0ec620ee2f68d2dc6d3badb08f0dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:19:59 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:20:00;
Channel: service-marketplace.DEBUG;
Runtime id: 58d0ec620ee2f68d2dc6d3badb08f0dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:20:00;
Channel: service-marketplace.DEBUG;
Runtime id: 58d0ec620ee2f68d2dc6d3badb08f0dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:20:00;
Channel: service-marketplace.DEBUG;
Runtime id: 58d0ec620ee2f68d2dc6d3badb08f0dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:20:00;
Channel: service-marketplace.WARNING;
Runtime id: 58d0ec620ee2f68d2dc6d3badb08f0dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:20:00;
Channel: service-marketplace.EMERGENCY;
Runtime id: 58d0ec620ee2f68d2dc6d3badb08f0dc;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:20:28;
Channel: service-marketplace.INFO;
Runtime id: 51cdc04e9d1780d33bc85be8d3b32e60;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:20:28;
Channel: service-marketplace.DEBUG;
Runtime id: 51cdc04e9d1780d33bc85be8d3b32e60;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:20:29 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:20:29;
Channel: service-marketplace.DEBUG;
Runtime id: 51cdc04e9d1780d33bc85be8d3b32e60;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:20:29;
Channel: service-marketplace.DEBUG;
Runtime id: 51cdc04e9d1780d33bc85be8d3b32e60;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:20:29;
Channel: service-marketplace.DEBUG;
Runtime id: 51cdc04e9d1780d33bc85be8d3b32e60;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:20:29;
Channel: service-marketplace.WARNING;
Runtime id: 51cdc04e9d1780d33bc85be8d3b32e60;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:20:29;
Channel: service-marketplace.EMERGENCY;
Runtime id: 51cdc04e9d1780d33bc85be8d3b32e60;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:27:40;
Channel: service-marketplace.INFO;
Runtime id: 7c56a9d1f682983447551f595c9c1865;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:27:40;
Channel: service-marketplace.DEBUG;
Runtime id: 7c56a9d1f682983447551f595c9c1865;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:27:40 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:27:40;
Channel: service-marketplace.DEBUG;
Runtime id: 7c56a9d1f682983447551f595c9c1865;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:27:40;
Channel: service-marketplace.DEBUG;
Runtime id: 7c56a9d1f682983447551f595c9c1865;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:27:40;
Channel: service-marketplace.DEBUG;
Runtime id: 7c56a9d1f682983447551f595c9c1865;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:27:40;
Channel: service-marketplace.WARNING;
Runtime id: 7c56a9d1f682983447551f595c9c1865;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:27:40;
Channel: service-marketplace.EMERGENCY;
Runtime id: 7c56a9d1f682983447551f595c9c1865;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:28:07;
Channel: service-marketplace.INFO;
Runtime id: a9773a1e503bc63c2babed265d80584a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:28:07;
Channel: service-marketplace.DEBUG;
Runtime id: a9773a1e503bc63c2babed265d80584a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:28:07 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:28:07;
Channel: service-marketplace.DEBUG;
Runtime id: a9773a1e503bc63c2babed265d80584a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:28:07;
Channel: service-marketplace.DEBUG;
Runtime id: a9773a1e503bc63c2babed265d80584a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:28:07;
Channel: service-marketplace.DEBUG;
Runtime id: a9773a1e503bc63c2babed265d80584a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:28:07;
Channel: service-marketplace.WARNING;
Runtime id: a9773a1e503bc63c2babed265d80584a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:28:07;
Channel: service-marketplace.EMERGENCY;
Runtime id: a9773a1e503bc63c2babed265d80584a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:30:25;
Channel: service-marketplace.INFO;
Runtime id: 15403a067fdc47803f85c1ccafa2cd40;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:30:25;
Channel: service-marketplace.DEBUG;
Runtime id: 15403a067fdc47803f85c1ccafa2cd40;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:30:25 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:30:25;
Channel: service-marketplace.DEBUG;
Runtime id: 15403a067fdc47803f85c1ccafa2cd40;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:30:25;
Channel: service-marketplace.DEBUG;
Runtime id: 15403a067fdc47803f85c1ccafa2cd40;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:30:25;
Channel: service-marketplace.DEBUG;
Runtime id: 15403a067fdc47803f85c1ccafa2cd40;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:30:25;
Channel: service-marketplace.WARNING;
Runtime id: 15403a067fdc47803f85c1ccafa2cd40;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:30:25;
Channel: service-marketplace.EMERGENCY;
Runtime id: 15403a067fdc47803f85c1ccafa2cd40;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:32:08;
Channel: service-marketplace.INFO;
Runtime id: d7a93e1a38d0481878c3c142afea5e24;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:32:08;
Channel: service-marketplace.DEBUG;
Runtime id: d7a93e1a38d0481878c3c142afea5e24;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:32:08 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:32:08;
Channel: service-marketplace.DEBUG;
Runtime id: d7a93e1a38d0481878c3c142afea5e24;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:32:08;
Channel: service-marketplace.DEBUG;
Runtime id: d7a93e1a38d0481878c3c142afea5e24;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:32:08;
Channel: service-marketplace.DEBUG;
Runtime id: d7a93e1a38d0481878c3c142afea5e24;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:32:08;
Channel: service-marketplace.WARNING;
Runtime id: d7a93e1a38d0481878c3c142afea5e24;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:32:08;
Channel: service-marketplace.EMERGENCY;
Runtime id: d7a93e1a38d0481878c3c142afea5e24;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:32:19;
Channel: service-marketplace.INFO;
Runtime id: 009c29d88397773555857c4117757916;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:32:19;
Channel: service-marketplace.DEBUG;
Runtime id: 009c29d88397773555857c4117757916;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:32:19 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:32:19;
Channel: service-marketplace.DEBUG;
Runtime id: 009c29d88397773555857c4117757916;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:32:19;
Channel: service-marketplace.DEBUG;
Runtime id: 009c29d88397773555857c4117757916;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:32:19;
Channel: service-marketplace.DEBUG;
Runtime id: 009c29d88397773555857c4117757916;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:32:19;
Channel: service-marketplace.WARNING;
Runtime id: 009c29d88397773555857c4117757916;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:32:19;
Channel: service-marketplace.EMERGENCY;
Runtime id: 009c29d88397773555857c4117757916;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:34:44;
Channel: service-marketplace.INFO;
Runtime id: 35eb325839679198b136c0a473444192;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:34:44;
Channel: service-marketplace.DEBUG;
Runtime id: 35eb325839679198b136c0a473444192;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:34:45 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:34:45;
Channel: service-marketplace.DEBUG;
Runtime id: 35eb325839679198b136c0a473444192;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:34:45;
Channel: service-marketplace.DEBUG;
Runtime id: 35eb325839679198b136c0a473444192;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:34:45;
Channel: service-marketplace.DEBUG;
Runtime id: 35eb325839679198b136c0a473444192;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:34:45;
Channel: service-marketplace.WARNING;
Runtime id: 35eb325839679198b136c0a473444192;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:34:45;
Channel: service-marketplace.EMERGENCY;
Runtime id: 35eb325839679198b136c0a473444192;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:35:12;
Channel: service-marketplace.INFO;
Runtime id: 0f402fe81b756b3d767b49c155b838ec;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:35:12;
Channel: service-marketplace.DEBUG;
Runtime id: 0f402fe81b756b3d767b49c155b838ec;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:35:13 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:35:13;
Channel: service-marketplace.DEBUG;
Runtime id: 0f402fe81b756b3d767b49c155b838ec;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:35:13;
Channel: service-marketplace.DEBUG;
Runtime id: 0f402fe81b756b3d767b49c155b838ec;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:35:13;
Channel: service-marketplace.DEBUG;
Runtime id: 0f402fe81b756b3d767b49c155b838ec;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:35:13;
Channel: service-marketplace.WARNING;
Runtime id: 0f402fe81b756b3d767b49c155b838ec;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:35:13;
Channel: service-marketplace.EMERGENCY;
Runtime id: 0f402fe81b756b3d767b49c155b838ec;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:36:08;
Channel: service-marketplace.INFO;
Runtime id: 23bf4744ffd8cce9c0e6fa25175d32de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:36:08;
Channel: service-marketplace.DEBUG;
Runtime id: 23bf4744ffd8cce9c0e6fa25175d32de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:36:08 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:36:08;
Channel: service-marketplace.DEBUG;
Runtime id: 23bf4744ffd8cce9c0e6fa25175d32de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:36:08;
Channel: service-marketplace.DEBUG;
Runtime id: 23bf4744ffd8cce9c0e6fa25175d32de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:36:08;
Channel: service-marketplace.DEBUG;
Runtime id: 23bf4744ffd8cce9c0e6fa25175d32de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:36:08;
Channel: service-marketplace.WARNING;
Runtime id: 23bf4744ffd8cce9c0e6fa25175d32de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:36:08;
Channel: service-marketplace.EMERGENCY;
Runtime id: 23bf4744ffd8cce9c0e6fa25175d32de;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:36:41;
Channel: service-marketplace.INFO;
Runtime id: e1894f94ad41af9f3f484a22dcd980c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:36:41;
Channel: service-marketplace.DEBUG;
Runtime id: e1894f94ad41af9f3f484a22dcd980c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:36:42 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:36:42;
Channel: service-marketplace.DEBUG;
Runtime id: e1894f94ad41af9f3f484a22dcd980c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:36:42;
Channel: service-marketplace.DEBUG;
Runtime id: e1894f94ad41af9f3f484a22dcd980c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:36:42;
Channel: service-marketplace.DEBUG;
Runtime id: e1894f94ad41af9f3f484a22dcd980c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:36:42;
Channel: service-marketplace.WARNING;
Runtime id: e1894f94ad41af9f3f484a22dcd980c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:36:42;
Channel: service-marketplace.EMERGENCY;
Runtime id: e1894f94ad41af9f3f484a22dcd980c9;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:37:59;
Channel: service-marketplace.INFO;
Runtime id: c2f1c5cc96d8576a24f5094d384bdc82;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:37:59;
Channel: service-marketplace.DEBUG;
Runtime id: c2f1c5cc96d8576a24f5094d384bdc82;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:37:59 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:37:59;
Channel: service-marketplace.DEBUG;
Runtime id: c2f1c5cc96d8576a24f5094d384bdc82;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:37:59;
Channel: service-marketplace.DEBUG;
Runtime id: c2f1c5cc96d8576a24f5094d384bdc82;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:37:59;
Channel: service-marketplace.DEBUG;
Runtime id: c2f1c5cc96d8576a24f5094d384bdc82;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:37:59;
Channel: service-marketplace.WARNING;
Runtime id: c2f1c5cc96d8576a24f5094d384bdc82;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:37:59;
Channel: service-marketplace.EMERGENCY;
Runtime id: c2f1c5cc96d8576a24f5094d384bdc82;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:38:27;
Channel: service-marketplace.INFO;
Runtime id: 510e3e77c276c33f176fe0513b1edb17;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:38:27;
Channel: service-marketplace.DEBUG;
Runtime id: 510e3e77c276c33f176fe0513b1edb17;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:38:27 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:38:27;
Channel: service-marketplace.DEBUG;
Runtime id: 510e3e77c276c33f176fe0513b1edb17;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:38:27;
Channel: service-marketplace.DEBUG;
Runtime id: 510e3e77c276c33f176fe0513b1edb17;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:38:27;
Channel: service-marketplace.DEBUG;
Runtime id: 510e3e77c276c33f176fe0513b1edb17;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:38:27;
Channel: service-marketplace.WARNING;
Runtime id: 510e3e77c276c33f176fe0513b1edb17;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:38:27;
Channel: service-marketplace.EMERGENCY;
Runtime id: 510e3e77c276c33f176fe0513b1edb17;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:42:02;
Channel: service-marketplace.INFO;
Runtime id: b00ed0f44802c5e2313a6d74291c9319;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:42:02;
Channel: service-marketplace.DEBUG;
Runtime id: b00ed0f44802c5e2313a6d74291c9319;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:42:02 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:42:02;
Channel: service-marketplace.DEBUG;
Runtime id: b00ed0f44802c5e2313a6d74291c9319;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:42:02;
Channel: service-marketplace.DEBUG;
Runtime id: b00ed0f44802c5e2313a6d74291c9319;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:42:02;
Channel: service-marketplace.DEBUG;
Runtime id: b00ed0f44802c5e2313a6d74291c9319;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:42:02;
Channel: service-marketplace.WARNING;
Runtime id: b00ed0f44802c5e2313a6d74291c9319;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:42:02;
Channel: service-marketplace.EMERGENCY;
Runtime id: b00ed0f44802c5e2313a6d74291c9319;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:47:08;
Channel: service-marketplace.INFO;
Runtime id: b815cdabe669861dd7fdabb8ca0ac54e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:47:08;
Channel: service-marketplace.DEBUG;
Runtime id: b815cdabe669861dd7fdabb8ca0ac54e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:47:08 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:47:08;
Channel: service-marketplace.DEBUG;
Runtime id: b815cdabe669861dd7fdabb8ca0ac54e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:47:08;
Channel: service-marketplace.DEBUG;
Runtime id: b815cdabe669861dd7fdabb8ca0ac54e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:47:08;
Channel: service-marketplace.DEBUG;
Runtime id: b815cdabe669861dd7fdabb8ca0ac54e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:47:08;
Channel: service-marketplace.WARNING;
Runtime id: b815cdabe669861dd7fdabb8ca0ac54e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:47:08;
Channel: service-marketplace.EMERGENCY;
Runtime id: b815cdabe669861dd7fdabb8ca0ac54e;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:47:35;
Channel: service-marketplace.INFO;
Runtime id: c0b2e744531e1bca26951f02c178064f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:47:35;
Channel: service-marketplace.DEBUG;
Runtime id: c0b2e744531e1bca26951f02c178064f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:47:36 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:47:36;
Channel: service-marketplace.DEBUG;
Runtime id: c0b2e744531e1bca26951f02c178064f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:47:36;
Channel: service-marketplace.DEBUG;
Runtime id: c0b2e744531e1bca26951f02c178064f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:47:36;
Channel: service-marketplace.DEBUG;
Runtime id: c0b2e744531e1bca26951f02c178064f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:47:36;
Channel: service-marketplace.WARNING;
Runtime id: c0b2e744531e1bca26951f02c178064f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:47:36;
Channel: service-marketplace.EMERGENCY;
Runtime id: c0b2e744531e1bca26951f02c178064f;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 17:53:28;
Channel: service-marketplace.INFO;
Runtime id: c7ce0c2ebb4de65dc9d2e35aaae5cded;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 17:53:28;
Channel: service-marketplace.DEBUG;
Runtime id: c7ce0c2ebb4de65dc9d2e35aaae5cded;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 13:53:29 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 17:53:29;
Channel: service-marketplace.DEBUG;
Runtime id: c7ce0c2ebb4de65dc9d2e35aaae5cded;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:53:29;
Channel: service-marketplace.DEBUG;
Runtime id: c7ce0c2ebb4de65dc9d2e35aaae5cded;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:53:29;
Channel: service-marketplace.DEBUG;
Runtime id: c7ce0c2ebb4de65dc9d2e35aaae5cded;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 17:53:29;
Channel: service-marketplace.WARNING;
Runtime id: c7ce0c2ebb4de65dc9d2e35aaae5cded;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 17:53:29;
Channel: service-marketplace.EMERGENCY;
Runtime id: c7ce0c2ebb4de65dc9d2e35aaae5cded;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\Set"

Time: 2019-09-05 18:00:00;
Channel: service-marketplace.INFO;
Runtime id: 1e5cc0c641f7f8fa45816e1fb8ad7768;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\Set
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "querySets": {
        "get_XC5_notifications": [
            0
        ]
    }
}

Time: 2019-09-05 18:00:00;
Channel: service-marketplace.DEBUG;
Runtime id: 1e5cc0c641f7f8fa45816e1fb8ad7768;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 14:00:00 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "799"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"get_XC5_notifications\":[{\"type\":\"module\",\"module\":\"XC-FacebookMarketing\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/facebookinsta.png\",\"title\":\"Advertise your products on Facebook and Instagram and see your sales grow\",\"description\":\"Try the free Facebook Ads and Instagram Ads addon for X-Cart\",\"link\":\"\",\"date\":\"1558040400\"},{\"type\":\"news\",\"module\":\"\",\"image\":\"https:\\/\\/my.x-cart.com\\/sites\\/default\\/files\\/news\\/images\\/GA1.png\",\"title\":\"FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.\",\"description\":\"\",\"link\":\"https:\\/\\/market.x-cart.com\\/addons\\/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup\",\"date\":\"1557262800\"}]}"
}

Time: 2019-09-05 18:00:00;
Channel: service-marketplace.DEBUG;
Runtime id: 1e5cc0c641f7f8fa45816e1fb8ad7768;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 18:00:00;
Channel: service-marketplace.DEBUG;
Runtime id: 1e5cc0c641f7f8fa45816e1fb8ad7768;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "get_XC5_notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 18:00:00;
Channel: service-marketplace.DEBUG;
Runtime id: 1e5cc0c641f7f8fa45816e1fb8ad7768;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace correctly received "XCart\Marketplace\Request\Set"

Time: 2019-09-05 18:00:00;
Channel: service-marketplace.INFO;
Runtime id: 1e5cc0c641f7f8fa45816e1fb8ad7768;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (formatted)

Context:
action => XCart\Marketplace\Request\Set
data => {
    "XCart\\Marketplace\\Request\\Notifications": [
        {
            "type": "module",
            "module": "XC-FacebookMarketing",
            "image": "https://my.x-cart.com/sites/default/files/news/images/facebookinsta.png",
            "title": "Advertise your products on Facebook and Instagram and see your sales grow",
            "description": "Try the free Facebook Ads and Instagram Ads addon for X-Cart",
            "link": "",
            "date": "1558040400"
        },
        {
            "type": "news",
            "module": "",
            "image": "https://my.x-cart.com/sites/default/files/news/images/GA1.png",
            "title": "FREE $150 Google credit goes with your first campaign. Install the addon, create your first campaign and get the credit for future Google Advertising campaigns.",
            "description": "",
            "link": "https://market.x-cart.com/addons/google-ads-for-xcart.html?utm_source=marketplace&amp;utm_medium=newsfeed-popup",
            "date": "1557262800"
        }
    ]
}

Time: 2019-09-05 18:00:00;
Channel: service-marketplace.DEBUG;
Runtime id: 1e5cc0c641f7f8fa45816e1fb8ad7768;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 18:00:27;
Channel: service-marketplace.INFO;
Runtime id: 96c46bfd3054d0a6ee403820cb8ec731;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 18:00:27;
Channel: service-marketplace.DEBUG;
Runtime id: 96c46bfd3054d0a6ee403820cb8ec731;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 14:00:27 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 18:00:28;
Channel: service-marketplace.DEBUG;
Runtime id: 96c46bfd3054d0a6ee403820cb8ec731;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:00:28;
Channel: service-marketplace.DEBUG;
Runtime id: 96c46bfd3054d0a6ee403820cb8ec731;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:00:28;
Channel: service-marketplace.DEBUG;
Runtime id: 96c46bfd3054d0a6ee403820cb8ec731;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:00:28;
Channel: service-marketplace.WARNING;
Runtime id: 96c46bfd3054d0a6ee403820cb8ec731;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 18:00:28;
Channel: service-marketplace.EMERGENCY;
Runtime id: 96c46bfd3054d0a6ee403820cb8ec731;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 18:01:00;
Channel: service-marketplace.INFO;
Runtime id: f62884528f18db665cfc067a38acff65;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 18:01:00;
Channel: service-marketplace.DEBUG;
Runtime id: f62884528f18db665cfc067a38acff65;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 14:01:00 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 18:01:01;
Channel: service-marketplace.DEBUG;
Runtime id: f62884528f18db665cfc067a38acff65;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:01:01;
Channel: service-marketplace.DEBUG;
Runtime id: f62884528f18db665cfc067a38acff65;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:01:01;
Channel: service-marketplace.DEBUG;
Runtime id: f62884528f18db665cfc067a38acff65;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:01:01;
Channel: service-marketplace.WARNING;
Runtime id: f62884528f18db665cfc067a38acff65;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 18:01:01;
Channel: service-marketplace.EMERGENCY;
Runtime id: f62884528f18db665cfc067a38acff65;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 18:01:30;
Channel: service-marketplace.INFO;
Runtime id: adddc9dec7f45941ca5ab88853592558;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 18:01:30;
Channel: service-marketplace.DEBUG;
Runtime id: adddc9dec7f45941ca5ab88853592558;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 14:01:30 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 18:01:30;
Channel: service-marketplace.DEBUG;
Runtime id: adddc9dec7f45941ca5ab88853592558;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:01:30;
Channel: service-marketplace.DEBUG;
Runtime id: adddc9dec7f45941ca5ab88853592558;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:01:30;
Channel: service-marketplace.DEBUG;
Runtime id: adddc9dec7f45941ca5ab88853592558;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:01:30;
Channel: service-marketplace.WARNING;
Runtime id: adddc9dec7f45941ca5ab88853592558;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 18:01:30;
Channel: service-marketplace.EMERGENCY;
Runtime id: adddc9dec7f45941ca5ab88853592558;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 18:01:52;
Channel: service-marketplace.INFO;
Runtime id: 77c1464eeae79f7620dde1eed3f8d042;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 18:01:52;
Channel: service-marketplace.DEBUG;
Runtime id: 77c1464eeae79f7620dde1eed3f8d042;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 14:01:52 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 18:01:52;
Channel: service-marketplace.DEBUG;
Runtime id: 77c1464eeae79f7620dde1eed3f8d042;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:01:52;
Channel: service-marketplace.DEBUG;
Runtime id: 77c1464eeae79f7620dde1eed3f8d042;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:01:52;
Channel: service-marketplace.DEBUG;
Runtime id: 77c1464eeae79f7620dde1eed3f8d042;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:01:52;
Channel: service-marketplace.WARNING;
Runtime id: 77c1464eeae79f7620dde1eed3f8d042;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 18:01:52;
Channel: service-marketplace.EMERGENCY;
Runtime id: 77c1464eeae79f7620dde1eed3f8d042;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 18:07:50;
Channel: service-marketplace.INFO;
Runtime id: f10883295dd9a6074c3abaf773b8240a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 18:07:50;
Channel: service-marketplace.DEBUG;
Runtime id: f10883295dd9a6074c3abaf773b8240a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 14:07:50 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 18:07:50;
Channel: service-marketplace.DEBUG;
Runtime id: f10883295dd9a6074c3abaf773b8240a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:07:50;
Channel: service-marketplace.DEBUG;
Runtime id: f10883295dd9a6074c3abaf773b8240a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:07:50;
Channel: service-marketplace.DEBUG;
Runtime id: f10883295dd9a6074c3abaf773b8240a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:07:50;
Channel: service-marketplace.WARNING;
Runtime id: f10883295dd9a6074c3abaf773b8240a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 18:07:50;
Channel: service-marketplace.EMERGENCY;
Runtime id: f10883295dd9a6074c3abaf773b8240a;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?/api;

Request to marketplace "XCart\Marketplace\Request\VersionInfo"

Time: 2019-09-05 18:08:26;
Channel: service-marketplace.INFO;
Runtime id: f4136563f5bdc262015ad71dbe292c85;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Request to marketplace

Context:
action => XCart\Marketplace\Request\VersionInfo
params => {
    "versionAPI": "2.3",
    "shopID": "1d5308033f4ed3069b7835308d5e9df5",
    "shopDomain": "xcarttest.com",
    "shopURL": "http://xcarttest.com",
    "currentCoreVersion": {
        "major": "5.4",
        "minor": "0",
        "build": "4"
    },
    "installation_lng": "en",
    "entities": {
        "XC-Service": {
            "author": "XC",
            "name": "Service",
            "versionFrom": "5.4.0.3",
            "versionTo": null,
            "infoType": [
                "changelog"
            ]
        }
    }
}

Time: 2019-09-05 18:08:26;
Channel: service-marketplace.DEBUG;
Runtime id: f4136563f5bdc262015ad71dbe292c85;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (raw)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "headers": {
        "server": [
            "nginx"
        ],
        "date": [
            "Thu, 05 Sep 2019 14:08:26 GMT"
        ],
        "content-type": [
            "application/json;charset=UTF-8"
        ],
        "content-length": [
            "81"
        ],
        "connection": [
            "close"
        ],
        "expires": [
            "Sun, 19 Nov 1978 05:00:00 GMT"
        ],
        "cache-control": [
            "no-cache, must-revalidate"
        ],
        "x-content-type-options": [
            "nosniff"
        ],
        "x-frame-options": [
            "sameorigin"
        ]
    },
    "body": "{\"error\":5000,\"message\":\"VersionFrom (5.4.0.3) cannot be more than versionTo ()\"}"
}

Time: 2019-09-05 18:08:26;
Channel: service-marketplace.DEBUG;
Runtime id: f4136563f5bdc262015ad71dbe292c85;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\VersionInfo
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:08:26;
Channel: service-marketplace.DEBUG;
Runtime id: f4136563f5bdc262015ad71dbe292c85;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Response from marketplace (parsed)

Context:
action => XCart\Marketplace\Request\Error
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:08:26;
Channel: service-marketplace.DEBUG;
Runtime id: f4136563f5bdc262015ad71dbe292c85;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

Invalid data received

Context:
action => get_version_info
data => {
    "error": 5000,
    "message": "VersionFrom (5.4.0.3) cannot be more than versionTo ()"
}

Time: 2019-09-05 18:08:26;
Channel: service-marketplace.WARNING;
Runtime id: f4136563f5bdc262015ad71dbe292c85;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

VersionFrom (5.4.0.3) cannot be more than versionTo ()

Time: 2019-09-05 18:08:26;
Channel: service-marketplace.EMERGENCY;
Runtime id: f4136563f5bdc262015ad71dbe292c85;
Server API: apache2handler; IP: 127.0.0.1;
Request method: POST;
URI: /service.php?%2Fapi;

