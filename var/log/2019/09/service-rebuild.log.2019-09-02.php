<?php die(); ?>
2019-09-02 15:08:11	[scenario5d6cf81b958e43.50226321]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 15:08:13	[scenario5d6cf81b958e43.50226321]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 15:08:13	[scenario5d6cf81b958e43.50226321]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 52bce8ec530abcb7282ad161bfc259d3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:08:13	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_first
2019-09-02 15:08:14	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_second
2019-09-02 15:08:19	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_third
2019-09-02 15:08:20	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_fourth
2019-09-02 15:08:23	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_fifth
2019-09-02 15:08:24	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_six
2019-09-02 15:08:25	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_seven
2019-09-02 15:08:27	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_nine
2019-09-02 15:08:28	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_ten
2019-09-02 15:08:29	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_eleven
2019-09-02 15:08:30	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_twelve
2019-09-02 15:08:39	[scenario5d6cf81b958e43.50226321]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 15:08:40	[scenario5d6cf81b958e43.50226321]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:08:40	[scenario5d6cf81b958e43.50226321]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 52bce8ec530abcb7282ad161bfc259d3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:08:41	[scenario5d6cf81b958e43.50226321]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:08:42	[scenario5d6cf81b958e43.50226321]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:08:43	[scenario5d6cf81b958e43.50226321]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:08:44	[scenario5d6cf81b958e43.50226321]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:08:44	[scenario5d6cf81b958e43.50226321]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 52bce8ec530abcb7282ad161bfc259d3
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:08:45	[scenario5d6cf81b958e43.50226321]	Execute X-Cart step: step_thirteen
2019-09-02 15:08:47	[scenario5d6cf81b958e43.50226321]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:13:04	[scenario5d6cf940db9e39.89226623]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 15:13:07	[scenario5d6cf940db9e39.89226623]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 15:13:07	[scenario5d6cf940db9e39.89226623]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 797193ca4a17935a8f31fb39d8349847
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:13:08	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_first
2019-09-02 15:13:09	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_second
2019-09-02 15:13:16	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_third
2019-09-02 15:13:17	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_fourth
2019-09-02 15:13:21	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_fifth
2019-09-02 15:13:22	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_six
2019-09-02 15:13:23	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_seven
2019-09-02 15:13:25	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_nine
2019-09-02 15:13:26	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_ten
2019-09-02 15:13:27	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_eleven
2019-09-02 15:13:28	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_twelve
2019-09-02 15:13:34	[scenario5d6cf940db9e39.89226623]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 15:13:35	[scenario5d6cf940db9e39.89226623]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:13:35	[scenario5d6cf940db9e39.89226623]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 797193ca4a17935a8f31fb39d8349847
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:13:36	[scenario5d6cf940db9e39.89226623]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:13:37	[scenario5d6cf940db9e39.89226623]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:13:38	[scenario5d6cf940db9e39.89226623]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:13:39	[scenario5d6cf940db9e39.89226623]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:13:39	[scenario5d6cf940db9e39.89226623]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 797193ca4a17935a8f31fb39d8349847
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:13:40	[scenario5d6cf940db9e39.89226623]	Execute X-Cart step: step_thirteen
2019-09-02 15:13:42	[scenario5d6cf940db9e39.89226623]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:14:52	[scenario5d6cf41bb6ad63.92871763]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:14:52	[scenario5d6cf41bb6ad63.92871763]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:14:53	[scenario5d6cf41bb6ad63.92871763]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:14:53	[scenario5d6cf41bb6ad63.92871763]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": false
    }
}

2019-09-02 15:14:53	[scenario5d6cf41bb6ad63.92871763]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a9e7fac5e13ace89a889a8b3b5d3b1a5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:14:53	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_first
2019-09-02 15:14:54	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_second
2019-09-02 15:14:59	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_third
2019-09-02 15:14:59	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_fourth
2019-09-02 15:15:01	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_fifth
2019-09-02 15:15:01	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_six
2019-09-02 15:15:02	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_seven
2019-09-02 15:15:04	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_nine
2019-09-02 15:15:04	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_ten
2019-09-02 15:15:04	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_eleven
2019-09-02 15:15:05	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_twelve
2019-09-02 15:15:10	[scenario5d6cf41bb6ad63.92871763]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:15:11	[scenario5d6cf41bb6ad63.92871763]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:15:11	[scenario5d6cf41bb6ad63.92871763]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:15:11	[scenario5d6cf41bb6ad63.92871763]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a9e7fac5e13ace89a889a8b3b5d3b1a5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:15:11	[scenario5d6cf41bb6ad63.92871763]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:15:12	[scenario5d6cf41bb6ad63.92871763]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:15:12	[scenario5d6cf41bb6ad63.92871763]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:15:12	[scenario5d6cf41bb6ad63.92871763]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:15:12	[scenario5d6cf41bb6ad63.92871763]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a9e7fac5e13ace89a889a8b3b5d3b1a5
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:15:12	[scenario5d6cf41bb6ad63.92871763]	Execute X-Cart step: step_thirteen
2019-09-02 15:15:14	[scenario5d6cf41bb6ad63.92871763]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:15:28	[scenario5d6cf9cbb9d208.27626260]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:15:28	[scenario5d6cf9cbb9d208.27626260]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:15:28	[scenario5d6cf9cbb9d208.27626260]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:15:28	[scenario5d6cf9cbb9d208.27626260]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 15:15:28	[scenario5d6cf9cbb9d208.27626260]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bb7a9f8fd3aa6d6a2694cfb02d4bb3f1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:15:28	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_first
2019-09-02 15:15:29	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_second
2019-09-02 15:15:31	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_third
2019-09-02 15:15:32	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_fourth
2019-09-02 15:15:34	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_fifth
2019-09-02 15:15:34	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_six
2019-09-02 15:15:35	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_seven
2019-09-02 15:15:36	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_nine
2019-09-02 15:15:36	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_ten
2019-09-02 15:15:37	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_eleven
2019-09-02 15:15:37	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_twelve
2019-09-02 15:15:42	[scenario5d6cf9cbb9d208.27626260]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:15:42	[scenario5d6cf9cbb9d208.27626260]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:15:42	[scenario5d6cf9cbb9d208.27626260]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:15:42	[scenario5d6cf9cbb9d208.27626260]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bb7a9f8fd3aa6d6a2694cfb02d4bb3f1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:15:42	[scenario5d6cf9cbb9d208.27626260]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:15:43	[scenario5d6cf9cbb9d208.27626260]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:15:43	[scenario5d6cf9cbb9d208.27626260]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:15:43	[scenario5d6cf9cbb9d208.27626260]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:15:43	[scenario5d6cf9cbb9d208.27626260]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bb7a9f8fd3aa6d6a2694cfb02d4bb3f1
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:15:43	[scenario5d6cf9cbb9d208.27626260]	Execute X-Cart step: step_thirteen
2019-09-02 15:15:44	[scenario5d6cf9cbb9d208.27626260]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:29:01	[scenario5d6cfcfdbc5a36.09352398]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 15:29:03	[scenario5d6cfcfdbc5a36.09352398]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 15:29:03	[scenario5d6cfcfdbc5a36.09352398]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0d3d0d692d44db50f1c50125b7cfd8e4
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:29:03	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_first
2019-09-02 15:29:04	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_second
2019-09-02 15:29:09	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_third
2019-09-02 15:29:10	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_fourth
2019-09-02 15:29:12	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_fifth
2019-09-02 15:29:12	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_six
2019-09-02 15:29:13	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_seven
2019-09-02 15:29:14	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_nine
2019-09-02 15:29:15	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_ten
2019-09-02 15:29:15	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_eleven
2019-09-02 15:29:15	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_twelve
2019-09-02 15:29:22	[scenario5d6cfcfdbc5a36.09352398]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 15:29:23	[scenario5d6cfcfdbc5a36.09352398]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:29:23	[scenario5d6cfcfdbc5a36.09352398]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 0d3d0d692d44db50f1c50125b7cfd8e4
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:29:23	[scenario5d6cfcfdbc5a36.09352398]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:29:23	[scenario5d6cfcfdbc5a36.09352398]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:29:23	[scenario5d6cfcfdbc5a36.09352398]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:29:24	[scenario5d6cfcfdbc5a36.09352398]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:29:24	[scenario5d6cfcfdbc5a36.09352398]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0d3d0d692d44db50f1c50125b7cfd8e4
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:29:24	[scenario5d6cfcfdbc5a36.09352398]	Execute X-Cart step: step_thirteen
2019-09-02 15:29:26	[scenario5d6cfcfdbc5a36.09352398]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:30:10	[scenario5d6cfcfeb13fe6.12011545]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:30:10	[scenario5d6cfcfeb13fe6.12011545]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:30:10	[scenario5d6cfcfeb13fe6.12011545]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:30:10	[scenario5d6cfcfeb13fe6.12011545]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": false
    }
}

2019-09-02 15:30:11	[scenario5d6cfcfeb13fe6.12011545]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 58e957e519790138f4ea4d05ba09c68f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:30:11	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_first
2019-09-02 15:30:11	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_second
2019-09-02 15:30:17	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_third
2019-09-02 15:30:17	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_fourth
2019-09-02 15:30:20	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_fifth
2019-09-02 15:30:20	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_six
2019-09-02 15:30:21	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_seven
2019-09-02 15:30:23	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_nine
2019-09-02 15:30:24	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_ten
2019-09-02 15:30:24	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_eleven
2019-09-02 15:30:24	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_twelve
2019-09-02 15:30:32	[scenario5d6cfcfeb13fe6.12011545]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:30:33	[scenario5d6cfcfeb13fe6.12011545]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:30:33	[scenario5d6cfcfeb13fe6.12011545]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:30:33	[scenario5d6cfcfeb13fe6.12011545]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 58e957e519790138f4ea4d05ba09c68f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:30:33	[scenario5d6cfcfeb13fe6.12011545]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:30:34	[scenario5d6cfcfeb13fe6.12011545]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:30:34	[scenario5d6cfcfeb13fe6.12011545]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:30:34	[scenario5d6cfcfeb13fe6.12011545]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:30:34	[scenario5d6cfcfeb13fe6.12011545]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 58e957e519790138f4ea4d05ba09c68f
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:30:35	[scenario5d6cfcfeb13fe6.12011545]	Execute X-Cart step: step_thirteen
2019-09-02 15:30:36	[scenario5d6cfcfeb13fe6.12011545]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:30:49	[scenario5d6cfd65972327.49540376]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:30:49	[scenario5d6cfd65972327.49540376]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:30:50	[scenario5d6cfd65972327.49540376]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:30:50	[scenario5d6cfd65972327.49540376]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 15:30:50	[scenario5d6cfd65972327.49540376]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1457d4ec5c625a7bac1932024744d50f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:30:50	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_first
2019-09-02 15:30:51	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_second
2019-09-02 15:30:55	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_third
2019-09-02 15:30:55	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_fourth
2019-09-02 15:30:59	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_fifth
2019-09-02 15:30:59	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_six
2019-09-02 15:31:00	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_seven
2019-09-02 15:31:02	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_nine
2019-09-02 15:31:03	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_ten
2019-09-02 15:31:03	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_eleven
2019-09-02 15:31:04	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_twelve
2019-09-02 15:31:11	[scenario5d6cfd65972327.49540376]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:31:11	[scenario5d6cfd65972327.49540376]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:31:12	[scenario5d6cfd65972327.49540376]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:31:12	[scenario5d6cfd65972327.49540376]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1457d4ec5c625a7bac1932024744d50f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:31:12	[scenario5d6cfd65972327.49540376]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:31:12	[scenario5d6cfd65972327.49540376]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:31:13	[scenario5d6cfd65972327.49540376]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:31:13	[scenario5d6cfd65972327.49540376]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:31:13	[scenario5d6cfd65972327.49540376]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1457d4ec5c625a7bac1932024744d50f
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:31:13	[scenario5d6cfd65972327.49540376]	Execute X-Cart step: step_thirteen
2019-09-02 15:31:15	[scenario5d6cfd65972327.49540376]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:33:12	[scenario5d6cfdf843f777.40553172]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 15:33:14	[scenario5d6cfdf843f777.40553172]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 15:33:14	[scenario5d6cfdf843f777.40553172]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 146105f18129958c9030125e9580bbd3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:33:15	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_first
2019-09-02 15:33:15	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_second
2019-09-02 15:33:21	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_third
2019-09-02 15:33:22	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_fourth
2019-09-02 15:33:25	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_fifth
2019-09-02 15:33:25	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_six
2019-09-02 15:33:26	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_seven
2019-09-02 15:33:28	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_nine
2019-09-02 15:33:29	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_ten
2019-09-02 15:33:29	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_eleven
2019-09-02 15:33:29	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_twelve
2019-09-02 15:33:37	[scenario5d6cfdf843f777.40553172]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 15:33:38	[scenario5d6cfdf843f777.40553172]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:33:38	[scenario5d6cfdf843f777.40553172]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 146105f18129958c9030125e9580bbd3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:33:38	[scenario5d6cfdf843f777.40553172]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:33:39	[scenario5d6cfdf843f777.40553172]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:33:39	[scenario5d6cfdf843f777.40553172]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:33:40	[scenario5d6cfdf843f777.40553172]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:33:40	[scenario5d6cfdf843f777.40553172]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 146105f18129958c9030125e9580bbd3
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:33:40	[scenario5d6cfdf843f777.40553172]	Execute X-Cart step: step_thirteen
2019-09-02 15:33:42	[scenario5d6cfdf843f777.40553172]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:34:31	[scenario5d6cfda91b8e20.38511608]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:34:31	[scenario5d6cfda91b8e20.38511608]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:34:31	[scenario5d6cfda91b8e20.38511608]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:34:31	[scenario5d6cfda91b8e20.38511608]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": false
    }
}

2019-09-02 15:34:31	[scenario5d6cfda91b8e20.38511608]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5f089af12815bf7012e6c3fffe8f33c1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:34:31	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_first
2019-09-02 15:34:32	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_second
2019-09-02 15:34:37	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_third
2019-09-02 15:34:37	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_fourth
2019-09-02 15:34:39	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_fifth
2019-09-02 15:34:39	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_six
2019-09-02 15:34:40	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_seven
2019-09-02 15:34:41	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_nine
2019-09-02 15:34:42	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_ten
2019-09-02 15:34:42	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_eleven
2019-09-02 15:34:42	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_twelve
2019-09-02 15:34:48	[scenario5d6cfda91b8e20.38511608]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:34:48	[scenario5d6cfda91b8e20.38511608]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:34:48	[scenario5d6cfda91b8e20.38511608]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:34:48	[scenario5d6cfda91b8e20.38511608]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5f089af12815bf7012e6c3fffe8f33c1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:34:48	[scenario5d6cfda91b8e20.38511608]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:34:49	[scenario5d6cfda91b8e20.38511608]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:34:49	[scenario5d6cfda91b8e20.38511608]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:34:49	[scenario5d6cfda91b8e20.38511608]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:34:49	[scenario5d6cfda91b8e20.38511608]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5f089af12815bf7012e6c3fffe8f33c1
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:34:50	[scenario5d6cfda91b8e20.38511608]	Execute X-Cart step: step_thirteen
2019-09-02 15:34:52	[scenario5d6cfda91b8e20.38511608]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:35:39	[scenario5d6cfe601d0be1.05652639]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:35:39	[scenario5d6cfe601d0be1.05652639]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:35:39	[scenario5d6cfe601d0be1.05652639]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:35:39	[scenario5d6cfe601d0be1.05652639]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 15:35:39	[scenario5d6cfe601d0be1.05652639]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1a3aff160ae8786059c739b8097637fc
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:35:39	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_first
2019-09-02 15:35:40	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_second
2019-09-02 15:35:42	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_third
2019-09-02 15:35:43	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_fourth
2019-09-02 15:35:45	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_fifth
2019-09-02 15:35:45	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_six
2019-09-02 15:35:45	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_seven
2019-09-02 15:35:47	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_nine
2019-09-02 15:35:47	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_ten
2019-09-02 15:35:47	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_eleven
2019-09-02 15:35:48	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_twelve
2019-09-02 15:35:52	[scenario5d6cfe601d0be1.05652639]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:35:53	[scenario5d6cfe601d0be1.05652639]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:35:53	[scenario5d6cfe601d0be1.05652639]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:35:53	[scenario5d6cfe601d0be1.05652639]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1a3aff160ae8786059c739b8097637fc
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:35:53	[scenario5d6cfe601d0be1.05652639]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:35:53	[scenario5d6cfe601d0be1.05652639]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:35:53	[scenario5d6cfe601d0be1.05652639]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:35:54	[scenario5d6cfe601d0be1.05652639]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:35:54	[scenario5d6cfe601d0be1.05652639]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1a3aff160ae8786059c739b8097637fc
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:35:54	[scenario5d6cfe601d0be1.05652639]	Execute X-Cart step: step_thirteen
2019-09-02 15:35:55	[scenario5d6cfe601d0be1.05652639]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:37:10	[scenario5d6cfede301aa4.69623046]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:37:10	[scenario5d6cfede301aa4.69623046]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:37:11	[scenario5d6cfede301aa4.69623046]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:37:11	[scenario5d6cfede301aa4.69623046]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-02 15:37:11	[scenario5d6cfede301aa4.69623046]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 43324fc146c30807e13836503ee5682f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:37:12	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_first
2019-09-02 15:37:12	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_second
2019-09-02 15:37:17	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_third
2019-09-02 15:37:18	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_fourth
2019-09-02 15:37:21	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_fifth
2019-09-02 15:37:21	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_six
2019-09-02 15:37:23	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_seven
2019-09-02 15:37:25	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_nine
2019-09-02 15:37:25	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_ten
2019-09-02 15:37:26	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_eleven
2019-09-02 15:37:26	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_twelve
2019-09-02 15:37:34	[scenario5d6cfede301aa4.69623046]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:37:34	[scenario5d6cfede301aa4.69623046]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
    "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
]

2019-09-02 15:37:34	[scenario5d6cfede301aa4.69623046]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:37:34	[scenario5d6cfede301aa4.69623046]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:37:35	[scenario5d6cfede301aa4.69623046]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:37:35	[scenario5d6cfede301aa4.69623046]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 43324fc146c30807e13836503ee5682f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:37:35	[scenario5d6cfede301aa4.69623046]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:37:35	[scenario5d6cfede301aa4.69623046]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:37:36	[scenario5d6cfede301aa4.69623046]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:37:36	[scenario5d6cfede301aa4.69623046]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:37:36	[scenario5d6cfede301aa4.69623046]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 43324fc146c30807e13836503ee5682f
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:37:36	[scenario5d6cfede301aa4.69623046]	Execute X-Cart step: step_thirteen
2019-09-02 15:37:38	[scenario5d6cfede301aa4.69623046]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:38:16	[scenario5d6cff28389065.47701160]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 15:38:17	[scenario5d6cff28389065.47701160]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-02 15:38:17	[scenario5d6cff28389065.47701160]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6a22a09f91bb5ef848cac041a3a6d560
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:38:17	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_first
2019-09-02 15:38:18	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_second
2019-09-02 15:38:22	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_third
2019-09-02 15:38:22	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_fourth
2019-09-02 15:38:24	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_fifth
2019-09-02 15:38:24	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_six
2019-09-02 15:38:25	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_seven
2019-09-02 15:38:26	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_nine
2019-09-02 15:38:27	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_ten
2019-09-02 15:38:27	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_eleven
2019-09-02 15:38:27	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_twelve
2019-09-02 15:38:32	[scenario5d6cff28389065.47701160]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 15:38:33	[scenario5d6cff28389065.47701160]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:38:33	[scenario5d6cff28389065.47701160]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6a22a09f91bb5ef848cac041a3a6d560
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:38:33	[scenario5d6cff28389065.47701160]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:38:33	[scenario5d6cff28389065.47701160]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:38:34	[scenario5d6cff28389065.47701160]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:38:34	[scenario5d6cff28389065.47701160]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:38:34	[scenario5d6cff28389065.47701160]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6a22a09f91bb5ef848cac041a3a6d560
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:38:34	[scenario5d6cff28389065.47701160]	Execute X-Cart step: step_thirteen
2019-09-02 15:38:35	[scenario5d6cff28389065.47701160]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:42:23	[scenario5d6d001f0e95c0.10407966]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 15:42:24	[scenario5d6d001f0e95c0.10407966]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-02 15:42:24	[scenario5d6d001f0e95c0.10407966]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5ee3ce23e5cbda5c7c55959818b5e9db
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:42:24	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_first
2019-09-02 15:42:25	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_second
2019-09-02 15:42:30	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_third
2019-09-02 15:42:31	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_fourth
2019-09-02 15:42:33	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_fifth
2019-09-02 15:42:33	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_six
2019-09-02 15:42:34	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_seven
2019-09-02 15:42:35	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_nine
2019-09-02 15:42:36	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_ten
2019-09-02 15:42:36	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_eleven
2019-09-02 15:42:36	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_twelve
2019-09-02 15:42:42	[scenario5d6d001f0e95c0.10407966]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 15:42:42	[scenario5d6d001f0e95c0.10407966]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-02 15:42:42	[scenario5d6d001f0e95c0.10407966]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5ee3ce23e5cbda5c7c55959818b5e9db
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:42:42	[scenario5d6d001f0e95c0.10407966]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:42:43	[scenario5d6d001f0e95c0.10407966]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:42:43	[scenario5d6d001f0e95c0.10407966]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:42:43	[scenario5d6d001f0e95c0.10407966]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:42:43	[scenario5d6d001f0e95c0.10407966]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5ee3ce23e5cbda5c7c55959818b5e9db
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:42:43	[scenario5d6d001f0e95c0.10407966]	Execute X-Cart step: step_thirteen
2019-09-02 15:42:45	[scenario5d6d001f0e95c0.10407966]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:43:03	[scenario5d6cff13f20cc2.51353551]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:43:03	[scenario5d6cff13f20cc2.51353551]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:43:04	[scenario5d6cff13f20cc2.51353551]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:43:04	[scenario5d6cff13f20cc2.51353551]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 15:43:04	[scenario5d6cff13f20cc2.51353551]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 915a73f7a980194944ffce4b4f32ea50
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:43:04	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_first
2019-09-02 15:43:05	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_second
2019-09-02 15:43:07	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_third
2019-09-02 15:43:08	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_fourth
2019-09-02 15:43:10	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_fifth
2019-09-02 15:43:10	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_six
2019-09-02 15:43:11	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_seven
2019-09-02 15:43:12	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_nine
2019-09-02 15:43:13	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_ten
2019-09-02 15:43:13	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_eleven
2019-09-02 15:43:13	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_twelve
2019-09-02 15:43:18	[scenario5d6cff13f20cc2.51353551]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:43:19	[scenario5d6cff13f20cc2.51353551]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:43:19	[scenario5d6cff13f20cc2.51353551]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:43:19	[scenario5d6cff13f20cc2.51353551]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 915a73f7a980194944ffce4b4f32ea50
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:43:19	[scenario5d6cff13f20cc2.51353551]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:43:19	[scenario5d6cff13f20cc2.51353551]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:43:19	[scenario5d6cff13f20cc2.51353551]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:43:20	[scenario5d6cff13f20cc2.51353551]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:43:20	[scenario5d6cff13f20cc2.51353551]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 915a73f7a980194944ffce4b4f32ea50
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:43:20	[scenario5d6cff13f20cc2.51353551]	Execute X-Cart step: step_thirteen
2019-09-02 15:43:21	[scenario5d6cff13f20cc2.51353551]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:47:15	[scenario5d6d0062408a69.49778008]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:47:15	[scenario5d6d0062408a69.49778008]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:47:15	[scenario5d6d0062408a69.49778008]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:47:15	[scenario5d6d0062408a69.49778008]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-02 15:47:15	[scenario5d6d0062408a69.49778008]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ad99b410a33c58405282b20de703daae
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:47:16	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_first
2019-09-02 15:47:16	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_second
2019-09-02 15:47:21	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_third
2019-09-02 15:47:22	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_fourth
2019-09-02 15:47:24	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_fifth
2019-09-02 15:47:25	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_six
2019-09-02 15:47:26	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_seven
2019-09-02 15:47:27	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_nine
2019-09-02 15:47:27	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_ten
2019-09-02 15:47:28	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_eleven
2019-09-02 15:47:28	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_twelve
2019-09-02 15:47:35	[scenario5d6d0062408a69.49778008]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:47:35	[scenario5d6d0062408a69.49778008]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
]

2019-09-02 15:47:35	[scenario5d6d0062408a69.49778008]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:47:35	[scenario5d6d0062408a69.49778008]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:47:35	[scenario5d6d0062408a69.49778008]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:47:35	[scenario5d6d0062408a69.49778008]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ad99b410a33c58405282b20de703daae
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:47:36	[scenario5d6d0062408a69.49778008]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:47:36	[scenario5d6d0062408a69.49778008]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:47:36	[scenario5d6d0062408a69.49778008]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:47:36	[scenario5d6d0062408a69.49778008]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:47:36	[scenario5d6d0062408a69.49778008]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ad99b410a33c58405282b20de703daae
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:47:37	[scenario5d6d0062408a69.49778008]	Execute X-Cart step: step_thirteen
2019-09-02 15:47:39	[scenario5d6d0062408a69.49778008]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:52:36	[scenario5d6d0284d05711.44828164]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 15:52:38	[scenario5d6d0284d05711.44828164]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-02 15:52:38	[scenario5d6d0284d05711.44828164]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fedc6271d9deee922d801b7378b86445
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:52:38	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_first
2019-09-02 15:52:39	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_second
2019-09-02 15:52:44	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_third
2019-09-02 15:52:44	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_fourth
2019-09-02 15:52:46	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_fifth
2019-09-02 15:52:46	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_six
2019-09-02 15:52:47	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_seven
2019-09-02 15:52:48	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_nine
2019-09-02 15:52:49	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_ten
2019-09-02 15:52:49	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_eleven
2019-09-02 15:52:49	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_twelve
2019-09-02 15:52:55	[scenario5d6d0284d05711.44828164]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 15:52:56	[scenario5d6d0284d05711.44828164]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-02 15:52:56	[scenario5d6d0284d05711.44828164]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => fedc6271d9deee922d801b7378b86445
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:52:56	[scenario5d6d0284d05711.44828164]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:52:56	[scenario5d6d0284d05711.44828164]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:52:57	[scenario5d6d0284d05711.44828164]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:52:57	[scenario5d6d0284d05711.44828164]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:52:57	[scenario5d6d0284d05711.44828164]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fedc6271d9deee922d801b7378b86445
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:52:57	[scenario5d6d0284d05711.44828164]	Execute X-Cart step: step_thirteen
2019-09-02 15:52:59	[scenario5d6d0284d05711.44828164]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 15:53:32	[scenario5d6d015de48870.30602080]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:53:32	[scenario5d6d015de48870.30602080]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:53:32	[scenario5d6d015de48870.30602080]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:53:32	[scenario5d6d015de48870.30602080]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 15:53:32	[scenario5d6d015de48870.30602080]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d8489cc467e619e3e2c473774cc0e968
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 15:53:33	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_first
2019-09-02 15:53:33	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_second
2019-09-02 15:53:35	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_third
2019-09-02 15:53:36	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_fourth
2019-09-02 15:53:38	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_fifth
2019-09-02 15:53:38	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_six
2019-09-02 15:53:39	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_seven
2019-09-02 15:53:40	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_nine
2019-09-02 15:53:41	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_ten
2019-09-02 15:53:41	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_eleven
2019-09-02 15:53:41	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_twelve
2019-09-02 15:53:46	[scenario5d6d015de48870.30602080]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 15:53:46	[scenario5d6d015de48870.30602080]	Data updated: TrueMachine-AgeVerification
2019-09-02 15:53:46	[scenario5d6d015de48870.30602080]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 15:53:46	[scenario5d6d015de48870.30602080]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d8489cc467e619e3e2c473774cc0e968
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 15:53:46	[scenario5d6d015de48870.30602080]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 15:53:47	[scenario5d6d015de48870.30602080]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 15:53:47	[scenario5d6d015de48870.30602080]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 15:53:47	[scenario5d6d015de48870.30602080]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 15:53:47	[scenario5d6d015de48870.30602080]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d8489cc467e619e3e2c473774cc0e968
Context transitions => [
    "step_thirteen"
]

2019-09-02 15:53:47	[scenario5d6d015de48870.30602080]	Execute X-Cart step: step_thirteen
2019-09-02 15:53:49	[scenario5d6d015de48870.30602080]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:07:29	[scenario5d6d0601269c51.47829792]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 16:07:30	[scenario5d6d0601269c51.47829792]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 16:07:30	[scenario5d6d0601269c51.47829792]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 26d290b1c51267b8149091116dba0e5c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:07:30	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_first
2019-09-02 16:07:31	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_second
2019-09-02 16:07:36	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_third
2019-09-02 16:07:37	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_fourth
2019-09-02 16:07:39	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_fifth
2019-09-02 16:07:39	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_six
2019-09-02 16:07:40	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_seven
2019-09-02 16:07:41	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_nine
2019-09-02 16:07:42	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_ten
2019-09-02 16:07:42	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_eleven
2019-09-02 16:07:42	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_twelve
2019-09-02 16:07:48	[scenario5d6d0601269c51.47829792]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 16:07:48	[scenario5d6d0601269c51.47829792]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:07:48	[scenario5d6d0601269c51.47829792]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 26d290b1c51267b8149091116dba0e5c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:07:48	[scenario5d6d0601269c51.47829792]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:07:49	[scenario5d6d0601269c51.47829792]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:07:49	[scenario5d6d0601269c51.47829792]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:07:49	[scenario5d6d0601269c51.47829792]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:07:49	[scenario5d6d0601269c51.47829792]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 26d290b1c51267b8149091116dba0e5c
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:07:49	[scenario5d6d0601269c51.47829792]	Execute X-Cart step: step_thirteen
2019-09-02 16:07:51	[scenario5d6d0601269c51.47829792]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:23:25	[scenario5d6d09bde8d618.65954275]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 16:23:28	[scenario5d6d09bde8d618.65954275]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 16:23:28	[scenario5d6d09bde8d618.65954275]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d693c6d53b445fc9653c4cc85c141994
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:23:28	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_first
2019-09-02 16:23:28	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_second
2019-09-02 16:23:34	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_third
2019-09-02 16:23:35	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_fourth
2019-09-02 16:23:40	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_fifth
2019-09-02 16:23:40	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_six
2019-09-02 16:23:41	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_seven
2019-09-02 16:23:43	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_nine
2019-09-02 16:23:44	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_ten
2019-09-02 16:23:44	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_eleven
2019-09-02 16:23:45	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_twelve
2019-09-02 16:23:53	[scenario5d6d09bde8d618.65954275]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 16:23:54	[scenario5d6d09bde8d618.65954275]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:23:54	[scenario5d6d09bde8d618.65954275]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d693c6d53b445fc9653c4cc85c141994
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:23:54	[scenario5d6d09bde8d618.65954275]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:23:55	[scenario5d6d09bde8d618.65954275]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:23:55	[scenario5d6d09bde8d618.65954275]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:23:55	[scenario5d6d09bde8d618.65954275]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:23:55	[scenario5d6d09bde8d618.65954275]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d693c6d53b445fc9653c4cc85c141994
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:23:56	[scenario5d6d09bde8d618.65954275]	Execute X-Cart step: step_thirteen
2019-09-02 16:23:58	[scenario5d6d09bde8d618.65954275]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:24:22	[scenario5d6d02ea5023c2.44420136]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:24:22	[scenario5d6d02ea5023c2.44420136]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:24:23	[scenario5d6d02ea5023c2.44420136]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:24:23	[scenario5d6d02ea5023c2.44420136]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-02 16:24:23	[scenario5d6d02ea5023c2.44420136]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2c77a2415b1582cbd5850d995a076a38
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:24:23	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_first
2019-09-02 16:24:24	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_second
2019-09-02 16:24:29	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_third
2019-09-02 16:24:29	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_fourth
2019-09-02 16:24:33	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_fifth
2019-09-02 16:24:33	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_six
2019-09-02 16:24:34	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_seven
2019-09-02 16:24:36	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_nine
2019-09-02 16:24:37	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_ten
2019-09-02 16:24:37	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_eleven
2019-09-02 16:24:37	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_twelve
2019-09-02 16:24:45	[scenario5d6d02ea5023c2.44420136]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:24:45	[scenario5d6d02ea5023c2.44420136]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
    "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
]

2019-09-02 16:24:45	[scenario5d6d02ea5023c2.44420136]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:24:46	[scenario5d6d02ea5023c2.44420136]	Data updated: TrueMachine-AgeVerification
2019-09-02 16:24:46	[scenario5d6d02ea5023c2.44420136]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:24:46	[scenario5d6d02ea5023c2.44420136]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2c77a2415b1582cbd5850d995a076a38
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:24:46	[scenario5d6d02ea5023c2.44420136]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:24:47	[scenario5d6d02ea5023c2.44420136]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:24:47	[scenario5d6d02ea5023c2.44420136]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:24:47	[scenario5d6d02ea5023c2.44420136]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:24:47	[scenario5d6d02ea5023c2.44420136]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2c77a2415b1582cbd5850d995a076a38
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:24:47	[scenario5d6d02ea5023c2.44420136]	Execute X-Cart step: step_thirteen
2019-09-02 16:24:50	[scenario5d6d02ea5023c2.44420136]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:27:56	[scenario5d6d0accbeba78.65149047]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 16:27:58	[scenario5d6d0accbeba78.65149047]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-02 16:27:58	[scenario5d6d0accbeba78.65149047]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1d0622d41ab5b32e050d37205578c1d5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:27:58	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_first
2019-09-02 16:27:59	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_second
2019-09-02 16:28:03	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_third
2019-09-02 16:28:04	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_fourth
2019-09-02 16:28:06	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_fifth
2019-09-02 16:28:06	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_six
2019-09-02 16:28:07	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_seven
2019-09-02 16:28:08	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_nine
2019-09-02 16:28:09	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_ten
2019-09-02 16:28:09	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_eleven
2019-09-02 16:28:09	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_twelve
2019-09-02 16:28:15	[scenario5d6d0accbeba78.65149047]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 16:28:15	[scenario5d6d0accbeba78.65149047]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "XCExample-AgeVerification"
]
Context missing => []

2019-09-02 16:28:15	[scenario5d6d0accbeba78.65149047]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1d0622d41ab5b32e050d37205578c1d5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:28:16	[scenario5d6d0accbeba78.65149047]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:28:16	[scenario5d6d0accbeba78.65149047]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:28:16	[scenario5d6d0accbeba78.65149047]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:28:17	[scenario5d6d0accbeba78.65149047]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:28:17	[scenario5d6d0accbeba78.65149047]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1d0622d41ab5b32e050d37205578c1d5
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:28:17	[scenario5d6d0accbeba78.65149047]	Execute X-Cart step: step_thirteen
2019-09-02 16:28:18	[scenario5d6d0accbeba78.65149047]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:28:37	[scenario5d6d0a1721a353.06198669]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:28:37	[scenario5d6d0a1721a353.06198669]	Update script transitions
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:28:37	[scenario5d6d0a1721a353.06198669]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:28:37	[scenario5d6d0a1721a353.06198669]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false,
        "AgeVerification": true
    }
}

2019-09-02 16:28:37	[scenario5d6d0a1721a353.06198669]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 48f11883a9dd04830864ce7200e656ba
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:28:38	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_first
2019-09-02 16:28:38	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_second
2019-09-02 16:28:41	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_third
2019-09-02 16:28:42	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_fourth
2019-09-02 16:28:44	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_fifth
2019-09-02 16:28:45	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_six
2019-09-02 16:28:45	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_seven
2019-09-02 16:28:47	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_nine
2019-09-02 16:28:47	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_ten
2019-09-02 16:28:47	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_eleven
2019-09-02 16:28:48	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_twelve
2019-09-02 16:28:53	[scenario5d6d0a1721a353.06198669]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:28:53	[scenario5d6d0a1721a353.06198669]	Data updated: XCExample-AgeVerification
2019-09-02 16:28:53	[scenario5d6d0a1721a353.06198669]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:28:53	[scenario5d6d0a1721a353.06198669]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 48f11883a9dd04830864ce7200e656ba
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:28:53	[scenario5d6d0a1721a353.06198669]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:28:54	[scenario5d6d0a1721a353.06198669]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:28:54	[scenario5d6d0a1721a353.06198669]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:28:54	[scenario5d6d0a1721a353.06198669]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:28:54	[scenario5d6d0a1721a353.06198669]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 48f11883a9dd04830864ce7200e656ba
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:28:54	[scenario5d6d0a1721a353.06198669]	Execute X-Cart step: step_thirteen
2019-09-02 16:28:56	[scenario5d6d0a1721a353.06198669]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:32:14	[scenario5d6d09bf38afb2.10486094]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:32:14	[scenario5d6d09bf38afb2.10486094]	Update script transitions
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:32:14	[scenario5d6d09bf38afb2.10486094]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:32:14	[scenario5d6d09bf38afb2.10486094]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false,
        "AgeVerification": "remove"
    }
}

2019-09-02 16:32:14	[scenario5d6d09bf38afb2.10486094]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9a20d9249a2196d170402d2640a1bb65
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:32:14	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_first
2019-09-02 16:32:15	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_second
2019-09-02 16:32:20	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_third
2019-09-02 16:32:21	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_fourth
2019-09-02 16:32:23	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_fifth
2019-09-02 16:32:23	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_six
2019-09-02 16:32:24	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_seven
2019-09-02 16:32:25	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_nine
2019-09-02 16:32:26	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_ten
2019-09-02 16:32:26	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_eleven
2019-09-02 16:32:27	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_twelve
2019-09-02 16:32:33	[scenario5d6d09bf38afb2.10486094]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:32:33	[scenario5d6d09bf38afb2.10486094]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
]

2019-09-02 16:32:33	[scenario5d6d09bf38afb2.10486094]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "XCExample-AgeVerification": {
        "id": "XCExample-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "XCExample-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "XCExample",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/XCExample/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:32:33	[scenario5d6d09bf38afb2.10486094]	Data updated: XCExample-AgeVerification
2019-09-02 16:32:33	[scenario5d6d09bf38afb2.10486094]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:32:33	[scenario5d6d09bf38afb2.10486094]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9a20d9249a2196d170402d2640a1bb65
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:32:34	[scenario5d6d09bf38afb2.10486094]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:32:34	[scenario5d6d09bf38afb2.10486094]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:32:34	[scenario5d6d09bf38afb2.10486094]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:32:34	[scenario5d6d09bf38afb2.10486094]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:32:34	[scenario5d6d09bf38afb2.10486094]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9a20d9249a2196d170402d2640a1bb65
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:32:35	[scenario5d6d09bf38afb2.10486094]	Execute X-Cart step: step_thirteen
2019-09-02 16:32:37	[scenario5d6d09bf38afb2.10486094]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:36:39	[scenario5d6d0cd7a7e0c6.98401478]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 16:36:41	[scenario5d6d0cd7a7e0c6.98401478]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-02 16:36:41	[scenario5d6d0cd7a7e0c6.98401478]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8e40b2e933fea7998eb69e5db800ac5e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:36:41	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_first
2019-09-02 16:36:42	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_second
2019-09-02 16:36:47	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_third
2019-09-02 16:36:48	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_fourth
2019-09-02 16:36:50	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_fifth
2019-09-02 16:36:50	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_six
2019-09-02 16:36:51	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_seven
2019-09-02 16:36:52	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_nine
2019-09-02 16:36:53	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_ten
2019-09-02 16:36:53	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_eleven
2019-09-02 16:36:53	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_twelve
2019-09-02 16:36:59	[scenario5d6d0cd7a7e0c6.98401478]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 16:37:00	[scenario5d6d0cd7a7e0c6.98401478]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-02 16:37:00	[scenario5d6d0cd7a7e0c6.98401478]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8e40b2e933fea7998eb69e5db800ac5e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:37:00	[scenario5d6d0cd7a7e0c6.98401478]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:37:00	[scenario5d6d0cd7a7e0c6.98401478]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:37:01	[scenario5d6d0cd7a7e0c6.98401478]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:37:01	[scenario5d6d0cd7a7e0c6.98401478]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:37:01	[scenario5d6d0cd7a7e0c6.98401478]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8e40b2e933fea7998eb69e5db800ac5e
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:37:01	[scenario5d6d0cd7a7e0c6.98401478]	Execute X-Cart step: step_thirteen
2019-09-02 16:37:03	[scenario5d6d0cd7a7e0c6.98401478]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:37:33	[scenario5d6d0cd8a4abb7.79545252]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:37:33	[scenario5d6d0cd8a4abb7.79545252]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:37:33	[scenario5d6d0cd8a4abb7.79545252]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:37:33	[scenario5d6d0cd8a4abb7.79545252]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 16:37:33	[scenario5d6d0cd8a4abb7.79545252]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ccf604552f6cfdf9b7dd5200523d0e6a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:37:33	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_first
2019-09-02 16:37:34	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_second
2019-09-02 16:37:37	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_third
2019-09-02 16:37:37	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_fourth
2019-09-02 16:37:39	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_fifth
2019-09-02 16:37:39	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_six
2019-09-02 16:37:40	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_seven
2019-09-02 16:37:42	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_nine
2019-09-02 16:37:42	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_ten
2019-09-02 16:37:42	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_eleven
2019-09-02 16:37:43	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_twelve
2019-09-02 16:37:48	[scenario5d6d0cd8a4abb7.79545252]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:37:48	[scenario5d6d0cd8a4abb7.79545252]	Data updated: TrueMachine-AgeVerification
2019-09-02 16:37:48	[scenario5d6d0cd8a4abb7.79545252]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:37:48	[scenario5d6d0cd8a4abb7.79545252]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ccf604552f6cfdf9b7dd5200523d0e6a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:37:48	[scenario5d6d0cd8a4abb7.79545252]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:37:49	[scenario5d6d0cd8a4abb7.79545252]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:37:49	[scenario5d6d0cd8a4abb7.79545252]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:37:49	[scenario5d6d0cd8a4abb7.79545252]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:37:49	[scenario5d6d0cd8a4abb7.79545252]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ccf604552f6cfdf9b7dd5200523d0e6a
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:37:49	[scenario5d6d0cd8a4abb7.79545252]	Execute X-Cart step: step_thirteen
2019-09-02 16:37:51	[scenario5d6d0cd8a4abb7.79545252]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:47:54	[scenario5d6d0f6f0b7bb6.58852824]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:47:54	[scenario5d6d0f6f0b7bb6.58852824]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:47:54	[scenario5d6d0f6f0b7bb6.58852824]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:47:54	[scenario5d6d0f6f0b7bb6.58852824]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": false
    }
}

2019-09-02 16:47:54	[scenario5d6d0f6f0b7bb6.58852824]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6d679775faadbd2ed23c976bf0bbfa76
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:47:55	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_first
2019-09-02 16:47:55	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_second
2019-09-02 16:48:00	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_third
2019-09-02 16:48:01	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_fourth
2019-09-02 16:48:03	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_fifth
2019-09-02 16:48:03	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_six
2019-09-02 16:48:04	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_seven
2019-09-02 16:48:05	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_nine
2019-09-02 16:48:06	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_ten
2019-09-02 16:48:06	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_eleven
2019-09-02 16:48:06	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_twelve
2019-09-02 16:48:12	[scenario5d6d0f6f0b7bb6.58852824]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:48:13	[scenario5d6d0f6f0b7bb6.58852824]	Data updated: TrueMachine-AgeVerification
2019-09-02 16:48:13	[scenario5d6d0f6f0b7bb6.58852824]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:48:13	[scenario5d6d0f6f0b7bb6.58852824]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6d679775faadbd2ed23c976bf0bbfa76
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:48:13	[scenario5d6d0f6f0b7bb6.58852824]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:48:13	[scenario5d6d0f6f0b7bb6.58852824]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:48:14	[scenario5d6d0f6f0b7bb6.58852824]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:48:14	[scenario5d6d0f6f0b7bb6.58852824]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:48:14	[scenario5d6d0f6f0b7bb6.58852824]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6d679775faadbd2ed23c976bf0bbfa76
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:48:14	[scenario5d6d0f6f0b7bb6.58852824]	Execute X-Cart step: step_thirteen
2019-09-02 16:48:16	[scenario5d6d0f6f0b7bb6.58852824]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:49:00	[scenario5d6d0fbcd52eb6.19705717]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 16:49:02	[scenario5d6d0fbcd52eb6.19705717]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": false
    }
}

2019-09-02 16:49:02	[scenario5d6d0fbcd52eb6.19705717]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2522bc8eaad934cd02d9bd97596680d5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:49:02	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_first
2019-09-02 16:49:03	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_second
2019-09-02 16:49:07	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_third
2019-09-02 16:49:08	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_fourth
2019-09-02 16:49:10	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_fifth
2019-09-02 16:49:10	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_six
2019-09-02 16:49:11	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_seven
2019-09-02 16:49:12	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_nine
2019-09-02 16:49:13	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_ten
2019-09-02 16:49:13	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_eleven
2019-09-02 16:49:13	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_twelve
2019-09-02 16:49:19	[scenario5d6d0fbcd52eb6.19705717]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 16:49:19	[scenario5d6d0fbcd52eb6.19705717]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:49:19	[scenario5d6d0fbcd52eb6.19705717]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2522bc8eaad934cd02d9bd97596680d5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:49:19	[scenario5d6d0fbcd52eb6.19705717]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:49:20	[scenario5d6d0fbcd52eb6.19705717]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:49:20	[scenario5d6d0fbcd52eb6.19705717]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:49:20	[scenario5d6d0fbcd52eb6.19705717]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:49:20	[scenario5d6d0fbcd52eb6.19705717]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2522bc8eaad934cd02d9bd97596680d5
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:49:20	[scenario5d6d0fbcd52eb6.19705717]	Execute X-Cart step: step_thirteen
2019-09-02 16:49:22	[scenario5d6d0fbcd52eb6.19705717]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:51:27	[scenario5d6d0d22c4f217.86976089]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:51:27	[scenario5d6d0d22c4f217.86976089]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:51:27	[scenario5d6d0d22c4f217.86976089]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:51:27	[scenario5d6d0d22c4f217.86976089]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 16:51:28	[scenario5d6d0d22c4f217.86976089]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e12669f686a83e6cf54b0d6f39a0c8bd
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:51:28	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_first
2019-09-02 16:51:29	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_second
2019-09-02 16:51:35	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_third
2019-09-02 16:51:36	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_fourth
2019-09-02 16:51:39	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_fifth
2019-09-02 16:51:40	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_six
2019-09-02 16:51:41	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_seven
2019-09-02 16:51:42	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_nine
2019-09-02 16:51:43	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_ten
2019-09-02 16:51:44	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_eleven
2019-09-02 16:51:44	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_twelve
2019-09-02 16:51:53	[scenario5d6d0d22c4f217.86976089]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:51:53	[scenario5d6d0d22c4f217.86976089]	Data updated: TrueMachine-AgeVerification
2019-09-02 16:51:53	[scenario5d6d0d22c4f217.86976089]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:51:53	[scenario5d6d0d22c4f217.86976089]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e12669f686a83e6cf54b0d6f39a0c8bd
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:51:54	[scenario5d6d0d22c4f217.86976089]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:51:54	[scenario5d6d0d22c4f217.86976089]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:51:54	[scenario5d6d0d22c4f217.86976089]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:51:55	[scenario5d6d0d22c4f217.86976089]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:51:55	[scenario5d6d0d22c4f217.86976089]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e12669f686a83e6cf54b0d6f39a0c8bd
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:51:55	[scenario5d6d0d22c4f217.86976089]	Execute X-Cart step: step_thirteen
2019-09-02 16:51:57	[scenario5d6d0d22c4f217.86976089]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:57:05	[scenario5d6d10bcd55066.60939565]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:57:05	[scenario5d6d10bcd55066.60939565]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:57:06	[scenario5d6d10bcd55066.60939565]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:57:06	[scenario5d6d10bcd55066.60939565]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": false
    }
}

2019-09-02 16:57:06	[scenario5d6d10bcd55066.60939565]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d2cfffd46a0cb41941319a10882efc66
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:57:06	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_first
2019-09-02 16:57:07	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_second
2019-09-02 16:57:12	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_third
2019-09-02 16:57:12	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_fourth
2019-09-02 16:57:15	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_fifth
2019-09-02 16:57:15	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_six
2019-09-02 16:57:16	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_seven
2019-09-02 16:57:17	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_nine
2019-09-02 16:57:18	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_ten
2019-09-02 16:57:18	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_eleven
2019-09-02 16:57:18	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_twelve
2019-09-02 16:57:24	[scenario5d6d10bcd55066.60939565]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "disable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:57:24	[scenario5d6d10bcd55066.60939565]	Data updated: TrueMachine-AgeVerification
2019-09-02 16:57:25	[scenario5d6d10bcd55066.60939565]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:57:25	[scenario5d6d10bcd55066.60939565]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d2cfffd46a0cb41941319a10882efc66
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:57:25	[scenario5d6d10bcd55066.60939565]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:57:25	[scenario5d6d10bcd55066.60939565]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:57:25	[scenario5d6d10bcd55066.60939565]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:57:26	[scenario5d6d10bcd55066.60939565]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:57:26	[scenario5d6d10bcd55066.60939565]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d2cfffd46a0cb41941319a10882efc66
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:57:26	[scenario5d6d10bcd55066.60939565]	Execute X-Cart step: step_thirteen
2019-09-02 16:57:28	[scenario5d6d10bcd55066.60939565]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 16:57:42	[scenario5d6d11c2051cb0.04060386]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": false
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:57:42	[scenario5d6d11c2051cb0.04060386]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": false
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:57:42	[scenario5d6d11c2051cb0.04060386]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": false
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:57:43	[scenario5d6d11c2051cb0.04060386]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-02 16:57:43	[scenario5d6d11c2051cb0.04060386]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8b73c8b4018d707d1c08e78dc6d2ef91
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 16:57:43	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_first
2019-09-02 16:57:43	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_second
2019-09-02 16:57:46	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_third
2019-09-02 16:57:46	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_fourth
2019-09-02 16:57:48	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_fifth
2019-09-02 16:57:49	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_six
2019-09-02 16:57:50	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_seven
2019-09-02 16:57:51	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_nine
2019-09-02 16:57:52	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_ten
2019-09-02 16:57:52	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_eleven
2019-09-02 16:57:52	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_twelve
2019-09-02 16:57:57	[scenario5d6d11c2051cb0.04060386]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": false
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:57:58	[scenario5d6d11c2051cb0.04060386]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
    "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
]

2019-09-02 16:57:58	[scenario5d6d11c2051cb0.04060386]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": false
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 16:57:58	[scenario5d6d11c2051cb0.04060386]	Data updated: TrueMachine-AgeVerification
2019-09-02 16:57:58	[scenario5d6d11c2051cb0.04060386]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 16:57:58	[scenario5d6d11c2051cb0.04060386]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8b73c8b4018d707d1c08e78dc6d2ef91
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 16:57:58	[scenario5d6d11c2051cb0.04060386]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 16:57:59	[scenario5d6d11c2051cb0.04060386]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 16:57:59	[scenario5d6d11c2051cb0.04060386]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 16:57:59	[scenario5d6d11c2051cb0.04060386]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 16:57:59	[scenario5d6d11c2051cb0.04060386]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8b73c8b4018d707d1c08e78dc6d2ef91
Context transitions => [
    "step_thirteen"
]

2019-09-02 16:57:59	[scenario5d6d11c2051cb0.04060386]	Execute X-Cart step: step_thirteen
2019-09-02 16:58:01	[scenario5d6d11c2051cb0.04060386]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 17:01:28	[scenario5d6d12a8ad1e52.27565686]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 17:01:30	[scenario5d6d12a8ad1e52.27565686]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-02 17:01:30	[scenario5d6d12a8ad1e52.27565686]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => aa4e4558c24073d76e313c763c775290
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 17:01:30	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_first
2019-09-02 17:01:31	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_second
2019-09-02 17:01:35	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_third
2019-09-02 17:01:35	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_fourth
2019-09-02 17:01:37	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_fifth
2019-09-02 17:01:38	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_six
2019-09-02 17:01:39	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_seven
2019-09-02 17:01:40	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_nine
2019-09-02 17:01:40	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_ten
2019-09-02 17:01:41	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_eleven
2019-09-02 17:01:41	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_twelve
2019-09-02 17:01:46	[scenario5d6d12a8ad1e52.27565686]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 17:01:47	[scenario5d6d12a8ad1e52.27565686]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-02 17:01:48	[scenario5d6d12a8ad1e52.27565686]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => aa4e4558c24073d76e313c763c775290
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 17:01:48	[scenario5d6d12a8ad1e52.27565686]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 17:01:49	[scenario5d6d12a8ad1e52.27565686]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 17:01:50	[scenario5d6d12a8ad1e52.27565686]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 17:01:51	[scenario5d6d12a8ad1e52.27565686]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 17:01:51	[scenario5d6d12a8ad1e52.27565686]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => aa4e4558c24073d76e313c763c775290
Context transitions => [
    "step_thirteen"
]

2019-09-02 17:01:52	[scenario5d6d12a8ad1e52.27565686]	Execute X-Cart step: step_thirteen
2019-09-02 17:01:53	[scenario5d6d12a8ad1e52.27565686]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 17:02:27	[scenario5d6d12a977e736.25306236]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:02:27	[scenario5d6d12a977e736.25306236]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:02:27	[scenario5d6d12a977e736.25306236]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:02:28	[scenario5d6d12a977e736.25306236]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 17:02:28	[scenario5d6d12a977e736.25306236]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b3020584be97c58d648a4d8953468b48
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 17:02:28	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_first
2019-09-02 17:02:28	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_second
2019-09-02 17:02:32	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_third
2019-09-02 17:02:32	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_fourth
2019-09-02 17:02:35	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_fifth
2019-09-02 17:02:35	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_six
2019-09-02 17:02:37	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_seven
2019-09-02 17:02:38	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_nine
2019-09-02 17:02:38	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_ten
2019-09-02 17:02:39	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_eleven
2019-09-02 17:02:39	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_twelve
2019-09-02 17:02:44	[scenario5d6d12a977e736.25306236]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:02:44	[scenario5d6d12a977e736.25306236]	Data updated: TrueMachine-AgeVerification
2019-09-02 17:02:44	[scenario5d6d12a977e736.25306236]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 17:02:44	[scenario5d6d12a977e736.25306236]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b3020584be97c58d648a4d8953468b48
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 17:02:45	[scenario5d6d12a977e736.25306236]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 17:02:45	[scenario5d6d12a977e736.25306236]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 17:02:45	[scenario5d6d12a977e736.25306236]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 17:02:45	[scenario5d6d12a977e736.25306236]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 17:02:45	[scenario5d6d12a977e736.25306236]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b3020584be97c58d648a4d8953468b48
Context transitions => [
    "step_thirteen"
]

2019-09-02 17:02:46	[scenario5d6d12a977e736.25306236]	Execute X-Cart step: step_thirteen
2019-09-02 17:02:47	[scenario5d6d12a977e736.25306236]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 17:06:04	[scenario5d6d1387b565e2.90559073]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:06:04	[scenario5d6d1387b565e2.90559073]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:06:04	[scenario5d6d1387b565e2.90559073]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:06:04	[scenario5d6d1387b565e2.90559073]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-02 17:06:04	[scenario5d6d1387b565e2.90559073]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d528be04995d6417c1f3b546af050a36
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 17:06:05	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_first
2019-09-02 17:06:05	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_second
2019-09-02 17:06:10	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_third
2019-09-02 17:06:11	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_fourth
2019-09-02 17:06:13	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_fifth
2019-09-02 17:06:14	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_six
2019-09-02 17:06:14	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_seven
2019-09-02 17:06:16	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_nine
2019-09-02 17:06:16	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_ten
2019-09-02 17:06:16	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_eleven
2019-09-02 17:06:17	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_twelve
2019-09-02 17:06:24	[scenario5d6d1387b565e2.90559073]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:06:24	[scenario5d6d1387b565e2.90559073]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
    "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
]

2019-09-02 17:06:24	[scenario5d6d1387b565e2.90559073]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.4",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:06:25	[scenario5d6d1387b565e2.90559073]	Data updated: TrueMachine-AgeVerification
2019-09-02 17:06:25	[scenario5d6d1387b565e2.90559073]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 17:06:25	[scenario5d6d1387b565e2.90559073]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d528be04995d6417c1f3b546af050a36
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 17:06:25	[scenario5d6d1387b565e2.90559073]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 17:06:25	[scenario5d6d1387b565e2.90559073]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 17:06:26	[scenario5d6d1387b565e2.90559073]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 17:06:26	[scenario5d6d1387b565e2.90559073]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 17:06:26	[scenario5d6d1387b565e2.90559073]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d528be04995d6417c1f3b546af050a36
Context transitions => [
    "step_thirteen"
]

2019-09-02 17:06:26	[scenario5d6d1387b565e2.90559073]	Execute X-Cart step: step_thirteen
2019-09-02 17:06:29	[scenario5d6d1387b565e2.90559073]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 17:08:36	[scenario5d6d1454b303d6.75650046]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-02 17:08:38	[scenario5d6d1454b303d6.75650046]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-02 17:08:38	[scenario5d6d1454b303d6.75650046]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1eb5dc48bfe7020cf5d86384e1e2e35f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 17:08:38	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_first
2019-09-02 17:08:39	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_second
2019-09-02 17:08:44	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_third
2019-09-02 17:08:45	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_fourth
2019-09-02 17:08:47	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_fifth
2019-09-02 17:08:47	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_six
2019-09-02 17:08:48	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_seven
2019-09-02 17:08:49	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_nine
2019-09-02 17:08:50	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_ten
2019-09-02 17:08:50	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_eleven
2019-09-02 17:08:50	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_twelve
2019-09-02 17:08:56	[scenario5d6d1454b303d6.75650046]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-02 17:08:57	[scenario5d6d1454b303d6.75650046]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-02 17:08:57	[scenario5d6d1454b303d6.75650046]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1eb5dc48bfe7020cf5d86384e1e2e35f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 17:08:57	[scenario5d6d1454b303d6.75650046]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 17:08:57	[scenario5d6d1454b303d6.75650046]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 17:08:57	[scenario5d6d1454b303d6.75650046]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 17:08:58	[scenario5d6d1454b303d6.75650046]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 17:08:58	[scenario5d6d1454b303d6.75650046]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1eb5dc48bfe7020cf5d86384e1e2e35f
Context transitions => [
    "step_thirteen"
]

2019-09-02 17:08:58	[scenario5d6d1454b303d6.75650046]	Execute X-Cart step: step_thirteen
2019-09-02 17:09:00	[scenario5d6d1454b303d6.75650046]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-02 17:09:14	[scenario5d6d14558beb17.56172687]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:09:14	[scenario5d6d14558beb17.56172687]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:09:14	[scenario5d6d14558beb17.56172687]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:09:15	[scenario5d6d14558beb17.56172687]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-02 17:09:15	[scenario5d6d14558beb17.56172687]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 415995bc771523cb89fc4fb2cd2a16d7
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-02 17:09:15	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_first
2019-09-02 17:09:15	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_second
2019-09-02 17:09:18	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_third
2019-09-02 17:09:20	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_fourth
2019-09-02 17:09:22	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_fifth
2019-09-02 17:09:22	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_six
2019-09-02 17:09:23	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_seven
2019-09-02 17:09:24	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_nine
2019-09-02 17:09:25	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_ten
2019-09-02 17:09:25	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_eleven
2019-09-02 17:09:25	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_twelve
2019-09-02 17:09:30	[scenario5d6d14558beb17.56172687]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-02 17:09:30	[scenario5d6d14558beb17.56172687]	Data updated: TrueMachine-AgeVerification
2019-09-02 17:09:31	[scenario5d6d14558beb17.56172687]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-02 17:09:31	[scenario5d6d14558beb17.56172687]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 415995bc771523cb89fc4fb2cd2a16d7
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-02 17:09:31	[scenario5d6d14558beb17.56172687]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-02 17:09:31	[scenario5d6d14558beb17.56172687]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-02 17:09:31	[scenario5d6d14558beb17.56172687]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-02 17:09:32	[scenario5d6d14558beb17.56172687]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-02 17:09:32	[scenario5d6d14558beb17.56172687]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 415995bc771523cb89fc4fb2cd2a16d7
Context transitions => [
    "step_thirteen"
]

2019-09-02 17:09:32	[scenario5d6d14558beb17.56172687]	Execute X-Cart step: step_thirteen
2019-09-02 17:09:33	[scenario5d6d14558beb17.56172687]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

