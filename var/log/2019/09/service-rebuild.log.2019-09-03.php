<?php die(); ?>
2019-09-03 10:29:33	[scenario5d6e084d8c5b21.88380769]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 10:29:35	[scenario5d6e084d8c5b21.88380769]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 10:29:35	[scenario5d6e084d8c5b21.88380769]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 70e7e0ac92d642376af22f1e353bc6da
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 10:29:35	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_first
2019-09-03 10:29:36	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_second
2019-09-03 10:29:41	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_third
2019-09-03 10:29:41	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_fourth
2019-09-03 10:29:44	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_fifth
2019-09-03 10:29:44	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_six
2019-09-03 10:29:45	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_seven
2019-09-03 10:29:46	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_nine
2019-09-03 10:29:47	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_ten
2019-09-03 10:29:47	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_eleven
2019-09-03 10:29:48	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_twelve
2019-09-03 10:29:54	[scenario5d6e084d8c5b21.88380769]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 10:29:55	[scenario5d6e084d8c5b21.88380769]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 10:29:55	[scenario5d6e084d8c5b21.88380769]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 70e7e0ac92d642376af22f1e353bc6da
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 10:29:55	[scenario5d6e084d8c5b21.88380769]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 10:29:56	[scenario5d6e084d8c5b21.88380769]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 10:29:56	[scenario5d6e084d8c5b21.88380769]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 10:29:56	[scenario5d6e084d8c5b21.88380769]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 10:29:57	[scenario5d6e084d8c5b21.88380769]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 70e7e0ac92d642376af22f1e353bc6da
Context transitions => [
    "step_thirteen"
]

2019-09-03 10:29:57	[scenario5d6e084d8c5b21.88380769]	Execute X-Cart step: step_thirteen
2019-09-03 10:29:59	[scenario5d6e084d8c5b21.88380769]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 10:36:10	[scenario5d6e09da6bbce1.15366666]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 10:36:13	[scenario5d6e09da6bbce1.15366666]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 10:36:13	[scenario5d6e09da6bbce1.15366666]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b66e590b7ee4193c6c7cfb3f4bfa370a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 10:36:13	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_first
2019-09-03 10:36:14	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_second
2019-09-03 10:36:20	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_third
2019-09-03 10:36:20	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_fourth
2019-09-03 10:36:24	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_fifth
2019-09-03 10:36:24	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_six
2019-09-03 10:36:25	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_seven
2019-09-03 10:36:27	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_nine
2019-09-03 10:36:28	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_ten
2019-09-03 10:36:28	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_eleven
2019-09-03 10:36:29	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_twelve
2019-09-03 10:36:36	[scenario5d6e09da6bbce1.15366666]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 10:36:36	[scenario5d6e09da6bbce1.15366666]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 10:36:36	[scenario5d6e09da6bbce1.15366666]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b66e590b7ee4193c6c7cfb3f4bfa370a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 10:36:36	[scenario5d6e09da6bbce1.15366666]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 10:36:37	[scenario5d6e09da6bbce1.15366666]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 10:36:37	[scenario5d6e09da6bbce1.15366666]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 10:36:37	[scenario5d6e09da6bbce1.15366666]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 10:36:38	[scenario5d6e09da6bbce1.15366666]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b66e590b7ee4193c6c7cfb3f4bfa370a
Context transitions => [
    "step_thirteen"
]

2019-09-03 10:36:38	[scenario5d6e09da6bbce1.15366666]	Execute X-Cart step: step_thirteen
2019-09-03 10:36:40	[scenario5d6e09da6bbce1.15366666]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 10:40:50	[scenario5d6d107b13b9e0.11464900]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 10:40:50	[scenario5d6d107b13b9e0.11464900]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 10:40:51	[scenario5d6d107b13b9e0.11464900]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 10:40:51	[scenario5d6d107b13b9e0.11464900]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-03 10:40:51	[scenario5d6d107b13b9e0.11464900]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 7858971fc7ae9552ef21910db72fa676
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 10:40:52	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_first
2019-09-03 10:40:52	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_second
2019-09-03 10:40:58	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_third
2019-09-03 10:40:59	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_fourth
2019-09-03 10:41:03	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_fifth
2019-09-03 10:41:03	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_six
2019-09-03 10:41:04	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_seven
2019-09-03 10:41:06	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_nine
2019-09-03 10:41:07	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_ten
2019-09-03 10:41:07	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_eleven
2019-09-03 10:41:08	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_twelve
2019-09-03 10:41:16	[scenario5d6d107b13b9e0.11464900]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 10:41:17	[scenario5d6d107b13b9e0.11464900]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
    "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
]

2019-09-03 10:41:17	[scenario5d6d107b13b9e0.11464900]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 10:41:17	[scenario5d6d107b13b9e0.11464900]	Data updated: TrueMachine-AgeVerification
2019-09-03 10:41:17	[scenario5d6d107b13b9e0.11464900]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 10:41:17	[scenario5d6d107b13b9e0.11464900]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 7858971fc7ae9552ef21910db72fa676
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 10:41:17	[scenario5d6d107b13b9e0.11464900]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 10:41:18	[scenario5d6d107b13b9e0.11464900]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 10:41:18	[scenario5d6d107b13b9e0.11464900]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 10:41:19	[scenario5d6d107b13b9e0.11464900]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 10:41:19	[scenario5d6d107b13b9e0.11464900]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 7858971fc7ae9552ef21910db72fa676
Context transitions => [
    "step_thirteen"
]

2019-09-03 10:41:19	[scenario5d6d107b13b9e0.11464900]	Execute X-Cart step: step_thirteen
2019-09-03 10:41:21	[scenario5d6d107b13b9e0.11464900]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 10:44:46	[scenario5d6e0bdeca62f1.24168622]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 10:44:48	[scenario5d6e0bdeca62f1.24168622]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-03 10:44:48	[scenario5d6e0bdeca62f1.24168622]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 51ef2167edaeba16a5c1a73f31482d58
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 10:44:48	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_first
2019-09-03 10:44:49	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_second
2019-09-03 10:44:54	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_third
2019-09-03 10:44:55	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_fourth
2019-09-03 10:44:57	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_fifth
2019-09-03 10:44:57	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_six
2019-09-03 10:44:58	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_seven
2019-09-03 10:44:59	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_nine
2019-09-03 10:45:00	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_ten
2019-09-03 10:45:00	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_eleven
2019-09-03 10:45:01	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_twelve
2019-09-03 10:45:08	[scenario5d6e0bdeca62f1.24168622]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 10:45:09	[scenario5d6e0bdeca62f1.24168622]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-03 10:45:09	[scenario5d6e0bdeca62f1.24168622]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 51ef2167edaeba16a5c1a73f31482d58
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 10:45:09	[scenario5d6e0bdeca62f1.24168622]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 10:45:10	[scenario5d6e0bdeca62f1.24168622]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 10:45:11	[scenario5d6e0bdeca62f1.24168622]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 10:45:12	[scenario5d6e0bdeca62f1.24168622]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 10:45:12	[scenario5d6e0bdeca62f1.24168622]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 51ef2167edaeba16a5c1a73f31482d58
Context transitions => [
    "step_thirteen"
]

2019-09-03 10:45:13	[scenario5d6e0bdeca62f1.24168622]	Execute X-Cart step: step_thirteen
2019-09-03 10:45:16	[scenario5d6e0bdeca62f1.24168622]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 10:48:08	[scenario5d6e0b23db7287.95862992]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 10:48:08	[scenario5d6e0b23db7287.95862992]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 10:48:08	[scenario5d6e0b23db7287.95862992]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 10:48:09	[scenario5d6e0b23db7287.95862992]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 10:48:09	[scenario5d6e0b23db7287.95862992]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d05468eac82c7d9eeb1270ae9deeae55
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 10:48:09	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_first
2019-09-03 10:48:09	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_second
2019-09-03 10:48:15	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_third
2019-09-03 10:48:15	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_fourth
2019-09-03 10:48:18	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_fifth
2019-09-03 10:48:18	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_six
2019-09-03 10:48:19	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_seven
2019-09-03 10:48:21	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_nine
2019-09-03 10:48:22	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_ten
2019-09-03 10:48:22	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_eleven
2019-09-03 10:48:23	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_twelve
2019-09-03 10:48:29	[scenario5d6e0b23db7287.95862992]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 10:48:29	[scenario5d6e0b23db7287.95862992]	Data updated: TrueMachine-AgeVerification
2019-09-03 10:48:29	[scenario5d6e0b23db7287.95862992]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 10:48:29	[scenario5d6e0b23db7287.95862992]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d05468eac82c7d9eeb1270ae9deeae55
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 10:48:30	[scenario5d6e0b23db7287.95862992]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 10:48:30	[scenario5d6e0b23db7287.95862992]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 10:48:30	[scenario5d6e0b23db7287.95862992]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 10:48:30	[scenario5d6e0b23db7287.95862992]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 10:48:30	[scenario5d6e0b23db7287.95862992]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d05468eac82c7d9eeb1270ae9deeae55
Context transitions => [
    "step_thirteen"
]

2019-09-03 10:48:31	[scenario5d6e0b23db7287.95862992]	Execute X-Cart step: step_thirteen
2019-09-03 10:48:33	[scenario5d6e0b23db7287.95862992]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 19:25:28	[scenario5d6e85e8cec489.95592059]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 19:25:33	[scenario5d6e85e8cec489.95592059]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 19:25:33	[scenario5d6e85e8cec489.95592059]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c25506b99fe893050c8b1179bf402936
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 19:25:33	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_first
2019-09-03 19:25:34	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_second
2019-09-03 19:25:44	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_third
2019-09-03 19:25:45	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_fourth
2019-09-03 19:25:49	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_fifth
2019-09-03 19:25:50	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_six
2019-09-03 19:25:51	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_seven
2019-09-03 19:25:54	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_nine
2019-09-03 19:25:55	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_ten
2019-09-03 19:25:56	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_eleven
2019-09-03 19:25:56	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_twelve
2019-09-03 19:26:03	[scenario5d6e85e8cec489.95592059]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 19:26:04	[scenario5d6e85e8cec489.95592059]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 19:26:04	[scenario5d6e85e8cec489.95592059]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => c25506b99fe893050c8b1179bf402936
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 19:26:04	[scenario5d6e85e8cec489.95592059]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 19:26:05	[scenario5d6e85e8cec489.95592059]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 19:26:05	[scenario5d6e85e8cec489.95592059]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 19:26:05	[scenario5d6e85e8cec489.95592059]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 19:26:05	[scenario5d6e85e8cec489.95592059]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c25506b99fe893050c8b1179bf402936
Context transitions => [
    "step_thirteen"
]

2019-09-03 19:26:05	[scenario5d6e85e8cec489.95592059]	Execute X-Cart step: step_thirteen
2019-09-03 19:26:07	[scenario5d6e85e8cec489.95592059]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 19:35:30	[scenario5d6e88423d5da1.11653997]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 19:35:32	[scenario5d6e88423d5da1.11653997]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 19:35:32	[scenario5d6e88423d5da1.11653997]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ac9509a3a36f61f3cae9231c39961a17
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 19:35:32	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_first
2019-09-03 19:35:33	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_second
2019-09-03 19:35:41	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_third
2019-09-03 19:35:42	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_fourth
2019-09-03 19:35:47	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_fifth
2019-09-03 19:35:48	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_six
2019-09-03 19:35:50	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_seven
2019-09-03 19:35:53	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_nine
2019-09-03 19:35:54	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_ten
2019-09-03 19:35:55	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_eleven
2019-09-03 19:35:56	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_twelve
2019-09-03 19:36:03	[scenario5d6e88423d5da1.11653997]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 19:36:05	[scenario5d6e88423d5da1.11653997]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 19:36:05	[scenario5d6e88423d5da1.11653997]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ac9509a3a36f61f3cae9231c39961a17
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 19:36:06	[scenario5d6e88423d5da1.11653997]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 19:36:07	[scenario5d6e88423d5da1.11653997]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 19:36:08	[scenario5d6e88423d5da1.11653997]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 19:36:09	[scenario5d6e88423d5da1.11653997]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 19:36:09	[scenario5d6e88423d5da1.11653997]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ac9509a3a36f61f3cae9231c39961a17
Context transitions => [
    "step_thirteen"
]

2019-09-03 19:36:10	[scenario5d6e88423d5da1.11653997]	Execute X-Cart step: step_thirteen
2019-09-03 19:36:13	[scenario5d6e88423d5da1.11653997]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 19:38:48	[scenario5d6e8908d390c3.28613668]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 19:38:52	[scenario5d6e8908d390c3.28613668]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 19:38:52	[scenario5d6e8908d390c3.28613668]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0d571509dd7e4e3402154487fe7e2593
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 19:38:52	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_first
2019-09-03 19:38:53	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_second
2019-09-03 19:39:03	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_third
2019-09-03 19:39:04	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_fourth
2019-09-03 19:39:09	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_fifth
2019-09-03 19:39:10	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_six
2019-09-03 19:39:12	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_seven
2019-09-03 19:39:16	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_nine
2019-09-03 19:39:18	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_ten
2019-09-03 19:39:19	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_eleven
2019-09-03 19:39:20	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_twelve
2019-09-03 19:39:28	[scenario5d6e8908d390c3.28613668]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 19:39:29	[scenario5d6e8908d390c3.28613668]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 19:39:29	[scenario5d6e8908d390c3.28613668]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 0d571509dd7e4e3402154487fe7e2593
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 19:39:29	[scenario5d6e8908d390c3.28613668]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 19:39:30	[scenario5d6e8908d390c3.28613668]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 19:39:31	[scenario5d6e8908d390c3.28613668]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 19:39:32	[scenario5d6e8908d390c3.28613668]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 19:39:32	[scenario5d6e8908d390c3.28613668]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0d571509dd7e4e3402154487fe7e2593
Context transitions => [
    "step_thirteen"
]

2019-09-03 19:39:33	[scenario5d6e8908d390c3.28613668]	Execute X-Cart step: step_thirteen
2019-09-03 19:39:35	[scenario5d6e8908d390c3.28613668]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 19:44:28	[scenario5d6e8a5c3ed819.70504997]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 19:44:30	[scenario5d6e8a5c3ed819.70504997]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 19:44:30	[scenario5d6e8a5c3ed819.70504997]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8761c785d25a26d22c6d98d94ce39c43
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 19:44:30	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_first
2019-09-03 19:44:31	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_second
2019-09-03 19:44:37	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_third
2019-09-03 19:44:38	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_fourth
2019-09-03 19:44:42	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_fifth
2019-09-03 19:44:42	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_six
2019-09-03 19:44:43	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_seven
2019-09-03 19:44:45	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_nine
2019-09-03 19:44:46	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_ten
2019-09-03 19:44:46	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_eleven
2019-09-03 19:44:46	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_twelve
2019-09-03 19:44:54	[scenario5d6e8a5c3ed819.70504997]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 19:44:55	[scenario5d6e8a5c3ed819.70504997]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 19:44:55	[scenario5d6e8a5c3ed819.70504997]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8761c785d25a26d22c6d98d94ce39c43
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 19:44:55	[scenario5d6e8a5c3ed819.70504997]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 19:44:56	[scenario5d6e8a5c3ed819.70504997]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 19:44:56	[scenario5d6e8a5c3ed819.70504997]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 19:44:56	[scenario5d6e8a5c3ed819.70504997]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 19:44:56	[scenario5d6e8a5c3ed819.70504997]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8761c785d25a26d22c6d98d94ce39c43
Context transitions => [
    "step_thirteen"
]

2019-09-03 19:44:57	[scenario5d6e8a5c3ed819.70504997]	Execute X-Cart step: step_thirteen
2019-09-03 19:45:00	[scenario5d6e8a5c3ed819.70504997]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 19:47:26	[scenario5d6e8b0e8715e1.45937241]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 19:47:28	[scenario5d6e8b0e8715e1.45937241]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 19:47:28	[scenario5d6e8b0e8715e1.45937241]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ce601ac47da2f6f42f09e3c5b67ba415
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 19:47:28	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_first
2019-09-03 19:47:29	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_second
2019-09-03 19:47:35	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_third
2019-09-03 19:47:36	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_fourth
2019-09-03 19:47:40	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_fifth
2019-09-03 19:47:41	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_six
2019-09-03 19:47:42	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_seven
2019-09-03 19:47:44	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_nine
2019-09-03 19:47:45	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_ten
2019-09-03 19:47:46	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_eleven
2019-09-03 19:47:46	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_twelve
2019-09-03 19:47:55	[scenario5d6e8b0e8715e1.45937241]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 19:47:56	[scenario5d6e8b0e8715e1.45937241]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 19:47:56	[scenario5d6e8b0e8715e1.45937241]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ce601ac47da2f6f42f09e3c5b67ba415
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 19:47:56	[scenario5d6e8b0e8715e1.45937241]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 19:47:57	[scenario5d6e8b0e8715e1.45937241]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 19:47:57	[scenario5d6e8b0e8715e1.45937241]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 19:47:57	[scenario5d6e8b0e8715e1.45937241]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 19:47:57	[scenario5d6e8b0e8715e1.45937241]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ce601ac47da2f6f42f09e3c5b67ba415
Context transitions => [
    "step_thirteen"
]

2019-09-03 19:47:58	[scenario5d6e8b0e8715e1.45937241]	Execute X-Cart step: step_thirteen
2019-09-03 19:48:00	[scenario5d6e8b0e8715e1.45937241]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 19:50:05	[scenario5d6e8bad755955.53128493]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 19:50:08	[scenario5d6e8bad755955.53128493]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 19:50:09	[scenario5d6e8bad755955.53128493]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4fd112a5eae3a58fe200483e9c21ed39
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 19:50:09	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_first
2019-09-03 19:50:10	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_second
2019-09-03 19:50:19	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_third
2019-09-03 19:50:20	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_fourth
2019-09-03 19:50:25	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_fifth
2019-09-03 19:50:25	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_six
2019-09-03 19:50:27	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_seven
2019-09-03 19:50:30	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_nine
2019-09-03 19:50:32	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_ten
2019-09-03 19:50:33	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_eleven
2019-09-03 19:50:34	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_twelve
2019-09-03 19:50:42	[scenario5d6e8bad755955.53128493]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 19:50:44	[scenario5d6e8bad755955.53128493]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 19:50:44	[scenario5d6e8bad755955.53128493]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4fd112a5eae3a58fe200483e9c21ed39
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 19:50:45	[scenario5d6e8bad755955.53128493]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 19:50:46	[scenario5d6e8bad755955.53128493]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 19:50:47	[scenario5d6e8bad755955.53128493]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 19:50:48	[scenario5d6e8bad755955.53128493]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 19:50:48	[scenario5d6e8bad755955.53128493]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4fd112a5eae3a58fe200483e9c21ed39
Context transitions => [
    "step_thirteen"
]

2019-09-03 19:50:49	[scenario5d6e8bad755955.53128493]	Execute X-Cart step: step_thirteen
2019-09-03 19:50:51	[scenario5d6e8bad755955.53128493]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 19:56:55	[scenario5d6e8d4728faf9.99934488]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 19:56:58	[scenario5d6e8d4728faf9.99934488]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 19:56:58	[scenario5d6e8d4728faf9.99934488]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6a5d35af3f3d17cd403f19a907802015
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 19:56:58	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_first
2019-09-03 19:56:59	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_second
2019-09-03 19:57:06	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_third
2019-09-03 19:57:06	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_fourth
2019-09-03 19:57:10	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_fifth
2019-09-03 19:57:11	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_six
2019-09-03 19:57:12	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_seven
2019-09-03 19:57:13	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_nine
2019-09-03 19:57:14	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_ten
2019-09-03 19:57:14	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_eleven
2019-09-03 19:57:15	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_twelve
2019-09-03 19:57:28	[scenario5d6e8d4728faf9.99934488]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 19:57:29	[scenario5d6e8d4728faf9.99934488]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 19:57:29	[scenario5d6e8d4728faf9.99934488]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6a5d35af3f3d17cd403f19a907802015
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 19:57:29	[scenario5d6e8d4728faf9.99934488]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 19:57:30	[scenario5d6e8d4728faf9.99934488]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 19:57:30	[scenario5d6e8d4728faf9.99934488]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 19:57:31	[scenario5d6e8d4728faf9.99934488]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 19:57:31	[scenario5d6e8d4728faf9.99934488]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6a5d35af3f3d17cd403f19a907802015
Context transitions => [
    "step_thirteen"
]

2019-09-03 19:57:31	[scenario5d6e8d4728faf9.99934488]	Execute X-Cart step: step_thirteen
2019-09-03 19:57:33	[scenario5d6e8d4728faf9.99934488]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 19:59:44	[scenario5d6e8df0e5fe12.39036365]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 19:59:46	[scenario5d6e8df0e5fe12.39036365]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 19:59:46	[scenario5d6e8df0e5fe12.39036365]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b9f69ce91effe523792dca2f4f2e3db0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 19:59:46	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_first
2019-09-03 19:59:47	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_second
2019-09-03 19:59:54	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_third
2019-09-03 19:59:55	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_fourth
2019-09-03 19:59:57	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_fifth
2019-09-03 19:59:57	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_six
2019-09-03 19:59:58	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_seven
2019-09-03 20:00:00	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_nine
2019-09-03 20:00:01	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_ten
2019-09-03 20:00:01	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_eleven
2019-09-03 20:00:01	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_twelve
2019-09-03 20:00:08	[scenario5d6e8df0e5fe12.39036365]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 20:00:09	[scenario5d6e8df0e5fe12.39036365]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 20:00:09	[scenario5d6e8df0e5fe12.39036365]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b9f69ce91effe523792dca2f4f2e3db0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 20:00:09	[scenario5d6e8df0e5fe12.39036365]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 20:00:10	[scenario5d6e8df0e5fe12.39036365]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 20:00:10	[scenario5d6e8df0e5fe12.39036365]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 20:00:10	[scenario5d6e8df0e5fe12.39036365]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 20:00:10	[scenario5d6e8df0e5fe12.39036365]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b9f69ce91effe523792dca2f4f2e3db0
Context transitions => [
    "step_thirteen"
]

2019-09-03 20:00:10	[scenario5d6e8df0e5fe12.39036365]	Execute X-Cart step: step_thirteen
2019-09-03 20:00:12	[scenario5d6e8df0e5fe12.39036365]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 20:01:17	[scenario5d6e8e4d6e9ad2.72655498]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 20:01:20	[scenario5d6e8e4d6e9ad2.72655498]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 20:01:20	[scenario5d6e8e4d6e9ad2.72655498]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bddfb0bb49c9db2df75ac5bb2524b35d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 20:01:20	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_first
2019-09-03 20:01:21	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_second
2019-09-03 20:01:33	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_third
2019-09-03 20:01:34	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_fourth
2019-09-03 20:01:38	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_fifth
2019-09-03 20:01:38	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_six
2019-09-03 20:01:40	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_seven
2019-09-03 20:01:42	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_nine
2019-09-03 20:01:43	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_ten
2019-09-03 20:01:43	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_eleven
2019-09-03 20:01:43	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_twelve
2019-09-03 20:01:50	[scenario5d6e8e4d6e9ad2.72655498]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 20:01:51	[scenario5d6e8e4d6e9ad2.72655498]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 20:01:51	[scenario5d6e8e4d6e9ad2.72655498]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bddfb0bb49c9db2df75ac5bb2524b35d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 20:01:51	[scenario5d6e8e4d6e9ad2.72655498]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 20:01:52	[scenario5d6e8e4d6e9ad2.72655498]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 20:01:52	[scenario5d6e8e4d6e9ad2.72655498]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 20:01:53	[scenario5d6e8e4d6e9ad2.72655498]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 20:01:53	[scenario5d6e8e4d6e9ad2.72655498]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bddfb0bb49c9db2df75ac5bb2524b35d
Context transitions => [
    "step_thirteen"
]

2019-09-03 20:01:53	[scenario5d6e8e4d6e9ad2.72655498]	Execute X-Cart step: step_thirteen
2019-09-03 20:01:55	[scenario5d6e8e4d6e9ad2.72655498]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 20:03:28	[scenario5d6e8ed05e32b1.59075231]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 20:03:31	[scenario5d6e8ed05e32b1.59075231]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 20:03:32	[scenario5d6e8ed05e32b1.59075231]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 88cd724945aee2d85859e772fef860a9
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 20:03:32	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_first
2019-09-03 20:03:33	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_second
2019-09-03 20:03:47	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_third
2019-09-03 20:03:48	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_fourth
2019-09-03 20:03:50	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_fifth
2019-09-03 20:03:51	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_six
2019-09-03 20:03:51	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_seven
2019-09-03 20:03:54	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_nine
2019-09-03 20:03:55	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_ten
2019-09-03 20:03:55	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_eleven
2019-09-03 20:03:55	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_twelve
2019-09-03 20:04:02	[scenario5d6e8ed05e32b1.59075231]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 20:04:03	[scenario5d6e8ed05e32b1.59075231]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 20:04:03	[scenario5d6e8ed05e32b1.59075231]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 88cd724945aee2d85859e772fef860a9
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 20:04:03	[scenario5d6e8ed05e32b1.59075231]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 20:04:04	[scenario5d6e8ed05e32b1.59075231]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 20:04:04	[scenario5d6e8ed05e32b1.59075231]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 20:04:04	[scenario5d6e8ed05e32b1.59075231]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 20:04:04	[scenario5d6e8ed05e32b1.59075231]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 88cd724945aee2d85859e772fef860a9
Context transitions => [
    "step_thirteen"
]

2019-09-03 20:04:05	[scenario5d6e8ed05e32b1.59075231]	Execute X-Cart step: step_thirteen
2019-09-03 20:04:07	[scenario5d6e8ed05e32b1.59075231]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 20:06:29	[scenario5d6e8f85033cc1.11920193]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 20:06:30	[scenario5d6e8f85033cc1.11920193]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 20:06:30	[scenario5d6e8f85033cc1.11920193]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 86af154d56e37355facbc7b482d60530
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 20:06:31	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_first
2019-09-03 20:06:31	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_second
2019-09-03 20:06:38	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_third
2019-09-03 20:06:38	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_fourth
2019-09-03 20:06:42	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_fifth
2019-09-03 20:06:42	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_six
2019-09-03 20:06:43	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_seven
2019-09-03 20:06:46	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_nine
2019-09-03 20:06:46	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_ten
2019-09-03 20:06:47	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_eleven
2019-09-03 20:06:47	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_twelve
2019-09-03 20:06:56	[scenario5d6e8f85033cc1.11920193]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 20:06:57	[scenario5d6e8f85033cc1.11920193]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 20:06:57	[scenario5d6e8f85033cc1.11920193]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 86af154d56e37355facbc7b482d60530
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 20:06:57	[scenario5d6e8f85033cc1.11920193]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 20:06:58	[scenario5d6e8f85033cc1.11920193]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 20:06:58	[scenario5d6e8f85033cc1.11920193]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 20:06:58	[scenario5d6e8f85033cc1.11920193]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 20:06:58	[scenario5d6e8f85033cc1.11920193]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 86af154d56e37355facbc7b482d60530
Context transitions => [
    "step_thirteen"
]

2019-09-03 20:06:59	[scenario5d6e8f85033cc1.11920193]	Execute X-Cart step: step_thirteen
2019-09-03 20:07:01	[scenario5d6e8f85033cc1.11920193]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 21:27:51	[scenario5d6ea297d0f954.31548261]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 21:27:56	[scenario5d6ea297d0f954.31548261]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 21:27:56	[scenario5d6ea297d0f954.31548261]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c79a863d4b0a613ed67a9998fe9e344d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 21:27:57	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_first
2019-09-03 21:27:58	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_second
2019-09-03 21:28:05	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_third
2019-09-03 21:28:07	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_fourth
2019-09-03 21:28:15	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_fifth
2019-09-03 21:28:16	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_six
2019-09-03 21:28:19	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_seven
2019-09-03 21:28:26	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_nine
2019-09-03 21:28:27	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_ten
2019-09-03 21:28:29	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_eleven
2019-09-03 21:28:36	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_twelve
2019-09-03 21:28:49	[scenario5d6ea297d0f954.31548261]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 21:28:51	[scenario5d6ea297d0f954.31548261]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 21:28:51	[scenario5d6ea297d0f954.31548261]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => c79a863d4b0a613ed67a9998fe9e344d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 21:28:52	[scenario5d6ea297d0f954.31548261]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 21:28:53	[scenario5d6ea297d0f954.31548261]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 21:28:54	[scenario5d6ea297d0f954.31548261]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 21:28:55	[scenario5d6ea297d0f954.31548261]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 21:28:55	[scenario5d6ea297d0f954.31548261]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c79a863d4b0a613ed67a9998fe9e344d
Context transitions => [
    "step_thirteen"
]

2019-09-03 21:28:56	[scenario5d6ea297d0f954.31548261]	Execute X-Cart step: step_thirteen
2019-09-03 21:28:58	[scenario5d6ea297d0f954.31548261]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 21:37:21	[scenario5d6ea4d13ca120.92415722]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 21:37:24	[scenario5d6ea4d13ca120.92415722]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 21:37:24	[scenario5d6ea4d13ca120.92415722]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 621a2dabe3305775180b821705d6d9d8
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 21:37:25	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_first
2019-09-03 21:37:26	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_second
2019-09-03 21:37:35	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_third
2019-09-03 21:37:36	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_fourth
2019-09-03 21:37:40	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_fifth
2019-09-03 21:37:41	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_six
2019-09-03 21:37:42	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_seven
2019-09-03 21:37:44	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_nine
2019-09-03 21:37:45	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_ten
2019-09-03 21:37:45	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_eleven
2019-09-03 21:37:45	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_twelve
2019-09-03 21:37:53	[scenario5d6ea4d13ca120.92415722]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 21:37:54	[scenario5d6ea4d13ca120.92415722]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 21:37:54	[scenario5d6ea4d13ca120.92415722]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 621a2dabe3305775180b821705d6d9d8
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 21:37:54	[scenario5d6ea4d13ca120.92415722]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 21:37:56	[scenario5d6ea4d13ca120.92415722]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 21:37:56	[scenario5d6ea4d13ca120.92415722]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 21:37:56	[scenario5d6ea4d13ca120.92415722]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 21:37:56	[scenario5d6ea4d13ca120.92415722]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 621a2dabe3305775180b821705d6d9d8
Context transitions => [
    "step_thirteen"
]

2019-09-03 21:37:57	[scenario5d6ea4d13ca120.92415722]	Execute X-Cart step: step_thirteen
2019-09-03 21:37:59	[scenario5d6ea4d13ca120.92415722]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 21:44:08	[scenario5d6ea668ce77e2.42986788]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 21:44:11	[scenario5d6ea668ce77e2.42986788]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 21:44:11	[scenario5d6ea668ce77e2.42986788]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => aa0bce3c2b79d8ed8f64396eb2bcb8dc
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 21:44:11	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_first
2019-09-03 21:44:12	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_second
2019-09-03 21:44:19	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_third
2019-09-03 21:44:20	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_fourth
2019-09-03 21:44:24	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_fifth
2019-09-03 21:44:24	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_six
2019-09-03 21:44:25	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_seven
2019-09-03 21:44:27	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_nine
2019-09-03 21:44:27	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_ten
2019-09-03 21:44:28	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_eleven
2019-09-03 21:44:28	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_twelve
2019-09-03 21:44:36	[scenario5d6ea668ce77e2.42986788]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 21:44:37	[scenario5d6ea668ce77e2.42986788]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 21:44:37	[scenario5d6ea668ce77e2.42986788]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => aa0bce3c2b79d8ed8f64396eb2bcb8dc
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 21:44:37	[scenario5d6ea668ce77e2.42986788]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 21:44:38	[scenario5d6ea668ce77e2.42986788]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 21:44:39	[scenario5d6ea668ce77e2.42986788]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 21:44:39	[scenario5d6ea668ce77e2.42986788]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 21:44:39	[scenario5d6ea668ce77e2.42986788]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => aa0bce3c2b79d8ed8f64396eb2bcb8dc
Context transitions => [
    "step_thirteen"
]

2019-09-03 21:44:39	[scenario5d6ea668ce77e2.42986788]	Execute X-Cart step: step_thirteen
2019-09-03 21:44:42	[scenario5d6ea668ce77e2.42986788]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 21:47:33	[scenario5d6ea735860fa1.70807082]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 21:47:36	[scenario5d6ea735860fa1.70807082]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 21:47:36	[scenario5d6ea735860fa1.70807082]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6ada973cd2da568e4bc63ed9b4800865
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 21:47:36	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_first
2019-09-03 21:47:37	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_second
2019-09-03 21:47:45	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_third
2019-09-03 21:47:46	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_fourth
2019-09-03 21:47:51	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_fifth
2019-09-03 21:47:51	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_six
2019-09-03 21:47:52	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_seven
2019-09-03 21:47:55	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_nine
2019-09-03 21:47:56	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_ten
2019-09-03 21:47:57	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_eleven
2019-09-03 21:47:57	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_twelve
2019-09-03 21:48:06	[scenario5d6ea735860fa1.70807082]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 21:48:08	[scenario5d6ea735860fa1.70807082]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 21:48:08	[scenario5d6ea735860fa1.70807082]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6ada973cd2da568e4bc63ed9b4800865
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 21:48:08	[scenario5d6ea735860fa1.70807082]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 21:48:08	[scenario5d6ea735860fa1.70807082]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 21:48:09	[scenario5d6ea735860fa1.70807082]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 21:48:09	[scenario5d6ea735860fa1.70807082]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 21:48:09	[scenario5d6ea735860fa1.70807082]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6ada973cd2da568e4bc63ed9b4800865
Context transitions => [
    "step_thirteen"
]

2019-09-03 21:48:09	[scenario5d6ea735860fa1.70807082]	Execute X-Cart step: step_thirteen
2019-09-03 21:48:12	[scenario5d6ea735860fa1.70807082]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 21:50:39	[scenario5d6ea7ef52dc11.18840428]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 21:50:42	[scenario5d6ea7ef52dc11.18840428]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 21:50:42	[scenario5d6ea7ef52dc11.18840428]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f1f898fc2e839e965602d6646191a89d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 21:50:42	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_first
2019-09-03 21:50:43	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_second
2019-09-03 21:50:52	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_third
2019-09-03 21:50:53	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_fourth
2019-09-03 21:50:57	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_fifth
2019-09-03 21:50:58	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_six
2019-09-03 21:50:59	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_seven
2019-09-03 21:51:02	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_nine
2019-09-03 21:51:03	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_ten
2019-09-03 21:51:04	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_eleven
2019-09-03 21:51:04	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_twelve
2019-09-03 21:51:15	[scenario5d6ea7ef52dc11.18840428]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 21:51:16	[scenario5d6ea7ef52dc11.18840428]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 21:51:16	[scenario5d6ea7ef52dc11.18840428]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f1f898fc2e839e965602d6646191a89d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 21:51:17	[scenario5d6ea7ef52dc11.18840428]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 21:51:17	[scenario5d6ea7ef52dc11.18840428]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 21:51:18	[scenario5d6ea7ef52dc11.18840428]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 21:51:18	[scenario5d6ea7ef52dc11.18840428]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 21:51:18	[scenario5d6ea7ef52dc11.18840428]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f1f898fc2e839e965602d6646191a89d
Context transitions => [
    "step_thirteen"
]

2019-09-03 21:51:19	[scenario5d6ea7ef52dc11.18840428]	Execute X-Cart step: step_thirteen
2019-09-03 21:51:22	[scenario5d6ea7ef52dc11.18840428]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 21:53:01	[scenario5d6ea87d4ac935.90441647]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 21:53:05	[scenario5d6ea87d4ac935.90441647]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 21:53:05	[scenario5d6ea87d4ac935.90441647]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f81ff3865350b0a54f70383cd32898a4
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 21:53:06	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_first
2019-09-03 21:53:07	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_second
2019-09-03 21:53:16	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_third
2019-09-03 21:53:18	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_fourth
2019-09-03 21:53:23	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_fifth
2019-09-03 21:53:24	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_six
2019-09-03 21:53:26	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_seven
2019-09-03 21:53:30	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_nine
2019-09-03 21:53:32	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_ten
2019-09-03 21:53:33	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_eleven
2019-09-03 21:53:34	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_twelve
2019-09-03 21:53:45	[scenario5d6ea87d4ac935.90441647]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 21:53:47	[scenario5d6ea87d4ac935.90441647]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 21:53:47	[scenario5d6ea87d4ac935.90441647]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f81ff3865350b0a54f70383cd32898a4
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 21:53:48	[scenario5d6ea87d4ac935.90441647]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 21:53:49	[scenario5d6ea87d4ac935.90441647]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 21:53:50	[scenario5d6ea87d4ac935.90441647]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 21:53:51	[scenario5d6ea87d4ac935.90441647]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 21:53:51	[scenario5d6ea87d4ac935.90441647]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f81ff3865350b0a54f70383cd32898a4
Context transitions => [
    "step_thirteen"
]

2019-09-03 21:53:52	[scenario5d6ea87d4ac935.90441647]	Execute X-Cart step: step_thirteen
2019-09-03 21:53:57	[scenario5d6ea87d4ac935.90441647]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:00:32	[scenario5d6eaa40772f89.83079986]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:00:36	[scenario5d6eaa40772f89.83079986]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:00:36	[scenario5d6eaa40772f89.83079986]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 78da9d656d4fd1747c2063a49c290f45
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:00:36	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_first
2019-09-03 22:00:37	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_second
2019-09-03 22:00:44	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_third
2019-09-03 22:00:45	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_fourth
2019-09-03 22:00:49	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_fifth
2019-09-03 22:00:49	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_six
2019-09-03 22:00:50	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_seven
2019-09-03 22:00:52	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_nine
2019-09-03 22:00:53	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_ten
2019-09-03 22:00:53	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_eleven
2019-09-03 22:00:54	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_twelve
2019-09-03 22:01:02	[scenario5d6eaa40772f89.83079986]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:01:03	[scenario5d6eaa40772f89.83079986]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:01:03	[scenario5d6eaa40772f89.83079986]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 78da9d656d4fd1747c2063a49c290f45
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:01:03	[scenario5d6eaa40772f89.83079986]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:01:04	[scenario5d6eaa40772f89.83079986]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:01:04	[scenario5d6eaa40772f89.83079986]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:01:04	[scenario5d6eaa40772f89.83079986]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:01:04	[scenario5d6eaa40772f89.83079986]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 78da9d656d4fd1747c2063a49c290f45
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:01:05	[scenario5d6eaa40772f89.83079986]	Execute X-Cart step: step_thirteen
2019-09-03 22:01:07	[scenario5d6eaa40772f89.83079986]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:08:08	[scenario5d6eac08246318.80429311]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:08:10	[scenario5d6eac08246318.80429311]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:08:10	[scenario5d6eac08246318.80429311]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => faa2fd8d5b2470212dfdc66604ee40ef
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:08:10	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_first
2019-09-03 22:08:11	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_second
2019-09-03 22:08:27	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_third
2019-09-03 22:08:27	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_fourth
2019-09-03 22:08:31	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_fifth
2019-09-03 22:08:31	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_six
2019-09-03 22:08:32	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_seven
2019-09-03 22:08:34	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_nine
2019-09-03 22:08:35	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_ten
2019-09-03 22:08:35	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_eleven
2019-09-03 22:08:35	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_twelve
2019-09-03 22:08:42	[scenario5d6eac08246318.80429311]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:08:43	[scenario5d6eac08246318.80429311]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:08:43	[scenario5d6eac08246318.80429311]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => faa2fd8d5b2470212dfdc66604ee40ef
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:08:44	[scenario5d6eac08246318.80429311]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:08:44	[scenario5d6eac08246318.80429311]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:08:45	[scenario5d6eac08246318.80429311]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:08:45	[scenario5d6eac08246318.80429311]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:08:45	[scenario5d6eac08246318.80429311]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => faa2fd8d5b2470212dfdc66604ee40ef
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:08:45	[scenario5d6eac08246318.80429311]	Execute X-Cart step: step_thirteen
2019-09-03 22:08:47	[scenario5d6eac08246318.80429311]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:11:04	[scenario5d6e85ecab8b46.10773397]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 22:11:04	[scenario5d6e85ecab8b46.10773397]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 22:11:04	[scenario5d6e85ecab8b46.10773397]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 22:11:04	[scenario5d6e85ecab8b46.10773397]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-03 22:11:04	[scenario5d6e85ecab8b46.10773397]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => af17ad15f3b927a06d7b57c24cb51e52
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:11:04	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_first
2019-09-03 22:11:05	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_second
2019-09-03 22:11:10	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_third
2019-09-03 22:11:11	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_fourth
2019-09-03 22:11:13	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_fifth
2019-09-03 22:11:14	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_six
2019-09-03 22:11:15	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_seven
2019-09-03 22:11:17	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_nine
2019-09-03 22:11:17	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_ten
2019-09-03 22:11:18	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_eleven
2019-09-03 22:11:18	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_twelve
2019-09-03 22:11:26	[scenario5d6e85ecab8b46.10773397]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 22:11:27	[scenario5d6e85ecab8b46.10773397]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
    "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
]

2019-09-03 22:11:27	[scenario5d6e85ecab8b46.10773397]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 22:11:27	[scenario5d6e85ecab8b46.10773397]	Data updated: TrueMachine-AgeVerification
2019-09-03 22:11:27	[scenario5d6e85ecab8b46.10773397]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:11:27	[scenario5d6e85ecab8b46.10773397]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => af17ad15f3b927a06d7b57c24cb51e52
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:11:28	[scenario5d6e85ecab8b46.10773397]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:11:28	[scenario5d6e85ecab8b46.10773397]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:11:29	[scenario5d6e85ecab8b46.10773397]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:11:29	[scenario5d6e85ecab8b46.10773397]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:11:29	[scenario5d6e85ecab8b46.10773397]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => af17ad15f3b927a06d7b57c24cb51e52
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:11:29	[scenario5d6e85ecab8b46.10773397]	Execute X-Cart step: step_thirteen
2019-09-03 22:11:31	[scenario5d6e85ecab8b46.10773397]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:15:03	[scenario5d6eada70be3a0.53636975]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:15:05	[scenario5d6eada70be3a0.53636975]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-03 22:15:05	[scenario5d6eada70be3a0.53636975]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 971b5c64a112730e294ff9c91502b95f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:15:05	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_first
2019-09-03 22:15:06	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_second
2019-09-03 22:15:11	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_third
2019-09-03 22:15:12	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_fourth
2019-09-03 22:15:14	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_fifth
2019-09-03 22:15:15	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_six
2019-09-03 22:15:15	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_seven
2019-09-03 22:15:17	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_nine
2019-09-03 22:15:17	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_ten
2019-09-03 22:15:18	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_eleven
2019-09-03 22:15:18	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_twelve
2019-09-03 22:15:24	[scenario5d6eada70be3a0.53636975]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:15:24	[scenario5d6eada70be3a0.53636975]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-03 22:15:24	[scenario5d6eada70be3a0.53636975]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 971b5c64a112730e294ff9c91502b95f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:15:24	[scenario5d6eada70be3a0.53636975]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:15:25	[scenario5d6eada70be3a0.53636975]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:15:25	[scenario5d6eada70be3a0.53636975]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:15:25	[scenario5d6eada70be3a0.53636975]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:15:25	[scenario5d6eada70be3a0.53636975]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 971b5c64a112730e294ff9c91502b95f
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:15:25	[scenario5d6eada70be3a0.53636975]	Execute X-Cart step: step_thirteen
2019-09-03 22:15:27	[scenario5d6eada70be3a0.53636975]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:15:41	[scenario5d6eada82e9077.40316557]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 22:15:41	[scenario5d6eada82e9077.40316557]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 22:15:41	[scenario5d6eada82e9077.40316557]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 22:15:42	[scenario5d6eada82e9077.40316557]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:15:42	[scenario5d6eada82e9077.40316557]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8823f231a4722f03568a5f6d6f6c238b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:15:42	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_first
2019-09-03 22:15:42	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_second
2019-09-03 22:15:45	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_third
2019-09-03 22:15:46	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_fourth
2019-09-03 22:15:48	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_fifth
2019-09-03 22:15:49	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_six
2019-09-03 22:15:50	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_seven
2019-09-03 22:15:51	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_nine
2019-09-03 22:15:52	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_ten
2019-09-03 22:15:52	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_eleven
2019-09-03 22:15:52	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_twelve
2019-09-03 22:15:57	[scenario5d6eada82e9077.40316557]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-03 22:15:58	[scenario5d6eada82e9077.40316557]	Data updated: TrueMachine-AgeVerification
2019-09-03 22:15:58	[scenario5d6eada82e9077.40316557]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:15:58	[scenario5d6eada82e9077.40316557]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8823f231a4722f03568a5f6d6f6c238b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:15:58	[scenario5d6eada82e9077.40316557]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:15:58	[scenario5d6eada82e9077.40316557]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:15:59	[scenario5d6eada82e9077.40316557]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:15:59	[scenario5d6eada82e9077.40316557]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:15:59	[scenario5d6eada82e9077.40316557]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8823f231a4722f03568a5f6d6f6c238b
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:15:59	[scenario5d6eada82e9077.40316557]	Execute X-Cart step: step_thirteen
2019-09-03 22:16:00	[scenario5d6eada82e9077.40316557]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:23:49	[scenario5d6eafb529a096.91785454]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:23:51	[scenario5d6eafb529a096.91785454]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:23:51	[scenario5d6eafb529a096.91785454]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 7b01967130272ad97365c22547f47cdd
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:23:51	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_first
2019-09-03 22:23:51	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_second
2019-09-03 22:23:57	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_third
2019-09-03 22:23:57	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_fourth
2019-09-03 22:24:00	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_fifth
2019-09-03 22:24:00	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_six
2019-09-03 22:24:01	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_seven
2019-09-03 22:24:02	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_nine
2019-09-03 22:24:03	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_ten
2019-09-03 22:24:03	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_eleven
2019-09-03 22:24:03	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_twelve
2019-09-03 22:24:09	[scenario5d6eafb529a096.91785454]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:24:10	[scenario5d6eafb529a096.91785454]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:24:10	[scenario5d6eafb529a096.91785454]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 7b01967130272ad97365c22547f47cdd
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:24:10	[scenario5d6eafb529a096.91785454]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:24:10	[scenario5d6eafb529a096.91785454]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:24:11	[scenario5d6eafb529a096.91785454]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:24:11	[scenario5d6eafb529a096.91785454]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:24:11	[scenario5d6eafb529a096.91785454]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 7b01967130272ad97365c22547f47cdd
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:24:11	[scenario5d6eafb529a096.91785454]	Execute X-Cart step: step_thirteen
2019-09-03 22:24:13	[scenario5d6eafb529a096.91785454]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:25:58	[scenario5d6eb03642e104.49653792]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:26:00	[scenario5d6eb03642e104.49653792]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:26:00	[scenario5d6eb03642e104.49653792]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b3091a0d136fcd894dcc15c77d2b23f5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:26:00	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_first
2019-09-03 22:26:00	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_second
2019-09-03 22:26:05	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_third
2019-09-03 22:26:05	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_fourth
2019-09-03 22:26:07	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_fifth
2019-09-03 22:26:08	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_six
2019-09-03 22:26:09	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_seven
2019-09-03 22:26:10	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_nine
2019-09-03 22:26:11	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_ten
2019-09-03 22:26:11	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_eleven
2019-09-03 22:26:12	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_twelve
2019-09-03 22:26:17	[scenario5d6eb03642e104.49653792]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:26:18	[scenario5d6eb03642e104.49653792]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:26:18	[scenario5d6eb03642e104.49653792]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b3091a0d136fcd894dcc15c77d2b23f5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:26:19	[scenario5d6eb03642e104.49653792]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:26:20	[scenario5d6eb03642e104.49653792]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:26:21	[scenario5d6eb03642e104.49653792]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:26:22	[scenario5d6eb03642e104.49653792]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:26:22	[scenario5d6eb03642e104.49653792]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b3091a0d136fcd894dcc15c77d2b23f5
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:26:23	[scenario5d6eb03642e104.49653792]	Execute X-Cart step: step_thirteen
2019-09-03 22:26:24	[scenario5d6eb03642e104.49653792]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:28:02	[scenario5d6eb0b288fe57.34217925]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:28:04	[scenario5d6eb0b288fe57.34217925]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:28:04	[scenario5d6eb0b288fe57.34217925]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a1b720b96daa916f4901c9985f4e3dbc
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:28:04	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_first
2019-09-03 22:28:05	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_second
2019-09-03 22:28:11	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_third
2019-09-03 22:28:11	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_fourth
2019-09-03 22:28:14	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_fifth
2019-09-03 22:28:14	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_six
2019-09-03 22:28:15	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_seven
2019-09-03 22:28:16	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_nine
2019-09-03 22:28:17	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_ten
2019-09-03 22:28:17	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_eleven
2019-09-03 22:28:17	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_twelve
2019-09-03 22:28:23	[scenario5d6eb0b288fe57.34217925]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:28:24	[scenario5d6eb0b288fe57.34217925]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:28:24	[scenario5d6eb0b288fe57.34217925]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a1b720b96daa916f4901c9985f4e3dbc
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:28:24	[scenario5d6eb0b288fe57.34217925]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:28:24	[scenario5d6eb0b288fe57.34217925]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:28:25	[scenario5d6eb0b288fe57.34217925]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:28:25	[scenario5d6eb0b288fe57.34217925]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:28:25	[scenario5d6eb0b288fe57.34217925]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a1b720b96daa916f4901c9985f4e3dbc
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:28:25	[scenario5d6eb0b288fe57.34217925]	Execute X-Cart step: step_thirteen
2019-09-03 22:28:27	[scenario5d6eb0b288fe57.34217925]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:30:59	[scenario5d6eb163c598d6.58236565]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:31:01	[scenario5d6eb163c598d6.58236565]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:31:01	[scenario5d6eb163c598d6.58236565]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bf1681407336c6ed1d9550b076253cc7
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:31:01	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_first
2019-09-03 22:31:02	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_second
2019-09-03 22:31:07	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_third
2019-09-03 22:31:08	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_fourth
2019-09-03 22:31:11	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_fifth
2019-09-03 22:31:11	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_six
2019-09-03 22:31:12	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_seven
2019-09-03 22:31:13	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_nine
2019-09-03 22:31:14	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_ten
2019-09-03 22:31:14	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_eleven
2019-09-03 22:31:14	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_twelve
2019-09-03 22:31:21	[scenario5d6eb163c598d6.58236565]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:31:22	[scenario5d6eb163c598d6.58236565]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:31:22	[scenario5d6eb163c598d6.58236565]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bf1681407336c6ed1d9550b076253cc7
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:31:22	[scenario5d6eb163c598d6.58236565]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:31:22	[scenario5d6eb163c598d6.58236565]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:31:23	[scenario5d6eb163c598d6.58236565]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:31:23	[scenario5d6eb163c598d6.58236565]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:31:23	[scenario5d6eb163c598d6.58236565]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bf1681407336c6ed1d9550b076253cc7
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:31:23	[scenario5d6eb163c598d6.58236565]	Execute X-Cart step: step_thirteen
2019-09-03 22:31:25	[scenario5d6eb163c598d6.58236565]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:38:38	[scenario5d6eb32ed60d48.42313270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:38:40	[scenario5d6eb32ed60d48.42313270]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:38:40	[scenario5d6eb32ed60d48.42313270]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9d6cd5129f9e84747c1d278cc6365f6a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:38:40	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_first
2019-09-03 22:38:41	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_second
2019-09-03 22:38:47	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_third
2019-09-03 22:38:47	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_fourth
2019-09-03 22:38:50	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_fifth
2019-09-03 22:38:50	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_six
2019-09-03 22:38:51	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_seven
2019-09-03 22:38:52	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_nine
2019-09-03 22:38:53	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_ten
2019-09-03 22:38:54	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_eleven
2019-09-03 22:38:54	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_twelve
2019-09-03 22:39:00	[scenario5d6eb32ed60d48.42313270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:39:00	[scenario5d6eb32ed60d48.42313270]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:39:00	[scenario5d6eb32ed60d48.42313270]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9d6cd5129f9e84747c1d278cc6365f6a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:39:00	[scenario5d6eb32ed60d48.42313270]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:39:01	[scenario5d6eb32ed60d48.42313270]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:39:01	[scenario5d6eb32ed60d48.42313270]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:39:02	[scenario5d6eb32ed60d48.42313270]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:39:02	[scenario5d6eb32ed60d48.42313270]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9d6cd5129f9e84747c1d278cc6365f6a
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:39:02	[scenario5d6eb32ed60d48.42313270]	Execute X-Cart step: step_thirteen
2019-09-03 22:39:04	[scenario5d6eb32ed60d48.42313270]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:39:34	[scenario5d6eb366a8c5f9.74513839]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:39:37	[scenario5d6eb366a8c5f9.74513839]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:39:37	[scenario5d6eb366a8c5f9.74513839]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a90acaf85b3a8162c3e171f81add4c28
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:39:37	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_first
2019-09-03 22:39:38	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_second
2019-09-03 22:39:43	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_third
2019-09-03 22:39:43	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_fourth
2019-09-03 22:39:47	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_fifth
2019-09-03 22:39:47	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_six
2019-09-03 22:39:48	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_seven
2019-09-03 22:39:50	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_nine
2019-09-03 22:39:51	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_ten
2019-09-03 22:39:51	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_eleven
2019-09-03 22:39:52	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_twelve
2019-09-03 22:39:59	[scenario5d6eb366a8c5f9.74513839]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:40:00	[scenario5d6eb366a8c5f9.74513839]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:40:00	[scenario5d6eb366a8c5f9.74513839]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a90acaf85b3a8162c3e171f81add4c28
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:40:00	[scenario5d6eb366a8c5f9.74513839]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:40:01	[scenario5d6eb366a8c5f9.74513839]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:40:01	[scenario5d6eb366a8c5f9.74513839]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:40:01	[scenario5d6eb366a8c5f9.74513839]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:40:02	[scenario5d6eb366a8c5f9.74513839]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a90acaf85b3a8162c3e171f81add4c28
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:40:02	[scenario5d6eb366a8c5f9.74513839]	Execute X-Cart step: step_thirteen
2019-09-03 22:40:04	[scenario5d6eb366a8c5f9.74513839]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:42:28	[scenario5d6eb414f1f4a8.13837329]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:42:31	[scenario5d6eb414f1f4a8.13837329]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:42:31	[scenario5d6eb414f1f4a8.13837329]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8dd734010233f5ed062f4233c63fc0d9
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:42:31	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_first
2019-09-03 22:42:32	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_second
2019-09-03 22:42:39	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_third
2019-09-03 22:42:39	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_fourth
2019-09-03 22:42:43	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_fifth
2019-09-03 22:42:44	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_six
2019-09-03 22:42:45	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_seven
2019-09-03 22:42:47	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_nine
2019-09-03 22:42:48	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_ten
2019-09-03 22:42:49	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_eleven
2019-09-03 22:42:49	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_twelve
2019-09-03 22:42:56	[scenario5d6eb414f1f4a8.13837329]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:42:56	[scenario5d6eb414f1f4a8.13837329]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:42:56	[scenario5d6eb414f1f4a8.13837329]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8dd734010233f5ed062f4233c63fc0d9
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:42:57	[scenario5d6eb414f1f4a8.13837329]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:42:57	[scenario5d6eb414f1f4a8.13837329]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:42:58	[scenario5d6eb414f1f4a8.13837329]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:42:58	[scenario5d6eb414f1f4a8.13837329]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:42:58	[scenario5d6eb414f1f4a8.13837329]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8dd734010233f5ed062f4233c63fc0d9
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:42:58	[scenario5d6eb414f1f4a8.13837329]	Execute X-Cart step: step_thirteen
2019-09-03 22:43:00	[scenario5d6eb414f1f4a8.13837329]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:43:57	[scenario5d6eb46d2a22b4.06350357]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:43:59	[scenario5d6eb46d2a22b4.06350357]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:43:59	[scenario5d6eb46d2a22b4.06350357]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 368fe648217a307f90592c41d5eaaa15
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:43:59	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_first
2019-09-03 22:43:59	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_second
2019-09-03 22:44:04	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_third
2019-09-03 22:44:04	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_fourth
2019-09-03 22:44:07	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_fifth
2019-09-03 22:44:07	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_six
2019-09-03 22:44:08	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_seven
2019-09-03 22:44:09	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_nine
2019-09-03 22:44:10	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_ten
2019-09-03 22:44:10	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_eleven
2019-09-03 22:44:10	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_twelve
2019-09-03 22:44:15	[scenario5d6eb46d2a22b4.06350357]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:44:16	[scenario5d6eb46d2a22b4.06350357]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:44:16	[scenario5d6eb46d2a22b4.06350357]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 368fe648217a307f90592c41d5eaaa15
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:44:16	[scenario5d6eb46d2a22b4.06350357]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:44:17	[scenario5d6eb46d2a22b4.06350357]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:44:17	[scenario5d6eb46d2a22b4.06350357]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:44:17	[scenario5d6eb46d2a22b4.06350357]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:44:18	[scenario5d6eb46d2a22b4.06350357]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 368fe648217a307f90592c41d5eaaa15
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:44:18	[scenario5d6eb46d2a22b4.06350357]	Execute X-Cart step: step_thirteen
2019-09-03 22:44:20	[scenario5d6eb46d2a22b4.06350357]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:45:50	[scenario5d6eb4dedeb536.52187915]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:45:53	[scenario5d6eb4dedeb536.52187915]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:45:53	[scenario5d6eb4dedeb536.52187915]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f94770a7f7c0a037c1b9d568c7bf3148
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:45:53	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_first
2019-09-03 22:45:54	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_second
2019-09-03 22:46:00	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_third
2019-09-03 22:46:01	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_fourth
2019-09-03 22:46:05	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_fifth
2019-09-03 22:46:05	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_six
2019-09-03 22:46:06	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_seven
2019-09-03 22:46:07	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_nine
2019-09-03 22:46:08	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_ten
2019-09-03 22:46:08	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_eleven
2019-09-03 22:46:09	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_twelve
2019-09-03 22:46:15	[scenario5d6eb4dedeb536.52187915]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:46:16	[scenario5d6eb4dedeb536.52187915]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:46:16	[scenario5d6eb4dedeb536.52187915]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f94770a7f7c0a037c1b9d568c7bf3148
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:46:17	[scenario5d6eb4dedeb536.52187915]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:46:18	[scenario5d6eb4dedeb536.52187915]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:46:19	[scenario5d6eb4dedeb536.52187915]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:46:20	[scenario5d6eb4dedeb536.52187915]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:46:20	[scenario5d6eb4dedeb536.52187915]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f94770a7f7c0a037c1b9d568c7bf3148
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:46:21	[scenario5d6eb4dedeb536.52187915]	Execute X-Cart step: step_thirteen
2019-09-03 22:46:23	[scenario5d6eb4dedeb536.52187915]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:48:05	[scenario5d6eb5650d5214.84607789]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:48:07	[scenario5d6eb5650d5214.84607789]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:48:07	[scenario5d6eb5650d5214.84607789]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 09f55ee779dabd5b623a3c241da5beb5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:48:08	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_first
2019-09-03 22:48:08	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_second
2019-09-03 22:48:14	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_third
2019-09-03 22:48:15	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_fourth
2019-09-03 22:48:18	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_fifth
2019-09-03 22:48:19	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_six
2019-09-03 22:48:20	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_seven
2019-09-03 22:48:22	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_nine
2019-09-03 22:48:23	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_ten
2019-09-03 22:48:23	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_eleven
2019-09-03 22:48:23	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_twelve
2019-09-03 22:48:32	[scenario5d6eb5650d5214.84607789]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:48:33	[scenario5d6eb5650d5214.84607789]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:48:33	[scenario5d6eb5650d5214.84607789]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 09f55ee779dabd5b623a3c241da5beb5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:48:33	[scenario5d6eb5650d5214.84607789]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:48:34	[scenario5d6eb5650d5214.84607789]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:48:34	[scenario5d6eb5650d5214.84607789]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:48:34	[scenario5d6eb5650d5214.84607789]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:48:34	[scenario5d6eb5650d5214.84607789]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 09f55ee779dabd5b623a3c241da5beb5
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:48:34	[scenario5d6eb5650d5214.84607789]	Execute X-Cart step: step_thirteen
2019-09-03 22:48:37	[scenario5d6eb5650d5214.84607789]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:49:28	[scenario5d6eb5b890d558.78281408]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:49:30	[scenario5d6eb5b890d558.78281408]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:49:30	[scenario5d6eb5b890d558.78281408]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 75f94dc143e0116499f3998ebfc8ff25
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:49:30	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_first
2019-09-03 22:49:30	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_second
2019-09-03 22:49:34	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_third
2019-09-03 22:49:34	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_fourth
2019-09-03 22:49:37	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_fifth
2019-09-03 22:49:37	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_six
2019-09-03 22:49:38	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_seven
2019-09-03 22:49:39	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_nine
2019-09-03 22:49:40	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_ten
2019-09-03 22:49:40	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_eleven
2019-09-03 22:49:40	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_twelve
2019-09-03 22:49:45	[scenario5d6eb5b890d558.78281408]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:49:46	[scenario5d6eb5b890d558.78281408]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:49:46	[scenario5d6eb5b890d558.78281408]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 75f94dc143e0116499f3998ebfc8ff25
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:49:46	[scenario5d6eb5b890d558.78281408]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:49:47	[scenario5d6eb5b890d558.78281408]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:49:47	[scenario5d6eb5b890d558.78281408]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:49:47	[scenario5d6eb5b890d558.78281408]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:49:47	[scenario5d6eb5b890d558.78281408]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 75f94dc143e0116499f3998ebfc8ff25
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:49:47	[scenario5d6eb5b890d558.78281408]	Execute X-Cart step: step_thirteen
2019-09-03 22:49:49	[scenario5d6eb5b890d558.78281408]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:51:13	[scenario5d6eb6216d3936.65893415]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:51:16	[scenario5d6eb6216d3936.65893415]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:51:16	[scenario5d6eb6216d3936.65893415]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2d93748e551a1cce5449bd0a25bfefe0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:51:16	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_first
2019-09-03 22:51:17	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_second
2019-09-03 22:51:23	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_third
2019-09-03 22:51:24	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_fourth
2019-09-03 22:51:27	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_fifth
2019-09-03 22:51:28	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_six
2019-09-03 22:51:29	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_seven
2019-09-03 22:51:31	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_nine
2019-09-03 22:51:32	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_ten
2019-09-03 22:51:32	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_eleven
2019-09-03 22:51:32	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_twelve
2019-09-03 22:51:39	[scenario5d6eb6216d3936.65893415]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:51:40	[scenario5d6eb6216d3936.65893415]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:51:40	[scenario5d6eb6216d3936.65893415]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2d93748e551a1cce5449bd0a25bfefe0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:51:40	[scenario5d6eb6216d3936.65893415]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:51:41	[scenario5d6eb6216d3936.65893415]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:51:41	[scenario5d6eb6216d3936.65893415]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:51:41	[scenario5d6eb6216d3936.65893415]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:51:41	[scenario5d6eb6216d3936.65893415]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2d93748e551a1cce5449bd0a25bfefe0
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:51:41	[scenario5d6eb6216d3936.65893415]	Execute X-Cart step: step_thirteen
2019-09-03 22:51:44	[scenario5d6eb6216d3936.65893415]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:52:05	[scenario5d6eb655184312.12191487]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:52:06	[scenario5d6eb655184312.12191487]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:52:06	[scenario5d6eb655184312.12191487]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 76cbf0572c08ce68a9ec54d024021f94
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:52:06	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_first
2019-09-03 22:52:07	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_second
2019-09-03 22:52:11	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_third
2019-09-03 22:52:12	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_fourth
2019-09-03 22:52:14	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_fifth
2019-09-03 22:52:14	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_six
2019-09-03 22:52:15	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_seven
2019-09-03 22:52:16	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_nine
2019-09-03 22:52:17	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_ten
2019-09-03 22:52:17	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_eleven
2019-09-03 22:52:17	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_twelve
2019-09-03 22:52:22	[scenario5d6eb655184312.12191487]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:52:23	[scenario5d6eb655184312.12191487]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:52:23	[scenario5d6eb655184312.12191487]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 76cbf0572c08ce68a9ec54d024021f94
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:52:23	[scenario5d6eb655184312.12191487]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:52:23	[scenario5d6eb655184312.12191487]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:52:24	[scenario5d6eb655184312.12191487]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:52:24	[scenario5d6eb655184312.12191487]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:52:24	[scenario5d6eb655184312.12191487]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 76cbf0572c08ce68a9ec54d024021f94
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:52:24	[scenario5d6eb655184312.12191487]	Execute X-Cart step: step_thirteen
2019-09-03 22:52:26	[scenario5d6eb655184312.12191487]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 22:54:27	[scenario5d6eb6e3ccdf61.08340640]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 22:54:30	[scenario5d6eb6e3ccdf61.08340640]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 22:54:30	[scenario5d6eb6e3ccdf61.08340640]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b0a034645659e1f0861536b5a921f661
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 22:54:30	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_first
2019-09-03 22:54:31	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_second
2019-09-03 22:54:36	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_third
2019-09-03 22:54:37	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_fourth
2019-09-03 22:54:39	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_fifth
2019-09-03 22:54:40	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_six
2019-09-03 22:54:41	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_seven
2019-09-03 22:54:42	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_nine
2019-09-03 22:54:43	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_ten
2019-09-03 22:54:43	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_eleven
2019-09-03 22:54:43	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_twelve
2019-09-03 22:54:51	[scenario5d6eb6e3ccdf61.08340640]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 22:54:51	[scenario5d6eb6e3ccdf61.08340640]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 22:54:51	[scenario5d6eb6e3ccdf61.08340640]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b0a034645659e1f0861536b5a921f661
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 22:54:51	[scenario5d6eb6e3ccdf61.08340640]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 22:54:52	[scenario5d6eb6e3ccdf61.08340640]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 22:54:52	[scenario5d6eb6e3ccdf61.08340640]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 22:54:52	[scenario5d6eb6e3ccdf61.08340640]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 22:54:52	[scenario5d6eb6e3ccdf61.08340640]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b0a034645659e1f0861536b5a921f661
Context transitions => [
    "step_thirteen"
]

2019-09-03 22:54:53	[scenario5d6eb6e3ccdf61.08340640]	Execute X-Cart step: step_thirteen
2019-09-03 22:54:55	[scenario5d6eb6e3ccdf61.08340640]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 23:00:40	[scenario5d6eb858982f60.39313011]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 23:00:42	[scenario5d6eb858982f60.39313011]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 23:00:42	[scenario5d6eb858982f60.39313011]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2d4183c39ed737058adb76ae7dc9946b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 23:00:42	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_first
2019-09-03 23:00:43	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_second
2019-09-03 23:00:48	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_third
2019-09-03 23:00:48	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_fourth
2019-09-03 23:00:51	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_fifth
2019-09-03 23:00:51	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_six
2019-09-03 23:00:52	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_seven
2019-09-03 23:00:53	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_nine
2019-09-03 23:00:54	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_ten
2019-09-03 23:00:54	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_eleven
2019-09-03 23:00:54	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_twelve
2019-09-03 23:01:00	[scenario5d6eb858982f60.39313011]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 23:01:01	[scenario5d6eb858982f60.39313011]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 23:01:01	[scenario5d6eb858982f60.39313011]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2d4183c39ed737058adb76ae7dc9946b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 23:01:01	[scenario5d6eb858982f60.39313011]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 23:01:01	[scenario5d6eb858982f60.39313011]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 23:01:01	[scenario5d6eb858982f60.39313011]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 23:01:02	[scenario5d6eb858982f60.39313011]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 23:01:02	[scenario5d6eb858982f60.39313011]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2d4183c39ed737058adb76ae7dc9946b
Context transitions => [
    "step_thirteen"
]

2019-09-03 23:01:02	[scenario5d6eb858982f60.39313011]	Execute X-Cart step: step_thirteen
2019-09-03 23:01:04	[scenario5d6eb858982f60.39313011]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 23:06:49	[scenario5d6eb9c9d9c576.71044351]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 23:06:51	[scenario5d6eb9c9d9c576.71044351]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 23:06:51	[scenario5d6eb9c9d9c576.71044351]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d0844af7a9857d996f34ebb90e1fba4e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 23:06:51	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_first
2019-09-03 23:06:52	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_second
2019-09-03 23:06:57	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_third
2019-09-03 23:06:58	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_fourth
2019-09-03 23:07:01	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_fifth
2019-09-03 23:07:01	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_six
2019-09-03 23:07:02	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_seven
2019-09-03 23:07:04	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_nine
2019-09-03 23:07:05	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_ten
2019-09-03 23:07:05	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_eleven
2019-09-03 23:07:06	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_twelve
2019-09-03 23:07:13	[scenario5d6eb9c9d9c576.71044351]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 23:07:14	[scenario5d6eb9c9d9c576.71044351]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 23:07:14	[scenario5d6eb9c9d9c576.71044351]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d0844af7a9857d996f34ebb90e1fba4e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 23:07:14	[scenario5d6eb9c9d9c576.71044351]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 23:07:15	[scenario5d6eb9c9d9c576.71044351]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 23:07:15	[scenario5d6eb9c9d9c576.71044351]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 23:07:16	[scenario5d6eb9c9d9c576.71044351]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 23:07:16	[scenario5d6eb9c9d9c576.71044351]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d0844af7a9857d996f34ebb90e1fba4e
Context transitions => [
    "step_thirteen"
]

2019-09-03 23:07:16	[scenario5d6eb9c9d9c576.71044351]	Execute X-Cart step: step_thirteen
2019-09-03 23:07:18	[scenario5d6eb9c9d9c576.71044351]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 23:43:10	[scenario5d6ec24e446827.14251551]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 23:43:12	[scenario5d6ec24e446827.14251551]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 23:43:12	[scenario5d6ec24e446827.14251551]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9611d4badd7ee2c039e3f358b9a20905
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 23:43:12	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_first
2019-09-03 23:43:13	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_second
2019-09-03 23:43:19	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_third
2019-09-03 23:43:19	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_fourth
2019-09-03 23:43:22	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_fifth
2019-09-03 23:43:22	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_six
2019-09-03 23:43:24	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_seven
2019-09-03 23:43:25	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_nine
2019-09-03 23:43:26	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_ten
2019-09-03 23:43:26	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_eleven
2019-09-03 23:43:26	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_twelve
2019-09-03 23:43:33	[scenario5d6ec24e446827.14251551]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 23:43:34	[scenario5d6ec24e446827.14251551]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 23:43:34	[scenario5d6ec24e446827.14251551]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9611d4badd7ee2c039e3f358b9a20905
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 23:43:34	[scenario5d6ec24e446827.14251551]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 23:43:35	[scenario5d6ec24e446827.14251551]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 23:43:35	[scenario5d6ec24e446827.14251551]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 23:43:35	[scenario5d6ec24e446827.14251551]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 23:43:35	[scenario5d6ec24e446827.14251551]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9611d4badd7ee2c039e3f358b9a20905
Context transitions => [
    "step_thirteen"
]

2019-09-03 23:43:36	[scenario5d6ec24e446827.14251551]	Execute X-Cart step: step_thirteen
2019-09-03 23:43:38	[scenario5d6ec24e446827.14251551]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-03 23:44:57	[scenario5d6ec2b9c91cd0.45169844]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-03 23:45:00	[scenario5d6ec2b9c91cd0.45169844]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-03 23:45:00	[scenario5d6ec2b9c91cd0.45169844]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 51ba37452b1d8efb5e1b9a90cf6c906f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-03 23:45:01	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_first
2019-09-03 23:45:02	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_second
2019-09-03 23:45:08	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_third
2019-09-03 23:45:09	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_fourth
2019-09-03 23:45:13	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_fifth
2019-09-03 23:45:13	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_six
2019-09-03 23:45:14	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_seven
2019-09-03 23:45:16	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_nine
2019-09-03 23:45:17	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_ten
2019-09-03 23:45:17	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_eleven
2019-09-03 23:45:17	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_twelve
2019-09-03 23:45:25	[scenario5d6ec2b9c91cd0.45169844]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-03 23:45:26	[scenario5d6ec2b9c91cd0.45169844]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-03 23:45:26	[scenario5d6ec2b9c91cd0.45169844]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 51ba37452b1d8efb5e1b9a90cf6c906f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-03 23:45:27	[scenario5d6ec2b9c91cd0.45169844]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-03 23:45:27	[scenario5d6ec2b9c91cd0.45169844]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-03 23:45:28	[scenario5d6ec2b9c91cd0.45169844]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-03 23:45:28	[scenario5d6ec2b9c91cd0.45169844]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-03 23:45:28	[scenario5d6ec2b9c91cd0.45169844]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 51ba37452b1d8efb5e1b9a90cf6c906f
Context transitions => [
    "step_thirteen"
]

2019-09-03 23:45:28	[scenario5d6ec2b9c91cd0.45169844]	Execute X-Cart step: step_thirteen
2019-09-03 23:45:31	[scenario5d6ec2b9c91cd0.45169844]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

