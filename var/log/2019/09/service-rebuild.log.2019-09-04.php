<?php die(); ?>
2019-09-04 08:03:13	[scenario5d6f3781c24e75.93218448]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 08:03:15	[scenario5d6f3781c24e75.93218448]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:03:15	[scenario5d6f3781c24e75.93218448]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 76c17be8c4031d30fe6bef0b9ce344cd
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:03:15	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_first
2019-09-04 08:03:15	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_second
2019-09-04 08:03:20	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_third
2019-09-04 08:03:21	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_fourth
2019-09-04 08:03:23	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_fifth
2019-09-04 08:03:23	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_six
2019-09-04 08:03:24	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_seven
2019-09-04 08:03:25	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_nine
2019-09-04 08:03:26	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_ten
2019-09-04 08:03:26	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_eleven
2019-09-04 08:03:26	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_twelve
2019-09-04 08:03:32	[scenario5d6f3781c24e75.93218448]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 08:03:33	[scenario5d6f3781c24e75.93218448]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 08:03:33	[scenario5d6f3781c24e75.93218448]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 76c17be8c4031d30fe6bef0b9ce344cd
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:03:33	[scenario5d6f3781c24e75.93218448]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:03:33	[scenario5d6f3781c24e75.93218448]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:03:33	[scenario5d6f3781c24e75.93218448]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:03:34	[scenario5d6f3781c24e75.93218448]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:03:34	[scenario5d6f3781c24e75.93218448]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 76c17be8c4031d30fe6bef0b9ce344cd
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:03:34	[scenario5d6f3781c24e75.93218448]	Execute X-Cart step: step_thirteen
2019-09-04 08:03:35	[scenario5d6f3781c24e75.93218448]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 08:04:08	[scenario5d6f37b8db3f01.25130598]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 08:04:10	[scenario5d6f37b8db3f01.25130598]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:04:10	[scenario5d6f37b8db3f01.25130598]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4aee1949e2860c8123c4723594b27902
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:04:10	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_first
2019-09-04 08:04:10	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_second
2019-09-04 08:04:13	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_third
2019-09-04 08:04:14	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_fourth
2019-09-04 08:04:16	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_fifth
2019-09-04 08:04:16	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_six
2019-09-04 08:04:17	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_seven
2019-09-04 08:04:18	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_nine
2019-09-04 08:04:18	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_ten
2019-09-04 08:04:19	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_eleven
2019-09-04 08:04:19	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_twelve
2019-09-04 08:04:24	[scenario5d6f37b8db3f01.25130598]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 08:04:25	[scenario5d6f37b8db3f01.25130598]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 08:04:25	[scenario5d6f37b8db3f01.25130598]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4aee1949e2860c8123c4723594b27902
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:04:25	[scenario5d6f37b8db3f01.25130598]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:04:25	[scenario5d6f37b8db3f01.25130598]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:04:25	[scenario5d6f37b8db3f01.25130598]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:04:25	[scenario5d6f37b8db3f01.25130598]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:04:26	[scenario5d6f37b8db3f01.25130598]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4aee1949e2860c8123c4723594b27902
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:04:26	[scenario5d6f37b8db3f01.25130598]	Execute X-Cart step: step_thirteen
2019-09-04 08:04:27	[scenario5d6f37b8db3f01.25130598]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 08:08:06	[scenario5d6f38a625f469.85923097]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 08:08:07	[scenario5d6f38a625f469.85923097]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:08:07	[scenario5d6f38a625f469.85923097]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 67c9cf46334e7b3a79bd0baeaa692e80
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:08:07	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_first
2019-09-04 08:08:08	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_second
2019-09-04 08:08:12	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_third
2019-09-04 08:08:13	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_fourth
2019-09-04 08:08:15	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_fifth
2019-09-04 08:08:15	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_six
2019-09-04 08:08:16	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_seven
2019-09-04 08:08:17	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_nine
2019-09-04 08:08:18	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_ten
2019-09-04 08:08:18	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_eleven
2019-09-04 08:08:18	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_twelve
2019-09-04 08:08:25	[scenario5d6f38a625f469.85923097]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 08:08:26	[scenario5d6f38a625f469.85923097]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 08:08:26	[scenario5d6f38a625f469.85923097]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 67c9cf46334e7b3a79bd0baeaa692e80
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:08:26	[scenario5d6f38a625f469.85923097]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:08:26	[scenario5d6f38a625f469.85923097]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:08:26	[scenario5d6f38a625f469.85923097]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:08:27	[scenario5d6f38a625f469.85923097]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:08:27	[scenario5d6f38a625f469.85923097]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 67c9cf46334e7b3a79bd0baeaa692e80
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:08:27	[scenario5d6f38a625f469.85923097]	Execute X-Cart step: step_thirteen
2019-09-04 08:08:28	[scenario5d6f38a625f469.85923097]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 08:11:05	[scenario5d6f3959501818.61950501]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 08:11:06	[scenario5d6f3959501818.61950501]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:11:06	[scenario5d6f3959501818.61950501]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5325ddbef60bec4c4fbf3c1e54257ba0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:11:06	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_first
2019-09-04 08:11:07	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_second
2019-09-04 08:11:12	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_third
2019-09-04 08:11:12	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_fourth
2019-09-04 08:11:14	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_fifth
2019-09-04 08:11:15	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_six
2019-09-04 08:11:15	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_seven
2019-09-04 08:11:17	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_nine
2019-09-04 08:11:17	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_ten
2019-09-04 08:11:18	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_eleven
2019-09-04 08:11:18	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_twelve
2019-09-04 08:11:25	[scenario5d6f3959501818.61950501]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 08:11:25	[scenario5d6f3959501818.61950501]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 08:11:25	[scenario5d6f3959501818.61950501]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5325ddbef60bec4c4fbf3c1e54257ba0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:11:25	[scenario5d6f3959501818.61950501]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:11:26	[scenario5d6f3959501818.61950501]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:11:26	[scenario5d6f3959501818.61950501]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:11:26	[scenario5d6f3959501818.61950501]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:11:26	[scenario5d6f3959501818.61950501]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5325ddbef60bec4c4fbf3c1e54257ba0
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:11:26	[scenario5d6f3959501818.61950501]	Execute X-Cart step: step_thirteen
2019-09-04 08:11:28	[scenario5d6f3959501818.61950501]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 08:12:32	[scenario5d6f39b043e864.09179500]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 08:12:33	[scenario5d6f39b043e864.09179500]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:12:33	[scenario5d6f39b043e864.09179500]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f39e5192c1408db00b721163ea35bd9e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:12:33	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_first
2019-09-04 08:12:34	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_second
2019-09-04 08:12:40	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_third
2019-09-04 08:12:40	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_fourth
2019-09-04 08:12:43	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_fifth
2019-09-04 08:12:43	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_six
2019-09-04 08:12:44	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_seven
2019-09-04 08:12:45	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_nine
2019-09-04 08:12:46	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_ten
2019-09-04 08:12:46	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_eleven
2019-09-04 08:12:47	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_twelve
2019-09-04 08:12:53	[scenario5d6f39b043e864.09179500]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 08:12:54	[scenario5d6f39b043e864.09179500]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 08:12:54	[scenario5d6f39b043e864.09179500]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f39e5192c1408db00b721163ea35bd9e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:12:54	[scenario5d6f39b043e864.09179500]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:12:54	[scenario5d6f39b043e864.09179500]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:12:55	[scenario5d6f39b043e864.09179500]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:12:55	[scenario5d6f39b043e864.09179500]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:12:55	[scenario5d6f39b043e864.09179500]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f39e5192c1408db00b721163ea35bd9e
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:12:55	[scenario5d6f39b043e864.09179500]	Execute X-Cart step: step_thirteen
2019-09-04 08:12:57	[scenario5d6f39b043e864.09179500]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 08:13:44	[scenario5d6f39f8ad7d92.19865503]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 08:13:46	[scenario5d6f39f8ad7d92.19865503]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:13:46	[scenario5d6f39f8ad7d92.19865503]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => de83e2c2a3b587b8c3721ec28bd7fcd3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:13:47	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_first
2019-09-04 08:13:47	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_second
2019-09-04 08:13:53	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_third
2019-09-04 08:13:54	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_fourth
2019-09-04 08:13:57	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_fifth
2019-09-04 08:13:58	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_six
2019-09-04 08:13:59	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_seven
2019-09-04 08:14:01	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_nine
2019-09-04 08:14:02	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_ten
2019-09-04 08:14:02	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_eleven
2019-09-04 08:14:03	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_twelve
2019-09-04 08:14:11	[scenario5d6f39f8ad7d92.19865503]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 08:14:12	[scenario5d6f39f8ad7d92.19865503]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 08:14:12	[scenario5d6f39f8ad7d92.19865503]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => de83e2c2a3b587b8c3721ec28bd7fcd3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:14:12	[scenario5d6f39f8ad7d92.19865503]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:14:13	[scenario5d6f39f8ad7d92.19865503]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:14:13	[scenario5d6f39f8ad7d92.19865503]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:14:14	[scenario5d6f39f8ad7d92.19865503]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:14:14	[scenario5d6f39f8ad7d92.19865503]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => de83e2c2a3b587b8c3721ec28bd7fcd3
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:14:14	[scenario5d6f39f8ad7d92.19865503]	Execute X-Cart step: step_thirteen
2019-09-04 08:14:16	[scenario5d6f39f8ad7d92.19865503]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 08:15:12	[scenario5d6f3a50127797.89258667]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 08:15:14	[scenario5d6f3a50127797.89258667]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:15:14	[scenario5d6f3a50127797.89258667]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 29407a617393e1ff56a8cc92fe8a85a0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:15:14	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_first
2019-09-04 08:15:15	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_second
2019-09-04 08:15:21	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_third
2019-09-04 08:15:22	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_fourth
2019-09-04 08:15:25	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_fifth
2019-09-04 08:15:26	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_six
2019-09-04 08:15:27	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_seven
2019-09-04 08:15:29	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_nine
2019-09-04 08:15:30	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_ten
2019-09-04 08:15:30	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_eleven
2019-09-04 08:15:30	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_twelve
2019-09-04 08:15:40	[scenario5d6f3a50127797.89258667]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 08:15:41	[scenario5d6f3a50127797.89258667]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 08:15:41	[scenario5d6f3a50127797.89258667]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 29407a617393e1ff56a8cc92fe8a85a0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:15:41	[scenario5d6f3a50127797.89258667]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:15:42	[scenario5d6f3a50127797.89258667]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:15:42	[scenario5d6f3a50127797.89258667]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:15:42	[scenario5d6f3a50127797.89258667]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:15:42	[scenario5d6f3a50127797.89258667]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 29407a617393e1ff56a8cc92fe8a85a0
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:15:43	[scenario5d6f3a50127797.89258667]	Execute X-Cart step: step_thirteen
2019-09-04 08:15:45	[scenario5d6f3a50127797.89258667]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 08:17:29	[scenario5d6f39fa014957.13466895]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:17:29	[scenario5d6f39fa014957.13466895]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:17:29	[scenario5d6f39fa014957.13466895]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:17:30	[scenario5d6f39fa014957.13466895]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-04 08:17:30	[scenario5d6f39fa014957.13466895]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 62d3e42a3abc8907969d0b92a80316c0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:17:30	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_first
2019-09-04 08:17:30	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_second
2019-09-04 08:17:36	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_third
2019-09-04 08:17:36	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_fourth
2019-09-04 08:17:39	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_fifth
2019-09-04 08:17:40	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_six
2019-09-04 08:17:41	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_seven
2019-09-04 08:17:42	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_nine
2019-09-04 08:17:42	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_ten
2019-09-04 08:17:43	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_eleven
2019-09-04 08:17:43	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_twelve
2019-09-04 08:17:49	[scenario5d6f39fa014957.13466895]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:17:49	[scenario5d6f39fa014957.13466895]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
    "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
]

2019-09-04 08:17:49	[scenario5d6f39fa014957.13466895]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:17:49	[scenario5d6f39fa014957.13466895]	Data updated: TrueMachine-AgeVerification
2019-09-04 08:17:49	[scenario5d6f39fa014957.13466895]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 08:17:49	[scenario5d6f39fa014957.13466895]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 62d3e42a3abc8907969d0b92a80316c0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:17:50	[scenario5d6f39fa014957.13466895]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:17:50	[scenario5d6f39fa014957.13466895]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:17:50	[scenario5d6f39fa014957.13466895]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:17:51	[scenario5d6f39fa014957.13466895]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:17:51	[scenario5d6f39fa014957.13466895]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 62d3e42a3abc8907969d0b92a80316c0
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:17:51	[scenario5d6f39fa014957.13466895]	Execute X-Cart step: step_thirteen
2019-09-04 08:17:53	[scenario5d6f39fa014957.13466895]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 08:36:04	[scenario5d6f3f346323e0.30838208]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 08:36:06	[scenario5d6f3f346323e0.30838208]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-04 08:36:06	[scenario5d6f3f346323e0.30838208]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0c8beb461238d38e915413424f5f55f7
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:36:06	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_first
2019-09-04 08:36:06	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_second
2019-09-04 08:36:12	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_third
2019-09-04 08:36:12	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_fourth
2019-09-04 08:36:15	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_fifth
2019-09-04 08:36:15	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_six
2019-09-04 08:36:16	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_seven
2019-09-04 08:36:17	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_nine
2019-09-04 08:36:18	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_ten
2019-09-04 08:36:18	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_eleven
2019-09-04 08:36:18	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_twelve
2019-09-04 08:36:24	[scenario5d6f3f346323e0.30838208]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 08:36:24	[scenario5d6f3f346323e0.30838208]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-04 08:36:24	[scenario5d6f3f346323e0.30838208]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 0c8beb461238d38e915413424f5f55f7
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:36:25	[scenario5d6f3f346323e0.30838208]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:36:25	[scenario5d6f3f346323e0.30838208]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:36:25	[scenario5d6f3f346323e0.30838208]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:36:25	[scenario5d6f3f346323e0.30838208]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:36:26	[scenario5d6f3f346323e0.30838208]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0c8beb461238d38e915413424f5f55f7
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:36:26	[scenario5d6f3f346323e0.30838208]	Execute X-Cart step: step_thirteen
2019-09-04 08:36:28	[scenario5d6f3f346323e0.30838208]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 08:36:45	[scenario5d6f3f35907f13.05410945]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:36:45	[scenario5d6f3f35907f13.05410945]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:36:46	[scenario5d6f3f35907f13.05410945]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:36:46	[scenario5d6f3f35907f13.05410945]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:36:46	[scenario5d6f3f35907f13.05410945]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 64ecefdfe528444e8c6f0130c93ec3bc
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:36:46	[scenario5d6f3f35907f13.05410945]	Execute X-Cart step: step_first
2019-09-04 08:36:47	[scenario5d6f3f35907f13.05410945]	Execute X-Cart step: step_second
2019-09-04 08:36:51	[scenario5d6f3f35907f13.05410945]	Execute X-Cart step: step_third
2019-09-04 08:36:51	[scenario5d6f3f35907f13.05410945]	Execute X-Cart step: step_fourth
2019-09-04 08:36:54	[scenario5d6f3f35907f13.05410945]	Execute X-Cart step: step_fifth
2019-09-04 08:36:55	[scenario5d6f3f35907f13.05410945]	Execute X-Cart step: step_six
2019-09-04 08:36:56	[scenario5d6f3f35907f13.05410945]	Execute X-Cart step: step_seven
2019-09-04 08:43:21	[scenario5d6f3f35907f13.05410945]	XCart\Bus\Rebuild\Executor\Step\Rollback\EditionChange::initialize
Context stepData => {
    "editionNameBefore": ""
}

2019-09-04 08:43:28	[scenario5d6f40eccdeb96.24029225]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:43:28	[scenario5d6f40eccdeb96.24029225]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:43:28	[scenario5d6f40eccdeb96.24029225]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:43:28	[scenario5d6f40eccdeb96.24029225]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:43:28	[scenario5d6f40eccdeb96.24029225]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 849eee6a6f8643fde1716293a116bf8e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:43:29	[scenario5d6f40eccdeb96.24029225]	Execute X-Cart step: step_first
2019-09-04 08:43:30	[scenario5d6f40eccdeb96.24029225]	Execute X-Cart step: step_second
2019-09-04 08:43:36	[scenario5d6f40eccdeb96.24029225]	Execute X-Cart step: step_third
2019-09-04 08:43:36	[scenario5d6f40eccdeb96.24029225]	Execute X-Cart step: step_fourth
2019-09-04 08:43:39	[scenario5d6f40eccdeb96.24029225]	Execute X-Cart step: step_fifth
2019-09-04 08:43:40	[scenario5d6f40eccdeb96.24029225]	Execute X-Cart step: step_six
2019-09-04 08:43:41	[scenario5d6f40eccdeb96.24029225]	Execute X-Cart step: step_seven
2019-09-04 08:44:14	[scenario5d6f40eccdeb96.24029225]	XCart\Bus\Rebuild\Executor\Step\Rollback\EditionChange::initialize
Context stepData => {
    "editionNameBefore": ""
}

2019-09-04 08:44:26	[scenario5d6f4124356e65.41244973]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:44:26	[scenario5d6f4124356e65.41244973]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:44:26	[scenario5d6f4124356e65.41244973]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:44:26	[scenario5d6f4124356e65.41244973]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 08:44:26	[scenario5d6f4124356e65.41244973]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c2b3163aadc5fe2467b416ef549e6f27
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 08:44:27	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_first
2019-09-04 08:44:29	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_second
2019-09-04 08:44:33	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_third
2019-09-04 08:44:34	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_fourth
2019-09-04 08:44:37	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_fifth
2019-09-04 08:44:37	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_six
2019-09-04 08:44:38	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_seven
2019-09-04 08:44:41	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_nine
2019-09-04 08:44:41	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_ten
2019-09-04 08:44:42	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_eleven
2019-09-04 08:44:42	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_twelve
2019-09-04 08:44:48	[scenario5d6f4124356e65.41244973]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 08:44:49	[scenario5d6f4124356e65.41244973]	Data updated: TrueMachine-AgeVerification
2019-09-04 08:44:49	[scenario5d6f4124356e65.41244973]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 08:44:49	[scenario5d6f4124356e65.41244973]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => c2b3163aadc5fe2467b416ef549e6f27
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 08:44:49	[scenario5d6f4124356e65.41244973]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 08:44:50	[scenario5d6f4124356e65.41244973]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 08:44:50	[scenario5d6f4124356e65.41244973]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 08:44:50	[scenario5d6f4124356e65.41244973]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 08:44:51	[scenario5d6f4124356e65.41244973]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c2b3163aadc5fe2467b416ef549e6f27
Context transitions => [
    "step_thirteen"
]

2019-09-04 08:44:51	[scenario5d6f4124356e65.41244973]	Execute X-Cart step: step_thirteen
2019-09-04 08:44:52	[scenario5d6f4124356e65.41244973]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:24:30	[scenario5d6f66ae1cd287.48245837]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:24:32	[scenario5d6f66ae1cd287.48245837]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:24:32	[scenario5d6f66ae1cd287.48245837]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b412eeb7b35fb608595491c006ce6c27
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:24:32	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_first
2019-09-04 11:24:33	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_second
2019-09-04 11:24:41	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_third
2019-09-04 11:24:41	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_fourth
2019-09-04 11:24:46	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_fifth
2019-09-04 11:24:46	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_six
2019-09-04 11:24:47	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_seven
2019-09-04 11:24:50	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_nine
2019-09-04 11:24:51	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_ten
2019-09-04 11:24:51	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_eleven
2019-09-04 11:24:52	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_twelve
2019-09-04 11:25:01	[scenario5d6f66ae1cd287.48245837]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:25:03	[scenario5d6f66ae1cd287.48245837]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:25:03	[scenario5d6f66ae1cd287.48245837]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b412eeb7b35fb608595491c006ce6c27
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:25:03	[scenario5d6f66ae1cd287.48245837]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:25:04	[scenario5d6f66ae1cd287.48245837]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:25:04	[scenario5d6f66ae1cd287.48245837]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:25:05	[scenario5d6f66ae1cd287.48245837]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:25:05	[scenario5d6f66ae1cd287.48245837]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b412eeb7b35fb608595491c006ce6c27
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:25:05	[scenario5d6f66ae1cd287.48245837]	Execute X-Cart step: step_thirteen
2019-09-04 11:25:08	[scenario5d6f66ae1cd287.48245837]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:31:19	[scenario5d6f68477331f4.59883530]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:31:21	[scenario5d6f68477331f4.59883530]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:31:21	[scenario5d6f68477331f4.59883530]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 300ba30593409463d56fa9d85240aecc
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:31:21	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_first
2019-09-04 11:31:22	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_second
2019-09-04 11:31:27	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_third
2019-09-04 11:31:28	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_fourth
2019-09-04 11:31:30	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_fifth
2019-09-04 11:31:31	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_six
2019-09-04 11:31:31	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_seven
2019-09-04 11:31:33	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_nine
2019-09-04 11:31:33	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_ten
2019-09-04 11:31:34	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_eleven
2019-09-04 11:31:34	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_twelve
2019-09-04 11:31:42	[scenario5d6f68477331f4.59883530]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:31:43	[scenario5d6f68477331f4.59883530]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:31:43	[scenario5d6f68477331f4.59883530]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 300ba30593409463d56fa9d85240aecc
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:31:43	[scenario5d6f68477331f4.59883530]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:31:44	[scenario5d6f68477331f4.59883530]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:31:44	[scenario5d6f68477331f4.59883530]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:31:45	[scenario5d6f68477331f4.59883530]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:31:45	[scenario5d6f68477331f4.59883530]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 300ba30593409463d56fa9d85240aecc
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:31:45	[scenario5d6f68477331f4.59883530]	Execute X-Cart step: step_thirteen
2019-09-04 11:31:47	[scenario5d6f68477331f4.59883530]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:32:10	[scenario5d6f687a177237.79307566]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:32:14	[scenario5d6f687a177237.79307566]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:32:14	[scenario5d6f687a177237.79307566]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 389c6de5f57f61577e806616c3281450
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:32:15	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_first
2019-09-04 11:32:17	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_second
2019-09-04 11:32:21	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_third
2019-09-04 11:32:22	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_fourth
2019-09-04 11:32:25	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_fifth
2019-09-04 11:32:26	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_six
2019-09-04 11:32:28	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_seven
2019-09-04 11:32:30	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_nine
2019-09-04 11:32:31	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_ten
2019-09-04 11:32:32	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_eleven
2019-09-04 11:32:33	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_twelve
2019-09-04 11:32:39	[scenario5d6f687a177237.79307566]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:32:41	[scenario5d6f687a177237.79307566]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:32:41	[scenario5d6f687a177237.79307566]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 389c6de5f57f61577e806616c3281450
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:32:41	[scenario5d6f687a177237.79307566]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:32:42	[scenario5d6f687a177237.79307566]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:32:43	[scenario5d6f687a177237.79307566]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:32:44	[scenario5d6f687a177237.79307566]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:32:44	[scenario5d6f687a177237.79307566]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 389c6de5f57f61577e806616c3281450
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:32:45	[scenario5d6f687a177237.79307566]	Execute X-Cart step: step_thirteen
2019-09-04 11:32:47	[scenario5d6f687a177237.79307566]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:39:41	[scenario5d6f6a3dc39165.30679450]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:39:43	[scenario5d6f6a3dc39165.30679450]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:39:43	[scenario5d6f6a3dc39165.30679450]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 354e764396ab3d5b671ba47e685bb075
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:39:44	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_first
2019-09-04 11:39:46	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_second
2019-09-04 11:39:52	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_third
2019-09-04 11:39:53	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_fourth
2019-09-04 11:39:56	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_fifth
2019-09-04 11:39:56	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_six
2019-09-04 11:39:57	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_seven
2019-09-04 11:39:59	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_nine
2019-09-04 11:40:00	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_ten
2019-09-04 11:40:01	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_eleven
2019-09-04 11:40:02	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_twelve
2019-09-04 11:40:09	[scenario5d6f6a3dc39165.30679450]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:40:10	[scenario5d6f6a3dc39165.30679450]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:40:10	[scenario5d6f6a3dc39165.30679450]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 354e764396ab3d5b671ba47e685bb075
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:40:11	[scenario5d6f6a3dc39165.30679450]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:40:11	[scenario5d6f6a3dc39165.30679450]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:40:12	[scenario5d6f6a3dc39165.30679450]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:40:12	[scenario5d6f6a3dc39165.30679450]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:40:12	[scenario5d6f6a3dc39165.30679450]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 354e764396ab3d5b671ba47e685bb075
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:40:12	[scenario5d6f6a3dc39165.30679450]	Execute X-Cart step: step_thirteen
2019-09-04 11:40:15	[scenario5d6f6a3dc39165.30679450]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:40:41	[scenario5d6f6a79b71a50.11325146]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:40:43	[scenario5d6f6a79b71a50.11325146]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:40:43	[scenario5d6f6a79b71a50.11325146]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e111fc08a84472447a3f7801eed3c745
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:40:43	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_first
2019-09-04 11:40:44	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_second
2019-09-04 11:40:50	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_third
2019-09-04 11:40:51	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_fourth
2019-09-04 11:40:54	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_fifth
2019-09-04 11:40:54	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_six
2019-09-04 11:40:55	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_seven
2019-09-04 11:40:57	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_nine
2019-09-04 11:40:57	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_ten
2019-09-04 11:40:58	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_eleven
2019-09-04 11:40:58	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_twelve
2019-09-04 11:41:04	[scenario5d6f6a79b71a50.11325146]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:41:04	[scenario5d6f6a79b71a50.11325146]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:41:04	[scenario5d6f6a79b71a50.11325146]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e111fc08a84472447a3f7801eed3c745
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:41:05	[scenario5d6f6a79b71a50.11325146]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:41:05	[scenario5d6f6a79b71a50.11325146]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:41:05	[scenario5d6f6a79b71a50.11325146]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:41:06	[scenario5d6f6a79b71a50.11325146]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:41:06	[scenario5d6f6a79b71a50.11325146]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e111fc08a84472447a3f7801eed3c745
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:41:06	[scenario5d6f6a79b71a50.11325146]	Execute X-Cart step: step_thirteen
2019-09-04 11:41:08	[scenario5d6f6a79b71a50.11325146]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:44:05	[scenario5d6f6b45d555e9.46898659]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:44:07	[scenario5d6f6b45d555e9.46898659]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:44:07	[scenario5d6f6b45d555e9.46898659]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0807c86e11ecfe8f7be3f225d27ddc31
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:44:07	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_first
2019-09-04 11:44:08	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_second
2019-09-04 11:44:14	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_third
2019-09-04 11:44:14	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_fourth
2019-09-04 11:44:17	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_fifth
2019-09-04 11:44:17	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_six
2019-09-04 11:44:18	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_seven
2019-09-04 11:44:19	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_nine
2019-09-04 11:44:20	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_ten
2019-09-04 11:44:20	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_eleven
2019-09-04 11:44:21	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_twelve
2019-09-04 11:44:27	[scenario5d6f6b45d555e9.46898659]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:44:28	[scenario5d6f6b45d555e9.46898659]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:44:28	[scenario5d6f6b45d555e9.46898659]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 0807c86e11ecfe8f7be3f225d27ddc31
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:44:28	[scenario5d6f6b45d555e9.46898659]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:44:28	[scenario5d6f6b45d555e9.46898659]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:44:29	[scenario5d6f6b45d555e9.46898659]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:44:29	[scenario5d6f6b45d555e9.46898659]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:44:29	[scenario5d6f6b45d555e9.46898659]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0807c86e11ecfe8f7be3f225d27ddc31
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:44:29	[scenario5d6f6b45d555e9.46898659]	Execute X-Cart step: step_thirteen
2019-09-04 11:44:31	[scenario5d6f6b45d555e9.46898659]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:47:23	[scenario5d6f6c0b927e02.47876812]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:47:25	[scenario5d6f6c0b927e02.47876812]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:47:25	[scenario5d6f6c0b927e02.47876812]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b699fce58b52801d41568346b6eb8667
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:47:25	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_first
2019-09-04 11:47:26	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_second
2019-09-04 11:47:32	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_third
2019-09-04 11:47:33	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_fourth
2019-09-04 11:47:35	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_fifth
2019-09-04 11:47:36	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_six
2019-09-04 11:47:37	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_seven
2019-09-04 11:47:39	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_nine
2019-09-04 11:47:40	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_ten
2019-09-04 11:47:41	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_eleven
2019-09-04 11:47:42	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_twelve
2019-09-04 11:47:48	[scenario5d6f6c0b927e02.47876812]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:47:49	[scenario5d6f6c0b927e02.47876812]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:47:49	[scenario5d6f6c0b927e02.47876812]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b699fce58b52801d41568346b6eb8667
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:47:49	[scenario5d6f6c0b927e02.47876812]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:47:50	[scenario5d6f6c0b927e02.47876812]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:47:51	[scenario5d6f6c0b927e02.47876812]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:47:52	[scenario5d6f6c0b927e02.47876812]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:47:52	[scenario5d6f6c0b927e02.47876812]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b699fce58b52801d41568346b6eb8667
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:47:53	[scenario5d6f6c0b927e02.47876812]	Execute X-Cart step: step_thirteen
2019-09-04 11:47:55	[scenario5d6f6c0b927e02.47876812]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:51:45	[scenario5d6f6d11cbd170.28790051]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:51:47	[scenario5d6f6d11cbd170.28790051]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:51:47	[scenario5d6f6d11cbd170.28790051]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3b10f4c293a6a7e0a533ecc9274e06b4
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:51:47	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_first
2019-09-04 11:51:48	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_second
2019-09-04 11:51:54	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_third
2019-09-04 11:51:54	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_fourth
2019-09-04 11:51:58	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_fifth
2019-09-04 11:51:58	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_six
2019-09-04 11:51:59	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_seven
2019-09-04 11:52:00	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_nine
2019-09-04 11:52:01	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_ten
2019-09-04 11:52:01	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_eleven
2019-09-04 11:52:02	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_twelve
2019-09-04 11:52:08	[scenario5d6f6d11cbd170.28790051]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:52:08	[scenario5d6f6d11cbd170.28790051]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:52:08	[scenario5d6f6d11cbd170.28790051]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 3b10f4c293a6a7e0a533ecc9274e06b4
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:52:08	[scenario5d6f6d11cbd170.28790051]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:52:09	[scenario5d6f6d11cbd170.28790051]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:52:09	[scenario5d6f6d11cbd170.28790051]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:52:09	[scenario5d6f6d11cbd170.28790051]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:52:09	[scenario5d6f6d11cbd170.28790051]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3b10f4c293a6a7e0a533ecc9274e06b4
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:52:10	[scenario5d6f6d11cbd170.28790051]	Execute X-Cart step: step_thirteen
2019-09-04 11:52:12	[scenario5d6f6d11cbd170.28790051]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:54:37	[scenario5d6f6dbd63cda2.53493682]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:54:39	[scenario5d6f6dbd63cda2.53493682]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:54:39	[scenario5d6f6dbd63cda2.53493682]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6bf38adce7927bcc59027a3f6d5d73d4
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:54:39	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_first
2019-09-04 11:54:40	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_second
2019-09-04 11:54:45	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_third
2019-09-04 11:54:46	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_fourth
2019-09-04 11:54:48	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_fifth
2019-09-04 11:54:49	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_six
2019-09-04 11:54:49	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_seven
2019-09-04 11:54:51	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_nine
2019-09-04 11:54:51	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_ten
2019-09-04 11:54:52	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_eleven
2019-09-04 11:54:52	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_twelve
2019-09-04 11:54:59	[scenario5d6f6dbd63cda2.53493682]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:55:00	[scenario5d6f6dbd63cda2.53493682]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:55:00	[scenario5d6f6dbd63cda2.53493682]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6bf38adce7927bcc59027a3f6d5d73d4
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:55:00	[scenario5d6f6dbd63cda2.53493682]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:55:00	[scenario5d6f6dbd63cda2.53493682]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:55:01	[scenario5d6f6dbd63cda2.53493682]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:55:01	[scenario5d6f6dbd63cda2.53493682]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:55:01	[scenario5d6f6dbd63cda2.53493682]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6bf38adce7927bcc59027a3f6d5d73d4
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:55:01	[scenario5d6f6dbd63cda2.53493682]	Execute X-Cart step: step_thirteen
2019-09-04 11:55:03	[scenario5d6f6dbd63cda2.53493682]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 11:58:40	[scenario5d6f6eb0163f72.30299980]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 11:58:41	[scenario5d6f6eb0163f72.30299980]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 11:58:42	[scenario5d6f6eb0163f72.30299980]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f2efc2593bc71454cdaa7db63594770e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 11:58:42	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_first
2019-09-04 11:58:42	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_second
2019-09-04 11:58:48	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_third
2019-09-04 11:58:49	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_fourth
2019-09-04 11:58:53	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_fifth
2019-09-04 11:58:53	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_six
2019-09-04 11:58:54	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_seven
2019-09-04 11:58:55	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_nine
2019-09-04 11:58:56	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_ten
2019-09-04 11:58:56	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_eleven
2019-09-04 11:58:56	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_twelve
2019-09-04 11:59:02	[scenario5d6f6eb0163f72.30299980]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 11:59:03	[scenario5d6f6eb0163f72.30299980]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 11:59:03	[scenario5d6f6eb0163f72.30299980]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f2efc2593bc71454cdaa7db63594770e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 11:59:03	[scenario5d6f6eb0163f72.30299980]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 11:59:04	[scenario5d6f6eb0163f72.30299980]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 11:59:04	[scenario5d6f6eb0163f72.30299980]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 11:59:04	[scenario5d6f6eb0163f72.30299980]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 11:59:04	[scenario5d6f6eb0163f72.30299980]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f2efc2593bc71454cdaa7db63594770e
Context transitions => [
    "step_thirteen"
]

2019-09-04 11:59:04	[scenario5d6f6eb0163f72.30299980]	Execute X-Cart step: step_thirteen
2019-09-04 11:59:06	[scenario5d6f6eb0163f72.30299980]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 12:01:35	[scenario5d6f6f5f68ae97.42740458]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 12:01:38	[scenario5d6f6f5f68ae97.42740458]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 12:01:38	[scenario5d6f6f5f68ae97.42740458]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3303fdd71deed550522f1ac2fc1fd658
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 12:01:38	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_first
2019-09-04 12:01:39	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_second
2019-09-04 12:01:45	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_third
2019-09-04 12:01:46	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_fourth
2019-09-04 12:01:49	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_fifth
2019-09-04 12:01:49	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_six
2019-09-04 12:01:50	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_seven
2019-09-04 12:01:52	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_nine
2019-09-04 12:01:53	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_ten
2019-09-04 12:01:54	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_eleven
2019-09-04 12:01:54	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_twelve
2019-09-04 12:02:01	[scenario5d6f6f5f68ae97.42740458]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 12:02:01	[scenario5d6f6f5f68ae97.42740458]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 12:02:01	[scenario5d6f6f5f68ae97.42740458]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 3303fdd71deed550522f1ac2fc1fd658
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 12:02:01	[scenario5d6f6f5f68ae97.42740458]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 12:02:02	[scenario5d6f6f5f68ae97.42740458]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 12:02:02	[scenario5d6f6f5f68ae97.42740458]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 12:02:02	[scenario5d6f6f5f68ae97.42740458]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 12:02:02	[scenario5d6f6f5f68ae97.42740458]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3303fdd71deed550522f1ac2fc1fd658
Context transitions => [
    "step_thirteen"
]

2019-09-04 12:02:03	[scenario5d6f6f5f68ae97.42740458]	Execute X-Cart step: step_thirteen
2019-09-04 12:02:05	[scenario5d6f6f5f68ae97.42740458]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 12:08:57	[scenario5d6f7119edaba6.71331599]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 12:08:59	[scenario5d6f7119edaba6.71331599]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 12:08:59	[scenario5d6f7119edaba6.71331599]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => da9f65710f656b84e37094573a42591f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 12:08:59	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_first
2019-09-04 12:09:00	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_second
2019-09-04 12:09:07	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_third
2019-09-04 12:09:07	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_fourth
2019-09-04 12:09:10	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_fifth
2019-09-04 12:09:10	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_six
2019-09-04 12:09:11	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_seven
2019-09-04 12:09:12	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_nine
2019-09-04 12:09:13	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_ten
2019-09-04 12:09:13	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_eleven
2019-09-04 12:09:14	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_twelve
2019-09-04 12:09:20	[scenario5d6f7119edaba6.71331599]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 12:09:21	[scenario5d6f7119edaba6.71331599]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 12:09:21	[scenario5d6f7119edaba6.71331599]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => da9f65710f656b84e37094573a42591f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 12:09:21	[scenario5d6f7119edaba6.71331599]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 12:09:22	[scenario5d6f7119edaba6.71331599]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 12:09:22	[scenario5d6f7119edaba6.71331599]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 12:09:22	[scenario5d6f7119edaba6.71331599]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 12:09:22	[scenario5d6f7119edaba6.71331599]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => da9f65710f656b84e37094573a42591f
Context transitions => [
    "step_thirteen"
]

2019-09-04 12:09:22	[scenario5d6f7119edaba6.71331599]	Execute X-Cart step: step_thirteen
2019-09-04 12:09:24	[scenario5d6f7119edaba6.71331599]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 12:10:17	[scenario5d6f71693c88b4.50212038]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 12:10:20	[scenario5d6f71693c88b4.50212038]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 12:10:20	[scenario5d6f71693c88b4.50212038]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b10922caa43bc05ee9cec8ba653a9d05
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 12:10:20	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_first
2019-09-04 12:10:21	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_second
2019-09-04 12:10:28	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_third
2019-09-04 12:10:28	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_fourth
2019-09-04 12:10:32	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_fifth
2019-09-04 12:10:32	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_six
2019-09-04 12:10:33	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_seven
2019-09-04 12:10:35	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_nine
2019-09-04 12:10:36	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_ten
2019-09-04 12:10:37	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_eleven
2019-09-04 12:10:37	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_twelve
2019-09-04 12:10:45	[scenario5d6f71693c88b4.50212038]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 12:10:46	[scenario5d6f71693c88b4.50212038]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 12:10:46	[scenario5d6f71693c88b4.50212038]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b10922caa43bc05ee9cec8ba653a9d05
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 12:10:46	[scenario5d6f71693c88b4.50212038]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 12:10:47	[scenario5d6f71693c88b4.50212038]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 12:10:47	[scenario5d6f71693c88b4.50212038]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 12:10:48	[scenario5d6f71693c88b4.50212038]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 12:10:48	[scenario5d6f71693c88b4.50212038]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b10922caa43bc05ee9cec8ba653a9d05
Context transitions => [
    "step_thirteen"
]

2019-09-04 12:10:48	[scenario5d6f71693c88b4.50212038]	Execute X-Cart step: step_thirteen
2019-09-04 12:10:50	[scenario5d6f71693c88b4.50212038]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 12:15:07	[scenario5d6f728b4b4057.28190581]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 12:15:09	[scenario5d6f728b4b4057.28190581]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 12:15:09	[scenario5d6f728b4b4057.28190581]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f781d46c71dc52fc8d70c5ab71c9a168
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 12:15:09	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_first
2019-09-04 12:15:10	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_second
2019-09-04 12:15:15	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_third
2019-09-04 12:15:16	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_fourth
2019-09-04 12:15:18	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_fifth
2019-09-04 12:15:18	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_six
2019-09-04 12:15:19	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_seven
2019-09-04 12:15:21	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_nine
2019-09-04 12:15:22	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_ten
2019-09-04 12:15:22	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_eleven
2019-09-04 12:15:23	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_twelve
2019-09-04 12:15:29	[scenario5d6f728b4b4057.28190581]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 12:15:30	[scenario5d6f728b4b4057.28190581]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 12:15:30	[scenario5d6f728b4b4057.28190581]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f781d46c71dc52fc8d70c5ab71c9a168
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 12:15:30	[scenario5d6f728b4b4057.28190581]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 12:15:30	[scenario5d6f728b4b4057.28190581]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 12:15:31	[scenario5d6f728b4b4057.28190581]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 12:15:31	[scenario5d6f728b4b4057.28190581]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 12:15:31	[scenario5d6f728b4b4057.28190581]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f781d46c71dc52fc8d70c5ab71c9a168
Context transitions => [
    "step_thirteen"
]

2019-09-04 12:15:31	[scenario5d6f728b4b4057.28190581]	Execute X-Cart step: step_thirteen
2019-09-04 12:15:33	[scenario5d6f728b4b4057.28190581]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 12:38:11	[scenario5d6f77f32f5a07.90829374]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 12:38:12	[scenario5d6f77f32f5a07.90829374]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 12:38:13	[scenario5d6f77f32f5a07.90829374]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4b49f33a35f66f257c8c70b193256dc5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 12:38:13	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_first
2019-09-04 12:38:13	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_second
2019-09-04 12:38:19	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_third
2019-09-04 12:38:19	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_fourth
2019-09-04 12:38:22	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_fifth
2019-09-04 12:38:22	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_six
2019-09-04 12:38:23	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_seven
2019-09-04 12:38:24	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_nine
2019-09-04 12:38:25	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_ten
2019-09-04 12:38:25	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_eleven
2019-09-04 12:38:25	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_twelve
2019-09-04 12:38:31	[scenario5d6f77f32f5a07.90829374]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 12:38:32	[scenario5d6f77f32f5a07.90829374]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 12:38:32	[scenario5d6f77f32f5a07.90829374]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4b49f33a35f66f257c8c70b193256dc5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 12:38:32	[scenario5d6f77f32f5a07.90829374]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 12:38:33	[scenario5d6f77f32f5a07.90829374]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 12:38:33	[scenario5d6f77f32f5a07.90829374]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 12:38:33	[scenario5d6f77f32f5a07.90829374]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 12:38:33	[scenario5d6f77f32f5a07.90829374]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4b49f33a35f66f257c8c70b193256dc5
Context transitions => [
    "step_thirteen"
]

2019-09-04 12:38:33	[scenario5d6f77f32f5a07.90829374]	Execute X-Cart step: step_thirteen
2019-09-04 12:38:35	[scenario5d6f77f32f5a07.90829374]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 12:38:52	[scenario5d6f781cde02f3.80384697]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 12:38:54	[scenario5d6f781cde02f3.80384697]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 12:38:54	[scenario5d6f781cde02f3.80384697]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8494fb574123acd185c2a8f138d22172
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 12:38:54	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_first
2019-09-04 12:38:55	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_second
2019-09-04 12:38:58	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_third
2019-09-04 12:38:59	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_fourth
2019-09-04 12:39:01	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_fifth
2019-09-04 12:39:01	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_six
2019-09-04 12:39:02	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_seven
2019-09-04 12:39:03	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_nine
2019-09-04 12:39:04	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_ten
2019-09-04 12:39:04	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_eleven
2019-09-04 12:39:04	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_twelve
2019-09-04 12:39:09	[scenario5d6f781cde02f3.80384697]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 12:39:10	[scenario5d6f781cde02f3.80384697]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 12:39:10	[scenario5d6f781cde02f3.80384697]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8494fb574123acd185c2a8f138d22172
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 12:39:10	[scenario5d6f781cde02f3.80384697]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 12:39:11	[scenario5d6f781cde02f3.80384697]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 12:39:11	[scenario5d6f781cde02f3.80384697]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 12:39:11	[scenario5d6f781cde02f3.80384697]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 12:39:11	[scenario5d6f781cde02f3.80384697]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8494fb574123acd185c2a8f138d22172
Context transitions => [
    "step_thirteen"
]

2019-09-04 12:39:11	[scenario5d6f781cde02f3.80384697]	Execute X-Cart step: step_thirteen
2019-09-04 12:39:13	[scenario5d6f781cde02f3.80384697]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 12:48:29	[scenario5d6f7a5db700f4.66170049]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 12:48:31	[scenario5d6f7a5db700f4.66170049]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 12:48:32	[scenario5d6f7a5db700f4.66170049]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f8009145b8872e6e91dce62ae0c59488
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 12:48:32	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_first
2019-09-04 12:48:32	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_second
2019-09-04 12:48:38	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_third
2019-09-04 12:48:39	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_fourth
2019-09-04 12:48:41	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_fifth
2019-09-04 12:48:41	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_six
2019-09-04 12:48:42	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_seven
2019-09-04 12:48:43	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_nine
2019-09-04 12:48:44	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_ten
2019-09-04 12:48:44	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_eleven
2019-09-04 12:48:44	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_twelve
2019-09-04 12:48:51	[scenario5d6f7a5db700f4.66170049]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 12:48:51	[scenario5d6f7a5db700f4.66170049]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 12:48:51	[scenario5d6f7a5db700f4.66170049]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f8009145b8872e6e91dce62ae0c59488
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 12:48:51	[scenario5d6f7a5db700f4.66170049]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 12:48:52	[scenario5d6f7a5db700f4.66170049]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 12:48:52	[scenario5d6f7a5db700f4.66170049]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 12:48:52	[scenario5d6f7a5db700f4.66170049]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 12:48:52	[scenario5d6f7a5db700f4.66170049]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f8009145b8872e6e91dce62ae0c59488
Context transitions => [
    "step_thirteen"
]

2019-09-04 12:48:53	[scenario5d6f7a5db700f4.66170049]	Execute X-Cart step: step_thirteen
2019-09-04 12:48:55	[scenario5d6f7a5db700f4.66170049]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 12:55:06	[scenario5d6f7bea864676.29476694]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 12:55:08	[scenario5d6f7bea864676.29476694]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 12:55:08	[scenario5d6f7bea864676.29476694]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e802826cbfd2e1a1dc9d2b8aa84b3003
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 12:55:08	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_first
2019-09-04 12:55:09	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_second
2019-09-04 12:55:15	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_third
2019-09-04 12:55:16	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_fourth
2019-09-04 12:55:20	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_fifth
2019-09-04 12:55:20	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_six
2019-09-04 12:55:21	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_seven
2019-09-04 12:55:23	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_nine
2019-09-04 12:55:24	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_ten
2019-09-04 12:55:25	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_eleven
2019-09-04 12:55:25	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_twelve
2019-09-04 12:55:33	[scenario5d6f7bea864676.29476694]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 12:55:34	[scenario5d6f7bea864676.29476694]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 12:55:34	[scenario5d6f7bea864676.29476694]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e802826cbfd2e1a1dc9d2b8aa84b3003
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 12:55:34	[scenario5d6f7bea864676.29476694]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 12:55:34	[scenario5d6f7bea864676.29476694]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 12:55:35	[scenario5d6f7bea864676.29476694]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 12:55:35	[scenario5d6f7bea864676.29476694]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 12:55:35	[scenario5d6f7bea864676.29476694]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e802826cbfd2e1a1dc9d2b8aa84b3003
Context transitions => [
    "step_thirteen"
]

2019-09-04 12:55:35	[scenario5d6f7bea864676.29476694]	Execute X-Cart step: step_thirteen
2019-09-04 12:55:38	[scenario5d6f7bea864676.29476694]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 13:33:19	[scenario5d6f84df284a64.15447753]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 13:33:22	[scenario5d6f84df284a64.15447753]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 13:33:22	[scenario5d6f84df284a64.15447753]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b52efe44695e2169bde554559594f307
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 13:33:23	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_first
2019-09-04 13:33:24	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_second
2019-09-04 13:33:30	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_third
2019-09-04 13:33:30	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_fourth
2019-09-04 13:33:33	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_fifth
2019-09-04 13:33:34	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_six
2019-09-04 13:33:35	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_seven
2019-09-04 13:33:36	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_nine
2019-09-04 13:33:37	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_ten
2019-09-04 13:33:37	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_eleven
2019-09-04 13:33:37	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_twelve
2019-09-04 13:33:43	[scenario5d6f84df284a64.15447753]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 13:33:44	[scenario5d6f84df284a64.15447753]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 13:33:44	[scenario5d6f84df284a64.15447753]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b52efe44695e2169bde554559594f307
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 13:33:44	[scenario5d6f84df284a64.15447753]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 13:33:45	[scenario5d6f84df284a64.15447753]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 13:33:45	[scenario5d6f84df284a64.15447753]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 13:33:45	[scenario5d6f84df284a64.15447753]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 13:33:45	[scenario5d6f84df284a64.15447753]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b52efe44695e2169bde554559594f307
Context transitions => [
    "step_thirteen"
]

2019-09-04 13:33:45	[scenario5d6f84df284a64.15447753]	Execute X-Cart step: step_thirteen
2019-09-04 13:33:47	[scenario5d6f84df284a64.15447753]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 13:45:22	[scenario5d6f87b27d5237.26363337]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 13:45:24	[scenario5d6f87b27d5237.26363337]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 13:45:24	[scenario5d6f87b27d5237.26363337]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b2fea3f1a75e3b656c29102ba3979c3a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 13:45:24	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_first
2019-09-04 13:45:25	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_second
2019-09-04 13:45:31	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_third
2019-09-04 13:45:32	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_fourth
2019-09-04 13:45:34	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_fifth
2019-09-04 13:45:35	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_six
2019-09-04 13:45:35	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_seven
2019-09-04 13:45:37	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_nine
2019-09-04 13:45:37	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_ten
2019-09-04 13:45:38	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_eleven
2019-09-04 13:45:38	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_twelve
2019-09-04 13:45:44	[scenario5d6f87b27d5237.26363337]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 13:45:44	[scenario5d6f87b27d5237.26363337]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 13:45:44	[scenario5d6f87b27d5237.26363337]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b2fea3f1a75e3b656c29102ba3979c3a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 13:45:44	[scenario5d6f87b27d5237.26363337]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 13:45:45	[scenario5d6f87b27d5237.26363337]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 13:45:45	[scenario5d6f87b27d5237.26363337]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 13:45:45	[scenario5d6f87b27d5237.26363337]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 13:45:45	[scenario5d6f87b27d5237.26363337]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b2fea3f1a75e3b656c29102ba3979c3a
Context transitions => [
    "step_thirteen"
]

2019-09-04 13:45:45	[scenario5d6f87b27d5237.26363337]	Execute X-Cart step: step_thirteen
2019-09-04 13:45:47	[scenario5d6f87b27d5237.26363337]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 15:03:57	[scenario5d6f4169dd4893.86156451]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 15:03:57	[scenario5d6f4169dd4893.86156451]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 15:03:58	[scenario5d6f4169dd4893.86156451]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 15:03:58	[scenario5d6f4169dd4893.86156451]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-04 15:03:58	[scenario5d6f4169dd4893.86156451]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f6fe971c5dc3605babc06756741d93b6
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 15:03:58	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_first
2019-09-04 15:03:59	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_second
2019-09-04 15:04:05	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_third
2019-09-04 15:04:06	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_fourth
2019-09-04 15:04:09	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_fifth
2019-09-04 15:04:10	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_six
2019-09-04 15:04:11	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_seven
2019-09-04 15:04:12	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_nine
2019-09-04 15:04:13	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_ten
2019-09-04 15:04:14	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_eleven
2019-09-04 15:04:14	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_twelve
2019-09-04 15:04:22	[scenario5d6f4169dd4893.86156451]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 15:04:23	[scenario5d6f4169dd4893.86156451]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
    "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
]

2019-09-04 15:04:23	[scenario5d6f4169dd4893.86156451]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 15:04:24	[scenario5d6f4169dd4893.86156451]	Data updated: TrueMachine-AgeVerification
2019-09-04 15:04:24	[scenario5d6f4169dd4893.86156451]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 15:04:24	[scenario5d6f4169dd4893.86156451]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f6fe971c5dc3605babc06756741d93b6
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 15:04:24	[scenario5d6f4169dd4893.86156451]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 15:04:25	[scenario5d6f4169dd4893.86156451]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 15:04:25	[scenario5d6f4169dd4893.86156451]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 15:04:25	[scenario5d6f4169dd4893.86156451]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 15:04:25	[scenario5d6f4169dd4893.86156451]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f6fe971c5dc3605babc06756741d93b6
Context transitions => [
    "step_thirteen"
]

2019-09-04 15:04:25	[scenario5d6f4169dd4893.86156451]	Execute X-Cart step: step_thirteen
2019-09-04 15:04:28	[scenario5d6f4169dd4893.86156451]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 15:35:48	[scenario5d6fa1943cf3a2.68278599]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 15:35:49	[scenario5d6fa1943cf3a2.68278599]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-04 15:35:50	[scenario5d6fa1943cf3a2.68278599]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2031102933f71d27c467e08cf2cf2063
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 15:35:50	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_first
2019-09-04 15:35:50	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_second
2019-09-04 15:35:56	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_third
2019-09-04 15:35:57	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_fourth
2019-09-04 15:35:59	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_fifth
2019-09-04 15:36:00	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_six
2019-09-04 15:36:01	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_seven
2019-09-04 15:36:03	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_nine
2019-09-04 15:36:03	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_ten
2019-09-04 15:36:03	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_eleven
2019-09-04 15:36:04	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_twelve
2019-09-04 15:36:10	[scenario5d6fa1943cf3a2.68278599]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 15:36:10	[scenario5d6fa1943cf3a2.68278599]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-04 15:36:10	[scenario5d6fa1943cf3a2.68278599]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2031102933f71d27c467e08cf2cf2063
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 15:36:10	[scenario5d6fa1943cf3a2.68278599]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 15:36:11	[scenario5d6fa1943cf3a2.68278599]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 15:36:11	[scenario5d6fa1943cf3a2.68278599]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 15:36:11	[scenario5d6fa1943cf3a2.68278599]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 15:36:11	[scenario5d6fa1943cf3a2.68278599]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2031102933f71d27c467e08cf2cf2063
Context transitions => [
    "step_thirteen"
]

2019-09-04 15:36:11	[scenario5d6fa1943cf3a2.68278599]	Execute X-Cart step: step_thirteen
2019-09-04 15:36:13	[scenario5d6fa1943cf3a2.68278599]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 15:36:23	[scenario5d6fa1b78a2cd3.40326597]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 15:36:25	[scenario5d6fa1b78a2cd3.40326597]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": null
    }
}

2019-09-04 15:36:25	[scenario5d6fa1b78a2cd3.40326597]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => edfd0f6c2df89121aea00094b6223889
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 15:36:25	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_first
2019-09-04 15:36:25	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_second
2019-09-04 15:36:28	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_third
2019-09-04 15:36:29	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_fourth
2019-09-04 15:36:31	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_fifth
2019-09-04 15:36:32	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_six
2019-09-04 15:36:33	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_seven
2019-09-04 15:36:34	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_nine
2019-09-04 15:36:35	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_ten
2019-09-04 15:36:35	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_eleven
2019-09-04 15:36:35	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_twelve
2019-09-04 15:36:41	[scenario5d6fa1b78a2cd3.40326597]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 15:36:42	[scenario5d6fa1b78a2cd3.40326597]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 15:36:42	[scenario5d6fa1b78a2cd3.40326597]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => edfd0f6c2df89121aea00094b6223889
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 15:36:42	[scenario5d6fa1b78a2cd3.40326597]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 15:36:42	[scenario5d6fa1b78a2cd3.40326597]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 15:36:42	[scenario5d6fa1b78a2cd3.40326597]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 15:36:43	[scenario5d6fa1b78a2cd3.40326597]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 15:36:43	[scenario5d6fa1b78a2cd3.40326597]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => edfd0f6c2df89121aea00094b6223889
Context transitions => [
    "step_thirteen"
]

2019-09-04 15:36:43	[scenario5d6fa1b78a2cd3.40326597]	Execute X-Cart step: step_thirteen
2019-09-04 15:36:44	[scenario5d6fa1b78a2cd3.40326597]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 15:37:01	[scenario5d6fa195215b69.72829846]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 15:37:01	[scenario5d6fa195215b69.72829846]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 15:37:01	[scenario5d6fa195215b69.72829846]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 15:37:02	[scenario5d6fa195215b69.72829846]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 15:37:02	[scenario5d6fa195215b69.72829846]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e12fbf700680edb5321f6e2c6288ae1d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 15:37:02	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_first
2019-09-04 15:37:02	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_second
2019-09-04 15:37:05	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_third
2019-09-04 15:37:06	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_fourth
2019-09-04 15:37:08	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_fifth
2019-09-04 15:37:09	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_six
2019-09-04 15:37:10	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_seven
2019-09-04 15:37:11	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_nine
2019-09-04 15:37:12	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_ten
2019-09-04 15:37:12	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_eleven
2019-09-04 15:37:13	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_twelve
2019-09-04 15:37:17	[scenario5d6fa195215b69.72829846]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 15:37:18	[scenario5d6fa195215b69.72829846]	Data updated: TrueMachine-AgeVerification
2019-09-04 15:37:18	[scenario5d6fa195215b69.72829846]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 15:37:18	[scenario5d6fa195215b69.72829846]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e12fbf700680edb5321f6e2c6288ae1d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 15:37:18	[scenario5d6fa195215b69.72829846]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 15:37:19	[scenario5d6fa195215b69.72829846]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 15:37:19	[scenario5d6fa195215b69.72829846]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 15:37:19	[scenario5d6fa195215b69.72829846]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 15:37:19	[scenario5d6fa195215b69.72829846]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e12fbf700680edb5321f6e2c6288ae1d
Context transitions => [
    "step_thirteen"
]

2019-09-04 15:37:19	[scenario5d6fa195215b69.72829846]	Execute X-Cart step: step_thirteen
2019-09-04 15:37:21	[scenario5d6fa195215b69.72829846]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 15:39:05	[scenario5d6fa25955ce00.58756791]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 15:39:07	[scenario5d6fa25955ce00.58756791]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 15:39:07	[scenario5d6fa25955ce00.58756791]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3e59b26230d4df69ffcafaf5c8562ce1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 15:39:07	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_first
2019-09-04 15:39:08	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_second
2019-09-04 15:39:14	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_third
2019-09-04 15:39:15	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_fourth
2019-09-04 15:39:18	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_fifth
2019-09-04 15:39:18	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_six
2019-09-04 15:39:19	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_seven
2019-09-04 15:39:21	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_nine
2019-09-04 15:39:22	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_ten
2019-09-04 15:39:22	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_eleven
2019-09-04 15:39:22	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_twelve
2019-09-04 15:39:29	[scenario5d6fa25955ce00.58756791]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 15:39:29	[scenario5d6fa25955ce00.58756791]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 15:39:29	[scenario5d6fa25955ce00.58756791]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 3e59b26230d4df69ffcafaf5c8562ce1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 15:39:30	[scenario5d6fa25955ce00.58756791]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 15:39:30	[scenario5d6fa25955ce00.58756791]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 15:39:30	[scenario5d6fa25955ce00.58756791]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 15:39:31	[scenario5d6fa25955ce00.58756791]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 15:39:31	[scenario5d6fa25955ce00.58756791]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3e59b26230d4df69ffcafaf5c8562ce1
Context transitions => [
    "step_thirteen"
]

2019-09-04 15:39:31	[scenario5d6fa25955ce00.58756791]	Execute X-Cart step: step_thirteen
2019-09-04 15:39:33	[scenario5d6fa25955ce00.58756791]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 15:44:04	[scenario5d6fa384350ce5.27364272]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 15:44:06	[scenario5d6fa384350ce5.27364272]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 15:44:06	[scenario5d6fa384350ce5.27364272]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 375028c8a02ef4b88e835cb3aae16bc6
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 15:44:06	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_first
2019-09-04 15:44:07	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_second
2019-09-04 15:44:13	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_third
2019-09-04 15:44:14	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_fourth
2019-09-04 15:44:17	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_fifth
2019-09-04 15:44:17	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_six
2019-09-04 15:44:18	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_seven
2019-09-04 15:44:20	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_nine
2019-09-04 15:44:21	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_ten
2019-09-04 15:44:21	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_eleven
2019-09-04 15:44:22	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_twelve
2019-09-04 15:44:28	[scenario5d6fa384350ce5.27364272]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 15:44:29	[scenario5d6fa384350ce5.27364272]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 15:44:29	[scenario5d6fa384350ce5.27364272]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 375028c8a02ef4b88e835cb3aae16bc6
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 15:44:30	[scenario5d6fa384350ce5.27364272]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 15:44:30	[scenario5d6fa384350ce5.27364272]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 15:44:30	[scenario5d6fa384350ce5.27364272]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 15:44:31	[scenario5d6fa384350ce5.27364272]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 15:44:31	[scenario5d6fa384350ce5.27364272]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 375028c8a02ef4b88e835cb3aae16bc6
Context transitions => [
    "step_thirteen"
]

2019-09-04 15:44:31	[scenario5d6fa384350ce5.27364272]	Execute X-Cart step: step_thirteen
2019-09-04 15:44:33	[scenario5d6fa384350ce5.27364272]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 15:48:29	[scenario5d6fa48d708968.31948935]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 15:48:31	[scenario5d6fa48d708968.31948935]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 15:48:31	[scenario5d6fa48d708968.31948935]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 908f0b6caf095bdd6c74e00927365496
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 15:48:31	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_first
2019-09-04 15:48:32	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_second
2019-09-04 15:48:37	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_third
2019-09-04 15:48:38	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_fourth
2019-09-04 15:48:41	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_fifth
2019-09-04 15:48:41	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_six
2019-09-04 15:48:42	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_seven
2019-09-04 15:48:43	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_nine
2019-09-04 15:48:44	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_ten
2019-09-04 15:48:44	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_eleven
2019-09-04 15:48:45	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_twelve
2019-09-04 15:48:53	[scenario5d6fa48d708968.31948935]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 15:48:54	[scenario5d6fa48d708968.31948935]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 15:48:54	[scenario5d6fa48d708968.31948935]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 908f0b6caf095bdd6c74e00927365496
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 15:48:54	[scenario5d6fa48d708968.31948935]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 15:48:54	[scenario5d6fa48d708968.31948935]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 15:48:55	[scenario5d6fa48d708968.31948935]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 15:48:55	[scenario5d6fa48d708968.31948935]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 15:48:55	[scenario5d6fa48d708968.31948935]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 908f0b6caf095bdd6c74e00927365496
Context transitions => [
    "step_thirteen"
]

2019-09-04 15:48:56	[scenario5d6fa48d708968.31948935]	Execute X-Cart step: step_thirteen
2019-09-04 15:48:58	[scenario5d6fa48d708968.31948935]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 16:05:37	[scenario5d6fa8913cbee5.00736579]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 16:05:39	[scenario5d6fa8913cbee5.00736579]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 16:05:39	[scenario5d6fa8913cbee5.00736579]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cb795d736d29c2a099b1a08b5610e21c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 16:05:39	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_first
2019-09-04 16:05:40	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_second
2019-09-04 16:05:46	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_third
2019-09-04 16:05:46	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_fourth
2019-09-04 16:05:49	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_fifth
2019-09-04 16:05:49	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_six
2019-09-04 16:05:50	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_seven
2019-09-04 16:05:52	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_nine
2019-09-04 16:05:52	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_ten
2019-09-04 16:05:53	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_eleven
2019-09-04 16:05:53	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_twelve
2019-09-04 16:05:59	[scenario5d6fa8913cbee5.00736579]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 16:06:00	[scenario5d6fa8913cbee5.00736579]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 16:06:00	[scenario5d6fa8913cbee5.00736579]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => cb795d736d29c2a099b1a08b5610e21c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 16:06:01	[scenario5d6fa8913cbee5.00736579]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 16:06:02	[scenario5d6fa8913cbee5.00736579]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 16:06:03	[scenario5d6fa8913cbee5.00736579]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 16:06:04	[scenario5d6fa8913cbee5.00736579]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 16:06:04	[scenario5d6fa8913cbee5.00736579]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cb795d736d29c2a099b1a08b5610e21c
Context transitions => [
    "step_thirteen"
]

2019-09-04 16:06:05	[scenario5d6fa8913cbee5.00736579]	Execute X-Cart step: step_thirteen
2019-09-04 16:06:07	[scenario5d6fa8913cbee5.00736579]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 16:15:17	[scenario5d6faad5634549.86968781]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 16:15:19	[scenario5d6faad5634549.86968781]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 16:15:19	[scenario5d6faad5634549.86968781]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 69035cf71aace3ef20aa645a4ec8e3b5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 16:15:19	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_first
2019-09-04 16:15:19	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_second
2019-09-04 16:15:25	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_third
2019-09-04 16:15:25	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_fourth
2019-09-04 16:15:29	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_fifth
2019-09-04 16:15:29	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_six
2019-09-04 16:15:30	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_seven
2019-09-04 16:15:32	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_nine
2019-09-04 16:15:32	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_ten
2019-09-04 16:15:33	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_eleven
2019-09-04 16:15:33	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_twelve
2019-09-04 16:15:39	[scenario5d6faad5634549.86968781]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 16:15:40	[scenario5d6faad5634549.86968781]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 16:15:40	[scenario5d6faad5634549.86968781]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 69035cf71aace3ef20aa645a4ec8e3b5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 16:15:40	[scenario5d6faad5634549.86968781]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 16:15:40	[scenario5d6faad5634549.86968781]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 16:15:40	[scenario5d6faad5634549.86968781]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 16:15:41	[scenario5d6faad5634549.86968781]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 16:15:41	[scenario5d6faad5634549.86968781]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 69035cf71aace3ef20aa645a4ec8e3b5
Context transitions => [
    "step_thirteen"
]

2019-09-04 16:15:41	[scenario5d6faad5634549.86968781]	Execute X-Cart step: step_thirteen
2019-09-04 16:15:43	[scenario5d6faad5634549.86968781]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 16:21:59	[scenario5d6fa25a3089e9.91185995]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 16:21:59	[scenario5d6fa25a3089e9.91185995]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 16:21:59	[scenario5d6fa25a3089e9.91185995]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 16:21:59	[scenario5d6fa25a3089e9.91185995]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": "remove"
    }
}

2019-09-04 16:21:59	[scenario5d6fa25a3089e9.91185995]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5f739e57ab703ef9c94992d836c0394f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 16:22:00	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_first
2019-09-04 16:22:00	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_second
2019-09-04 16:22:05	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_third
2019-09-04 16:22:05	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_fourth
2019-09-04 16:22:08	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_fifth
2019-09-04 16:22:08	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_six
2019-09-04 16:22:09	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_seven
2019-09-04 16:22:10	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_nine
2019-09-04 16:22:11	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_ten
2019-09-04 16:22:11	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_eleven
2019-09-04 16:22:12	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_twelve
2019-09-04 16:22:17	[scenario5d6fa25a3089e9.91185995]	XCart\Bus\Rebuild\Executor\Step\Execute\RemoveModules::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 16:22:18	[scenario5d6fa25a3089e9.91185995]	Remove dirs
Context modified => [
    "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
    "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
]

2019-09-04 16:22:18	[scenario5d6fa25a3089e9.91185995]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "remove",
        "stateBeforeTransition": {
            "installed": true,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "integrated": true,
            "enabled": true
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": false,
            "integrated": false,
            "enabled": false
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 16:22:18	[scenario5d6fa25a3089e9.91185995]	Data updated: TrueMachine-AgeVerification
2019-09-04 16:22:18	[scenario5d6fa25a3089e9.91185995]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 16:22:18	[scenario5d6fa25a3089e9.91185995]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5f739e57ab703ef9c94992d836c0394f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 16:22:18	[scenario5d6fa25a3089e9.91185995]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 16:22:19	[scenario5d6fa25a3089e9.91185995]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 16:22:19	[scenario5d6fa25a3089e9.91185995]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 16:22:19	[scenario5d6fa25a3089e9.91185995]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 16:22:19	[scenario5d6fa25a3089e9.91185995]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5f739e57ab703ef9c94992d836c0394f
Context transitions => [
    "step_thirteen"
]

2019-09-04 16:22:19	[scenario5d6fa25a3089e9.91185995]	Execute X-Cart step: step_thirteen
2019-09-04 16:22:21	[scenario5d6fa25a3089e9.91185995]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 16:29:19	[scenario5d6fae1f52d378.85629390]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 16:29:21	[scenario5d6fae1f52d378.85629390]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    }
}

2019-09-04 16:29:21	[scenario5d6fae1f52d378.85629390]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c222b9a1a2373f18eac2a8aad93e014f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 16:29:21	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_first
2019-09-04 16:29:21	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_second
2019-09-04 16:29:27	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_third
2019-09-04 16:29:28	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_fourth
2019-09-04 16:29:30	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_fifth
2019-09-04 16:29:30	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_six
2019-09-04 16:29:31	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_seven
2019-09-04 16:29:32	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_nine
2019-09-04 16:29:33	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_ten
2019-09-04 16:29:33	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_eleven
2019-09-04 16:29:34	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_twelve
2019-09-04 16:29:40	[scenario5d6fae1f52d378.85629390]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 16:29:40	[scenario5d6fae1f52d378.85629390]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => [
    "TrueMachine-AgeVerification"
]
Context missing => []

2019-09-04 16:29:40	[scenario5d6fae1f52d378.85629390]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => c222b9a1a2373f18eac2a8aad93e014f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 16:29:40	[scenario5d6fae1f52d378.85629390]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 16:29:41	[scenario5d6fae1f52d378.85629390]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 16:29:41	[scenario5d6fae1f52d378.85629390]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 16:29:41	[scenario5d6fae1f52d378.85629390]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 16:29:41	[scenario5d6fae1f52d378.85629390]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c222b9a1a2373f18eac2a8aad93e014f
Context transitions => [
    "step_thirteen"
]

2019-09-04 16:29:41	[scenario5d6fae1f52d378.85629390]	Execute X-Cart step: step_thirteen
2019-09-04 16:29:44	[scenario5d6fae1f52d378.85629390]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 16:30:44	[scenario5d6fae20307995.52739184]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateScriptState::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 16:30:44	[scenario5d6fae20307995.52739184]	Update script transitions
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 16:30:45	[scenario5d6fae20307995.52739184]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 16:30:45	[scenario5d6fae20307995.52739184]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 16:30:45	[scenario5d6fae20307995.52739184]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 90af8fc06516c3585e694c00f8dec01c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 16:30:45	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_first
2019-09-04 16:30:46	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_second
2019-09-04 16:30:52	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_third
2019-09-04 16:30:53	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_fourth
2019-09-04 16:30:56	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_fifth
2019-09-04 16:30:57	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_six
2019-09-04 16:30:58	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_seven
2019-09-04 16:31:00	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_nine
2019-09-04 16:31:01	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_ten
2019-09-04 16:31:02	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_eleven
2019-09-04 16:31:03	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_twelve
2019-09-04 16:31:10	[scenario5d6fae20307995.52739184]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => {
    "TrueMachine-AgeVerification": {
        "id": "TrueMachine-AgeVerification",
        "transition": "enable",
        "stateBeforeTransition": {
            "installed": true,
            "integrated": true,
            "enabled": false,
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": []
        },
        "stateAfterTransition": {
            "id": "TrueMachine-AgeVerification",
            "version": "5.4.0.2",
            "type": "common",
            "author": "TrueMachine",
            "name": "AgeVerification",
            "authorName": "TrueMachine",
            "moduleName": "AgeVerification",
            "description": "The module adds popup verification on storefront",
            "minorRequiredCoreVersion": 0,
            "dependsOn": [],
            "incompatibleWith": [],
            "skins": [],
            "showSettingsForm": true,
            "isSystem": false,
            "canDisable": true,
            "directories": [
                "/home/kirill/domen/xcart/classes/XLite/Module/TrueMachine/AgeVerification/",
                "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/"
            ],
            "icon": "skins/admin/images/addon_default.png",
            "service": [],
            "installed": true,
            "integrated": true,
            "enabled": true
        },
        "info": {
            "reason": null,
            "humanReason": null
        }
    }
}

2019-09-04 16:31:10	[scenario5d6fae20307995.52739184]	Data updated: TrueMachine-AgeVerification
2019-09-04 16:31:10	[scenario5d6fae20307995.52739184]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 16:31:10	[scenario5d6fae20307995.52739184]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 90af8fc06516c3585e694c00f8dec01c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 16:31:11	[scenario5d6fae20307995.52739184]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 16:31:12	[scenario5d6fae20307995.52739184]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 16:31:13	[scenario5d6fae20307995.52739184]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 16:31:14	[scenario5d6fae20307995.52739184]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 16:31:14	[scenario5d6fae20307995.52739184]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 90af8fc06516c3585e694c00f8dec01c
Context transitions => [
    "step_thirteen"
]

2019-09-04 16:31:14	[scenario5d6fae20307995.52739184]	Execute X-Cart step: step_thirteen
2019-09-04 16:31:16	[scenario5d6fae20307995.52739184]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 17:09:31	[scenario5d6fb78b5705a9.52827857]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 17:09:33	[scenario5d6fb78b5705a9.52827857]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 17:09:33	[scenario5d6fb78b5705a9.52827857]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e3d120ac9f3091f0ecf3c11241f10b21
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 17:09:33	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_first
2019-09-04 17:09:34	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_second
2019-09-04 17:09:39	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_third
2019-09-04 17:09:39	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_fourth
2019-09-04 17:09:43	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_fifth
2019-09-04 17:09:43	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_six
2019-09-04 17:09:45	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_seven
2019-09-04 17:09:47	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_nine
2019-09-04 17:09:48	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_ten
2019-09-04 17:09:48	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_eleven
2019-09-04 17:09:48	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_twelve
2019-09-04 17:09:56	[scenario5d6fb78b5705a9.52827857]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 17:09:56	[scenario5d6fb78b5705a9.52827857]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 17:09:56	[scenario5d6fb78b5705a9.52827857]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e3d120ac9f3091f0ecf3c11241f10b21
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 17:09:57	[scenario5d6fb78b5705a9.52827857]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 17:09:57	[scenario5d6fb78b5705a9.52827857]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 17:09:57	[scenario5d6fb78b5705a9.52827857]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 17:09:58	[scenario5d6fb78b5705a9.52827857]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 17:09:58	[scenario5d6fb78b5705a9.52827857]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e3d120ac9f3091f0ecf3c11241f10b21
Context transitions => [
    "step_thirteen"
]

2019-09-04 17:09:58	[scenario5d6fb78b5705a9.52827857]	Execute X-Cart step: step_thirteen
2019-09-04 17:10:00	[scenario5d6fb78b5705a9.52827857]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 17:52:14	[scenario5d6fc18e870079.99184359]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 17:52:16	[scenario5d6fc18e870079.99184359]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 17:52:16	[scenario5d6fc18e870079.99184359]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 45cb616b21d40793966168628cfb086f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 17:52:16	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_first
2019-09-04 17:52:17	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_second
2019-09-04 17:52:23	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_third
2019-09-04 17:52:24	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_fourth
2019-09-04 17:52:27	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_fifth
2019-09-04 17:52:27	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_six
2019-09-04 17:52:28	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_seven
2019-09-04 17:52:29	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_nine
2019-09-04 17:52:30	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_ten
2019-09-04 17:52:31	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_eleven
2019-09-04 17:52:31	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_twelve
2019-09-04 17:52:38	[scenario5d6fc18e870079.99184359]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 17:52:39	[scenario5d6fc18e870079.99184359]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 17:52:39	[scenario5d6fc18e870079.99184359]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 45cb616b21d40793966168628cfb086f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 17:52:39	[scenario5d6fc18e870079.99184359]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 17:52:40	[scenario5d6fc18e870079.99184359]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 17:52:40	[scenario5d6fc18e870079.99184359]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 17:52:40	[scenario5d6fc18e870079.99184359]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 17:52:40	[scenario5d6fc18e870079.99184359]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 45cb616b21d40793966168628cfb086f
Context transitions => [
    "step_thirteen"
]

2019-09-04 17:52:41	[scenario5d6fc18e870079.99184359]	Execute X-Cart step: step_thirteen
2019-09-04 17:52:43	[scenario5d6fc18e870079.99184359]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 18:14:58	[scenario5d6fc6e2776d36.12434964]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 18:15:01	[scenario5d6fc6e2776d36.12434964]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 18:15:01	[scenario5d6fc6e2776d36.12434964]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e5b8c6b465daa2d537f0da023d3e1af5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 18:15:01	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_first
2019-09-04 18:15:01	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_second
2019-09-04 18:15:07	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_third
2019-09-04 18:15:08	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_fourth
2019-09-04 18:15:11	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_fifth
2019-09-04 18:15:11	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_six
2019-09-04 18:15:12	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_seven
2019-09-04 18:15:14	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_nine
2019-09-04 18:15:15	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_ten
2019-09-04 18:15:16	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_eleven
2019-09-04 18:15:16	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_twelve
2019-09-04 18:15:24	[scenario5d6fc6e2776d36.12434964]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 18:15:25	[scenario5d6fc6e2776d36.12434964]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 18:15:25	[scenario5d6fc6e2776d36.12434964]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e5b8c6b465daa2d537f0da023d3e1af5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 18:15:25	[scenario5d6fc6e2776d36.12434964]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 18:15:25	[scenario5d6fc6e2776d36.12434964]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 18:15:26	[scenario5d6fc6e2776d36.12434964]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 18:15:26	[scenario5d6fc6e2776d36.12434964]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 18:15:26	[scenario5d6fc6e2776d36.12434964]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e5b8c6b465daa2d537f0da023d3e1af5
Context transitions => [
    "step_thirteen"
]

2019-09-04 18:15:26	[scenario5d6fc6e2776d36.12434964]	Execute X-Cart step: step_thirteen
2019-09-04 18:15:29	[scenario5d6fc6e2776d36.12434964]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 18:16:30	[scenario5d6fc73ea01a04.66046470]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 18:16:33	[scenario5d6fc73ea01a04.66046470]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 18:16:33	[scenario5d6fc73ea01a04.66046470]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a7c68b070ef6e0d8925cbff2ec4417cf
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 18:16:33	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_first
2019-09-04 18:16:34	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_second
2019-09-04 18:16:42	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_third
2019-09-04 18:16:43	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_fourth
2019-09-04 18:16:45	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_fifth
2019-09-04 18:16:46	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_six
2019-09-04 18:16:46	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_seven
2019-09-04 18:16:49	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_nine
2019-09-04 18:16:49	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_ten
2019-09-04 18:16:50	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_eleven
2019-09-04 18:16:50	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_twelve
2019-09-04 18:16:57	[scenario5d6fc73ea01a04.66046470]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 18:16:58	[scenario5d6fc73ea01a04.66046470]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 18:16:58	[scenario5d6fc73ea01a04.66046470]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a7c68b070ef6e0d8925cbff2ec4417cf
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 18:16:58	[scenario5d6fc73ea01a04.66046470]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 18:16:59	[scenario5d6fc73ea01a04.66046470]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 18:16:59	[scenario5d6fc73ea01a04.66046470]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 18:16:59	[scenario5d6fc73ea01a04.66046470]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 18:16:59	[scenario5d6fc73ea01a04.66046470]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a7c68b070ef6e0d8925cbff2ec4417cf
Context transitions => [
    "step_thirteen"
]

2019-09-04 18:17:00	[scenario5d6fc73ea01a04.66046470]	Execute X-Cart step: step_thirteen
2019-09-04 18:17:02	[scenario5d6fc73ea01a04.66046470]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 18:41:13	[scenario5d6fcd090974b4.52489835]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 18:41:14	[scenario5d6fcd090974b4.52489835]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 18:41:15	[scenario5d6fcd090974b4.52489835]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c0f5584090280c258e50b7380aa9580d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 18:41:15	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_first
2019-09-04 18:41:15	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_second
2019-09-04 18:41:22	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_third
2019-09-04 18:41:22	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_fourth
2019-09-04 18:41:26	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_fifth
2019-09-04 18:41:26	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_six
2019-09-04 18:41:27	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_seven
2019-09-04 18:41:28	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_nine
2019-09-04 18:41:29	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_ten
2019-09-04 18:41:29	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_eleven
2019-09-04 18:41:30	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_twelve
2019-09-04 18:41:37	[scenario5d6fcd090974b4.52489835]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 18:41:37	[scenario5d6fcd090974b4.52489835]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 18:41:37	[scenario5d6fcd090974b4.52489835]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => c0f5584090280c258e50b7380aa9580d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 18:41:37	[scenario5d6fcd090974b4.52489835]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 18:41:38	[scenario5d6fcd090974b4.52489835]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 18:41:39	[scenario5d6fcd090974b4.52489835]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 18:41:39	[scenario5d6fcd090974b4.52489835]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 18:41:39	[scenario5d6fcd090974b4.52489835]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c0f5584090280c258e50b7380aa9580d
Context transitions => [
    "step_thirteen"
]

2019-09-04 18:41:40	[scenario5d6fcd090974b4.52489835]	Execute X-Cart step: step_thirteen
2019-09-04 18:41:42	[scenario5d6fcd090974b4.52489835]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 18:46:46	[scenario5d6fce56279ad2.50715244]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 18:46:47	[scenario5d6fce56279ad2.50715244]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 18:46:47	[scenario5d6fce56279ad2.50715244]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b7a425b63a1dbd360df716600e4446fb
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 18:46:48	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_first
2019-09-04 18:46:48	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_second
2019-09-04 18:46:54	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_third
2019-09-04 18:46:55	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_fourth
2019-09-04 18:46:58	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_fifth
2019-09-04 18:46:58	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_six
2019-09-04 18:46:59	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_seven
2019-09-04 18:47:01	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_nine
2019-09-04 18:47:01	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_ten
2019-09-04 18:47:02	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_eleven
2019-09-04 18:47:02	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_twelve
2019-09-04 18:47:08	[scenario5d6fce56279ad2.50715244]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 18:47:09	[scenario5d6fce56279ad2.50715244]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 18:47:09	[scenario5d6fce56279ad2.50715244]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b7a425b63a1dbd360df716600e4446fb
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 18:47:09	[scenario5d6fce56279ad2.50715244]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 18:47:10	[scenario5d6fce56279ad2.50715244]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 18:47:10	[scenario5d6fce56279ad2.50715244]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 18:47:10	[scenario5d6fce56279ad2.50715244]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 18:47:10	[scenario5d6fce56279ad2.50715244]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b7a425b63a1dbd360df716600e4446fb
Context transitions => [
    "step_thirteen"
]

2019-09-04 18:47:10	[scenario5d6fce56279ad2.50715244]	Execute X-Cart step: step_thirteen
2019-09-04 18:47:13	[scenario5d6fce56279ad2.50715244]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 18:49:49	[scenario5d6fcf0d3b6a25.68639323]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 18:49:51	[scenario5d6fcf0d3b6a25.68639323]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 18:49:51	[scenario5d6fcf0d3b6a25.68639323]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 05d279b832381b117f04f3a75b67a2a2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 18:49:51	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_first
2019-09-04 18:49:52	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_second
2019-09-04 18:49:59	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_third
2019-09-04 18:50:00	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_fourth
2019-09-04 18:50:03	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_fifth
2019-09-04 18:50:03	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_six
2019-09-04 18:50:04	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_seven
2019-09-04 18:50:06	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_nine
2019-09-04 18:50:07	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_ten
2019-09-04 18:50:07	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_eleven
2019-09-04 18:50:08	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_twelve
2019-09-04 18:50:14	[scenario5d6fcf0d3b6a25.68639323]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 18:50:15	[scenario5d6fcf0d3b6a25.68639323]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 18:50:15	[scenario5d6fcf0d3b6a25.68639323]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 05d279b832381b117f04f3a75b67a2a2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 18:50:15	[scenario5d6fcf0d3b6a25.68639323]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 18:50:16	[scenario5d6fcf0d3b6a25.68639323]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 18:50:16	[scenario5d6fcf0d3b6a25.68639323]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 18:50:16	[scenario5d6fcf0d3b6a25.68639323]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 18:50:16	[scenario5d6fcf0d3b6a25.68639323]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 05d279b832381b117f04f3a75b67a2a2
Context transitions => [
    "step_thirteen"
]

2019-09-04 18:50:16	[scenario5d6fcf0d3b6a25.68639323]	Execute X-Cart step: step_thirteen
2019-09-04 18:50:19	[scenario5d6fcf0d3b6a25.68639323]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 19:14:58	[scenario5d6fd4f2a6f795.53191471]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 19:15:00	[scenario5d6fd4f2a6f795.53191471]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 19:15:00	[scenario5d6fd4f2a6f795.53191471]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5d726a590cce494e2454ed48ee5f526d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 19:15:01	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_first
2019-09-04 19:15:01	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_second
2019-09-04 19:15:09	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_third
2019-09-04 19:15:09	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_fourth
2019-09-04 19:15:12	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_fifth
2019-09-04 19:15:12	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_six
2019-09-04 19:15:13	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_seven
2019-09-04 19:15:14	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_nine
2019-09-04 19:15:15	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_ten
2019-09-04 19:15:16	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_eleven
2019-09-04 19:15:16	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_twelve
2019-09-04 19:15:23	[scenario5d6fd4f2a6f795.53191471]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 19:15:23	[scenario5d6fd4f2a6f795.53191471]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 19:15:23	[scenario5d6fd4f2a6f795.53191471]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5d726a590cce494e2454ed48ee5f526d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 19:15:23	[scenario5d6fd4f2a6f795.53191471]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 19:15:24	[scenario5d6fd4f2a6f795.53191471]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 19:15:24	[scenario5d6fd4f2a6f795.53191471]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 19:15:24	[scenario5d6fd4f2a6f795.53191471]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 19:15:24	[scenario5d6fd4f2a6f795.53191471]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5d726a590cce494e2454ed48ee5f526d
Context transitions => [
    "step_thirteen"
]

2019-09-04 19:15:25	[scenario5d6fd4f2a6f795.53191471]	Execute X-Cart step: step_thirteen
2019-09-04 19:15:26	[scenario5d6fd4f2a6f795.53191471]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 19:22:27	[scenario5d6fd6b3316065.90015662]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 19:22:29	[scenario5d6fd6b3316065.90015662]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 19:22:29	[scenario5d6fd6b3316065.90015662]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => defdb6e237c04d361f68f02c3a1534e6
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 19:22:29	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_first
2019-09-04 19:22:30	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_second
2019-09-04 19:22:36	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_third
2019-09-04 19:22:36	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_fourth
2019-09-04 19:22:39	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_fifth
2019-09-04 19:22:40	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_six
2019-09-04 19:22:41	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_seven
2019-09-04 19:22:42	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_nine
2019-09-04 19:22:43	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_ten
2019-09-04 19:22:43	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_eleven
2019-09-04 19:22:44	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_twelve
2019-09-04 19:22:50	[scenario5d6fd6b3316065.90015662]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 19:22:51	[scenario5d6fd6b3316065.90015662]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 19:22:51	[scenario5d6fd6b3316065.90015662]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => defdb6e237c04d361f68f02c3a1534e6
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 19:22:51	[scenario5d6fd6b3316065.90015662]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 19:22:51	[scenario5d6fd6b3316065.90015662]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 19:22:51	[scenario5d6fd6b3316065.90015662]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 19:22:52	[scenario5d6fd6b3316065.90015662]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 19:22:52	[scenario5d6fd6b3316065.90015662]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => defdb6e237c04d361f68f02c3a1534e6
Context transitions => [
    "step_thirteen"
]

2019-09-04 19:22:52	[scenario5d6fd6b3316065.90015662]	Execute X-Cart step: step_thirteen
2019-09-04 19:22:54	[scenario5d6fd6b3316065.90015662]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 19:41:46	[scenario5d6fdb3adb5119.09512438]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 19:41:49	[scenario5d6fdb3adb5119.09512438]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 19:41:50	[scenario5d6fdb3adb5119.09512438]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2265e3d67edd451267a9c69fa906e344
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 19:41:50	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_first
2019-09-04 19:41:51	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_second
2019-09-04 19:41:58	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_third
2019-09-04 19:41:59	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_fourth
2019-09-04 19:42:02	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_fifth
2019-09-04 19:42:02	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_six
2019-09-04 19:42:03	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_seven
2019-09-04 19:42:06	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_nine
2019-09-04 19:42:06	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_ten
2019-09-04 19:42:07	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_eleven
2019-09-04 19:42:07	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_twelve
2019-09-04 19:42:18	[scenario5d6fdb3adb5119.09512438]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 19:42:19	[scenario5d6fdb3adb5119.09512438]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 19:42:19	[scenario5d6fdb3adb5119.09512438]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2265e3d67edd451267a9c69fa906e344
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 19:42:19	[scenario5d6fdb3adb5119.09512438]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 19:42:20	[scenario5d6fdb3adb5119.09512438]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 19:42:20	[scenario5d6fdb3adb5119.09512438]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 19:42:21	[scenario5d6fdb3adb5119.09512438]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 19:42:21	[scenario5d6fdb3adb5119.09512438]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2265e3d67edd451267a9c69fa906e344
Context transitions => [
    "step_thirteen"
]

2019-09-04 19:42:21	[scenario5d6fdb3adb5119.09512438]	Execute X-Cart step: step_thirteen
2019-09-04 19:42:23	[scenario5d6fdb3adb5119.09512438]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 19:46:50	[scenario5d6fdc6acc9423.34532652]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 19:46:52	[scenario5d6fdc6acc9423.34532652]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 19:46:52	[scenario5d6fdc6acc9423.34532652]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0b5acab04ee05b42bf92722e5bba2318
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 19:46:53	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_first
2019-09-04 19:46:53	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_second
2019-09-04 19:46:58	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_third
2019-09-04 19:46:59	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_fourth
2019-09-04 19:47:02	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_fifth
2019-09-04 19:47:02	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_six
2019-09-04 19:47:03	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_seven
2019-09-04 19:47:04	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_nine
2019-09-04 19:47:05	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_ten
2019-09-04 19:47:05	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_eleven
2019-09-04 19:47:05	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_twelve
2019-09-04 19:47:13	[scenario5d6fdc6acc9423.34532652]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 19:47:13	[scenario5d6fdc6acc9423.34532652]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 19:47:13	[scenario5d6fdc6acc9423.34532652]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 0b5acab04ee05b42bf92722e5bba2318
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 19:47:13	[scenario5d6fdc6acc9423.34532652]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 19:47:14	[scenario5d6fdc6acc9423.34532652]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 19:47:14	[scenario5d6fdc6acc9423.34532652]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 19:47:14	[scenario5d6fdc6acc9423.34532652]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 19:47:14	[scenario5d6fdc6acc9423.34532652]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0b5acab04ee05b42bf92722e5bba2318
Context transitions => [
    "step_thirteen"
]

2019-09-04 19:47:14	[scenario5d6fdc6acc9423.34532652]	Execute X-Cart step: step_thirteen
2019-09-04 19:47:16	[scenario5d6fdc6acc9423.34532652]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 19:50:46	[scenario5d6fdd56e831e7.82885305]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 19:50:48	[scenario5d6fdd56e831e7.82885305]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 19:50:48	[scenario5d6fdd56e831e7.82885305]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2edafb0009b6a6006ff4b58763fe1d41
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 19:50:48	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_first
2019-09-04 19:50:49	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_second
2019-09-04 19:50:54	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_third
2019-09-04 19:50:54	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_fourth
2019-09-04 19:50:57	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_fifth
2019-09-04 19:50:57	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_six
2019-09-04 19:50:58	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_seven
2019-09-04 19:50:59	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_nine
2019-09-04 19:51:00	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_ten
2019-09-04 19:51:00	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_eleven
2019-09-04 19:51:00	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_twelve
2019-09-04 19:51:06	[scenario5d6fdd56e831e7.82885305]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 19:51:07	[scenario5d6fdd56e831e7.82885305]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 19:51:07	[scenario5d6fdd56e831e7.82885305]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2edafb0009b6a6006ff4b58763fe1d41
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 19:51:07	[scenario5d6fdd56e831e7.82885305]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 19:51:08	[scenario5d6fdd56e831e7.82885305]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 19:51:08	[scenario5d6fdd56e831e7.82885305]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 19:51:08	[scenario5d6fdd56e831e7.82885305]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 19:51:08	[scenario5d6fdd56e831e7.82885305]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2edafb0009b6a6006ff4b58763fe1d41
Context transitions => [
    "step_thirteen"
]

2019-09-04 19:51:08	[scenario5d6fdd56e831e7.82885305]	Execute X-Cart step: step_thirteen
2019-09-04 19:51:10	[scenario5d6fdd56e831e7.82885305]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 19:57:24	[scenario5d6fdee4200323.36146163]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 19:57:26	[scenario5d6fdee4200323.36146163]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 19:57:26	[scenario5d6fdee4200323.36146163]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f037602af87664bb5b4ddf8881b1d2f9
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 19:57:26	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_first
2019-09-04 19:57:26	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_second
2019-09-04 19:57:32	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_third
2019-09-04 19:57:32	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_fourth
2019-09-04 19:57:35	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_fifth
2019-09-04 19:57:35	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_six
2019-09-04 19:57:36	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_seven
2019-09-04 19:57:37	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_nine
2019-09-04 19:57:38	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_ten
2019-09-04 19:57:38	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_eleven
2019-09-04 19:57:38	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_twelve
2019-09-04 19:57:44	[scenario5d6fdee4200323.36146163]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 19:57:45	[scenario5d6fdee4200323.36146163]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 19:57:45	[scenario5d6fdee4200323.36146163]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f037602af87664bb5b4ddf8881b1d2f9
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 19:57:45	[scenario5d6fdee4200323.36146163]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 19:57:46	[scenario5d6fdee4200323.36146163]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 19:57:46	[scenario5d6fdee4200323.36146163]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 19:57:46	[scenario5d6fdee4200323.36146163]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 19:57:46	[scenario5d6fdee4200323.36146163]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f037602af87664bb5b4ddf8881b1d2f9
Context transitions => [
    "step_thirteen"
]

2019-09-04 19:57:46	[scenario5d6fdee4200323.36146163]	Execute X-Cart step: step_thirteen
2019-09-04 19:57:48	[scenario5d6fdee4200323.36146163]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 20:00:59	[scenario5d6fdfbb455149.34667782]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 20:01:01	[scenario5d6fdfbb455149.34667782]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 20:01:01	[scenario5d6fdfbb455149.34667782]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e6abdb868379629978a4ff6e0f3b60bb
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 20:01:01	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_first
2019-09-04 20:01:01	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_second
2019-09-04 20:01:06	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_third
2019-09-04 20:01:07	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_fourth
2019-09-04 20:01:10	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_fifth
2019-09-04 20:01:10	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_six
2019-09-04 20:01:11	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_seven
2019-09-04 20:01:12	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_nine
2019-09-04 20:01:13	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_ten
2019-09-04 20:01:13	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_eleven
2019-09-04 20:01:13	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_twelve
2019-09-04 20:01:21	[scenario5d6fdfbb455149.34667782]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 20:01:22	[scenario5d6fdfbb455149.34667782]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 20:01:22	[scenario5d6fdfbb455149.34667782]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e6abdb868379629978a4ff6e0f3b60bb
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 20:01:22	[scenario5d6fdfbb455149.34667782]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 20:01:23	[scenario5d6fdfbb455149.34667782]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 20:01:24	[scenario5d6fdfbb455149.34667782]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 20:01:25	[scenario5d6fdfbb455149.34667782]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 20:01:25	[scenario5d6fdfbb455149.34667782]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e6abdb868379629978a4ff6e0f3b60bb
Context transitions => [
    "step_thirteen"
]

2019-09-04 20:01:26	[scenario5d6fdfbb455149.34667782]	Execute X-Cart step: step_thirteen
2019-09-04 20:01:28	[scenario5d6fdfbb455149.34667782]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 20:09:05	[scenario5d6fe1a1e67601.40694918]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 20:09:07	[scenario5d6fe1a1e67601.40694918]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 20:09:07	[scenario5d6fe1a1e67601.40694918]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 737e857473960c0201628eefab595fa7
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 20:09:07	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_first
2019-09-04 20:09:08	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_second
2019-09-04 20:09:13	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_third
2019-09-04 20:09:14	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_fourth
2019-09-04 20:09:17	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_fifth
2019-09-04 20:09:18	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_six
2019-09-04 20:09:20	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_seven
2019-09-04 20:09:22	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_nine
2019-09-04 20:09:23	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_ten
2019-09-04 20:09:23	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_eleven
2019-09-04 20:09:24	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_twelve
2019-09-04 20:09:30	[scenario5d6fe1a1e67601.40694918]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 20:09:31	[scenario5d6fe1a1e67601.40694918]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 20:09:31	[scenario5d6fe1a1e67601.40694918]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 737e857473960c0201628eefab595fa7
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 20:09:31	[scenario5d6fe1a1e67601.40694918]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 20:09:31	[scenario5d6fe1a1e67601.40694918]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 20:09:31	[scenario5d6fe1a1e67601.40694918]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 20:09:32	[scenario5d6fe1a1e67601.40694918]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 20:09:32	[scenario5d6fe1a1e67601.40694918]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 737e857473960c0201628eefab595fa7
Context transitions => [
    "step_thirteen"
]

2019-09-04 20:09:32	[scenario5d6fe1a1e67601.40694918]	Execute X-Cart step: step_thirteen
2019-09-04 20:09:34	[scenario5d6fe1a1e67601.40694918]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-04 20:12:46	[scenario5d6fe27e4c10e6.35562101]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-04 20:12:48	[scenario5d6fe27e4c10e6.35562101]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-04 20:12:48	[scenario5d6fe27e4c10e6.35562101]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b98d0298af7dc2f0501d3d2c4d22b9c3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-04 20:12:48	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_first
2019-09-04 20:12:49	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_second
2019-09-04 20:12:55	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_third
2019-09-04 20:12:56	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_fourth
2019-09-04 20:12:59	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_fifth
2019-09-04 20:12:59	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_six
2019-09-04 20:13:00	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_seven
2019-09-04 20:13:03	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_nine
2019-09-04 20:13:04	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_ten
2019-09-04 20:13:04	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_eleven
2019-09-04 20:13:04	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_twelve
2019-09-04 20:13:13	[scenario5d6fe27e4c10e6.35562101]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-04 20:13:13	[scenario5d6fe27e4c10e6.35562101]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-04 20:13:13	[scenario5d6fe27e4c10e6.35562101]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b98d0298af7dc2f0501d3d2c4d22b9c3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-04 20:13:14	[scenario5d6fe27e4c10e6.35562101]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-04 20:13:14	[scenario5d6fe27e4c10e6.35562101]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-04 20:13:14	[scenario5d6fe27e4c10e6.35562101]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-04 20:13:15	[scenario5d6fe27e4c10e6.35562101]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-04 20:13:15	[scenario5d6fe27e4c10e6.35562101]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b98d0298af7dc2f0501d3d2c4d22b9c3
Context transitions => [
    "step_thirteen"
]

2019-09-04 20:13:15	[scenario5d6fe27e4c10e6.35562101]	Execute X-Cart step: step_thirteen
2019-09-04 20:13:17	[scenario5d6fe27e4c10e6.35562101]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

