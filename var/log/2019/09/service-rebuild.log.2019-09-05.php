<?php die(); ?>
2019-09-05 07:56:17	[scenario5d7087616fb128.00225027]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 07:56:20	[scenario5d7087616fb128.00225027]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 07:56:20	[scenario5d7087616fb128.00225027]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 533f24bc3368a25c397c2ca49ad2c9c2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 07:56:21	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_first
2019-09-05 07:56:23	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_second
2019-09-05 07:56:30	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_third
2019-09-05 07:56:31	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_fourth
2019-09-05 07:56:35	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_fifth
2019-09-05 07:56:36	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_six
2019-09-05 07:56:37	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_seven
2019-09-05 07:56:39	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_nine
2019-09-05 07:56:40	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_ten
2019-09-05 07:56:41	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_eleven
2019-09-05 07:56:41	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_twelve
2019-09-05 07:56:50	[scenario5d7087616fb128.00225027]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 07:56:52	[scenario5d7087616fb128.00225027]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 07:56:52	[scenario5d7087616fb128.00225027]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 533f24bc3368a25c397c2ca49ad2c9c2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 07:56:52	[scenario5d7087616fb128.00225027]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 07:56:53	[scenario5d7087616fb128.00225027]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 07:56:53	[scenario5d7087616fb128.00225027]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 07:56:54	[scenario5d7087616fb128.00225027]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 07:56:54	[scenario5d7087616fb128.00225027]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 533f24bc3368a25c397c2ca49ad2c9c2
Context transitions => [
    "step_thirteen"
]

2019-09-05 07:56:54	[scenario5d7087616fb128.00225027]	Execute X-Cart step: step_thirteen
2019-09-05 07:56:57	[scenario5d7087616fb128.00225027]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 07:57:27	[scenario5d7087a7bce813.64691505]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 07:57:30	[scenario5d7087a7bce813.64691505]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 07:57:30	[scenario5d7087a7bce813.64691505]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 19996be3753170458faec1224c398f71
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 07:57:30	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_first
2019-09-05 07:57:31	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_second
2019-09-05 07:57:38	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_third
2019-09-05 07:57:39	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_fourth
2019-09-05 07:57:43	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_fifth
2019-09-05 07:57:43	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_six
2019-09-05 07:57:44	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_seven
2019-09-05 07:57:47	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_nine
2019-09-05 07:57:48	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_ten
2019-09-05 07:57:48	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_eleven
2019-09-05 07:57:49	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_twelve
2019-09-05 07:57:59	[scenario5d7087a7bce813.64691505]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 07:58:00	[scenario5d7087a7bce813.64691505]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 07:58:00	[scenario5d7087a7bce813.64691505]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 19996be3753170458faec1224c398f71
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 07:58:01	[scenario5d7087a7bce813.64691505]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 07:58:02	[scenario5d7087a7bce813.64691505]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 07:58:02	[scenario5d7087a7bce813.64691505]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 07:58:03	[scenario5d7087a7bce813.64691505]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 07:58:03	[scenario5d7087a7bce813.64691505]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 19996be3753170458faec1224c398f71
Context transitions => [
    "step_thirteen"
]

2019-09-05 07:58:03	[scenario5d7087a7bce813.64691505]	Execute X-Cart step: step_thirteen
2019-09-05 07:58:06	[scenario5d7087a7bce813.64691505]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 08:03:08	[scenario5d7088fc505180.56994212]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 08:03:10	[scenario5d7088fc505180.56994212]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 08:03:10	[scenario5d7088fc505180.56994212]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ec601bf036d486a72bfc66fcd430ade3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 08:03:10	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_first
2019-09-05 08:03:11	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_second
2019-09-05 08:03:18	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_third
2019-09-05 08:03:19	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_fourth
2019-09-05 08:03:21	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_fifth
2019-09-05 08:03:21	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_six
2019-09-05 08:03:22	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_seven
2019-09-05 08:03:24	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_nine
2019-09-05 08:03:24	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_ten
2019-09-05 08:03:25	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_eleven
2019-09-05 08:03:25	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_twelve
2019-09-05 08:03:32	[scenario5d7088fc505180.56994212]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 08:03:32	[scenario5d7088fc505180.56994212]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 08:03:32	[scenario5d7088fc505180.56994212]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ec601bf036d486a72bfc66fcd430ade3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 08:03:33	[scenario5d7088fc505180.56994212]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 08:03:33	[scenario5d7088fc505180.56994212]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 08:03:33	[scenario5d7088fc505180.56994212]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 08:03:34	[scenario5d7088fc505180.56994212]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 08:03:34	[scenario5d7088fc505180.56994212]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ec601bf036d486a72bfc66fcd430ade3
Context transitions => [
    "step_thirteen"
]

2019-09-05 08:03:34	[scenario5d7088fc505180.56994212]	Execute X-Cart step: step_thirteen
2019-09-05 08:03:36	[scenario5d7088fc505180.56994212]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 08:19:46	[scenario5d708ce23cc570.28323782]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 08:19:48	[scenario5d708ce23cc570.28323782]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 08:19:48	[scenario5d708ce23cc570.28323782]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ed89cfede4ac3176794f7c7c5ade9328
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 08:19:48	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_first
2019-09-05 08:19:49	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_second
2019-09-05 08:19:56	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_third
2019-09-05 08:19:57	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_fourth
2019-09-05 08:19:59	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_fifth
2019-09-05 08:20:00	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_six
2019-09-05 08:20:02	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_seven
2019-09-05 08:20:04	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_nine
2019-09-05 08:20:05	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_ten
2019-09-05 08:20:06	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_eleven
2019-09-05 08:20:07	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_twelve
2019-09-05 08:20:13	[scenario5d708ce23cc570.28323782]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 08:20:15	[scenario5d708ce23cc570.28323782]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 08:20:15	[scenario5d708ce23cc570.28323782]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ed89cfede4ac3176794f7c7c5ade9328
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 08:20:15	[scenario5d708ce23cc570.28323782]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 08:20:16	[scenario5d708ce23cc570.28323782]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 08:20:17	[scenario5d708ce23cc570.28323782]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 08:20:18	[scenario5d708ce23cc570.28323782]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 08:20:18	[scenario5d708ce23cc570.28323782]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ed89cfede4ac3176794f7c7c5ade9328
Context transitions => [
    "step_thirteen"
]

2019-09-05 08:20:19	[scenario5d708ce23cc570.28323782]	Execute X-Cart step: step_thirteen
2019-09-05 08:20:21	[scenario5d708ce23cc570.28323782]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 08:24:11	[scenario5d708deb10eec4.54768338]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 08:24:13	[scenario5d708deb10eec4.54768338]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 08:24:13	[scenario5d708deb10eec4.54768338]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 95009aca7cefa480b61d6387ca3b6e52
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 08:24:13	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_first
2019-09-05 08:24:14	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_second
2019-09-05 08:24:20	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_third
2019-09-05 08:24:20	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_fourth
2019-09-05 08:24:23	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_fifth
2019-09-05 08:24:23	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_six
2019-09-05 08:24:24	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_seven
2019-09-05 08:24:25	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_nine
2019-09-05 08:24:26	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_ten
2019-09-05 08:24:26	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_eleven
2019-09-05 08:24:26	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_twelve
2019-09-05 08:24:33	[scenario5d708deb10eec4.54768338]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 08:24:33	[scenario5d708deb10eec4.54768338]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 08:24:33	[scenario5d708deb10eec4.54768338]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 95009aca7cefa480b61d6387ca3b6e52
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 08:24:34	[scenario5d708deb10eec4.54768338]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 08:24:34	[scenario5d708deb10eec4.54768338]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 08:24:34	[scenario5d708deb10eec4.54768338]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 08:24:34	[scenario5d708deb10eec4.54768338]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 08:24:34	[scenario5d708deb10eec4.54768338]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 95009aca7cefa480b61d6387ca3b6e52
Context transitions => [
    "step_thirteen"
]

2019-09-05 08:24:35	[scenario5d708deb10eec4.54768338]	Execute X-Cart step: step_thirteen
2019-09-05 08:24:37	[scenario5d708deb10eec4.54768338]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 08:30:36	[scenario5d708f6c8d7056.64304663]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 08:30:38	[scenario5d708f6c8d7056.64304663]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 08:30:38	[scenario5d708f6c8d7056.64304663]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ef926fd2b45ae06b3189e78e15605aa3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 08:30:38	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_first
2019-09-05 08:30:39	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_second
2019-09-05 08:30:45	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_third
2019-09-05 08:30:45	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_fourth
2019-09-05 08:30:48	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_fifth
2019-09-05 08:30:48	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_six
2019-09-05 08:30:49	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_seven
2019-09-05 08:30:50	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_nine
2019-09-05 08:30:51	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_ten
2019-09-05 08:30:51	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_eleven
2019-09-05 08:30:51	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_twelve
2019-09-05 08:30:58	[scenario5d708f6c8d7056.64304663]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 08:30:58	[scenario5d708f6c8d7056.64304663]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 08:30:58	[scenario5d708f6c8d7056.64304663]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ef926fd2b45ae06b3189e78e15605aa3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 08:30:58	[scenario5d708f6c8d7056.64304663]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 08:30:59	[scenario5d708f6c8d7056.64304663]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 08:30:59	[scenario5d708f6c8d7056.64304663]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 08:30:59	[scenario5d708f6c8d7056.64304663]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 08:30:59	[scenario5d708f6c8d7056.64304663]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ef926fd2b45ae06b3189e78e15605aa3
Context transitions => [
    "step_thirteen"
]

2019-09-05 08:30:59	[scenario5d708f6c8d7056.64304663]	Execute X-Cart step: step_thirteen
2019-09-05 08:31:02	[scenario5d708f6c8d7056.64304663]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 08:52:08	[scenario5d70947865a405.38194301]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 08:52:10	[scenario5d70947865a405.38194301]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 08:52:10	[scenario5d70947865a405.38194301]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1cd98904aabee1fa6de810b70df1f86d
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 08:52:10	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_first
2019-09-05 08:52:11	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_second
2019-09-05 08:52:17	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_third
2019-09-05 08:52:17	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_fourth
2019-09-05 08:52:20	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_fifth
2019-09-05 08:52:20	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_six
2019-09-05 08:52:21	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_seven
2019-09-05 08:52:23	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_nine
2019-09-05 08:52:24	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_ten
2019-09-05 08:52:24	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_eleven
2019-09-05 08:52:24	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_twelve
2019-09-05 08:52:30	[scenario5d70947865a405.38194301]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 08:52:31	[scenario5d70947865a405.38194301]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 08:52:31	[scenario5d70947865a405.38194301]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1cd98904aabee1fa6de810b70df1f86d
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 08:52:31	[scenario5d70947865a405.38194301]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 08:52:31	[scenario5d70947865a405.38194301]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 08:52:32	[scenario5d70947865a405.38194301]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 08:52:32	[scenario5d70947865a405.38194301]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 08:52:32	[scenario5d70947865a405.38194301]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1cd98904aabee1fa6de810b70df1f86d
Context transitions => [
    "step_thirteen"
]

2019-09-05 08:52:32	[scenario5d70947865a405.38194301]	Execute X-Cart step: step_thirteen
2019-09-05 08:52:34	[scenario5d70947865a405.38194301]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 08:59:00	[scenario5d7096147e17c8.69311327]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 08:59:02	[scenario5d7096147e17c8.69311327]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 08:59:02	[scenario5d7096147e17c8.69311327]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 135a52cd7c18fca34c30d0090749e5e3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 08:59:02	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_first
2019-09-05 08:59:03	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_second
2019-09-05 08:59:09	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_third
2019-09-05 08:59:10	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_fourth
2019-09-05 08:59:13	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_fifth
2019-09-05 08:59:14	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_six
2019-09-05 08:59:15	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_seven
2019-09-05 08:59:17	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_nine
2019-09-05 08:59:18	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_ten
2019-09-05 08:59:19	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_eleven
2019-09-05 08:59:20	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_twelve
2019-09-05 08:59:26	[scenario5d7096147e17c8.69311327]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 08:59:28	[scenario5d7096147e17c8.69311327]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 08:59:28	[scenario5d7096147e17c8.69311327]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 135a52cd7c18fca34c30d0090749e5e3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 08:59:28	[scenario5d7096147e17c8.69311327]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 08:59:29	[scenario5d7096147e17c8.69311327]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 08:59:30	[scenario5d7096147e17c8.69311327]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 08:59:31	[scenario5d7096147e17c8.69311327]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 08:59:31	[scenario5d7096147e17c8.69311327]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 135a52cd7c18fca34c30d0090749e5e3
Context transitions => [
    "step_thirteen"
]

2019-09-05 08:59:32	[scenario5d7096147e17c8.69311327]	Execute X-Cart step: step_thirteen
2019-09-05 08:59:34	[scenario5d7096147e17c8.69311327]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:02:37	[scenario5d70dd3dde7000.24952451]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:02:40	[scenario5d70dd3dde7000.24952451]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:02:40	[scenario5d70dd3dde7000.24952451]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => efbb484ca855eccf098d047aac886392
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:02:40	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_first
2019-09-05 14:02:41	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_second
2019-09-05 14:02:46	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_third
2019-09-05 14:02:47	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_fourth
2019-09-05 14:02:50	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_fifth
2019-09-05 14:02:50	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_six
2019-09-05 14:02:51	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_seven
2019-09-05 14:02:52	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_nine
2019-09-05 14:02:53	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_ten
2019-09-05 14:02:53	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_eleven
2019-09-05 14:02:54	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_twelve
2019-09-05 14:03:00	[scenario5d70dd3dde7000.24952451]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:03:01	[scenario5d70dd3dde7000.24952451]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:03:01	[scenario5d70dd3dde7000.24952451]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => efbb484ca855eccf098d047aac886392
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:03:01	[scenario5d70dd3dde7000.24952451]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:03:01	[scenario5d70dd3dde7000.24952451]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:03:02	[scenario5d70dd3dde7000.24952451]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:03:02	[scenario5d70dd3dde7000.24952451]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:03:02	[scenario5d70dd3dde7000.24952451]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => efbb484ca855eccf098d047aac886392
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:03:02	[scenario5d70dd3dde7000.24952451]	Execute X-Cart step: step_thirteen
2019-09-05 14:03:04	[scenario5d70dd3dde7000.24952451]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:28:20	[scenario5d70e3441090b4.66366106]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:28:22	[scenario5d70e3441090b4.66366106]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:28:22	[scenario5d70e3441090b4.66366106]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cb032b04d4aae81e63cb606e0186d4b8
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:28:22	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_first
2019-09-05 14:28:23	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_second
2019-09-05 14:28:29	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_third
2019-09-05 14:28:30	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_fourth
2019-09-05 14:28:33	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_fifth
2019-09-05 14:28:34	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_six
2019-09-05 14:28:34	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_seven
2019-09-05 14:28:36	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_nine
2019-09-05 14:28:37	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_ten
2019-09-05 14:28:37	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_eleven
2019-09-05 14:28:38	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_twelve
2019-09-05 14:28:45	[scenario5d70e3441090b4.66366106]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:28:46	[scenario5d70e3441090b4.66366106]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:28:46	[scenario5d70e3441090b4.66366106]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => cb032b04d4aae81e63cb606e0186d4b8
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:28:46	[scenario5d70e3441090b4.66366106]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:28:47	[scenario5d70e3441090b4.66366106]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:28:47	[scenario5d70e3441090b4.66366106]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:28:47	[scenario5d70e3441090b4.66366106]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:28:48	[scenario5d70e3441090b4.66366106]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => cb032b04d4aae81e63cb606e0186d4b8
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:28:48	[scenario5d70e3441090b4.66366106]	Execute X-Cart step: step_thirteen
2019-09-05 14:28:50	[scenario5d70e3441090b4.66366106]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:31:22	[scenario5d70e3fa889595.00303482]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:31:25	[scenario5d70e3fa889595.00303482]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:31:25	[scenario5d70e3fa889595.00303482]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6a123d634687f0a60f46ec11cdf6994b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:31:25	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_first
2019-09-05 14:31:26	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_second
2019-09-05 14:31:32	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_third
2019-09-05 14:31:33	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_fourth
2019-09-05 14:31:36	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_fifth
2019-09-05 14:31:37	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_six
2019-09-05 14:31:38	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_seven
2019-09-05 14:31:39	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_nine
2019-09-05 14:31:40	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_ten
2019-09-05 14:31:40	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_eleven
2019-09-05 14:31:41	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_twelve
2019-09-05 14:31:47	[scenario5d70e3fa889595.00303482]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:31:48	[scenario5d70e3fa889595.00303482]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:31:48	[scenario5d70e3fa889595.00303482]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6a123d634687f0a60f46ec11cdf6994b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:31:48	[scenario5d70e3fa889595.00303482]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:31:49	[scenario5d70e3fa889595.00303482]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:31:49	[scenario5d70e3fa889595.00303482]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:31:50	[scenario5d70e3fa889595.00303482]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:31:50	[scenario5d70e3fa889595.00303482]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6a123d634687f0a60f46ec11cdf6994b
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:31:50	[scenario5d70e3fa889595.00303482]	Execute X-Cart step: step_thirteen
2019-09-05 14:31:52	[scenario5d70e3fa889595.00303482]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:36:18	[scenario5d70e5224c0d75.40944007]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:36:21	[scenario5d70e5224c0d75.40944007]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:36:21	[scenario5d70e5224c0d75.40944007]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b18b2d15401a7c52c1aa41962549956b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:36:21	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_first
2019-09-05 14:36:22	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_second
2019-09-05 14:36:29	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_third
2019-09-05 14:36:30	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_fourth
2019-09-05 14:36:33	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_fifth
2019-09-05 14:36:34	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_six
2019-09-05 14:36:35	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_seven
2019-09-05 14:36:37	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_nine
2019-09-05 14:36:38	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_ten
2019-09-05 14:36:38	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_eleven
2019-09-05 14:36:38	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_twelve
2019-09-05 14:36:47	[scenario5d70e5224c0d75.40944007]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:36:48	[scenario5d70e5224c0d75.40944007]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:36:48	[scenario5d70e5224c0d75.40944007]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b18b2d15401a7c52c1aa41962549956b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:36:48	[scenario5d70e5224c0d75.40944007]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:36:49	[scenario5d70e5224c0d75.40944007]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:36:49	[scenario5d70e5224c0d75.40944007]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:36:50	[scenario5d70e5224c0d75.40944007]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:36:50	[scenario5d70e5224c0d75.40944007]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b18b2d15401a7c52c1aa41962549956b
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:36:50	[scenario5d70e5224c0d75.40944007]	Execute X-Cart step: step_thirteen
2019-09-05 14:36:52	[scenario5d70e5224c0d75.40944007]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:38:51	[scenario5d70e5bbb47e80.47652828]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:38:54	[scenario5d70e5bbb47e80.47652828]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:38:55	[scenario5d70e5bbb47e80.47652828]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6901b21a1223dab7147b54cc18fdcdcf
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:38:55	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_first
2019-09-05 14:38:56	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_second
2019-09-05 14:39:04	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_third
2019-09-05 14:39:05	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_fourth
2019-09-05 14:39:08	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_fifth
2019-09-05 14:39:08	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_six
2019-09-05 14:39:10	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_seven
2019-09-05 14:39:12	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_nine
2019-09-05 14:39:13	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_ten
2019-09-05 14:39:13	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_eleven
2019-09-05 14:39:14	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_twelve
2019-09-05 14:39:22	[scenario5d70e5bbb47e80.47652828]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:39:24	[scenario5d70e5bbb47e80.47652828]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:39:24	[scenario5d70e5bbb47e80.47652828]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6901b21a1223dab7147b54cc18fdcdcf
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:39:25	[scenario5d70e5bbb47e80.47652828]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:39:26	[scenario5d70e5bbb47e80.47652828]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:39:27	[scenario5d70e5bbb47e80.47652828]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:39:28	[scenario5d70e5bbb47e80.47652828]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:39:28	[scenario5d70e5bbb47e80.47652828]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6901b21a1223dab7147b54cc18fdcdcf
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:39:29	[scenario5d70e5bbb47e80.47652828]	Execute X-Cart step: step_thirteen
2019-09-05 14:39:31	[scenario5d70e5bbb47e80.47652828]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:42:56	[scenario5d70e6b0cd0389.98292204]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:42:58	[scenario5d70e6b0cd0389.98292204]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:42:58	[scenario5d70e6b0cd0389.98292204]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => deeb4106919ec81003380598fa8de7f7
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:42:59	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_first
2019-09-05 14:42:59	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_second
2019-09-05 14:43:05	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_third
2019-09-05 14:43:05	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_fourth
2019-09-05 14:43:08	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_fifth
2019-09-05 14:43:08	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_six
2019-09-05 14:43:09	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_seven
2019-09-05 14:43:10	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_nine
2019-09-05 14:43:11	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_ten
2019-09-05 14:43:11	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_eleven
2019-09-05 14:43:11	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_twelve
2019-09-05 14:43:17	[scenario5d70e6b0cd0389.98292204]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:43:18	[scenario5d70e6b0cd0389.98292204]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:43:18	[scenario5d70e6b0cd0389.98292204]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => deeb4106919ec81003380598fa8de7f7
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:43:18	[scenario5d70e6b0cd0389.98292204]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:43:19	[scenario5d70e6b0cd0389.98292204]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:43:19	[scenario5d70e6b0cd0389.98292204]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:43:19	[scenario5d70e6b0cd0389.98292204]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:43:19	[scenario5d70e6b0cd0389.98292204]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => deeb4106919ec81003380598fa8de7f7
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:43:19	[scenario5d70e6b0cd0389.98292204]	Execute X-Cart step: step_thirteen
2019-09-05 14:43:21	[scenario5d70e6b0cd0389.98292204]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:44:53	[scenario5d70e725b40741.98962788]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:44:56	[scenario5d70e725b40741.98962788]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:44:56	[scenario5d70e725b40741.98962788]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6360b3effbea6f65fac344f456bf3212
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:44:56	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_first
2019-09-05 14:44:57	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_second
2019-09-05 14:45:03	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_third
2019-09-05 14:45:03	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_fourth
2019-09-05 14:45:07	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_fifth
2019-09-05 14:45:07	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_six
2019-09-05 14:45:08	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_seven
2019-09-05 14:45:10	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_nine
2019-09-05 14:45:11	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_ten
2019-09-05 14:45:11	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_eleven
2019-09-05 14:45:11	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_twelve
2019-09-05 14:45:19	[scenario5d70e725b40741.98962788]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:45:20	[scenario5d70e725b40741.98962788]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:45:20	[scenario5d70e725b40741.98962788]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6360b3effbea6f65fac344f456bf3212
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:45:20	[scenario5d70e725b40741.98962788]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:45:21	[scenario5d70e725b40741.98962788]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:45:21	[scenario5d70e725b40741.98962788]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:45:22	[scenario5d70e725b40741.98962788]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:45:22	[scenario5d70e725b40741.98962788]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6360b3effbea6f65fac344f456bf3212
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:45:22	[scenario5d70e725b40741.98962788]	Execute X-Cart step: step_thirteen
2019-09-05 14:45:24	[scenario5d70e725b40741.98962788]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:47:14	[scenario5d70e7b29c2e19.05800402]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:47:16	[scenario5d70e7b29c2e19.05800402]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:47:16	[scenario5d70e7b29c2e19.05800402]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 93b8fb53677f370fa7327b0e7540219c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:47:17	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_first
2019-09-05 14:47:18	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_second
2019-09-05 14:47:23	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_third
2019-09-05 14:47:23	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_fourth
2019-09-05 14:47:25	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_fifth
2019-09-05 14:47:26	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_six
2019-09-05 14:47:26	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_seven
2019-09-05 14:47:28	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_nine
2019-09-05 14:47:28	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_ten
2019-09-05 14:47:29	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_eleven
2019-09-05 14:47:29	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_twelve
2019-09-05 14:47:35	[scenario5d70e7b29c2e19.05800402]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:47:35	[scenario5d70e7b29c2e19.05800402]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:47:35	[scenario5d70e7b29c2e19.05800402]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 93b8fb53677f370fa7327b0e7540219c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:47:35	[scenario5d70e7b29c2e19.05800402]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:47:36	[scenario5d70e7b29c2e19.05800402]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:47:36	[scenario5d70e7b29c2e19.05800402]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:47:36	[scenario5d70e7b29c2e19.05800402]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:47:36	[scenario5d70e7b29c2e19.05800402]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 93b8fb53677f370fa7327b0e7540219c
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:47:37	[scenario5d70e7b29c2e19.05800402]	Execute X-Cart step: step_thirteen
2019-09-05 14:47:38	[scenario5d70e7b29c2e19.05800402]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:49:32	[scenario5d70e83cd0a4b1.38810932]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:49:35	[scenario5d70e83cd0a4b1.38810932]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:49:35	[scenario5d70e83cd0a4b1.38810932]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e22887bc4126f71228e269fe41b0ce59
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:49:36	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_first
2019-09-05 14:49:38	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_second
2019-09-05 14:49:43	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_third
2019-09-05 14:49:43	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_fourth
2019-09-05 14:49:45	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_fifth
2019-09-05 14:49:46	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_six
2019-09-05 14:49:46	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_seven
2019-09-05 14:49:48	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_nine
2019-09-05 14:49:48	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_ten
2019-09-05 14:49:49	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_eleven
2019-09-05 14:49:49	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_twelve
2019-09-05 14:49:55	[scenario5d70e83cd0a4b1.38810932]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:49:55	[scenario5d70e83cd0a4b1.38810932]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:49:55	[scenario5d70e83cd0a4b1.38810932]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e22887bc4126f71228e269fe41b0ce59
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:49:55	[scenario5d70e83cd0a4b1.38810932]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:49:56	[scenario5d70e83cd0a4b1.38810932]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:49:56	[scenario5d70e83cd0a4b1.38810932]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:49:56	[scenario5d70e83cd0a4b1.38810932]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:49:56	[scenario5d70e83cd0a4b1.38810932]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e22887bc4126f71228e269fe41b0ce59
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:49:57	[scenario5d70e83cd0a4b1.38810932]	Execute X-Cart step: step_thirteen
2019-09-05 14:49:59	[scenario5d70e83cd0a4b1.38810932]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:52:12	[scenario5d70e8dc4dbe70.67639030]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:52:14	[scenario5d70e8dc4dbe70.67639030]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:52:14	[scenario5d70e8dc4dbe70.67639030]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 339981af785f3acea64c34f44f42befc
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:52:14	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_first
2019-09-05 14:52:15	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_second
2019-09-05 14:52:21	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_third
2019-09-05 14:52:22	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_fourth
2019-09-05 14:52:25	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_fifth
2019-09-05 14:52:26	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_six
2019-09-05 14:52:27	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_seven
2019-09-05 14:52:28	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_nine
2019-09-05 14:52:29	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_ten
2019-09-05 14:52:29	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_eleven
2019-09-05 14:52:30	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_twelve
2019-09-05 14:52:38	[scenario5d70e8dc4dbe70.67639030]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:52:39	[scenario5d70e8dc4dbe70.67639030]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:52:39	[scenario5d70e8dc4dbe70.67639030]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 339981af785f3acea64c34f44f42befc
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:52:39	[scenario5d70e8dc4dbe70.67639030]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:52:40	[scenario5d70e8dc4dbe70.67639030]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:52:40	[scenario5d70e8dc4dbe70.67639030]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:52:40	[scenario5d70e8dc4dbe70.67639030]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:52:40	[scenario5d70e8dc4dbe70.67639030]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 339981af785f3acea64c34f44f42befc
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:52:41	[scenario5d70e8dc4dbe70.67639030]	Execute X-Cart step: step_thirteen
2019-09-05 14:52:44	[scenario5d70e8dc4dbe70.67639030]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 14:54:45	[scenario5d70e975a905c1.87655765]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 14:54:47	[scenario5d70e975a905c1.87655765]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 14:54:47	[scenario5d70e975a905c1.87655765]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 7b8a135b6b885c9789737ce9a6d28a01
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 14:54:47	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_first
2019-09-05 14:54:48	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_second
2019-09-05 14:54:54	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_third
2019-09-05 14:54:54	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_fourth
2019-09-05 14:54:57	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_fifth
2019-09-05 14:54:57	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_six
2019-09-05 14:54:58	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_seven
2019-09-05 14:54:59	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_nine
2019-09-05 14:55:00	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_ten
2019-09-05 14:55:00	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_eleven
2019-09-05 14:55:00	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_twelve
2019-09-05 14:55:06	[scenario5d70e975a905c1.87655765]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 14:55:06	[scenario5d70e975a905c1.87655765]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 14:55:06	[scenario5d70e975a905c1.87655765]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 7b8a135b6b885c9789737ce9a6d28a01
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 14:55:07	[scenario5d70e975a905c1.87655765]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 14:55:07	[scenario5d70e975a905c1.87655765]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 14:55:07	[scenario5d70e975a905c1.87655765]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 14:55:08	[scenario5d70e975a905c1.87655765]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 14:55:08	[scenario5d70e975a905c1.87655765]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 7b8a135b6b885c9789737ce9a6d28a01
Context transitions => [
    "step_thirteen"
]

2019-09-05 14:55:08	[scenario5d70e975a905c1.87655765]	Execute X-Cart step: step_thirteen
2019-09-05 14:55:09	[scenario5d70e975a905c1.87655765]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:07:50	[scenario5d70ec86669c00.10135221]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:07:51	[scenario5d70ec86669c00.10135221]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:07:51	[scenario5d70ec86669c00.10135221]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 03772d2bafccc39a29c427c8bd4f2f54
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:07:52	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_first
2019-09-05 15:07:52	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_second
2019-09-05 15:07:58	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_third
2019-09-05 15:07:58	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_fourth
2019-09-05 15:08:01	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_fifth
2019-09-05 15:08:01	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_six
2019-09-05 15:08:03	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_seven
2019-09-05 15:08:04	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_nine
2019-09-05 15:08:05	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_ten
2019-09-05 15:08:05	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_eleven
2019-09-05 15:08:05	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_twelve
2019-09-05 15:08:11	[scenario5d70ec86669c00.10135221]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:08:12	[scenario5d70ec86669c00.10135221]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:08:12	[scenario5d70ec86669c00.10135221]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 03772d2bafccc39a29c427c8bd4f2f54
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:08:12	[scenario5d70ec86669c00.10135221]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:08:12	[scenario5d70ec86669c00.10135221]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:08:12	[scenario5d70ec86669c00.10135221]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:08:13	[scenario5d70ec86669c00.10135221]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:08:13	[scenario5d70ec86669c00.10135221]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 03772d2bafccc39a29c427c8bd4f2f54
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:08:13	[scenario5d70ec86669c00.10135221]	Execute X-Cart step: step_thirteen
2019-09-05 15:08:15	[scenario5d70ec86669c00.10135221]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:21:37	[scenario5d70efc10bed33.45195572]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:21:39	[scenario5d70efc10bed33.45195572]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:21:39	[scenario5d70efc10bed33.45195572]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8e4d60599bc077bf6ff2280f32edd5bb
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:21:39	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_first
2019-09-05 15:21:40	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_second
2019-09-05 15:21:45	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_third
2019-09-05 15:21:46	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_fourth
2019-09-05 15:21:50	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_fifth
2019-09-05 15:21:51	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_six
2019-09-05 15:21:52	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_seven
2019-09-05 15:21:54	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_nine
2019-09-05 15:21:55	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_ten
2019-09-05 15:21:55	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_eleven
2019-09-05 15:21:56	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_twelve
2019-09-05 15:22:02	[scenario5d70efc10bed33.45195572]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:22:03	[scenario5d70efc10bed33.45195572]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:22:03	[scenario5d70efc10bed33.45195572]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8e4d60599bc077bf6ff2280f32edd5bb
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:22:03	[scenario5d70efc10bed33.45195572]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:22:03	[scenario5d70efc10bed33.45195572]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:22:04	[scenario5d70efc10bed33.45195572]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:22:04	[scenario5d70efc10bed33.45195572]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:22:04	[scenario5d70efc10bed33.45195572]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8e4d60599bc077bf6ff2280f32edd5bb
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:22:04	[scenario5d70efc10bed33.45195572]	Execute X-Cart step: step_thirteen
2019-09-05 15:22:06	[scenario5d70efc10bed33.45195572]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:31:16	[scenario5d70f204ddc923.46513579]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:31:18	[scenario5d70f204ddc923.46513579]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:31:18	[scenario5d70f204ddc923.46513579]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 26cd01f363909a1ea7d217d0907309c4
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:31:18	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_first
2019-09-05 15:31:19	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_second
2019-09-05 15:31:24	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_third
2019-09-05 15:31:25	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_fourth
2019-09-05 15:31:27	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_fifth
2019-09-05 15:31:28	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_six
2019-09-05 15:31:28	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_seven
2019-09-05 15:31:30	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_nine
2019-09-05 15:31:30	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_ten
2019-09-05 15:31:31	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_eleven
2019-09-05 15:31:31	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_twelve
2019-09-05 15:31:38	[scenario5d70f204ddc923.46513579]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:31:39	[scenario5d70f204ddc923.46513579]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:31:39	[scenario5d70f204ddc923.46513579]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 26cd01f363909a1ea7d217d0907309c4
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:31:39	[scenario5d70f204ddc923.46513579]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:31:39	[scenario5d70f204ddc923.46513579]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:31:40	[scenario5d70f204ddc923.46513579]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:31:40	[scenario5d70f204ddc923.46513579]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:31:40	[scenario5d70f204ddc923.46513579]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 26cd01f363909a1ea7d217d0907309c4
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:31:40	[scenario5d70f204ddc923.46513579]	Execute X-Cart step: step_thirteen
2019-09-05 15:31:42	[scenario5d70f204ddc923.46513579]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:38:11	[scenario5d70f3a324c513.47087773]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:38:12	[scenario5d70f3a324c513.47087773]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:38:12	[scenario5d70f3a324c513.47087773]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d2a5dd773fe53b17c586ac02eabadcf3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:38:12	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_first
2019-09-05 15:38:13	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_second
2019-09-05 15:38:18	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_third
2019-09-05 15:38:19	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_fourth
2019-09-05 15:38:21	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_fifth
2019-09-05 15:38:22	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_six
2019-09-05 15:38:22	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_seven
2019-09-05 15:38:24	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_nine
2019-09-05 15:38:24	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_ten
2019-09-05 15:38:25	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_eleven
2019-09-05 15:38:25	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_twelve
2019-09-05 15:38:31	[scenario5d70f3a324c513.47087773]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:38:32	[scenario5d70f3a324c513.47087773]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:38:32	[scenario5d70f3a324c513.47087773]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d2a5dd773fe53b17c586ac02eabadcf3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:38:32	[scenario5d70f3a324c513.47087773]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:38:33	[scenario5d70f3a324c513.47087773]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:38:33	[scenario5d70f3a324c513.47087773]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:38:33	[scenario5d70f3a324c513.47087773]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:38:34	[scenario5d70f3a324c513.47087773]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d2a5dd773fe53b17c586ac02eabadcf3
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:38:34	[scenario5d70f3a324c513.47087773]	Execute X-Cart step: step_thirteen
2019-09-05 15:38:36	[scenario5d70f3a324c513.47087773]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:45:35	[scenario5d70f55f589614.39011222]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:45:36	[scenario5d70f55f589614.39011222]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:45:36	[scenario5d70f55f589614.39011222]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 314879b9e92478192a510bf3158dac5c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:45:36	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_first
2019-09-05 15:45:37	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_second
2019-09-05 15:45:43	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_third
2019-09-05 15:45:43	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_fourth
2019-09-05 15:45:47	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_fifth
2019-09-05 15:45:47	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_six
2019-09-05 15:45:48	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_seven
2019-09-05 15:45:50	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_nine
2019-09-05 15:45:51	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_ten
2019-09-05 15:45:51	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_eleven
2019-09-05 15:45:51	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_twelve
2019-09-05 15:45:59	[scenario5d70f55f589614.39011222]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:46:00	[scenario5d70f55f589614.39011222]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:46:00	[scenario5d70f55f589614.39011222]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 314879b9e92478192a510bf3158dac5c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:46:01	[scenario5d70f55f589614.39011222]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:46:01	[scenario5d70f55f589614.39011222]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:46:02	[scenario5d70f55f589614.39011222]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:46:02	[scenario5d70f55f589614.39011222]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:46:02	[scenario5d70f55f589614.39011222]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 314879b9e92478192a510bf3158dac5c
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:46:02	[scenario5d70f55f589614.39011222]	Execute X-Cart step: step_thirteen
2019-09-05 15:46:05	[scenario5d70f55f589614.39011222]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:48:18	[scenario5d70f6024eb751.17879868]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:48:20	[scenario5d70f6024eb751.17879868]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:48:20	[scenario5d70f6024eb751.17879868]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6c6487b6fc44704238070bc5b4f16b56
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:48:20	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_first
2019-09-05 15:48:21	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_second
2019-09-05 15:48:28	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_third
2019-09-05 15:48:29	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_fourth
2019-09-05 15:48:32	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_fifth
2019-09-05 15:48:32	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_six
2019-09-05 15:48:33	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_seven
2019-09-05 15:48:35	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_nine
2019-09-05 15:48:36	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_ten
2019-09-05 15:48:37	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_eleven
2019-09-05 15:48:37	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_twelve
2019-09-05 15:48:45	[scenario5d70f6024eb751.17879868]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:48:46	[scenario5d70f6024eb751.17879868]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:48:46	[scenario5d70f6024eb751.17879868]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6c6487b6fc44704238070bc5b4f16b56
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:48:46	[scenario5d70f6024eb751.17879868]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:48:47	[scenario5d70f6024eb751.17879868]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:48:47	[scenario5d70f6024eb751.17879868]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:48:47	[scenario5d70f6024eb751.17879868]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:48:47	[scenario5d70f6024eb751.17879868]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6c6487b6fc44704238070bc5b4f16b56
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:48:48	[scenario5d70f6024eb751.17879868]	Execute X-Cart step: step_thirteen
2019-09-05 15:48:50	[scenario5d70f6024eb751.17879868]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:49:40	[scenario5d70f654d30f83.17308168]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:49:43	[scenario5d70f654d30f83.17308168]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:49:43	[scenario5d70f654d30f83.17308168]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4455ec59e1d93252e7cce5e88b55534a
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:49:44	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_first
2019-09-05 15:49:45	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_second
2019-09-05 15:49:51	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_third
2019-09-05 15:49:52	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_fourth
2019-09-05 15:49:55	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_fifth
2019-09-05 15:49:56	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_six
2019-09-05 15:49:57	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_seven
2019-09-05 15:49:58	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_nine
2019-09-05 15:49:59	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_ten
2019-09-05 15:50:00	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_eleven
2019-09-05 15:50:01	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_twelve
2019-09-05 15:50:06	[scenario5d70f654d30f83.17308168]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:50:07	[scenario5d70f654d30f83.17308168]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:50:07	[scenario5d70f654d30f83.17308168]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4455ec59e1d93252e7cce5e88b55534a
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:50:08	[scenario5d70f654d30f83.17308168]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:50:09	[scenario5d70f654d30f83.17308168]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:50:10	[scenario5d70f654d30f83.17308168]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:50:11	[scenario5d70f654d30f83.17308168]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:50:11	[scenario5d70f654d30f83.17308168]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4455ec59e1d93252e7cce5e88b55534a
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:50:12	[scenario5d70f654d30f83.17308168]	Execute X-Cart step: step_thirteen
2019-09-05 15:50:13	[scenario5d70f654d30f83.17308168]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:54:19	[scenario5d70f76b5ae6e8.06551025]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:54:21	[scenario5d70f76b5ae6e8.06551025]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:54:21	[scenario5d70f76b5ae6e8.06551025]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8cb4e95c5377924704bfc31508baf2a5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:54:21	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_first
2019-09-05 15:54:22	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_second
2019-09-05 15:54:28	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_third
2019-09-05 15:54:28	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_fourth
2019-09-05 15:54:31	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_fifth
2019-09-05 15:54:31	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_six
2019-09-05 15:54:32	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_seven
2019-09-05 15:54:33	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_nine
2019-09-05 15:54:34	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_ten
2019-09-05 15:54:34	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_eleven
2019-09-05 15:54:34	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_twelve
2019-09-05 15:54:40	[scenario5d70f76b5ae6e8.06551025]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:54:41	[scenario5d70f76b5ae6e8.06551025]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:54:41	[scenario5d70f76b5ae6e8.06551025]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8cb4e95c5377924704bfc31508baf2a5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:54:41	[scenario5d70f76b5ae6e8.06551025]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:54:42	[scenario5d70f76b5ae6e8.06551025]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:54:42	[scenario5d70f76b5ae6e8.06551025]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:54:42	[scenario5d70f76b5ae6e8.06551025]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:54:42	[scenario5d70f76b5ae6e8.06551025]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8cb4e95c5377924704bfc31508baf2a5
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:54:43	[scenario5d70f76b5ae6e8.06551025]	Execute X-Cart step: step_thirteen
2019-09-05 15:54:45	[scenario5d70f76b5ae6e8.06551025]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:55:27	[scenario5d70f7afcd9de0.53593312]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:55:30	[scenario5d70f7afcd9de0.53593312]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:55:30	[scenario5d70f7afcd9de0.53593312]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f929506ec4ff7acb7addcc3287494a41
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:55:30	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_first
2019-09-05 15:55:31	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_second
2019-09-05 15:55:37	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_third
2019-09-05 15:55:38	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_fourth
2019-09-05 15:55:41	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_fifth
2019-09-05 15:55:42	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_six
2019-09-05 15:55:43	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_seven
2019-09-05 15:55:45	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_nine
2019-09-05 15:55:46	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_ten
2019-09-05 15:55:46	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_eleven
2019-09-05 15:55:46	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_twelve
2019-09-05 15:55:54	[scenario5d70f7afcd9de0.53593312]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:55:55	[scenario5d70f7afcd9de0.53593312]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:55:55	[scenario5d70f7afcd9de0.53593312]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f929506ec4ff7acb7addcc3287494a41
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:55:55	[scenario5d70f7afcd9de0.53593312]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:55:56	[scenario5d70f7afcd9de0.53593312]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:55:56	[scenario5d70f7afcd9de0.53593312]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:55:57	[scenario5d70f7afcd9de0.53593312]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:55:57	[scenario5d70f7afcd9de0.53593312]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f929506ec4ff7acb7addcc3287494a41
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:55:57	[scenario5d70f7afcd9de0.53593312]	Execute X-Cart step: step_thirteen
2019-09-05 15:55:59	[scenario5d70f7afcd9de0.53593312]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:57:26	[scenario5d70f826cc5869.89019146]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:57:29	[scenario5d70f826cc5869.89019146]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:57:29	[scenario5d70f826cc5869.89019146]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fff00a0ac0a4fa6e3af3b4b732cfe536
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:57:29	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_first
2019-09-05 15:57:30	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_second
2019-09-05 15:57:36	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_third
2019-09-05 15:57:37	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_fourth
2019-09-05 15:57:41	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_fifth
2019-09-05 15:57:41	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_six
2019-09-05 15:57:42	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_seven
2019-09-05 15:57:44	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_nine
2019-09-05 15:57:45	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_ten
2019-09-05 15:57:46	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_eleven
2019-09-05 15:57:46	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_twelve
2019-09-05 15:57:54	[scenario5d70f826cc5869.89019146]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:57:55	[scenario5d70f826cc5869.89019146]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:57:55	[scenario5d70f826cc5869.89019146]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => fff00a0ac0a4fa6e3af3b4b732cfe536
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:57:56	[scenario5d70f826cc5869.89019146]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:57:56	[scenario5d70f826cc5869.89019146]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:57:57	[scenario5d70f826cc5869.89019146]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:57:57	[scenario5d70f826cc5869.89019146]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:57:57	[scenario5d70f826cc5869.89019146]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fff00a0ac0a4fa6e3af3b4b732cfe536
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:57:57	[scenario5d70f826cc5869.89019146]	Execute X-Cart step: step_thirteen
2019-09-05 15:57:59	[scenario5d70f826cc5869.89019146]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:58:57	[scenario5d70f88118b556.34949803]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:58:59	[scenario5d70f88118b556.34949803]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:58:59	[scenario5d70f88118b556.34949803]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c442468e9d312aecba5b46c598ae5287
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:59:00	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_first
2019-09-05 15:59:00	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_second
2019-09-05 15:59:07	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_third
2019-09-05 15:59:08	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_fourth
2019-09-05 15:59:11	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_fifth
2019-09-05 15:59:11	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_six
2019-09-05 15:59:12	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_seven
2019-09-05 15:59:14	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_nine
2019-09-05 15:59:15	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_ten
2019-09-05 15:59:15	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_eleven
2019-09-05 15:59:16	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_twelve
2019-09-05 15:59:24	[scenario5d70f88118b556.34949803]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 15:59:24	[scenario5d70f88118b556.34949803]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 15:59:24	[scenario5d70f88118b556.34949803]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => c442468e9d312aecba5b46c598ae5287
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 15:59:25	[scenario5d70f88118b556.34949803]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 15:59:25	[scenario5d70f88118b556.34949803]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 15:59:26	[scenario5d70f88118b556.34949803]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 15:59:26	[scenario5d70f88118b556.34949803]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 15:59:26	[scenario5d70f88118b556.34949803]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c442468e9d312aecba5b46c598ae5287
Context transitions => [
    "step_thirteen"
]

2019-09-05 15:59:26	[scenario5d70f88118b556.34949803]	Execute X-Cart step: step_thirteen
2019-09-05 15:59:29	[scenario5d70f88118b556.34949803]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 15:59:46	[scenario5d70f8b28e5c50.53420868]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 15:59:48	[scenario5d70f8b28e5c50.53420868]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 15:59:48	[scenario5d70f8b28e5c50.53420868]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8c3e664385c955e159ff3db9480c2cb8
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 15:59:48	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_first
2019-09-05 15:59:49	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_second
2019-09-05 15:59:54	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_third
2019-09-05 15:59:55	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_fourth
2019-09-05 15:59:57	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_fifth
2019-09-05 15:59:57	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_six
2019-09-05 15:59:58	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_seven
2019-09-05 15:59:59	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_nine
2019-09-05 16:00:00	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_ten
2019-09-05 16:00:00	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_eleven
2019-09-05 16:00:01	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_twelve
2019-09-05 16:00:06	[scenario5d70f8b28e5c50.53420868]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 16:00:07	[scenario5d70f8b28e5c50.53420868]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 16:00:07	[scenario5d70f8b28e5c50.53420868]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8c3e664385c955e159ff3db9480c2cb8
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 16:00:07	[scenario5d70f8b28e5c50.53420868]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 16:00:08	[scenario5d70f8b28e5c50.53420868]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 16:00:08	[scenario5d70f8b28e5c50.53420868]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 16:00:08	[scenario5d70f8b28e5c50.53420868]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 16:00:08	[scenario5d70f8b28e5c50.53420868]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8c3e664385c955e159ff3db9480c2cb8
Context transitions => [
    "step_thirteen"
]

2019-09-05 16:00:08	[scenario5d70f8b28e5c50.53420868]	Execute X-Cart step: step_thirteen
2019-09-05 16:00:10	[scenario5d70f8b28e5c50.53420868]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 16:01:27	[scenario5d70f91792ba94.67761291]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 16:01:30	[scenario5d70f91792ba94.67761291]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 16:01:30	[scenario5d70f91792ba94.67761291]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3d57ada3438aa096d7dd0b32d47a30a2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 16:01:30	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_first
2019-09-05 16:01:31	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_second
2019-09-05 16:01:37	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_third
2019-09-05 16:01:38	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_fourth
2019-09-05 16:01:41	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_fifth
2019-09-05 16:01:41	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_six
2019-09-05 16:01:43	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_seven
2019-09-05 16:01:46	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_nine
2019-09-05 16:01:47	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_ten
2019-09-05 16:01:47	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_eleven
2019-09-05 16:01:48	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_twelve
2019-09-05 16:01:56	[scenario5d70f91792ba94.67761291]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 16:01:56	[scenario5d70f91792ba94.67761291]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 16:01:56	[scenario5d70f91792ba94.67761291]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 3d57ada3438aa096d7dd0b32d47a30a2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 16:01:57	[scenario5d70f91792ba94.67761291]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 16:01:57	[scenario5d70f91792ba94.67761291]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 16:01:58	[scenario5d70f91792ba94.67761291]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 16:01:58	[scenario5d70f91792ba94.67761291]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 16:01:58	[scenario5d70f91792ba94.67761291]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 3d57ada3438aa096d7dd0b32d47a30a2
Context transitions => [
    "step_thirteen"
]

2019-09-05 16:01:58	[scenario5d70f91792ba94.67761291]	Execute X-Cart step: step_thirteen
2019-09-05 16:02:01	[scenario5d70f91792ba94.67761291]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 16:02:51	[scenario5d70f96b535116.91683203]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 16:02:53	[scenario5d70f96b535116.91683203]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 16:02:54	[scenario5d70f96b535116.91683203]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1eb5d34157d4fa380f79895881a010cb
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 16:02:54	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_first
2019-09-05 16:02:55	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_second
2019-09-05 16:03:01	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_third
2019-09-05 16:03:02	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_fourth
2019-09-05 16:03:05	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_fifth
2019-09-05 16:03:05	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_six
2019-09-05 16:03:06	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_seven
2019-09-05 16:03:08	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_nine
2019-09-05 16:03:09	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_ten
2019-09-05 16:03:10	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_eleven
2019-09-05 16:03:10	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_twelve
2019-09-05 16:03:18	[scenario5d70f96b535116.91683203]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 16:03:19	[scenario5d70f96b535116.91683203]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 16:03:19	[scenario5d70f96b535116.91683203]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1eb5d34157d4fa380f79895881a010cb
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 16:03:19	[scenario5d70f96b535116.91683203]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 16:03:20	[scenario5d70f96b535116.91683203]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 16:03:20	[scenario5d70f96b535116.91683203]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 16:03:21	[scenario5d70f96b535116.91683203]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 16:03:21	[scenario5d70f96b535116.91683203]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1eb5d34157d4fa380f79895881a010cb
Context transitions => [
    "step_thirteen"
]

2019-09-05 16:03:21	[scenario5d70f96b535116.91683203]	Execute X-Cart step: step_thirteen
2019-09-05 16:03:23	[scenario5d70f96b535116.91683203]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 16:39:00	[scenario5d7101e42a9702.77372968]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 16:39:02	[scenario5d7101e42a9702.77372968]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 16:39:02	[scenario5d7101e42a9702.77372968]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c5c5c6d701df407499623b5883d85343
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 16:39:02	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_first
2019-09-05 16:39:03	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_second
2019-09-05 16:39:09	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_third
2019-09-05 16:39:10	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_fourth
2019-09-05 16:39:12	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_fifth
2019-09-05 16:39:12	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_six
2019-09-05 16:39:13	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_seven
2019-09-05 16:39:14	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_nine
2019-09-05 16:39:15	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_ten
2019-09-05 16:39:15	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_eleven
2019-09-05 16:39:16	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_twelve
2019-09-05 16:39:22	[scenario5d7101e42a9702.77372968]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 16:39:22	[scenario5d7101e42a9702.77372968]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 16:39:22	[scenario5d7101e42a9702.77372968]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => c5c5c6d701df407499623b5883d85343
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 16:39:23	[scenario5d7101e42a9702.77372968]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 16:39:24	[scenario5d7101e42a9702.77372968]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 16:39:25	[scenario5d7101e42a9702.77372968]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 16:39:26	[scenario5d7101e42a9702.77372968]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 16:39:26	[scenario5d7101e42a9702.77372968]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => c5c5c6d701df407499623b5883d85343
Context transitions => [
    "step_thirteen"
]

2019-09-05 16:39:27	[scenario5d7101e42a9702.77372968]	Execute X-Cart step: step_thirteen
2019-09-05 16:39:29	[scenario5d7101e42a9702.77372968]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 16:41:37	[scenario5d710281e1e8f0.06818498]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 16:41:40	[scenario5d710281e1e8f0.06818498]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 16:41:40	[scenario5d710281e1e8f0.06818498]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9ae2d5ffe421d00ce826e52566b6dde2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 16:41:40	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_first
2019-09-05 16:41:40	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_second
2019-09-05 16:41:47	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_third
2019-09-05 16:41:48	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_fourth
2019-09-05 16:41:51	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_fifth
2019-09-05 16:41:51	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_six
2019-09-05 16:41:51	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_seven
2019-09-05 16:41:53	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_nine
2019-09-05 16:41:54	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_ten
2019-09-05 16:41:54	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_eleven
2019-09-05 16:41:55	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_twelve
2019-09-05 16:42:02	[scenario5d710281e1e8f0.06818498]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 16:42:02	[scenario5d710281e1e8f0.06818498]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 16:42:03	[scenario5d710281e1e8f0.06818498]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9ae2d5ffe421d00ce826e52566b6dde2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 16:42:03	[scenario5d710281e1e8f0.06818498]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 16:42:03	[scenario5d710281e1e8f0.06818498]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 16:42:04	[scenario5d710281e1e8f0.06818498]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 16:42:04	[scenario5d710281e1e8f0.06818498]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 16:42:04	[scenario5d710281e1e8f0.06818498]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9ae2d5ffe421d00ce826e52566b6dde2
Context transitions => [
    "step_thirteen"
]

2019-09-05 16:42:04	[scenario5d710281e1e8f0.06818498]	Execute X-Cart step: step_thirteen
2019-09-05 16:42:06	[scenario5d710281e1e8f0.06818498]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 16:55:09	[scenario5d7105ad9151b9.42932434]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 16:55:11	[scenario5d7105ad9151b9.42932434]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 16:55:11	[scenario5d7105ad9151b9.42932434]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 28425ce3080fad0a962bbfca2c108690
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 16:55:12	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_first
2019-09-05 16:55:12	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_second
2019-09-05 16:55:19	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_third
2019-09-05 16:55:20	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_fourth
2019-09-05 16:55:23	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_fifth
2019-09-05 16:55:23	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_six
2019-09-05 16:55:24	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_seven
2019-09-05 16:55:26	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_nine
2019-09-05 16:55:27	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_ten
2019-09-05 16:55:27	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_eleven
2019-09-05 16:55:28	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_twelve
2019-09-05 16:55:35	[scenario5d7105ad9151b9.42932434]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 16:55:36	[scenario5d7105ad9151b9.42932434]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 16:55:36	[scenario5d7105ad9151b9.42932434]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 28425ce3080fad0a962bbfca2c108690
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 16:55:36	[scenario5d7105ad9151b9.42932434]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 16:55:36	[scenario5d7105ad9151b9.42932434]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 16:55:37	[scenario5d7105ad9151b9.42932434]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 16:55:37	[scenario5d7105ad9151b9.42932434]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 16:55:37	[scenario5d7105ad9151b9.42932434]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 28425ce3080fad0a962bbfca2c108690
Context transitions => [
    "step_thirteen"
]

2019-09-05 16:55:37	[scenario5d7105ad9151b9.42932434]	Execute X-Cart step: step_thirteen
2019-09-05 16:55:39	[scenario5d7105ad9151b9.42932434]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:05:07	[scenario5d7108031ea454.09435796]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:05:08	[scenario5d7108031ea454.09435796]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:05:08	[scenario5d7108031ea454.09435796]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5f05642481d12839809f29cb408f77f7
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:05:09	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_first
2019-09-05 17:05:09	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_second
2019-09-05 17:05:15	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_third
2019-09-05 17:05:16	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_fourth
2019-09-05 17:05:19	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_fifth
2019-09-05 17:05:19	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_six
2019-09-05 17:05:20	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_seven
2019-09-05 17:05:21	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_nine
2019-09-05 17:05:22	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_ten
2019-09-05 17:05:22	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_eleven
2019-09-05 17:05:22	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_twelve
2019-09-05 17:05:28	[scenario5d7108031ea454.09435796]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:05:29	[scenario5d7108031ea454.09435796]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:05:29	[scenario5d7108031ea454.09435796]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5f05642481d12839809f29cb408f77f7
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:05:29	[scenario5d7108031ea454.09435796]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:05:29	[scenario5d7108031ea454.09435796]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:05:29	[scenario5d7108031ea454.09435796]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:05:30	[scenario5d7108031ea454.09435796]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:05:30	[scenario5d7108031ea454.09435796]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5f05642481d12839809f29cb408f77f7
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:05:30	[scenario5d7108031ea454.09435796]	Execute X-Cart step: step_thirteen
2019-09-05 17:05:32	[scenario5d7108031ea454.09435796]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:08:38	[scenario5d7108d6d90789.14620401]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:08:40	[scenario5d7108d6d90789.14620401]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:08:40	[scenario5d7108d6d90789.14620401]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b4ac27ff61c0a6dbe6d1fd4d9e84443f
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:08:41	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_first
2019-09-05 17:08:41	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_second
2019-09-05 17:08:46	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_third
2019-09-05 17:08:47	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_fourth
2019-09-05 17:08:49	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_fifth
2019-09-05 17:08:49	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_six
2019-09-05 17:08:50	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_seven
2019-09-05 17:08:51	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_nine
2019-09-05 17:08:52	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_ten
2019-09-05 17:08:53	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_eleven
2019-09-05 17:08:53	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_twelve
2019-09-05 17:08:59	[scenario5d7108d6d90789.14620401]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:08:59	[scenario5d7108d6d90789.14620401]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:08:59	[scenario5d7108d6d90789.14620401]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b4ac27ff61c0a6dbe6d1fd4d9e84443f
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:08:59	[scenario5d7108d6d90789.14620401]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:09:00	[scenario5d7108d6d90789.14620401]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:09:00	[scenario5d7108d6d90789.14620401]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:09:00	[scenario5d7108d6d90789.14620401]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:09:00	[scenario5d7108d6d90789.14620401]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b4ac27ff61c0a6dbe6d1fd4d9e84443f
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:09:00	[scenario5d7108d6d90789.14620401]	Execute X-Cart step: step_thirteen
2019-09-05 17:09:03	[scenario5d7108d6d90789.14620401]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:10:09	[scenario5d710931d72098.59141307]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:10:12	[scenario5d710931d72098.59141307]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:10:12	[scenario5d710931d72098.59141307]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4d36dac40450c7db5e742755c10fe1c2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:10:12	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_first
2019-09-05 17:10:13	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_second
2019-09-05 17:10:19	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_third
2019-09-05 17:10:20	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_fourth
2019-09-05 17:10:23	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_fifth
2019-09-05 17:10:23	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_six
2019-09-05 17:10:25	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_seven
2019-09-05 17:10:27	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_nine
2019-09-05 17:10:27	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_ten
2019-09-05 17:10:28	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_eleven
2019-09-05 17:10:28	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_twelve
2019-09-05 17:10:36	[scenario5d710931d72098.59141307]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:10:37	[scenario5d710931d72098.59141307]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:10:37	[scenario5d710931d72098.59141307]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4d36dac40450c7db5e742755c10fe1c2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:10:37	[scenario5d710931d72098.59141307]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:10:37	[scenario5d710931d72098.59141307]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:10:38	[scenario5d710931d72098.59141307]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:10:38	[scenario5d710931d72098.59141307]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:10:38	[scenario5d710931d72098.59141307]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4d36dac40450c7db5e742755c10fe1c2
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:10:38	[scenario5d710931d72098.59141307]	Execute X-Cart step: step_thirteen
2019-09-05 17:10:41	[scenario5d710931d72098.59141307]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:13:22	[scenario5d7109f25691c7.53497756]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:13:24	[scenario5d7109f25691c7.53497756]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:13:24	[scenario5d7109f25691c7.53497756]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a28a44c626692806865c75e9450d3631
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:13:24	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_first
2019-09-05 17:13:25	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_second
2019-09-05 17:13:30	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_third
2019-09-05 17:13:31	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_fourth
2019-09-05 17:13:33	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_fifth
2019-09-05 17:13:33	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_six
2019-09-05 17:13:34	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_seven
2019-09-05 17:13:35	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_nine
2019-09-05 17:13:36	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_ten
2019-09-05 17:13:36	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_eleven
2019-09-05 17:13:37	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_twelve
2019-09-05 17:13:43	[scenario5d7109f25691c7.53497756]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:13:43	[scenario5d7109f25691c7.53497756]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:13:43	[scenario5d7109f25691c7.53497756]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a28a44c626692806865c75e9450d3631
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:13:43	[scenario5d7109f25691c7.53497756]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:13:44	[scenario5d7109f25691c7.53497756]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:13:44	[scenario5d7109f25691c7.53497756]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:13:44	[scenario5d7109f25691c7.53497756]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:13:44	[scenario5d7109f25691c7.53497756]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a28a44c626692806865c75e9450d3631
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:13:45	[scenario5d7109f25691c7.53497756]	Execute X-Cart step: step_thirteen
2019-09-05 17:13:46	[scenario5d7109f25691c7.53497756]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:17:13	[scenario5d710ad90fc4d9.25637826]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:17:14	[scenario5d710ad90fc4d9.25637826]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:17:14	[scenario5d710ad90fc4d9.25637826]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e643335f81e62d11853b8ba94290dc4e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:17:15	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_first
2019-09-05 17:17:15	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_second
2019-09-05 17:17:20	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_third
2019-09-05 17:17:21	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_fourth
2019-09-05 17:17:23	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_fifth
2019-09-05 17:17:24	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_six
2019-09-05 17:17:25	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_seven
2019-09-05 17:17:26	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_nine
2019-09-05 17:17:27	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_ten
2019-09-05 17:17:28	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_eleven
2019-09-05 17:17:28	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_twelve
2019-09-05 17:17:34	[scenario5d710ad90fc4d9.25637826]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:17:35	[scenario5d710ad90fc4d9.25637826]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:17:35	[scenario5d710ad90fc4d9.25637826]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e643335f81e62d11853b8ba94290dc4e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:17:35	[scenario5d710ad90fc4d9.25637826]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:17:35	[scenario5d710ad90fc4d9.25637826]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:17:36	[scenario5d710ad90fc4d9.25637826]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:17:36	[scenario5d710ad90fc4d9.25637826]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:17:36	[scenario5d710ad90fc4d9.25637826]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e643335f81e62d11853b8ba94290dc4e
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:17:36	[scenario5d710ad90fc4d9.25637826]	Execute X-Cart step: step_thirteen
2019-09-05 17:17:38	[scenario5d710ad90fc4d9.25637826]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:19:58	[scenario5d710b7ea414e5.21856842]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:20:00	[scenario5d710b7ea414e5.21856842]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:20:00	[scenario5d710b7ea414e5.21856842]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 273ef4127c64a784f0a5366caa82be6c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:20:00	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_first
2019-09-05 17:20:01	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_second
2019-09-05 17:20:06	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_third
2019-09-05 17:20:07	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_fourth
2019-09-05 17:20:10	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_fifth
2019-09-05 17:20:10	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_six
2019-09-05 17:20:12	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_seven
2019-09-05 17:20:13	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_nine
2019-09-05 17:20:14	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_ten
2019-09-05 17:20:14	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_eleven
2019-09-05 17:20:14	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_twelve
2019-09-05 17:20:21	[scenario5d710b7ea414e5.21856842]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:20:22	[scenario5d710b7ea414e5.21856842]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:20:22	[scenario5d710b7ea414e5.21856842]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 273ef4127c64a784f0a5366caa82be6c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:20:22	[scenario5d710b7ea414e5.21856842]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:20:23	[scenario5d710b7ea414e5.21856842]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:20:23	[scenario5d710b7ea414e5.21856842]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:20:23	[scenario5d710b7ea414e5.21856842]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:20:23	[scenario5d710b7ea414e5.21856842]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 273ef4127c64a784f0a5366caa82be6c
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:20:23	[scenario5d710b7ea414e5.21856842]	Execute X-Cart step: step_thirteen
2019-09-05 17:20:26	[scenario5d710b7ea414e5.21856842]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:27:39	[scenario5d710d4b2c72f5.03035838]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:27:41	[scenario5d710d4b2c72f5.03035838]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:27:41	[scenario5d710d4b2c72f5.03035838]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b8c721b0db484a93b3b72b55f3f6664c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:27:41	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_first
2019-09-05 17:27:42	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_second
2019-09-05 17:27:48	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_third
2019-09-05 17:27:48	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_fourth
2019-09-05 17:27:51	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_fifth
2019-09-05 17:27:51	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_six
2019-09-05 17:27:52	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_seven
2019-09-05 17:27:53	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_nine
2019-09-05 17:27:54	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_ten
2019-09-05 17:27:54	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_eleven
2019-09-05 17:27:55	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_twelve
2019-09-05 17:28:01	[scenario5d710d4b2c72f5.03035838]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:28:01	[scenario5d710d4b2c72f5.03035838]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:28:01	[scenario5d710d4b2c72f5.03035838]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b8c721b0db484a93b3b72b55f3f6664c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:28:01	[scenario5d710d4b2c72f5.03035838]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:28:02	[scenario5d710d4b2c72f5.03035838]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:28:02	[scenario5d710d4b2c72f5.03035838]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:28:02	[scenario5d710d4b2c72f5.03035838]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:28:03	[scenario5d710d4b2c72f5.03035838]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b8c721b0db484a93b3b72b55f3f6664c
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:28:03	[scenario5d710d4b2c72f5.03035838]	Execute X-Cart step: step_thirteen
2019-09-05 17:28:05	[scenario5d710d4b2c72f5.03035838]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:34:43	[scenario5d710ef3a5aad3.52311028]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:34:45	[scenario5d710ef3a5aad3.52311028]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:34:46	[scenario5d710ef3a5aad3.52311028]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ed2fbfb45e5de64df03e924860f93a79
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:34:46	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_first
2019-09-05 17:34:48	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_second
2019-09-05 17:34:52	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_third
2019-09-05 17:34:54	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_fourth
2019-09-05 17:34:56	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_fifth
2019-09-05 17:34:57	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_six
2019-09-05 17:34:57	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_seven
2019-09-05 17:34:59	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_nine
2019-09-05 17:34:59	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_ten
2019-09-05 17:35:00	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_eleven
2019-09-05 17:35:00	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_twelve
2019-09-05 17:35:06	[scenario5d710ef3a5aad3.52311028]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:35:07	[scenario5d710ef3a5aad3.52311028]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:35:07	[scenario5d710ef3a5aad3.52311028]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ed2fbfb45e5de64df03e924860f93a79
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:35:07	[scenario5d710ef3a5aad3.52311028]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:35:07	[scenario5d710ef3a5aad3.52311028]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:35:08	[scenario5d710ef3a5aad3.52311028]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:35:08	[scenario5d710ef3a5aad3.52311028]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:35:08	[scenario5d710ef3a5aad3.52311028]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ed2fbfb45e5de64df03e924860f93a79
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:35:08	[scenario5d710ef3a5aad3.52311028]	Execute X-Cart step: step_thirteen
2019-09-05 17:35:10	[scenario5d710ef3a5aad3.52311028]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:36:06	[scenario5d710f4691eab3.45019546]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:36:09	[scenario5d710f4691eab3.45019546]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:36:09	[scenario5d710f4691eab3.45019546]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2aa1ef59e7d339e225a2fb155616114b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:36:09	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_first
2019-09-05 17:36:10	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_second
2019-09-05 17:36:16	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_third
2019-09-05 17:36:17	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_fourth
2019-09-05 17:36:20	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_fifth
2019-09-05 17:36:21	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_six
2019-09-05 17:36:22	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_seven
2019-09-05 17:36:24	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_nine
2019-09-05 17:36:25	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_ten
2019-09-05 17:36:25	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_eleven
2019-09-05 17:36:26	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_twelve
2019-09-05 17:36:34	[scenario5d710f4691eab3.45019546]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:36:34	[scenario5d710f4691eab3.45019546]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:36:34	[scenario5d710f4691eab3.45019546]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2aa1ef59e7d339e225a2fb155616114b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:36:35	[scenario5d710f4691eab3.45019546]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:36:35	[scenario5d710f4691eab3.45019546]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:36:36	[scenario5d710f4691eab3.45019546]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:36:36	[scenario5d710f4691eab3.45019546]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:36:36	[scenario5d710f4691eab3.45019546]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2aa1ef59e7d339e225a2fb155616114b
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:36:36	[scenario5d710f4691eab3.45019546]	Execute X-Cart step: step_thirteen
2019-09-05 17:36:39	[scenario5d710f4691eab3.45019546]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:37:57	[scenario5d710fb5919da4.71756731]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:38:00	[scenario5d710fb5919da4.71756731]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:38:00	[scenario5d710fb5919da4.71756731]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 18ef518234d00b71189a50260119c298
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:38:00	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_first
2019-09-05 17:38:01	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_second
2019-09-05 17:38:05	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_third
2019-09-05 17:38:06	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_fourth
2019-09-05 17:38:09	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_fifth
2019-09-05 17:38:09	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_six
2019-09-05 17:38:10	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_seven
2019-09-05 17:38:11	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_nine
2019-09-05 17:38:12	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_ten
2019-09-05 17:38:12	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_eleven
2019-09-05 17:38:13	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_twelve
2019-09-05 17:38:19	[scenario5d710fb5919da4.71756731]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:38:20	[scenario5d710fb5919da4.71756731]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:38:20	[scenario5d710fb5919da4.71756731]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 18ef518234d00b71189a50260119c298
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:38:21	[scenario5d710fb5919da4.71756731]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:38:21	[scenario5d710fb5919da4.71756731]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:38:22	[scenario5d710fb5919da4.71756731]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:38:22	[scenario5d710fb5919da4.71756731]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:38:22	[scenario5d710fb5919da4.71756731]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 18ef518234d00b71189a50260119c298
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:38:22	[scenario5d710fb5919da4.71756731]	Execute X-Cart step: step_thirteen
2019-09-05 17:38:25	[scenario5d710fb5919da4.71756731]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 17:47:07	[scenario5d7111db666255.62026852]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 17:47:09	[scenario5d7111db666255.62026852]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 17:47:09	[scenario5d7111db666255.62026852]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 243f8ded4a757c672a21420f6eb614d4
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 17:47:09	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_first
2019-09-05 17:47:10	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_second
2019-09-05 17:47:15	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_third
2019-09-05 17:47:16	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_fourth
2019-09-05 17:47:19	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_fifth
2019-09-05 17:47:19	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_six
2019-09-05 17:47:20	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_seven
2019-09-05 17:47:21	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_nine
2019-09-05 17:47:22	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_ten
2019-09-05 17:47:22	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_eleven
2019-09-05 17:47:23	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_twelve
2019-09-05 17:47:29	[scenario5d7111db666255.62026852]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 17:47:30	[scenario5d7111db666255.62026852]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 17:47:30	[scenario5d7111db666255.62026852]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 243f8ded4a757c672a21420f6eb614d4
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 17:47:30	[scenario5d7111db666255.62026852]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 17:47:30	[scenario5d7111db666255.62026852]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 17:47:31	[scenario5d7111db666255.62026852]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 17:47:31	[scenario5d7111db666255.62026852]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 17:47:31	[scenario5d7111db666255.62026852]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 243f8ded4a757c672a21420f6eb614d4
Context transitions => [
    "step_thirteen"
]

2019-09-05 17:47:31	[scenario5d7111db666255.62026852]	Execute X-Cart step: step_thirteen
2019-09-05 17:47:33	[scenario5d7111db666255.62026852]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 18:00:26	[scenario5d7114fab2add4.72520538]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 18:00:28	[scenario5d7114fab2add4.72520538]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 18:00:28	[scenario5d7114fab2add4.72520538]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1fb55002593f7396178c42f13f73e0fb
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 18:00:28	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_first
2019-09-05 18:00:29	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_second
2019-09-05 18:00:34	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_third
2019-09-05 18:00:36	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_fourth
2019-09-05 18:00:38	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_fifth
2019-09-05 18:00:39	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_six
2019-09-05 18:00:40	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_seven
2019-09-05 18:00:42	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_nine
2019-09-05 18:00:43	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_ten
2019-09-05 18:00:44	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_eleven
2019-09-05 18:00:45	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_twelve
2019-09-05 18:00:51	[scenario5d7114fab2add4.72520538]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 18:00:52	[scenario5d7114fab2add4.72520538]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 18:00:52	[scenario5d7114fab2add4.72520538]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1fb55002593f7396178c42f13f73e0fb
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 18:00:52	[scenario5d7114fab2add4.72520538]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 18:00:53	[scenario5d7114fab2add4.72520538]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 18:00:54	[scenario5d7114fab2add4.72520538]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 18:00:55	[scenario5d7114fab2add4.72520538]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 18:00:55	[scenario5d7114fab2add4.72520538]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1fb55002593f7396178c42f13f73e0fb
Context transitions => [
    "step_thirteen"
]

2019-09-05 18:00:56	[scenario5d7114fab2add4.72520538]	Execute X-Cart step: step_thirteen
2019-09-05 18:00:58	[scenario5d7114fab2add4.72520538]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 18:01:29	[scenario5d7115393024d6.06316032]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 18:01:30	[scenario5d7115393024d6.06316032]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 18:01:30	[scenario5d7115393024d6.06316032]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 67b90043a10e07998e9d5c61a78a7269
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 18:01:31	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_first
2019-09-05 18:01:31	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_second
2019-09-05 18:01:34	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_third
2019-09-05 18:01:34	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_fourth
2019-09-05 18:01:37	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_fifth
2019-09-05 18:01:37	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_six
2019-09-05 18:01:38	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_seven
2019-09-05 18:01:40	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_nine
2019-09-05 18:01:40	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_ten
2019-09-05 18:01:41	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_eleven
2019-09-05 18:01:41	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_twelve
2019-09-05 18:01:46	[scenario5d7115393024d6.06316032]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 18:01:47	[scenario5d7115393024d6.06316032]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 18:01:47	[scenario5d7115393024d6.06316032]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 67b90043a10e07998e9d5c61a78a7269
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 18:01:47	[scenario5d7115393024d6.06316032]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 18:01:48	[scenario5d7115393024d6.06316032]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 18:01:48	[scenario5d7115393024d6.06316032]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 18:01:48	[scenario5d7115393024d6.06316032]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 18:01:48	[scenario5d7115393024d6.06316032]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 67b90043a10e07998e9d5c61a78a7269
Context transitions => [
    "step_thirteen"
]

2019-09-05 18:01:48	[scenario5d7115393024d6.06316032]	Execute X-Cart step: step_thirteen
2019-09-05 18:01:50	[scenario5d7115393024d6.06316032]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-05 18:07:47	[scenario5d7116b38d9281.04022361]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-05 18:07:51	[scenario5d7116b38d9281.04022361]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-05 18:07:51	[scenario5d7116b38d9281.04022361]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 16e56de2fd639536404a6264a3a87a36
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-05 18:07:51	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_first
2019-09-05 18:07:52	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_second
2019-09-05 18:08:00	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_third
2019-09-05 18:08:01	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_fourth
2019-09-05 18:08:05	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_fifth
2019-09-05 18:08:05	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_six
2019-09-05 18:08:06	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_seven
2019-09-05 18:08:09	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_nine
2019-09-05 18:08:10	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_ten
2019-09-05 18:08:11	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_eleven
2019-09-05 18:08:11	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_twelve
2019-09-05 18:08:18	[scenario5d7116b38d9281.04022361]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-05 18:08:19	[scenario5d7116b38d9281.04022361]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-05 18:08:19	[scenario5d7116b38d9281.04022361]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 16e56de2fd639536404a6264a3a87a36
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-05 18:08:20	[scenario5d7116b38d9281.04022361]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-05 18:08:20	[scenario5d7116b38d9281.04022361]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-05 18:08:21	[scenario5d7116b38d9281.04022361]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-05 18:08:21	[scenario5d7116b38d9281.04022361]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-05 18:08:21	[scenario5d7116b38d9281.04022361]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 16e56de2fd639536404a6264a3a87a36
Context transitions => [
    "step_thirteen"
]

2019-09-05 18:08:22	[scenario5d7116b38d9281.04022361]	Execute X-Cart step: step_thirteen
2019-09-05 18:08:24	[scenario5d7116b38d9281.04022361]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

