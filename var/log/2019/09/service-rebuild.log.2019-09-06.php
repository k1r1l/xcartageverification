<?php die(); ?>
2019-09-06 09:06:48	[scenario5d71e96865bc28.28901905]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 09:06:50	[scenario5d71e96865bc28.28901905]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 09:06:50	[scenario5d71e96865bc28.28901905]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a13bc24eb7ec289ab7d8945a48f35508
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 09:06:50	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_first
2019-09-06 09:06:51	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_second
2019-09-06 09:06:56	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_third
2019-09-06 09:06:57	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_fourth
2019-09-06 09:07:01	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_fifth
2019-09-06 09:07:01	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_six
2019-09-06 09:07:03	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_seven
2019-09-06 09:07:05	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_nine
2019-09-06 09:07:06	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_ten
2019-09-06 09:07:06	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_eleven
2019-09-06 09:07:06	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_twelve
2019-09-06 09:07:15	[scenario5d71e96865bc28.28901905]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 09:07:16	[scenario5d71e96865bc28.28901905]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 09:07:16	[scenario5d71e96865bc28.28901905]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => a13bc24eb7ec289ab7d8945a48f35508
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 09:07:16	[scenario5d71e96865bc28.28901905]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 09:07:17	[scenario5d71e96865bc28.28901905]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 09:07:17	[scenario5d71e96865bc28.28901905]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 09:07:17	[scenario5d71e96865bc28.28901905]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 09:07:18	[scenario5d71e96865bc28.28901905]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => a13bc24eb7ec289ab7d8945a48f35508
Context transitions => [
    "step_thirteen"
]

2019-09-06 09:07:18	[scenario5d71e96865bc28.28901905]	Execute X-Cart step: step_thirteen
2019-09-06 09:07:20	[scenario5d71e96865bc28.28901905]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 09:13:59	[scenario5d71eb17b35444.72352628]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 09:14:02	[scenario5d71eb17b35444.72352628]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 09:14:02	[scenario5d71eb17b35444.72352628]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1f466c5052ebb60b4f578a6704892e73
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 09:14:02	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_first
2019-09-06 09:14:03	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_second
2019-09-06 09:14:09	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_third
2019-09-06 09:14:10	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_fourth
2019-09-06 09:14:13	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_fifth
2019-09-06 09:14:15	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_six
2019-09-06 09:14:17	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_seven
2019-09-06 09:14:19	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_nine
2019-09-06 09:14:20	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_ten
2019-09-06 09:14:21	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_eleven
2019-09-06 09:14:22	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_twelve
2019-09-06 09:14:27	[scenario5d71eb17b35444.72352628]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 09:14:28	[scenario5d71eb17b35444.72352628]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 09:14:28	[scenario5d71eb17b35444.72352628]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1f466c5052ebb60b4f578a6704892e73
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 09:14:29	[scenario5d71eb17b35444.72352628]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 09:14:30	[scenario5d71eb17b35444.72352628]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 09:14:31	[scenario5d71eb17b35444.72352628]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 09:14:32	[scenario5d71eb17b35444.72352628]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 09:14:32	[scenario5d71eb17b35444.72352628]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1f466c5052ebb60b4f578a6704892e73
Context transitions => [
    "step_thirteen"
]

2019-09-06 09:14:33	[scenario5d71eb17b35444.72352628]	Execute X-Cart step: step_thirteen
2019-09-06 09:14:35	[scenario5d71eb17b35444.72352628]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 09:16:17	[scenario5d71eba1d9f1f6.34206738]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 09:16:19	[scenario5d71eba1d9f1f6.34206738]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 09:16:19	[scenario5d71eba1d9f1f6.34206738]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f4523e8926b2a7af0cd4b7e12bef6ce4
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 09:16:19	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_first
2019-09-06 09:16:20	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_second
2019-09-06 09:16:24	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_third
2019-09-06 09:16:25	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_fourth
2019-09-06 09:16:28	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_fifth
2019-09-06 09:16:29	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_six
2019-09-06 09:16:31	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_seven
2019-09-06 09:16:33	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_nine
2019-09-06 09:16:34	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_ten
2019-09-06 09:16:35	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_eleven
2019-09-06 09:16:36	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_twelve
2019-09-06 09:16:42	[scenario5d71eba1d9f1f6.34206738]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 09:16:43	[scenario5d71eba1d9f1f6.34206738]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 09:16:43	[scenario5d71eba1d9f1f6.34206738]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f4523e8926b2a7af0cd4b7e12bef6ce4
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 09:16:43	[scenario5d71eba1d9f1f6.34206738]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 09:16:44	[scenario5d71eba1d9f1f6.34206738]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 09:16:44	[scenario5d71eba1d9f1f6.34206738]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 09:16:45	[scenario5d71eba1d9f1f6.34206738]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 09:16:45	[scenario5d71eba1d9f1f6.34206738]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f4523e8926b2a7af0cd4b7e12bef6ce4
Context transitions => [
    "step_thirteen"
]

2019-09-06 09:16:45	[scenario5d71eba1d9f1f6.34206738]	Execute X-Cart step: step_thirteen
2019-09-06 09:16:47	[scenario5d71eba1d9f1f6.34206738]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 09:20:03	[scenario5d71ec83ce95d7.52578924]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 09:20:05	[scenario5d71ec83ce95d7.52578924]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 09:20:05	[scenario5d71ec83ce95d7.52578924]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 171c5638941e8a63c5b0ad9cac7b9dbf
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 09:20:06	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_first
2019-09-06 09:20:06	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_second
2019-09-06 09:20:12	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_third
2019-09-06 09:20:12	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_fourth
2019-09-06 09:20:15	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_fifth
2019-09-06 09:20:15	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_six
2019-09-06 09:20:16	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_seven
2019-09-06 09:20:18	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_nine
2019-09-06 09:20:19	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_ten
2019-09-06 09:20:20	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_eleven
2019-09-06 09:20:20	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_twelve
2019-09-06 09:20:27	[scenario5d71ec83ce95d7.52578924]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 09:20:27	[scenario5d71ec83ce95d7.52578924]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 09:20:27	[scenario5d71ec83ce95d7.52578924]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 171c5638941e8a63c5b0ad9cac7b9dbf
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 09:20:27	[scenario5d71ec83ce95d7.52578924]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 09:20:28	[scenario5d71ec83ce95d7.52578924]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 09:20:28	[scenario5d71ec83ce95d7.52578924]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 09:20:28	[scenario5d71ec83ce95d7.52578924]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 09:20:28	[scenario5d71ec83ce95d7.52578924]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 171c5638941e8a63c5b0ad9cac7b9dbf
Context transitions => [
    "step_thirteen"
]

2019-09-06 09:20:29	[scenario5d71ec83ce95d7.52578924]	Execute X-Cart step: step_thirteen
2019-09-06 09:20:31	[scenario5d71ec83ce95d7.52578924]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 09:23:12	[scenario5d71ed408a8fa0.96564369]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 09:23:15	[scenario5d71ed408a8fa0.96564369]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 09:23:15	[scenario5d71ed408a8fa0.96564369]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bf351756880a3aca053ae7b9fc9b7f39
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 09:23:16	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_first
2019-09-06 09:23:17	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_second
2019-09-06 09:23:24	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_third
2019-09-06 09:23:25	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_fourth
2019-09-06 09:23:29	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_fifth
2019-09-06 09:23:29	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_six
2019-09-06 09:23:30	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_seven
2019-09-06 09:23:32	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_nine
2019-09-06 09:23:33	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_ten
2019-09-06 09:23:34	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_eleven
2019-09-06 09:23:34	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_twelve
2019-09-06 09:23:42	[scenario5d71ed408a8fa0.96564369]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 09:23:44	[scenario5d71ed408a8fa0.96564369]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 09:23:44	[scenario5d71ed408a8fa0.96564369]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bf351756880a3aca053ae7b9fc9b7f39
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 09:23:44	[scenario5d71ed408a8fa0.96564369]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 09:23:45	[scenario5d71ed408a8fa0.96564369]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 09:23:45	[scenario5d71ed408a8fa0.96564369]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 09:23:45	[scenario5d71ed408a8fa0.96564369]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 09:23:45	[scenario5d71ed408a8fa0.96564369]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bf351756880a3aca053ae7b9fc9b7f39
Context transitions => [
    "step_thirteen"
]

2019-09-06 09:23:46	[scenario5d71ed408a8fa0.96564369]	Execute X-Cart step: step_thirteen
2019-09-06 09:23:48	[scenario5d71ed408a8fa0.96564369]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 09:29:59	[scenario5d71eed7efe656.93404939]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 09:30:02	[scenario5d71eed7efe656.93404939]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 09:30:02	[scenario5d71eed7efe656.93404939]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bad253efd41a02c4cf6213a4980a9bc1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 09:30:02	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_first
2019-09-06 09:30:03	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_second
2019-09-06 09:30:08	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_third
2019-09-06 09:30:09	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_fourth
2019-09-06 09:30:12	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_fifth
2019-09-06 09:30:12	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_six
2019-09-06 09:30:13	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_seven
2019-09-06 09:30:14	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_nine
2019-09-06 09:30:15	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_ten
2019-09-06 09:30:16	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_eleven
2019-09-06 09:30:16	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_twelve
2019-09-06 09:30:22	[scenario5d71eed7efe656.93404939]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 09:30:23	[scenario5d71eed7efe656.93404939]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 09:30:23	[scenario5d71eed7efe656.93404939]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => bad253efd41a02c4cf6213a4980a9bc1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 09:30:23	[scenario5d71eed7efe656.93404939]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 09:30:23	[scenario5d71eed7efe656.93404939]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 09:30:24	[scenario5d71eed7efe656.93404939]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 09:30:24	[scenario5d71eed7efe656.93404939]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 09:30:24	[scenario5d71eed7efe656.93404939]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => bad253efd41a02c4cf6213a4980a9bc1
Context transitions => [
    "step_thirteen"
]

2019-09-06 09:30:24	[scenario5d71eed7efe656.93404939]	Execute X-Cart step: step_thirteen
2019-09-06 09:30:26	[scenario5d71eed7efe656.93404939]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 09:34:19	[scenario5d71efdbbef843.75463749]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 09:34:23	[scenario5d71efdbbef843.75463749]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 09:34:23	[scenario5d71efdbbef843.75463749]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 37bb632ce74c7fdd1fc6864eda743319
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 09:34:24	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_first
2019-09-06 09:34:25	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_second
2019-09-06 09:34:31	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_third
2019-09-06 09:34:32	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_fourth
2019-09-06 09:34:35	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_fifth
2019-09-06 09:34:36	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_six
2019-09-06 09:34:37	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_seven
2019-09-06 09:34:39	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_nine
2019-09-06 09:34:40	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_ten
2019-09-06 09:34:41	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_eleven
2019-09-06 09:34:42	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_twelve
2019-09-06 09:34:47	[scenario5d71efdbbef843.75463749]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 09:34:48	[scenario5d71efdbbef843.75463749]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 09:34:48	[scenario5d71efdbbef843.75463749]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 37bb632ce74c7fdd1fc6864eda743319
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 09:34:49	[scenario5d71efdbbef843.75463749]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 09:34:50	[scenario5d71efdbbef843.75463749]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 09:34:51	[scenario5d71efdbbef843.75463749]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 09:34:52	[scenario5d71efdbbef843.75463749]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 09:34:52	[scenario5d71efdbbef843.75463749]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 37bb632ce74c7fdd1fc6864eda743319
Context transitions => [
    "step_thirteen"
]

2019-09-06 09:34:52	[scenario5d71efdbbef843.75463749]	Execute X-Cart step: step_thirteen
2019-09-06 09:34:54	[scenario5d71efdbbef843.75463749]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 09:36:47	[scenario5d71f06fa48bb2.50927050]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 09:36:49	[scenario5d71f06fa48bb2.50927050]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 09:36:50	[scenario5d71f06fa48bb2.50927050]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f670da33c4da1dea61387d4dbef5d8c8
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 09:36:50	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_first
2019-09-06 09:36:51	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_second
2019-09-06 09:36:55	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_third
2019-09-06 09:36:56	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_fourth
2019-09-06 09:36:59	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_fifth
2019-09-06 09:36:59	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_six
2019-09-06 09:37:00	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_seven
2019-09-06 09:37:01	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_nine
2019-09-06 09:37:02	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_ten
2019-09-06 09:37:02	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_eleven
2019-09-06 09:37:02	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_twelve
2019-09-06 09:37:08	[scenario5d71f06fa48bb2.50927050]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 09:37:08	[scenario5d71f06fa48bb2.50927050]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 09:37:08	[scenario5d71f06fa48bb2.50927050]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f670da33c4da1dea61387d4dbef5d8c8
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 09:37:09	[scenario5d71f06fa48bb2.50927050]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 09:37:09	[scenario5d71f06fa48bb2.50927050]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 09:37:09	[scenario5d71f06fa48bb2.50927050]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 09:37:10	[scenario5d71f06fa48bb2.50927050]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 09:37:10	[scenario5d71f06fa48bb2.50927050]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f670da33c4da1dea61387d4dbef5d8c8
Context transitions => [
    "step_thirteen"
]

2019-09-06 09:37:10	[scenario5d71f06fa48bb2.50927050]	Execute X-Cart step: step_thirteen
2019-09-06 09:37:12	[scenario5d71f06fa48bb2.50927050]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 09:39:03	[scenario5d71f0f7e68e27.33639233]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 09:39:05	[scenario5d71f0f7e68e27.33639233]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 09:39:05	[scenario5d71f0f7e68e27.33639233]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fcfc63545bb5948d9b3e220dbc9252b3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 09:39:05	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_first
2019-09-06 09:39:06	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_second
2019-09-06 09:39:11	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_third
2019-09-06 09:39:11	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_fourth
2019-09-06 09:39:14	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_fifth
2019-09-06 09:39:14	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_six
2019-09-06 09:39:15	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_seven
2019-09-06 09:39:16	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_nine
2019-09-06 09:39:17	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_ten
2019-09-06 09:39:17	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_eleven
2019-09-06 09:39:18	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_twelve
2019-09-06 09:39:24	[scenario5d71f0f7e68e27.33639233]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 09:39:24	[scenario5d71f0f7e68e27.33639233]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 09:39:24	[scenario5d71f0f7e68e27.33639233]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => fcfc63545bb5948d9b3e220dbc9252b3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 09:39:25	[scenario5d71f0f7e68e27.33639233]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 09:39:25	[scenario5d71f0f7e68e27.33639233]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 09:39:25	[scenario5d71f0f7e68e27.33639233]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 09:39:26	[scenario5d71f0f7e68e27.33639233]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 09:39:26	[scenario5d71f0f7e68e27.33639233]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fcfc63545bb5948d9b3e220dbc9252b3
Context transitions => [
    "step_thirteen"
]

2019-09-06 09:39:26	[scenario5d71f0f7e68e27.33639233]	Execute X-Cart step: step_thirteen
2019-09-06 09:39:27	[scenario5d71f0f7e68e27.33639233]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 11:10:58	[scenario5d72068272b056.18803844]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 11:11:00	[scenario5d72068272b056.18803844]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 11:11:00	[scenario5d72068272b056.18803844]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9325308bb57b058491fa3fc6edd2f874
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 11:11:00	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_first
2019-09-06 11:11:01	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_second
2019-09-06 11:11:07	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_third
2019-09-06 11:11:08	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_fourth
2019-09-06 11:11:11	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_fifth
2019-09-06 11:11:11	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_six
2019-09-06 11:11:12	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_seven
2019-09-06 11:11:13	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_nine
2019-09-06 11:11:14	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_ten
2019-09-06 11:11:14	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_eleven
2019-09-06 11:11:14	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_twelve
2019-09-06 11:11:21	[scenario5d72068272b056.18803844]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 11:11:22	[scenario5d72068272b056.18803844]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 11:11:22	[scenario5d72068272b056.18803844]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9325308bb57b058491fa3fc6edd2f874
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 11:11:22	[scenario5d72068272b056.18803844]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 11:11:22	[scenario5d72068272b056.18803844]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 11:11:23	[scenario5d72068272b056.18803844]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 11:11:23	[scenario5d72068272b056.18803844]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 11:11:23	[scenario5d72068272b056.18803844]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9325308bb57b058491fa3fc6edd2f874
Context transitions => [
    "step_thirteen"
]

2019-09-06 11:11:23	[scenario5d72068272b056.18803844]	Execute X-Cart step: step_thirteen
2019-09-06 11:11:25	[scenario5d72068272b056.18803844]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 11:13:56	[scenario5d72073442a552.28568262]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 11:13:59	[scenario5d72073442a552.28568262]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 11:13:59	[scenario5d72073442a552.28568262]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 86968dfa3e55627a02e30ee9c16615fd
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 11:13:59	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_first
2019-09-06 11:14:00	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_second
2019-09-06 11:14:06	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_third
2019-09-06 11:14:07	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_fourth
2019-09-06 11:14:10	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_fifth
2019-09-06 11:14:11	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_six
2019-09-06 11:14:12	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_seven
2019-09-06 11:14:14	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_nine
2019-09-06 11:14:15	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_ten
2019-09-06 11:14:16	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_eleven
2019-09-06 11:14:16	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_twelve
2019-09-06 11:14:25	[scenario5d72073442a552.28568262]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 11:14:26	[scenario5d72073442a552.28568262]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 11:14:26	[scenario5d72073442a552.28568262]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 86968dfa3e55627a02e30ee9c16615fd
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 11:14:26	[scenario5d72073442a552.28568262]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 11:14:27	[scenario5d72073442a552.28568262]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 11:14:27	[scenario5d72073442a552.28568262]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 11:14:28	[scenario5d72073442a552.28568262]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 11:14:28	[scenario5d72073442a552.28568262]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 86968dfa3e55627a02e30ee9c16615fd
Context transitions => [
    "step_thirteen"
]

2019-09-06 11:14:28	[scenario5d72073442a552.28568262]	Execute X-Cart step: step_thirteen
2019-09-06 11:14:30	[scenario5d72073442a552.28568262]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 11:18:45	[scenario5d720855b203d8.56190208]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 11:18:48	[scenario5d720855b203d8.56190208]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 11:18:48	[scenario5d720855b203d8.56190208]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d37b6a1dfef9dffc6a304633f50ff4bd
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 11:18:48	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_first
2019-09-06 11:18:49	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_second
2019-09-06 11:18:56	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_third
2019-09-06 11:18:57	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_fourth
2019-09-06 11:19:00	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_fifth
2019-09-06 11:19:00	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_six
2019-09-06 11:19:01	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_seven
2019-09-06 11:19:03	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_nine
2019-09-06 11:19:03	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_ten
2019-09-06 11:19:04	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_eleven
2019-09-06 11:19:04	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_twelve
2019-09-06 11:19:12	[scenario5d720855b203d8.56190208]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 11:19:13	[scenario5d720855b203d8.56190208]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 11:19:13	[scenario5d720855b203d8.56190208]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d37b6a1dfef9dffc6a304633f50ff4bd
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 11:19:13	[scenario5d720855b203d8.56190208]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 11:19:13	[scenario5d720855b203d8.56190208]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 11:19:14	[scenario5d720855b203d8.56190208]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 11:19:14	[scenario5d720855b203d8.56190208]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 11:19:14	[scenario5d720855b203d8.56190208]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d37b6a1dfef9dffc6a304633f50ff4bd
Context transitions => [
    "step_thirteen"
]

2019-09-06 11:19:14	[scenario5d720855b203d8.56190208]	Execute X-Cart step: step_thirteen
2019-09-06 11:19:16	[scenario5d720855b203d8.56190208]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 11:24:12	[scenario5d72099ccf8d21.30887952]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 11:24:14	[scenario5d72099ccf8d21.30887952]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 11:24:14	[scenario5d72099ccf8d21.30887952]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 37a02e109bf30f9081e519e8f57b6389
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 11:24:14	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_first
2019-09-06 11:24:15	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_second
2019-09-06 11:24:20	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_third
2019-09-06 11:24:20	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_fourth
2019-09-06 11:24:23	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_fifth
2019-09-06 11:24:23	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_six
2019-09-06 11:24:24	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_seven
2019-09-06 11:24:25	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_nine
2019-09-06 11:24:26	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_ten
2019-09-06 11:24:26	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_eleven
2019-09-06 11:24:26	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_twelve
2019-09-06 11:24:33	[scenario5d72099ccf8d21.30887952]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 11:24:33	[scenario5d72099ccf8d21.30887952]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 11:24:33	[scenario5d72099ccf8d21.30887952]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 37a02e109bf30f9081e519e8f57b6389
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 11:24:34	[scenario5d72099ccf8d21.30887952]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 11:24:35	[scenario5d72099ccf8d21.30887952]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 11:24:36	[scenario5d72099ccf8d21.30887952]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 11:24:37	[scenario5d72099ccf8d21.30887952]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 11:24:37	[scenario5d72099ccf8d21.30887952]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 37a02e109bf30f9081e519e8f57b6389
Context transitions => [
    "step_thirteen"
]

2019-09-06 11:24:38	[scenario5d72099ccf8d21.30887952]	Execute X-Cart step: step_thirteen
2019-09-06 11:24:40	[scenario5d72099ccf8d21.30887952]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 11:44:27	[scenario5d720e5b437e20.65664215]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 11:44:29	[scenario5d720e5b437e20.65664215]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 11:44:29	[scenario5d720e5b437e20.65664215]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 82807453b1927c17a143712a65e71bdc
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 11:44:30	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_first
2019-09-06 11:44:30	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_second
2019-09-06 11:44:36	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_third
2019-09-06 11:44:36	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_fourth
2019-09-06 11:44:39	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_fifth
2019-09-06 11:44:39	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_six
2019-09-06 11:44:40	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_seven
2019-09-06 11:44:41	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_nine
2019-09-06 11:44:42	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_ten
2019-09-06 11:44:42	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_eleven
2019-09-06 11:44:42	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_twelve
2019-09-06 11:44:48	[scenario5d720e5b437e20.65664215]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 11:44:49	[scenario5d720e5b437e20.65664215]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 11:44:49	[scenario5d720e5b437e20.65664215]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 82807453b1927c17a143712a65e71bdc
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 11:44:49	[scenario5d720e5b437e20.65664215]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 11:44:50	[scenario5d720e5b437e20.65664215]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 11:44:50	[scenario5d720e5b437e20.65664215]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 11:44:50	[scenario5d720e5b437e20.65664215]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 11:44:50	[scenario5d720e5b437e20.65664215]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 82807453b1927c17a143712a65e71bdc
Context transitions => [
    "step_thirteen"
]

2019-09-06 11:44:50	[scenario5d720e5b437e20.65664215]	Execute X-Cart step: step_thirteen
2019-09-06 11:44:53	[scenario5d720e5b437e20.65664215]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 11:45:16	[scenario5d720e8ca23126.99524013]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 11:45:18	[scenario5d720e8ca23126.99524013]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 11:45:18	[scenario5d720e8ca23126.99524013]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 11e6f8380b97036d0cbdce665bf1839b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 11:45:18	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_first
2019-09-06 11:45:18	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_second
2019-09-06 11:45:22	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_third
2019-09-06 11:45:22	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_fourth
2019-09-06 11:45:25	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_fifth
2019-09-06 11:45:25	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_six
2019-09-06 11:45:26	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_seven
2019-09-06 11:45:27	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_nine
2019-09-06 11:45:28	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_ten
2019-09-06 11:45:28	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_eleven
2019-09-06 11:45:28	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_twelve
2019-09-06 11:45:34	[scenario5d720e8ca23126.99524013]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 11:45:34	[scenario5d720e8ca23126.99524013]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 11:45:34	[scenario5d720e8ca23126.99524013]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 11e6f8380b97036d0cbdce665bf1839b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 11:45:34	[scenario5d720e8ca23126.99524013]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 11:45:35	[scenario5d720e8ca23126.99524013]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 11:45:35	[scenario5d720e8ca23126.99524013]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 11:45:35	[scenario5d720e8ca23126.99524013]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 11:45:36	[scenario5d720e8ca23126.99524013]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 11e6f8380b97036d0cbdce665bf1839b
Context transitions => [
    "step_thirteen"
]

2019-09-06 11:45:36	[scenario5d720e8ca23126.99524013]	Execute X-Cart step: step_thirteen
2019-09-06 11:45:38	[scenario5d720e8ca23126.99524013]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:18:34	[scenario5d72165a1965b8.99623964]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:18:36	[scenario5d72165a1965b8.99623964]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:18:36	[scenario5d72165a1965b8.99623964]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => efc191ff69e476967122be78629efd23
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:18:36	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_first
2019-09-06 12:18:36	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_second
2019-09-06 12:18:43	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_third
2019-09-06 12:18:43	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_fourth
2019-09-06 12:18:46	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_fifth
2019-09-06 12:18:47	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_six
2019-09-06 12:18:48	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_seven
2019-09-06 12:18:50	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_nine
2019-09-06 12:18:51	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_ten
2019-09-06 12:18:52	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_eleven
2019-09-06 12:18:53	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_twelve
2019-09-06 12:19:00	[scenario5d72165a1965b8.99623964]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:19:00	[scenario5d72165a1965b8.99623964]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:19:00	[scenario5d72165a1965b8.99623964]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => efc191ff69e476967122be78629efd23
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:19:01	[scenario5d72165a1965b8.99623964]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:19:01	[scenario5d72165a1965b8.99623964]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:19:01	[scenario5d72165a1965b8.99623964]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:19:02	[scenario5d72165a1965b8.99623964]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:19:02	[scenario5d72165a1965b8.99623964]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => efc191ff69e476967122be78629efd23
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:19:02	[scenario5d72165a1965b8.99623964]	Execute X-Cart step: step_thirteen
2019-09-06 12:19:04	[scenario5d72165a1965b8.99623964]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:22:22	[scenario5d72173e898301.61731779]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:22:25	[scenario5d72173e898301.61731779]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:22:25	[scenario5d72173e898301.61731779]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5caf23606f55aa48b427559b38203de9
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:22:26	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_first
2019-09-06 12:22:27	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_second
2019-09-06 12:22:33	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_third
2019-09-06 12:22:34	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_fourth
2019-09-06 12:22:38	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_fifth
2019-09-06 12:22:38	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_six
2019-09-06 12:22:40	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_seven
2019-09-06 12:22:42	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_nine
2019-09-06 12:22:42	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_ten
2019-09-06 12:22:43	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_eleven
2019-09-06 12:22:43	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_twelve
2019-09-06 12:22:52	[scenario5d72173e898301.61731779]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:22:52	[scenario5d72173e898301.61731779]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:22:52	[scenario5d72173e898301.61731779]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5caf23606f55aa48b427559b38203de9
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:22:53	[scenario5d72173e898301.61731779]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:22:53	[scenario5d72173e898301.61731779]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:22:53	[scenario5d72173e898301.61731779]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:22:54	[scenario5d72173e898301.61731779]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:22:54	[scenario5d72173e898301.61731779]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5caf23606f55aa48b427559b38203de9
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:22:54	[scenario5d72173e898301.61731779]	Execute X-Cart step: step_thirteen
2019-09-06 12:22:56	[scenario5d72173e898301.61731779]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:24:59	[scenario5d7217db3e19d6.92225273]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:25:01	[scenario5d7217db3e19d6.92225273]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:25:01	[scenario5d7217db3e19d6.92225273]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fac478fc9df826f07afe82c5c331ce9c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:25:01	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_first
2019-09-06 12:25:02	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_second
2019-09-06 12:25:07	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_third
2019-09-06 12:25:08	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_fourth
2019-09-06 12:25:11	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_fifth
2019-09-06 12:25:11	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_six
2019-09-06 12:25:13	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_seven
2019-09-06 12:25:14	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_nine
2019-09-06 12:25:15	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_ten
2019-09-06 12:25:15	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_eleven
2019-09-06 12:25:16	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_twelve
2019-09-06 12:25:23	[scenario5d7217db3e19d6.92225273]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:25:23	[scenario5d7217db3e19d6.92225273]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:25:23	[scenario5d7217db3e19d6.92225273]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => fac478fc9df826f07afe82c5c331ce9c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:25:24	[scenario5d7217db3e19d6.92225273]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:25:24	[scenario5d7217db3e19d6.92225273]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:25:24	[scenario5d7217db3e19d6.92225273]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:25:25	[scenario5d7217db3e19d6.92225273]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:25:25	[scenario5d7217db3e19d6.92225273]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fac478fc9df826f07afe82c5c331ce9c
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:25:25	[scenario5d7217db3e19d6.92225273]	Execute X-Cart step: step_thirteen
2019-09-06 12:25:27	[scenario5d7217db3e19d6.92225273]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:27:49	[scenario5d72188537b5a8.16927364]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:27:51	[scenario5d72188537b5a8.16927364]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:27:51	[scenario5d72188537b5a8.16927364]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ce8d24f7b2b732fb21cc88fecafc6174
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:27:51	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_first
2019-09-06 12:27:52	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_second
2019-09-06 12:27:57	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_third
2019-09-06 12:27:57	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_fourth
2019-09-06 12:28:00	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_fifth
2019-09-06 12:28:01	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_six
2019-09-06 12:28:01	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_seven
2019-09-06 12:28:03	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_nine
2019-09-06 12:28:04	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_ten
2019-09-06 12:28:04	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_eleven
2019-09-06 12:28:04	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_twelve
2019-09-06 12:28:11	[scenario5d72188537b5a8.16927364]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:28:11	[scenario5d72188537b5a8.16927364]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:28:11	[scenario5d72188537b5a8.16927364]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => ce8d24f7b2b732fb21cc88fecafc6174
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:28:11	[scenario5d72188537b5a8.16927364]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:28:12	[scenario5d72188537b5a8.16927364]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:28:12	[scenario5d72188537b5a8.16927364]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:28:13	[scenario5d72188537b5a8.16927364]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:28:13	[scenario5d72188537b5a8.16927364]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => ce8d24f7b2b732fb21cc88fecafc6174
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:28:13	[scenario5d72188537b5a8.16927364]	Execute X-Cart step: step_thirteen
2019-09-06 12:28:15	[scenario5d72188537b5a8.16927364]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:29:08	[scenario5d7218d4432f99.67626673]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:29:10	[scenario5d7218d4432f99.67626673]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:29:10	[scenario5d7218d4432f99.67626673]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 55c7c5f656de0403ae453a0a588601a0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:29:11	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_first
2019-09-06 12:29:12	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_second
2019-09-06 12:29:18	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_third
2019-09-06 12:29:18	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_fourth
2019-09-06 12:29:22	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_fifth
2019-09-06 12:29:22	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_six
2019-09-06 12:29:24	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_seven
2019-09-06 12:29:26	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_nine
2019-09-06 12:29:27	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_ten
2019-09-06 12:29:27	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_eleven
2019-09-06 12:29:27	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_twelve
2019-09-06 12:29:35	[scenario5d7218d4432f99.67626673]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:29:36	[scenario5d7218d4432f99.67626673]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:29:36	[scenario5d7218d4432f99.67626673]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 55c7c5f656de0403ae453a0a588601a0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:29:36	[scenario5d7218d4432f99.67626673]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:29:37	[scenario5d7218d4432f99.67626673]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:29:38	[scenario5d7218d4432f99.67626673]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:29:38	[scenario5d7218d4432f99.67626673]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:29:38	[scenario5d7218d4432f99.67626673]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 55c7c5f656de0403ae453a0a588601a0
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:29:38	[scenario5d7218d4432f99.67626673]	Execute X-Cart step: step_thirteen
2019-09-06 12:29:41	[scenario5d7218d4432f99.67626673]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:34:00	[scenario5d7219f8d8c7d1.68179814]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:34:02	[scenario5d7219f8d8c7d1.68179814]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:34:02	[scenario5d7219f8d8c7d1.68179814]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d1f9185728a072d90c69076c9ee37e14
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:34:03	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_first
2019-09-06 12:34:03	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_second
2019-09-06 12:34:09	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_third
2019-09-06 12:34:10	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_fourth
2019-09-06 12:34:13	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_fifth
2019-09-06 12:34:13	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_six
2019-09-06 12:34:15	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_seven
2019-09-06 12:34:16	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_nine
2019-09-06 12:34:17	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_ten
2019-09-06 12:34:17	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_eleven
2019-09-06 12:34:17	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_twelve
2019-09-06 12:34:24	[scenario5d7219f8d8c7d1.68179814]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:34:24	[scenario5d7219f8d8c7d1.68179814]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:34:24	[scenario5d7219f8d8c7d1.68179814]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => d1f9185728a072d90c69076c9ee37e14
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:34:25	[scenario5d7219f8d8c7d1.68179814]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:34:25	[scenario5d7219f8d8c7d1.68179814]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:34:25	[scenario5d7219f8d8c7d1.68179814]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:34:26	[scenario5d7219f8d8c7d1.68179814]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:34:26	[scenario5d7219f8d8c7d1.68179814]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => d1f9185728a072d90c69076c9ee37e14
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:34:26	[scenario5d7219f8d8c7d1.68179814]	Execute X-Cart step: step_thirteen
2019-09-06 12:34:28	[scenario5d7219f8d8c7d1.68179814]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:42:33	[scenario5d721bf9e0e796.17138807]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:42:36	[scenario5d721bf9e0e796.17138807]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:42:36	[scenario5d721bf9e0e796.17138807]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4b09de5396147931d40e58e71a47133c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:42:36	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_first
2019-09-06 12:42:37	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_second
2019-09-06 12:42:42	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_third
2019-09-06 12:42:43	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_fourth
2019-09-06 12:42:46	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_fifth
2019-09-06 12:42:46	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_six
2019-09-06 12:42:47	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_seven
2019-09-06 12:42:49	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_nine
2019-09-06 12:42:49	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_ten
2019-09-06 12:42:50	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_eleven
2019-09-06 12:42:50	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_twelve
2019-09-06 12:42:57	[scenario5d721bf9e0e796.17138807]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:42:57	[scenario5d721bf9e0e796.17138807]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:42:57	[scenario5d721bf9e0e796.17138807]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4b09de5396147931d40e58e71a47133c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:42:58	[scenario5d721bf9e0e796.17138807]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:42:58	[scenario5d721bf9e0e796.17138807]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:42:59	[scenario5d721bf9e0e796.17138807]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:42:59	[scenario5d721bf9e0e796.17138807]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:42:59	[scenario5d721bf9e0e796.17138807]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4b09de5396147931d40e58e71a47133c
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:42:59	[scenario5d721bf9e0e796.17138807]	Execute X-Cart step: step_thirteen
2019-09-06 12:43:01	[scenario5d721bf9e0e796.17138807]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:43:53	[scenario5d721c49d6bec8.48782837]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:43:56	[scenario5d721c49d6bec8.48782837]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:43:57	[scenario5d721c49d6bec8.48782837]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8dfe7860b969909812f08829fba58a4b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:43:57	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_first
2019-09-06 12:43:58	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_second
2019-09-06 12:44:04	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_third
2019-09-06 12:44:05	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_fourth
2019-09-06 12:44:09	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_fifth
2019-09-06 12:44:09	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_six
2019-09-06 12:44:10	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_seven
2019-09-06 12:44:12	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_nine
2019-09-06 12:44:13	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_ten
2019-09-06 12:44:14	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_eleven
2019-09-06 12:44:14	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_twelve
2019-09-06 12:44:23	[scenario5d721c49d6bec8.48782837]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:44:24	[scenario5d721c49d6bec8.48782837]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:44:24	[scenario5d721c49d6bec8.48782837]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8dfe7860b969909812f08829fba58a4b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:44:24	[scenario5d721c49d6bec8.48782837]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:44:25	[scenario5d721c49d6bec8.48782837]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:44:26	[scenario5d721c49d6bec8.48782837]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:44:26	[scenario5d721c49d6bec8.48782837]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:44:27	[scenario5d721c49d6bec8.48782837]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8dfe7860b969909812f08829fba58a4b
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:44:27	[scenario5d721c49d6bec8.48782837]	Execute X-Cart step: step_thirteen
2019-09-06 12:44:30	[scenario5d721c49d6bec8.48782837]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:46:44	[scenario5d721cf438f5a7.74688695]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:46:46	[scenario5d721cf438f5a7.74688695]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:46:46	[scenario5d721cf438f5a7.74688695]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5173fbdf0de7df26f7cbf3ba8a643f77
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:46:46	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_first
2019-09-06 12:46:47	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_second
2019-09-06 12:46:53	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_third
2019-09-06 12:46:54	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_fourth
2019-09-06 12:46:57	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_fifth
2019-09-06 12:46:57	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_six
2019-09-06 12:46:58	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_seven
2019-09-06 12:46:59	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_nine
2019-09-06 12:47:00	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_ten
2019-09-06 12:47:00	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_eleven
2019-09-06 12:47:01	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_twelve
2019-09-06 12:47:09	[scenario5d721cf438f5a7.74688695]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:47:09	[scenario5d721cf438f5a7.74688695]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:47:09	[scenario5d721cf438f5a7.74688695]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 5173fbdf0de7df26f7cbf3ba8a643f77
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:47:10	[scenario5d721cf438f5a7.74688695]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:47:10	[scenario5d721cf438f5a7.74688695]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:47:10	[scenario5d721cf438f5a7.74688695]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:47:11	[scenario5d721cf438f5a7.74688695]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:47:11	[scenario5d721cf438f5a7.74688695]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 5173fbdf0de7df26f7cbf3ba8a643f77
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:47:11	[scenario5d721cf438f5a7.74688695]	Execute X-Cart step: step_thirteen
2019-09-06 12:47:13	[scenario5d721cf438f5a7.74688695]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 12:49:11	[scenario5d721d876327d6.74389466]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 12:49:13	[scenario5d721d876327d6.74389466]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 12:49:13	[scenario5d721d876327d6.74389466]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b2652c8bdf02a5694c03f3e8bdb6950c
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 12:49:14	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_first
2019-09-06 12:49:16	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_second
2019-09-06 12:49:23	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_third
2019-09-06 12:49:24	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_fourth
2019-09-06 12:49:27	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_fifth
2019-09-06 12:49:28	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_six
2019-09-06 12:49:29	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_seven
2019-09-06 12:49:31	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_nine
2019-09-06 12:49:32	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_ten
2019-09-06 12:49:33	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_eleven
2019-09-06 12:49:34	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_twelve
2019-09-06 12:49:40	[scenario5d721d876327d6.74389466]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 12:49:41	[scenario5d721d876327d6.74389466]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 12:49:41	[scenario5d721d876327d6.74389466]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => b2652c8bdf02a5694c03f3e8bdb6950c
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 12:49:42	[scenario5d721d876327d6.74389466]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 12:49:43	[scenario5d721d876327d6.74389466]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 12:49:44	[scenario5d721d876327d6.74389466]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 12:49:45	[scenario5d721d876327d6.74389466]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 12:49:45	[scenario5d721d876327d6.74389466]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => b2652c8bdf02a5694c03f3e8bdb6950c
Context transitions => [
    "step_thirteen"
]

2019-09-06 12:49:46	[scenario5d721d876327d6.74389466]	Execute X-Cart step: step_thirteen
2019-09-06 12:49:49	[scenario5d721d876327d6.74389466]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:03:58	[scenario5d723d1e5dd965.75759648]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:04:00	[scenario5d723d1e5dd965.75759648]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:04:00	[scenario5d723d1e5dd965.75759648]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0512b56cd0210ddf3a60e1b57bce3c42
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:04:00	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_first
2019-09-06 15:04:01	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_second
2019-09-06 15:04:07	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_third
2019-09-06 15:04:07	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_fourth
2019-09-06 15:04:10	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_fifth
2019-09-06 15:04:10	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_six
2019-09-06 15:04:12	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_seven
2019-09-06 15:04:14	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_nine
2019-09-06 15:04:14	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_ten
2019-09-06 15:04:15	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_eleven
2019-09-06 15:04:15	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_twelve
2019-09-06 15:04:22	[scenario5d723d1e5dd965.75759648]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:04:23	[scenario5d723d1e5dd965.75759648]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:04:23	[scenario5d723d1e5dd965.75759648]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 0512b56cd0210ddf3a60e1b57bce3c42
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:04:23	[scenario5d723d1e5dd965.75759648]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:04:24	[scenario5d723d1e5dd965.75759648]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:04:25	[scenario5d723d1e5dd965.75759648]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:04:26	[scenario5d723d1e5dd965.75759648]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:04:26	[scenario5d723d1e5dd965.75759648]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 0512b56cd0210ddf3a60e1b57bce3c42
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:04:27	[scenario5d723d1e5dd965.75759648]	Execute X-Cart step: step_thirteen
2019-09-06 15:04:29	[scenario5d723d1e5dd965.75759648]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:13:38	[scenario5d723f626cb8c1.19880172]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:13:40	[scenario5d723f626cb8c1.19880172]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:13:40	[scenario5d723f626cb8c1.19880172]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e20fdac0eba65cb55ff609497aac44e5
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:13:41	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_first
2019-09-06 15:13:41	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_second
2019-09-06 15:13:48	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_third
2019-09-06 15:13:49	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_fourth
2019-09-06 15:13:52	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_fifth
2019-09-06 15:13:52	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_six
2019-09-06 15:13:53	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_seven
2019-09-06 15:13:55	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_nine
2019-09-06 15:13:56	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_ten
2019-09-06 15:13:57	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_eleven
2019-09-06 15:13:57	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_twelve
2019-09-06 15:14:04	[scenario5d723f626cb8c1.19880172]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:14:05	[scenario5d723f626cb8c1.19880172]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:14:05	[scenario5d723f626cb8c1.19880172]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e20fdac0eba65cb55ff609497aac44e5
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:14:05	[scenario5d723f626cb8c1.19880172]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:14:06	[scenario5d723f626cb8c1.19880172]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:14:06	[scenario5d723f626cb8c1.19880172]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:14:07	[scenario5d723f626cb8c1.19880172]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:14:07	[scenario5d723f626cb8c1.19880172]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e20fdac0eba65cb55ff609497aac44e5
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:14:07	[scenario5d723f626cb8c1.19880172]	Execute X-Cart step: step_thirteen
2019-09-06 15:14:09	[scenario5d723f626cb8c1.19880172]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:15:19	[scenario5d723fc75d38e2.38623396]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:15:22	[scenario5d723fc75d38e2.38623396]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:15:22	[scenario5d723fc75d38e2.38623396]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f8961938c80e8bf0daeaa010c26b8875
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:15:22	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_first
2019-09-06 15:15:23	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_second
2019-09-06 15:15:30	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_third
2019-09-06 15:15:31	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_fourth
2019-09-06 15:15:35	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_fifth
2019-09-06 15:15:35	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_six
2019-09-06 15:15:36	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_seven
2019-09-06 15:15:38	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_nine
2019-09-06 15:15:39	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_ten
2019-09-06 15:15:40	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_eleven
2019-09-06 15:15:40	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_twelve
2019-09-06 15:15:49	[scenario5d723fc75d38e2.38623396]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:15:50	[scenario5d723fc75d38e2.38623396]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:15:50	[scenario5d723fc75d38e2.38623396]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => f8961938c80e8bf0daeaa010c26b8875
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:15:50	[scenario5d723fc75d38e2.38623396]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:15:51	[scenario5d723fc75d38e2.38623396]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:15:52	[scenario5d723fc75d38e2.38623396]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:15:52	[scenario5d723fc75d38e2.38623396]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:15:52	[scenario5d723fc75d38e2.38623396]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => f8961938c80e8bf0daeaa010c26b8875
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:15:53	[scenario5d723fc75d38e2.38623396]	Execute X-Cart step: step_thirteen
2019-09-06 15:15:55	[scenario5d723fc75d38e2.38623396]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:24:06	[scenario5d7241d67d5727.94625679]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:24:08	[scenario5d7241d67d5727.94625679]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:24:08	[scenario5d7241d67d5727.94625679]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2f8c2884492941c83bdb15f5ef36d915
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:24:08	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_first
2019-09-06 15:24:09	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_second
2019-09-06 15:24:13	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_third
2019-09-06 15:24:14	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_fourth
2019-09-06 15:24:17	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_fifth
2019-09-06 15:24:17	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_six
2019-09-06 15:24:19	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_seven
2019-09-06 15:24:21	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_nine
2019-09-06 15:24:22	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_ten
2019-09-06 15:24:22	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_eleven
2019-09-06 15:24:23	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_twelve
2019-09-06 15:24:32	[scenario5d7241d67d5727.94625679]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:24:33	[scenario5d7241d67d5727.94625679]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:24:33	[scenario5d7241d67d5727.94625679]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2f8c2884492941c83bdb15f5ef36d915
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:24:33	[scenario5d7241d67d5727.94625679]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:24:34	[scenario5d7241d67d5727.94625679]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:24:35	[scenario5d7241d67d5727.94625679]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:24:36	[scenario5d7241d67d5727.94625679]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:24:36	[scenario5d7241d67d5727.94625679]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2f8c2884492941c83bdb15f5ef36d915
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:24:37	[scenario5d7241d67d5727.94625679]	Execute X-Cart step: step_thirteen
2019-09-06 15:24:40	[scenario5d7241d67d5727.94625679]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:27:26	[scenario5d72429e41a832.04320829]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:27:29	[scenario5d72429e41a832.04320829]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:27:29	[scenario5d72429e41a832.04320829]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 66058e7b8718a36ec498b055df43010e
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:27:29	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_first
2019-09-06 15:27:30	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_second
2019-09-06 15:27:38	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_third
2019-09-06 15:27:38	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_fourth
2019-09-06 15:27:42	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_fifth
2019-09-06 15:27:42	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_six
2019-09-06 15:27:43	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_seven
2019-09-06 15:27:45	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_nine
2019-09-06 15:27:45	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_ten
2019-09-06 15:27:46	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_eleven
2019-09-06 15:27:46	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_twelve
2019-09-06 15:27:53	[scenario5d72429e41a832.04320829]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:27:54	[scenario5d72429e41a832.04320829]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:27:54	[scenario5d72429e41a832.04320829]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 66058e7b8718a36ec498b055df43010e
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:27:54	[scenario5d72429e41a832.04320829]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:27:55	[scenario5d72429e41a832.04320829]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:27:55	[scenario5d72429e41a832.04320829]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:27:55	[scenario5d72429e41a832.04320829]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:27:55	[scenario5d72429e41a832.04320829]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 66058e7b8718a36ec498b055df43010e
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:27:56	[scenario5d72429e41a832.04320829]	Execute X-Cart step: step_thirteen
2019-09-06 15:27:58	[scenario5d72429e41a832.04320829]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:35:49	[scenario5d724495dc4d71.98589570]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:35:52	[scenario5d724495dc4d71.98589570]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:35:52	[scenario5d724495dc4d71.98589570]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e7709c835bbe462f552663b356cb7ef0
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:35:52	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_first
2019-09-06 15:35:53	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_second
2019-09-06 15:36:00	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_third
2019-09-06 15:36:01	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_fourth
2019-09-06 15:36:04	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_fifth
2019-09-06 15:36:04	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_six
2019-09-06 15:36:05	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_seven
2019-09-06 15:36:08	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_nine
2019-09-06 15:36:10	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_ten
2019-09-06 15:36:11	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_eleven
2019-09-06 15:36:12	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_twelve
2019-09-06 15:36:21	[scenario5d724495dc4d71.98589570]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:36:23	[scenario5d724495dc4d71.98589570]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:36:23	[scenario5d724495dc4d71.98589570]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e7709c835bbe462f552663b356cb7ef0
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:36:24	[scenario5d724495dc4d71.98589570]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:36:25	[scenario5d724495dc4d71.98589570]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:36:26	[scenario5d724495dc4d71.98589570]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:36:27	[scenario5d724495dc4d71.98589570]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:36:27	[scenario5d724495dc4d71.98589570]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e7709c835bbe462f552663b356cb7ef0
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:36:28	[scenario5d724495dc4d71.98589570]	Execute X-Cart step: step_thirteen
2019-09-06 15:36:30	[scenario5d724495dc4d71.98589570]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:38:49	[scenario5d7245493fb1b7.54676497]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:38:51	[scenario5d7245493fb1b7.54676497]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:38:51	[scenario5d7245493fb1b7.54676497]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2ae24ae2836c102257bc94cb506e19e3
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:38:52	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_first
2019-09-06 15:38:53	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_second
2019-09-06 15:39:00	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_third
2019-09-06 15:39:01	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_fourth
2019-09-06 15:39:05	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_fifth
2019-09-06 15:39:06	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_six
2019-09-06 15:39:07	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_seven
2019-09-06 15:39:09	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_nine
2019-09-06 15:39:10	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_ten
2019-09-06 15:39:11	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_eleven
2019-09-06 15:39:11	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_twelve
2019-09-06 15:39:20	[scenario5d7245493fb1b7.54676497]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:39:21	[scenario5d7245493fb1b7.54676497]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:39:21	[scenario5d7245493fb1b7.54676497]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2ae24ae2836c102257bc94cb506e19e3
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:39:21	[scenario5d7245493fb1b7.54676497]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:39:22	[scenario5d7245493fb1b7.54676497]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:39:22	[scenario5d7245493fb1b7.54676497]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:39:23	[scenario5d7245493fb1b7.54676497]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:39:23	[scenario5d7245493fb1b7.54676497]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2ae24ae2836c102257bc94cb506e19e3
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:39:23	[scenario5d7245493fb1b7.54676497]	Execute X-Cart step: step_thirteen
2019-09-06 15:39:25	[scenario5d7245493fb1b7.54676497]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:42:12	[scenario5d7246140897d8.50249413]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:42:15	[scenario5d7246140897d8.50249413]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:42:15	[scenario5d7246140897d8.50249413]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1312409b10fc970aeaee944cdfd8797b
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:42:15	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_first
2019-09-06 15:42:16	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_second
2019-09-06 15:42:23	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_third
2019-09-06 15:42:24	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_fourth
2019-09-06 15:42:28	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_fifth
2019-09-06 15:42:28	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_six
2019-09-06 15:42:30	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_seven
2019-09-06 15:42:32	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_nine
2019-09-06 15:42:33	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_ten
2019-09-06 15:42:33	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_eleven
2019-09-06 15:42:34	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_twelve
2019-09-06 15:42:42	[scenario5d7246140897d8.50249413]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:42:43	[scenario5d7246140897d8.50249413]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:42:43	[scenario5d7246140897d8.50249413]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1312409b10fc970aeaee944cdfd8797b
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:42:43	[scenario5d7246140897d8.50249413]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:42:44	[scenario5d7246140897d8.50249413]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:42:44	[scenario5d7246140897d8.50249413]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:42:45	[scenario5d7246140897d8.50249413]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:42:45	[scenario5d7246140897d8.50249413]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1312409b10fc970aeaee944cdfd8797b
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:42:45	[scenario5d7246140897d8.50249413]	Execute X-Cart step: step_thirteen
2019-09-06 15:42:48	[scenario5d7246140897d8.50249413]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:45:59	[scenario5d7246f70cd416.67630849]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:46:03	[scenario5d7246f70cd416.67630849]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:46:03	[scenario5d7246f70cd416.67630849]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2b58fdfd8f5bd61cc1fbb9d119d6e6ba
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:46:04	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_first
2019-09-06 15:46:05	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_second
2019-09-06 15:46:11	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_third
2019-09-06 15:46:12	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_fourth
2019-09-06 15:46:15	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_fifth
2019-09-06 15:46:16	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_six
2019-09-06 15:46:17	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_seven
2019-09-06 15:46:19	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_nine
2019-09-06 15:46:20	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_ten
2019-09-06 15:46:21	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_eleven
2019-09-06 15:46:22	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_twelve
2019-09-06 15:46:28	[scenario5d7246f70cd416.67630849]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:46:30	[scenario5d7246f70cd416.67630849]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:46:30	[scenario5d7246f70cd416.67630849]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 2b58fdfd8f5bd61cc1fbb9d119d6e6ba
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:46:30	[scenario5d7246f70cd416.67630849]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:46:31	[scenario5d7246f70cd416.67630849]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:46:32	[scenario5d7246f70cd416.67630849]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:46:33	[scenario5d7246f70cd416.67630849]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:46:33	[scenario5d7246f70cd416.67630849]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 2b58fdfd8f5bd61cc1fbb9d119d6e6ba
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:46:34	[scenario5d7246f70cd416.67630849]	Execute X-Cart step: step_thirteen
2019-09-06 15:46:36	[scenario5d7246f70cd416.67630849]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 15:54:20	[scenario5d7248ecdb5ae8.80613054]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 15:54:22	[scenario5d7248ecdb5ae8.80613054]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 15:54:22	[scenario5d7248ecdb5ae8.80613054]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 613f3ef6f63a5922d04e27549712d9be
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 15:54:23	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_first
2019-09-06 15:54:23	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_second
2019-09-06 15:54:28	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_third
2019-09-06 15:54:29	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_fourth
2019-09-06 15:54:31	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_fifth
2019-09-06 15:54:32	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_six
2019-09-06 15:54:33	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_seven
2019-09-06 15:54:34	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_nine
2019-09-06 15:54:35	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_ten
2019-09-06 15:54:35	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_eleven
2019-09-06 15:54:35	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_twelve
2019-09-06 15:54:43	[scenario5d7248ecdb5ae8.80613054]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 15:54:43	[scenario5d7248ecdb5ae8.80613054]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 15:54:43	[scenario5d7248ecdb5ae8.80613054]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 613f3ef6f63a5922d04e27549712d9be
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 15:54:44	[scenario5d7248ecdb5ae8.80613054]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 15:54:44	[scenario5d7248ecdb5ae8.80613054]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 15:54:44	[scenario5d7248ecdb5ae8.80613054]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 15:54:45	[scenario5d7248ecdb5ae8.80613054]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 15:54:45	[scenario5d7248ecdb5ae8.80613054]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 613f3ef6f63a5922d04e27549712d9be
Context transitions => [
    "step_thirteen"
]

2019-09-06 15:54:45	[scenario5d7248ecdb5ae8.80613054]	Execute X-Cart step: step_thirteen
2019-09-06 15:54:47	[scenario5d7248ecdb5ae8.80613054]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:03:18	[scenario5d724b06506c02.05657153]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:03:20	[scenario5d724b06506c02.05657153]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:03:20	[scenario5d724b06506c02.05657153]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 7cc8dcbc7f7c653d5cd64103f6613cd7
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:03:21	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_first
2019-09-06 16:03:21	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_second
2019-09-06 16:03:28	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_third
2019-09-06 16:03:29	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_fourth
2019-09-06 16:03:33	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_fifth
2019-09-06 16:03:33	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_six
2019-09-06 16:03:34	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_seven
2019-09-06 16:03:36	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_nine
2019-09-06 16:03:36	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_ten
2019-09-06 16:03:37	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_eleven
2019-09-06 16:03:37	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_twelve
2019-09-06 16:03:44	[scenario5d724b06506c02.05657153]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:03:45	[scenario5d724b06506c02.05657153]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:03:45	[scenario5d724b06506c02.05657153]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 7cc8dcbc7f7c653d5cd64103f6613cd7
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:03:45	[scenario5d724b06506c02.05657153]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:03:45	[scenario5d724b06506c02.05657153]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:03:46	[scenario5d724b06506c02.05657153]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:03:46	[scenario5d724b06506c02.05657153]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:03:46	[scenario5d724b06506c02.05657153]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 7cc8dcbc7f7c653d5cd64103f6613cd7
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:03:46	[scenario5d724b06506c02.05657153]	Execute X-Cart step: step_thirteen
2019-09-06 16:03:49	[scenario5d724b06506c02.05657153]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:06:40	[scenario5d724bd068b859.81698042]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:06:42	[scenario5d724bd068b859.81698042]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:06:42	[scenario5d724bd068b859.81698042]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6a16655c3f43711be77b51fe58580121
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:06:42	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_first
2019-09-06 16:06:43	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_second
2019-09-06 16:06:49	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_third
2019-09-06 16:06:50	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_fourth
2019-09-06 16:06:53	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_fifth
2019-09-06 16:06:53	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_six
2019-09-06 16:06:54	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_seven
2019-09-06 16:06:55	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_nine
2019-09-06 16:06:56	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_ten
2019-09-06 16:06:56	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_eleven
2019-09-06 16:06:57	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_twelve
2019-09-06 16:07:03	[scenario5d724bd068b859.81698042]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:07:04	[scenario5d724bd068b859.81698042]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:07:04	[scenario5d724bd068b859.81698042]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 6a16655c3f43711be77b51fe58580121
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:07:04	[scenario5d724bd068b859.81698042]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:07:05	[scenario5d724bd068b859.81698042]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:07:05	[scenario5d724bd068b859.81698042]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:07:05	[scenario5d724bd068b859.81698042]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:07:05	[scenario5d724bd068b859.81698042]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 6a16655c3f43711be77b51fe58580121
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:07:06	[scenario5d724bd068b859.81698042]	Execute X-Cart step: step_thirteen
2019-09-06 16:07:08	[scenario5d724bd068b859.81698042]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:10:23	[scenario5d724cafe689f1.61525740]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:10:26	[scenario5d724cafe689f1.61525740]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:10:26	[scenario5d724cafe689f1.61525740]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e655f477099e8c73e3dad840ffaaade9
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:10:26	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_first
2019-09-06 16:10:27	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_second
2019-09-06 16:10:33	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_third
2019-09-06 16:10:34	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_fourth
2019-09-06 16:10:37	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_fifth
2019-09-06 16:10:38	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_six
2019-09-06 16:10:39	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_seven
2019-09-06 16:10:40	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_nine
2019-09-06 16:10:41	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_ten
2019-09-06 16:10:42	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_eleven
2019-09-06 16:10:42	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_twelve
2019-09-06 16:10:48	[scenario5d724cafe689f1.61525740]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:10:49	[scenario5d724cafe689f1.61525740]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:10:49	[scenario5d724cafe689f1.61525740]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e655f477099e8c73e3dad840ffaaade9
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:10:49	[scenario5d724cafe689f1.61525740]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:10:50	[scenario5d724cafe689f1.61525740]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:10:50	[scenario5d724cafe689f1.61525740]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:10:50	[scenario5d724cafe689f1.61525740]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:10:50	[scenario5d724cafe689f1.61525740]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e655f477099e8c73e3dad840ffaaade9
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:10:50	[scenario5d724cafe689f1.61525740]	Execute X-Cart step: step_thirteen
2019-09-06 16:10:55	[scenario5d724cafe689f1.61525740]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:14:23	[scenario5d724d9f01cd30.02982038]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:14:24	[scenario5d724d9f01cd30.02982038]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:14:24	[scenario5d724d9f01cd30.02982038]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4bbc70e24e9d38296eaeb850caa49ab1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:14:25	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_first
2019-09-06 16:14:25	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_second
2019-09-06 16:14:31	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_third
2019-09-06 16:14:31	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_fourth
2019-09-06 16:14:34	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_fifth
2019-09-06 16:14:34	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_six
2019-09-06 16:14:35	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_seven
2019-09-06 16:14:36	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_nine
2019-09-06 16:14:37	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_ten
2019-09-06 16:14:37	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_eleven
2019-09-06 16:14:38	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_twelve
2019-09-06 16:14:44	[scenario5d724d9f01cd30.02982038]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:14:45	[scenario5d724d9f01cd30.02982038]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:14:45	[scenario5d724d9f01cd30.02982038]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4bbc70e24e9d38296eaeb850caa49ab1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:14:45	[scenario5d724d9f01cd30.02982038]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:14:46	[scenario5d724d9f01cd30.02982038]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:14:46	[scenario5d724d9f01cd30.02982038]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:14:46	[scenario5d724d9f01cd30.02982038]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:14:46	[scenario5d724d9f01cd30.02982038]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4bbc70e24e9d38296eaeb850caa49ab1
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:14:46	[scenario5d724d9f01cd30.02982038]	Execute X-Cart step: step_thirteen
2019-09-06 16:14:48	[scenario5d724d9f01cd30.02982038]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:16:11	[scenario5d724e0b346a41.20200009]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:16:13	[scenario5d724e0b346a41.20200009]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:16:13	[scenario5d724e0b346a41.20200009]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9b73e6d148f9f985e816b30ccb5aa9a1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:16:14	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_first
2019-09-06 16:16:15	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_second
2019-09-06 16:16:21	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_third
2019-09-06 16:16:21	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_fourth
2019-09-06 16:16:25	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_fifth
2019-09-06 16:16:26	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_six
2019-09-06 16:16:27	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_seven
2019-09-06 16:16:29	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_nine
2019-09-06 16:16:30	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_ten
2019-09-06 16:16:31	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_eleven
2019-09-06 16:16:31	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_twelve
2019-09-06 16:16:39	[scenario5d724e0b346a41.20200009]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:16:39	[scenario5d724e0b346a41.20200009]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:16:39	[scenario5d724e0b346a41.20200009]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 9b73e6d148f9f985e816b30ccb5aa9a1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:16:40	[scenario5d724e0b346a41.20200009]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:16:40	[scenario5d724e0b346a41.20200009]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:16:41	[scenario5d724e0b346a41.20200009]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:16:41	[scenario5d724e0b346a41.20200009]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:16:41	[scenario5d724e0b346a41.20200009]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 9b73e6d148f9f985e816b30ccb5aa9a1
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:16:41	[scenario5d724e0b346a41.20200009]	Execute X-Cart step: step_thirteen
2019-09-06 16:16:43	[scenario5d724e0b346a41.20200009]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:18:55	[scenario5d724eaf250d19.02428825]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:18:56	[scenario5d724eaf250d19.02428825]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:18:57	[scenario5d724eaf250d19.02428825]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8adb5d9ab0decf674a80b6b8ce68e366
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:18:57	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_first
2019-09-06 16:18:57	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_second
2019-09-06 16:19:03	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_third
2019-09-06 16:19:03	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_fourth
2019-09-06 16:19:06	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_fifth
2019-09-06 16:19:06	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_six
2019-09-06 16:19:07	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_seven
2019-09-06 16:19:09	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_nine
2019-09-06 16:19:10	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_ten
2019-09-06 16:19:10	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_eleven
2019-09-06 16:19:10	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_twelve
2019-09-06 16:19:16	[scenario5d724eaf250d19.02428825]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:19:17	[scenario5d724eaf250d19.02428825]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:19:17	[scenario5d724eaf250d19.02428825]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 8adb5d9ab0decf674a80b6b8ce68e366
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:19:17	[scenario5d724eaf250d19.02428825]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:19:17	[scenario5d724eaf250d19.02428825]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:19:18	[scenario5d724eaf250d19.02428825]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:19:18	[scenario5d724eaf250d19.02428825]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:19:18	[scenario5d724eaf250d19.02428825]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 8adb5d9ab0decf674a80b6b8ce68e366
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:19:18	[scenario5d724eaf250d19.02428825]	Execute X-Cart step: step_thirteen
2019-09-06 16:19:20	[scenario5d724eaf250d19.02428825]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:25:37	[scenario5d725041029225.48043961]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:25:38	[scenario5d725041029225.48043961]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:25:39	[scenario5d725041029225.48043961]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fd55e05e46a6fdf9b4cd194517181146
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:25:39	[scenario5d725041029225.48043961]	Execute X-Cart step: step_first
2019-09-06 16:25:39	[scenario5d725041029225.48043961]	Execute X-Cart step: step_second
2019-09-06 16:25:45	[scenario5d725041029225.48043961]	Execute X-Cart step: step_third
2019-09-06 16:25:45	[scenario5d725041029225.48043961]	Execute X-Cart step: step_fourth
2019-09-06 16:25:49	[scenario5d725041029225.48043961]	Execute X-Cart step: step_fifth
2019-09-06 16:25:49	[scenario5d725041029225.48043961]	Execute X-Cart step: step_six
2019-09-06 16:25:50	[scenario5d725041029225.48043961]	Execute X-Cart step: step_seven
2019-09-06 16:25:51	[scenario5d725041029225.48043961]	Execute X-Cart step: step_nine
2019-09-06 16:25:52	[scenario5d725041029225.48043961]	Execute X-Cart step: step_ten
2019-09-06 16:25:52	[scenario5d725041029225.48043961]	Execute X-Cart step: step_eleven
2019-09-06 16:25:53	[scenario5d725041029225.48043961]	Execute X-Cart step: step_twelve
2019-09-06 16:25:59	[scenario5d725041029225.48043961]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:25:59	[scenario5d725041029225.48043961]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:25:59	[scenario5d725041029225.48043961]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => fd55e05e46a6fdf9b4cd194517181146
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:26:00	[scenario5d725041029225.48043961]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:26:00	[scenario5d725041029225.48043961]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:26:00	[scenario5d725041029225.48043961]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:26:01	[scenario5d725041029225.48043961]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:26:01	[scenario5d725041029225.48043961]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => fd55e05e46a6fdf9b4cd194517181146
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:26:01	[scenario5d725041029225.48043961]	Execute X-Cart step: step_thirteen
2019-09-06 16:26:03	[scenario5d725041029225.48043961]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:27:09	[scenario5d72509d775bc3.50788282]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:27:13	[scenario5d72509d775bc3.50788282]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:27:13	[scenario5d72509d775bc3.50788282]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1227bccfdc8a3a4d0af01598eb57a967
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:27:13	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_first
2019-09-06 16:27:14	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_second
2019-09-06 16:27:22	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_third
2019-09-06 16:27:23	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_fourth
2019-09-06 16:27:28	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_fifth
2019-09-06 16:27:28	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_six
2019-09-06 16:27:29	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_seven
2019-09-06 16:27:32	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_nine
2019-09-06 16:27:33	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_ten
2019-09-06 16:27:33	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_eleven
2019-09-06 16:27:34	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_twelve
2019-09-06 16:27:44	[scenario5d72509d775bc3.50788282]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:27:45	[scenario5d72509d775bc3.50788282]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:27:45	[scenario5d72509d775bc3.50788282]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 1227bccfdc8a3a4d0af01598eb57a967
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:27:45	[scenario5d72509d775bc3.50788282]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:27:46	[scenario5d72509d775bc3.50788282]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:27:47	[scenario5d72509d775bc3.50788282]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:27:48	[scenario5d72509d775bc3.50788282]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:27:48	[scenario5d72509d775bc3.50788282]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 1227bccfdc8a3a4d0af01598eb57a967
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:27:48	[scenario5d72509d775bc3.50788282]	Execute X-Cart step: step_thirteen
2019-09-06 16:27:51	[scenario5d72509d775bc3.50788282]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:36:20	[scenario5d7252c44d9a55.92591536]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:36:22	[scenario5d7252c44d9a55.92591536]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:36:22	[scenario5d7252c44d9a55.92591536]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e3c9d4179ecdf0214a84dacf1f16f4fb
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:36:22	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_first
2019-09-06 16:36:23	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_second
2019-09-06 16:36:28	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_third
2019-09-06 16:36:29	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_fourth
2019-09-06 16:36:32	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_fifth
2019-09-06 16:36:32	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_six
2019-09-06 16:36:33	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_seven
2019-09-06 16:36:35	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_nine
2019-09-06 16:36:35	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_ten
2019-09-06 16:36:36	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_eleven
2019-09-06 16:36:36	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_twelve
2019-09-06 16:36:42	[scenario5d7252c44d9a55.92591536]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:36:43	[scenario5d7252c44d9a55.92591536]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:36:43	[scenario5d7252c44d9a55.92591536]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e3c9d4179ecdf0214a84dacf1f16f4fb
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:36:43	[scenario5d7252c44d9a55.92591536]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:36:43	[scenario5d7252c44d9a55.92591536]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:36:44	[scenario5d7252c44d9a55.92591536]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:36:44	[scenario5d7252c44d9a55.92591536]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:36:44	[scenario5d7252c44d9a55.92591536]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e3c9d4179ecdf0214a84dacf1f16f4fb
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:36:44	[scenario5d7252c44d9a55.92591536]	Execute X-Cart step: step_thirteen
2019-09-06 16:36:47	[scenario5d7252c44d9a55.92591536]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:37:35	[scenario5d72530f917394.94294813]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:37:40	[scenario5d72530f917394.94294813]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:37:40	[scenario5d72530f917394.94294813]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => afcd3f85c6dc716d67e546eed86bf0d1
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:37:41	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_first
2019-09-06 16:37:43	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_second
2019-09-06 16:37:48	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_third
2019-09-06 16:37:49	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_fourth
2019-09-06 16:37:53	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_fifth
2019-09-06 16:37:54	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_six
2019-09-06 16:37:55	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_seven
2019-09-06 16:37:57	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_nine
2019-09-06 16:37:58	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_ten
2019-09-06 16:37:59	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_eleven
2019-09-06 16:37:59	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_twelve
2019-09-06 16:38:08	[scenario5d72530f917394.94294813]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:38:08	[scenario5d72530f917394.94294813]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:38:08	[scenario5d72530f917394.94294813]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => afcd3f85c6dc716d67e546eed86bf0d1
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:38:09	[scenario5d72530f917394.94294813]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:38:10	[scenario5d72530f917394.94294813]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:38:10	[scenario5d72530f917394.94294813]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:38:11	[scenario5d72530f917394.94294813]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:38:11	[scenario5d72530f917394.94294813]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => afcd3f85c6dc716d67e546eed86bf0d1
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:38:11	[scenario5d72530f917394.94294813]	Execute X-Cart step: step_thirteen
2019-09-06 16:38:14	[scenario5d72530f917394.94294813]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:45:48	[scenario5d7254fcbd71b6.29030152]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:45:52	[scenario5d7254fcbd71b6.29030152]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:45:52	[scenario5d7254fcbd71b6.29030152]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4bd7571208f83068211264d666f29ce7
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:45:53	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_first
2019-09-06 16:45:53	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_second
2019-09-06 16:46:02	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_third
2019-09-06 16:46:03	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_fourth
2019-09-06 16:46:07	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_fifth
2019-09-06 16:46:07	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_six
2019-09-06 16:46:08	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_seven
2019-09-06 16:46:10	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_nine
2019-09-06 16:46:11	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_ten
2019-09-06 16:46:12	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_eleven
2019-09-06 16:46:12	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_twelve
2019-09-06 16:46:19	[scenario5d7254fcbd71b6.29030152]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:46:20	[scenario5d7254fcbd71b6.29030152]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:46:20	[scenario5d7254fcbd71b6.29030152]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4bd7571208f83068211264d666f29ce7
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:46:21	[scenario5d7254fcbd71b6.29030152]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:46:21	[scenario5d7254fcbd71b6.29030152]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:46:22	[scenario5d7254fcbd71b6.29030152]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:46:22	[scenario5d7254fcbd71b6.29030152]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:46:22	[scenario5d7254fcbd71b6.29030152]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4bd7571208f83068211264d666f29ce7
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:46:22	[scenario5d7254fcbd71b6.29030152]	Execute X-Cart step: step_thirteen
2019-09-06 16:46:25	[scenario5d7254fcbd71b6.29030152]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:47:16	[scenario5d725554be7ce1.62440153]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:47:20	[scenario5d725554be7ce1.62440153]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:47:20	[scenario5d725554be7ce1.62440153]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e750cd2ab07010c4f62712e6f01e7af2
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:47:21	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_first
2019-09-06 16:47:23	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_second
2019-09-06 16:47:29	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_third
2019-09-06 16:47:30	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_fourth
2019-09-06 16:47:34	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_fifth
2019-09-06 16:47:35	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_six
2019-09-06 16:47:37	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_seven
2019-09-06 16:47:39	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_nine
2019-09-06 16:47:40	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_ten
2019-09-06 16:47:41	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_eleven
2019-09-06 16:47:42	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_twelve
2019-09-06 16:47:50	[scenario5d725554be7ce1.62440153]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:47:50	[scenario5d725554be7ce1.62440153]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:47:50	[scenario5d725554be7ce1.62440153]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => e750cd2ab07010c4f62712e6f01e7af2
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:47:51	[scenario5d725554be7ce1.62440153]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:47:51	[scenario5d725554be7ce1.62440153]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:47:52	[scenario5d725554be7ce1.62440153]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:47:52	[scenario5d725554be7ce1.62440153]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:47:52	[scenario5d725554be7ce1.62440153]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => e750cd2ab07010c4f62712e6f01e7af2
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:47:53	[scenario5d725554be7ce1.62440153]	Execute X-Cart step: step_thirteen
2019-09-06 16:47:55	[scenario5d725554be7ce1.62440153]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

2019-09-06 16:52:10	[scenario5d72567a322164.85116441]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateModulesList::initialize
Context transitions => []

2019-09-06 16:52:14	[scenario5d72567a322164.85116441]	Send actual modules list
Context modules_list => {
    "CDev": {
        "Core": true,
        "AustraliaPost": true,
        "AuthorizeNet": true,
        "Bestsellers": true,
        "ContactUs": true,
        "Coupons": true,
        "Egoods": true,
        "FeaturedProducts": true,
        "FedEx": true,
        "FileAttachments": true,
        "GoSocial": true,
        "GoogleAnalytics": true,
        "MarketPrice": true,
        "PINCodes": true,
        "Paypal": true,
        "ProductAdvisor": true,
        "Quantum": true,
        "Sale": true,
        "SalesTax": true,
        "SimpleCMS": true,
        "SocialLogin": false,
        "TwoCheckout": true,
        "USPS": true,
        "UserPermissions": true,
        "VolumeDiscounts": true,
        "Wholesale": false,
        "XMLSitemap": true,
        "XPaymentsConnector": true
    },
    "Amazon": {
        "PayWithAmazon": true
    },
    "QSL": {
        "AuthorizenetAcceptjs": true,
        "BraintreeVZ": true,
        "CloudSearch": false,
        "FlyoutCategoriesMenu": true
    },
    "XC": {
        "Add2CartPopup": false,
        "BulkEditing": true,
        "CanadaPost": true,
        "Concierge": true,
        "CrispWhiteSkin": true,
        "CustomOrderStatuses": true,
        "CustomProductTabs": true,
        "CustomerAttachments": true,
        "EPDQ": true,
        "ESelectHPP": true,
        "FacebookMarketing": true,
        "FastLaneCheckout": true,
        "FreeShipping": true,
        "FroalaEditor": true,
        "Geolocation": true,
        "GoogleFeed": true,
        "IdealPayments": true,
        "MailChimp": true,
        "MultiCurrency": false,
        "News": true,
        "NewsletterSubscriptions": true,
        "NextPreviousProduct": true,
        "NotFinishedOrders": true,
        "Onboarding": true,
        "ProductComparison": true,
        "ProductFilter": true,
        "ProductTags": true,
        "ProductVariants": false,
        "RESTAPI": true,
        "Reviews": true,
        "SagePay": true,
        "Sitemap": true,
        "Stripe": true,
        "ThemeTweaker": true,
        "TwoFactorAuthentication": false,
        "UPS": true,
        "UpdateInventory": true,
        "Upselling": true,
        "VendorMessages": true
    },
    "XCExample": {
        "ImageWidgetDemo": false,
        "ImageColumnDemo": false
    },
    "TrueMachine": {
        "AgeVerification": true
    }
}

2019-09-06 16:52:14	[scenario5d72567a322164.85116441]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4fe68606e3decbfe90d768cd9c4f3ab9
Context transitions => [
    "step_first",
    "step_second",
    "step_third",
    "step_fourth",
    "step_fifth",
    "step_six",
    "step_seven",
    "step_nine",
    "step_ten",
    "step_eleven",
    "step_twelve"
]

2019-09-06 16:52:14	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_first
2019-09-06 16:52:15	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_second
2019-09-06 16:52:22	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_third
2019-09-06 16:52:22	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_fourth
2019-09-06 16:52:25	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_fifth
2019-09-06 16:52:26	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_six
2019-09-06 16:52:27	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_seven
2019-09-06 16:52:28	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_nine
2019-09-06 16:52:29	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_ten
2019-09-06 16:52:29	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_eleven
2019-09-06 16:52:30	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_twelve
2019-09-06 16:52:36	[scenario5d72567a322164.85116441]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::initialize
Context transitions => []

2019-09-06 16:52:37	[scenario5d72567a322164.85116441]	XCart\Bus\Rebuild\Executor\Step\Execute\UpdateDataSource::refreshInstalledModulesDataSource
Context existent => []
Context missing => []

2019-09-06 16:52:37	[scenario5d72567a322164.85116441]	XCart\Bus\Rebuild\Executor\Step\AHook::initialize
Context cacheId => 4fe68606e3decbfe90d768cd9c4f3ab9
Context transitions => {
    "CDev-UserPermissions": {
        "id": "CDev-UserPermissions",
        "remain_hooks": [
            "classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-CustomProductTabs": {
        "id": "XC-CustomProductTabs",
        "remain_hooks": [
            "classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-FreeShipping": {
        "id": "XC-FreeShipping",
        "remain_hooks": [
            "classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    },
    "XC-ThemeTweaker": {
        "id": "XC-ThemeTweaker",
        "remain_hooks": [
            "classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php"
        ],
        "pack_dir": "/home/kirill/domen/xcart/"
    }
}

2019-09-06 16:52:37	[scenario5d72567a322164.85116441]	Run hook: classes/XLite/Module/CDev/UserPermissions/hooks/rebuild.php
2019-09-06 16:52:38	[scenario5d72567a322164.85116441]	Run hook: classes/XLite/Module/XC/CustomProductTabs/hooks/rebuild.php
2019-09-06 16:52:38	[scenario5d72567a322164.85116441]	Run hook: classes/XLite/Module/XC/FreeShipping/hooks/rebuild.php
2019-09-06 16:52:38	[scenario5d72567a322164.85116441]	Run hook: classes/XLite/Module/XC/ThemeTweaker/hooks/rebuild.php
2019-09-06 16:52:38	[scenario5d72567a322164.85116441]	XCart\Bus\Rebuild\Executor\Step\AXCartStep::initialize
Context cacheId => 4fe68606e3decbfe90d768cd9c4f3ab9
Context transitions => [
    "step_thirteen"
]

2019-09-06 16:52:39	[scenario5d72567a322164.85116441]	Execute X-Cart step: step_thirteen
2019-09-06 16:52:41	[scenario5d72567a322164.85116441]	XCart\Bus\Rebuild\Executor\Step\Execute\EditionChange::initialize
Context stepData => {
    "editionNameBefore": null,
    "editionNameAfter": null
}

