<?php

namespace XLite\Module\TrueMachine\AgeVerification\Model;

abstract class Profile extends \XLite\Module\XC\Concierge\Model\Profile implements \XLite\Base\IDecorator
{
    /**
     * @Column (type="string", nullable=true)
     *
     */
    protected $ageVerification;
    
    public function getAgeVerification()
    {
        return $this->ageVerification;
    }

    public function setAgeVerification($value)
    {
        $this->ageVerification = $value;
        return $this;
    }
}