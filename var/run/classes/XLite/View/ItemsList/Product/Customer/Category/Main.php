<?php
namespace XLite\View\ItemsList\Product\Customer\Category;
/**
 * Category products list widget
 *
 * @ListChild (list="center.bottom", zone="customer", weight="200")
 */
class Main extends \XLite\Module\XC\ProductFilter\View\ItemsList\Product\Customer\Category\Main {}