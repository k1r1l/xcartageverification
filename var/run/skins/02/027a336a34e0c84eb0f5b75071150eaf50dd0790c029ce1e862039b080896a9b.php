<?php

/* /home/kirill/domen/xcart/skins/customer/modules/XC/CanadaPost/products_return/create/parts/main.info.twig */
class __TwigTemplate_127ddcf55795b0e52d6a78131b557f4528e9fc482e8d071cc56b51cd07f21e55 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
<div class=\"section-text\">
  ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["To request a return please fill the form below"]), "html", null, true);
        echo "
</div>
 
<hr size=\"1\" />
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/XC/CanadaPost/products_return/create/parts/main.info.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/XC/CanadaPost/products_return/create/parts/main.info.twig", "");
    }
}
