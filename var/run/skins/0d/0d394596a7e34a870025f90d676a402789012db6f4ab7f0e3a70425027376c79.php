<?php

/* /home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/add_new_card.address.twig */
class __TwigTemplate_55fedb7eef72be5e860024fa33eeb7976899787d3db19d3739f69d7660f113e0 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo "
";
        // line 8
        if ($this->getAttribute(($context["this"] ?? null), "getAddressList", [], "method")) {
            // line 9
            echo "
  ";
            // line 10
            $this->startForm("\\XLite\\Module\\CDev\\XPaymentsConnector\\View\\Form\\ZeroAuthAddress");            // line 11
            echo "    <input type=\"hidden\" name=\"action\" value=\"update_address\">

    <div class=\"zero-auth-address\">

      <strong>";
            // line 15
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Billing address"]), "html", null, true);
            echo ":</strong>

      <select name=\"address_id\" value=\"";
            // line 17
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getAddressId", [], "method"), "html", null, true);
            echo "\" onchange=\"javascript: shadeIframe(); this.form.submit();\">
        ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getAddressList", [], "method"));
            foreach ($context['_seq'] as $context["addressId"] => $context["address"]) {
                // line 19
                echo "          <option value=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["addressId"], "html", null, true);
                echo "\" ";
                if (($context["addressId"] == $this->getAttribute(($context["this"] ?? null), "getAddressId", [], "method"))) {
                    echo "selected=\"selected\"";
                }
                echo ">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["address"], "html", null, true);
                echo "</option>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['addressId'], $context['address'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "      </select>

      ";
            // line 23
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\Module\\CDev\\XPaymentsConnector\\View\\Button\\AddAddress", "label" => "New address", "profileId" => $this->getAttribute(($context["this"] ?? null), "getProfileId", [], "method")]]), "html", null, true);
            echo "

    </div>
  ";
            $this->endForm();            // line 27
            echo "
";
        } else {
            // line 29
            echo "
  <div class=\"alert alert-danger add-new-card-error\">
    <strong class=\"important-label\">";
            // line 31
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Warning"]), "html", null, true);
            echo "!</strong>
    ";
            // line 32
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["No billing address is defined. To add new card please "]), "html", null, true);
            echo "
    ";
            // line 33
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\Module\\CDev\\XPaymentsConnector\\View\\Button\\AddAddress", "label" => "enter your address", "profileId" => $this->getAttribute(($context["this"] ?? null), "getProfileId", [], "method")]]), "html", null, true);
            echo "
  </div>

";
        }
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/add_new_card.address.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 33,  84 => 32,  80 => 31,  76 => 29,  72 => 27,  66 => 23,  62 => 21,  47 => 19,  43 => 18,  39 => 17,  34 => 15,  28 => 11,  27 => 10,  24 => 9,  22 => 8,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/add_new_card.address.twig", "");
    }
}
