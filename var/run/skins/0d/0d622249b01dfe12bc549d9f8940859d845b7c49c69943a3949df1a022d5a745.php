<?php

/* /home/kirill/domen/xcart/skins/admin/export/parts/begin.buttons.export.twig */
class __TwigTemplate_cf4a2f9aec594d4631b2a00f2a61b4b37c768f1994d5458d8dd5d01aa9f3758a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
<div class=\"export-button-container\">
\t";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "XLite\\View\\Button\\StartExport"]]), "html", null, true);
        echo "
\t";
        // line 9
        if ($this->getAttribute(($context["this"] ?? null), "isExportLocked", [], "method")) {
            // line 10
            echo "\t\t<p class=\"export-blocked\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Background export is in progress"]), "html", null, true);
            echo "</p>
\t";
        }
        // line 12
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/admin/export/parts/begin.buttons.export.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 12,  29 => 10,  27 => 9,  23 => 8,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/admin/export/parts/begin.buttons.export.twig", "");
    }
}
