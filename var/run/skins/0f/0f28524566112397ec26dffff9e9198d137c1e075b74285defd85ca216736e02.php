<?php

/* /home/kirill/domen/xcart/skins/mail/admin/modules/CDev/XPaymentsConnector/kount.twig */
class __TwigTemplate_2786fdc3e566c93db61a0d02316d3b54ed7b118603b72ba0240fc1312e804f5f extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        if (($this->getAttribute(($context["this"] ?? null), "isDisplayKountResult", [], "method") &&  !$this->getAttribute($this->getAttribute(($context["this"] ?? null), "order", []), "getFraudCheckData", [], "method"))) {
            // line 7
            echo "<table>
  <tr>
    <td colspan=\"3\" style=\"background: #f9f9f9; padding: 15px;\">
  
    <h2 style=\"font-weight: normal; font-size: 24px; margin: 18px 0;\">";
            // line 11
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["KOUNT Antifraud screening result"]), "html", null, true);
            echo "</h2>
  
    ";
            // line 13
            if ($this->getAttribute(($context["this"] ?? null), "getKountErrors", [], "method")) {
                // line 14
                echo "      ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getKountErrors", [], "method"));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 15
                    echo "        <div style=\"";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getKountErrorStyle", [], "method"), "html", null, true);
                    echo "\">
          <strong>";
                    // line 16
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Error"]), "html", null, true);
                    echo "!</strong>
            ";
                    // line 17
                    echo $context["error"];
                    echo "
        </div>
      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 20
                echo "    ";
            }
            // line 21
            echo "  
    ";
            // line 22
            if ($this->getAttribute(($context["this"] ?? null), "getKountMessage", [], "method")) {
                // line 23
                echo "      <p style=\"font-size: 15px; line-height: 20px;\">
        ";
                // line 24
                echo $this->getAttribute(($context["this"] ?? null), "getKountMessage", [], "method");
                echo ". ";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Score"]), "html", null, true);
                echo ":
        <span class=\"lead ";
                // line 25
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getKountScoreClass", [], "method"), "html", null, true);
                echo "\">
          ";
                // line 26
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getKountScore", [], "method"), "html", null, true);
                echo "
        </span>
      </p>
    ";
            }
            // line 30
            echo "  
    ";
            // line 31
            if ($this->getAttribute(($context["this"] ?? null), "getKountTransactionId", [], "method")) {
                // line 32
                echo "      <p style=\"font-size: 15px; line-height: 20px;\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Transaction ID"]), "html", null, true);
                echo ": ";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getKountTransactionId", [], "method"), "html", null, true);
                echo "</p>
    ";
            }
            // line 34
            echo "  
    ";
            // line 35
            if ($this->getAttribute(($context["this"] ?? null), "getKountRules", [], "method")) {
                // line 36
                echo "  
      <h3 style=\"line-height: 50px; font-size: 18px; font-weight: normal;\">";
                // line 37
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Rules triggered"]), "html", null, true);
                echo ":</h3>
  
      <ul class=\"kount-result-lines\">
        ";
                // line 40
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getKountRules", [], "method"));
                foreach ($context['_seq'] as $context["title"] => $context["value"]) {
                    // line 41
                    echo "          <li style=\"font-size: 15px; line-height: 20px;\">";
                    echo $context["value"];
                    echo "</li>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['title'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 43
                echo "      </ul>
  
    ";
            }
            // line 46
            echo "  
    </td>
  </tr>
</table>
";
        }
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/mail/admin/modules/CDev/XPaymentsConnector/kount.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 46,  125 => 43,  116 => 41,  112 => 40,  106 => 37,  103 => 36,  101 => 35,  98 => 34,  90 => 32,  88 => 31,  85 => 30,  78 => 26,  74 => 25,  68 => 24,  65 => 23,  63 => 22,  60 => 21,  57 => 20,  48 => 17,  44 => 16,  39 => 15,  34 => 14,  32 => 13,  27 => 11,  21 => 7,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/mail/admin/modules/CDev/XPaymentsConnector/kount.twig", "");
    }
}
