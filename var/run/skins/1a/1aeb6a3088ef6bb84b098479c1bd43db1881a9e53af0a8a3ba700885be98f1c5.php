<?php

/* layout/header/header.right.mobile.minicart.twig */
class __TwigTemplate_f82ef6acd25871743222594f54f6a9313f5a0154e5b2ba19d4702ea09a2d6c89 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
<div class=\"lc-minicart-placeholder\"></div>
";
    }

    public function getTemplateName()
    {
        return "layout/header/header.right.mobile.minicart.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "layout/header/header.right.mobile.minicart.twig", "/home/kirill/domen/xcart/skins/crisp_white/customer/layout/header/header.right.mobile.minicart.twig");
    }
}
