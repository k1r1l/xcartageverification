<?php

/* /home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/saved_cards.table.twig */
class __TwigTemplate_07f4447530a6a1419dfe2cef9e085c13a307530a44ae83baa9c3932d5e894ca0 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo "

";
        // line 9
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "profile", []), "getSavedCards", [], "method")) {
            // line 10
            echo "<div>

  ";
            // line 12
            $this->startForm("\\XLite\\Module\\CDev\\XPaymentsConnector\\View\\Form\\SavedCards");            // line 13
            echo "
  <table class=\"saved-cards\">
    <tr>
      <th>";
            // line 16
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Order"]), "html", null, true);
            echo "</th>
      <th>";
            // line 17
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Credit card"]), "html", null, true);
            echo "</th>
      <th>";
            // line 18
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Billing Address"]), "html", null, true);
            echo "</th>
      <th>";
            // line 19
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Default"]), "html", null, true);
            echo "</th>
      <th></th>
    </tr>
    ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["this"] ?? null), "profile", []), "getSavedCards", [], "method"));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["cc"]) {
                // line 23
                echo "      <tr>
        <td class=\"orderid\"><a href=\"";
                // line 24
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('url')->getCallable(), [$this->env, $context, "order", "", ["order_number" => $this->getAttribute($context["cc"], "invoice_id", [])]]), "html", null, true);
                echo "\">#";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "invoice_id", []), "html", null, true);
                echo "</a></td>
        <td>

          <div class=\"saved-card\">
            <div class=\"card-icon-container\">
              <span class=\"xpc-card-type ";
                // line 29
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "card_type_css", []), "html", null, true);
                echo "\"><img src=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('asset')->getCallable(), ["images/spacer.gif"]), "html", null, true);
                echo "\" alt=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "card_type", []), "html", null, true);
                echo "\"/></span>
            </div>
            <div class=\"card-number\">
              ";
                // line 32
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "card_number", []), "html", null, true);
                echo "
            </div>
            ";
                // line 34
                if ($this->getAttribute($context["cc"], "expire", [])) {
                    // line 35
                    echo "              <div class=\"card-expire\">
                ";
                    // line 36
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "expire", []), "html", null, true);
                    echo "
              </div>
            ";
                }
                // line 39
                echo "          </div>

        </td>
        <td class=\"address\">
          ";
                // line 43
                if ($this->getAttribute(($context["this"] ?? null), "getAddressList", [], "method")) {
                    // line 44
                    echo "
            ";
                    // line 45
                    if ($this->getAttribute(($context["this"] ?? null), "isSingleAddress", [], "method")) {
                        // line 46
                        echo "              <div class=\"single\">
                ";
                        // line 47
                        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getSingleAddress", [], "method"), "html", null, true);
                        echo "
              </div>
            ";
                    } else {
                        // line 50
                        echo "              <select name=\"address_id[";
                        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "card_id", []), "html", null, true);
                        echo "]\" value=\"";
                        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "address_id", []), "html", null, true);
                        echo "\">
                ";
                        // line 51
                        if ( !$this->getAttribute($context["cc"], "address_id", [])) {
                            // line 52
                            echo "                  <option value=\"0\" selected=\"selected\"></option>
                ";
                        }
                        // line 54
                        echo "                ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getAddressList", [], "method"));
                        foreach ($context['_seq'] as $context["addressId"] => $context["address"]) {
                            // line 55
                            echo "                  <option value=\"";
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["addressId"], "html", null, true);
                            echo "\" ";
                            if (($context["addressId"] == $this->getAttribute($context["cc"], "address_id", []))) {
                                echo "selected=\"selected\"";
                            }
                            echo ">";
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["address"], "html", null, true);
                            echo "</option>
                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['addressId'], $context['address'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 57
                        echo "              </select>
            ";
                    }
                    // line 59
                    echo "          ";
                }
                // line 60
                echo "        </td>
        <td class=\"default-column\">
          ";
                // line 62
                if ($this->getAttribute($context["cc"], "is_default", [])) {
                    // line 63
                    echo "            <input checked type=\"radio\" name=\"default_card_id\" value=\"";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "card_id", []), "html", null, true);
                    echo "\" />
          ";
                } else {
                    // line 65
                    echo "            <input type=\"radio\" name=\"default_card_id\" value=\"";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "card_id", []), "html", null, true);
                    echo "\" />
          ";
                }
                // line 67
                echo "        </td>
        <td class=\"remove-column\">
          ";
                $fullPath = \XLite\Core\Layout::getInstance()->getResourceFullPath($this->getAttribute(                // line 69
($context["this"] ?? null), "getRemoveTemplate", [0 => $this->getAttribute($context["cc"], "card_id", [])], "method"));                list($templateWrapperText, $templateWrapperStart) = $this->getThis()->startMarker($fullPath);
                if ($templateWrapperText) {
echo $templateWrapperStart;
}

                $this->loadTemplate($this->getAttribute(($context["this"] ?? null), "getRemoveTemplate", [0 => $this->getAttribute($context["cc"], "card_id", [])], "method"), "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/saved_cards.table.twig", 69)->display($context);
                if ($templateWrapperText) {
                    echo $this->getThis()->endMarker($fullPath, $templateWrapperText);
                }
                // line 70
                echo "        </td>
      </tr>  
    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cc'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "  </table>

  ";
            // line 75
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\View\\Button\\Submit", "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), ["Update saved credit cards"]), "style" => "main"]]), "html", null, true);
            echo "

  ";
            // line 77
            if ($this->getAttribute(($context["this"] ?? null), "allowZeroAuth", [], "method")) {
                // line 78
                echo "    &nbsp;&nbsp;<a class=\"add-new-card\" href=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('url')->getCallable(), [$this->env, $context, "add_new_card"]), "html", null, true);
                echo "\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Add new credit card"]), "html", null, true);
                echo "</a>
  ";
            }
            // line 80
            echo "
  ";
            $this->endForm();            // line 82
            echo "
</div>
";
        } else {
            // line 85
            echo "
  ";
            // line 86
            if ($this->getAttribute(($context["this"] ?? null), "allowZeroAuth", [], "method")) {
                // line 87
                echo "    <br/>
    &nbsp;&nbsp;<a class=\"add-new-card\" href=\"";
                // line 88
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('url')->getCallable(), [$this->env, $context, "add_new_card"]), "html", null, true);
                echo "\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Add new credit card"]), "html", null, true);
                echo "</a>
  ";
            }
            // line 90
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/saved_cards.table.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 90,  254 => 88,  251 => 87,  249 => 86,  246 => 85,  241 => 82,  238 => 80,  230 => 78,  228 => 77,  223 => 75,  219 => 73,  203 => 70,  193 => 69,  189 => 67,  183 => 65,  177 => 63,  175 => 62,  171 => 60,  168 => 59,  164 => 57,  149 => 55,  144 => 54,  140 => 52,  138 => 51,  131 => 50,  125 => 47,  122 => 46,  120 => 45,  117 => 44,  115 => 43,  109 => 39,  103 => 36,  100 => 35,  98 => 34,  93 => 32,  83 => 29,  73 => 24,  70 => 23,  53 => 22,  47 => 19,  43 => 18,  39 => 17,  35 => 16,  30 => 13,  29 => 12,  25 => 10,  23 => 9,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/saved_cards.table.twig", "");
    }
}
