<?php

/* /home/kirill/domen/xcart/skins/admin/modules/CDev/XPaymentsConnector/order/fraud_status/kount.twig */
class __TwigTemplate_71c5587d4b420dd4ec7326aca9964366cc28a130e099f8382ed6603ae45f1274 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo "
";
        // line 8
        if (($this->getAttribute(($context["this"] ?? null), "getKountData", [], "method") &&  !$this->getAttribute($this->getAttribute(($context["this"] ?? null), "order", []), "getFraudCheckData", [], "method"))) {
            // line 9
            echo "  <div class=\"kount-result\">
  
    <a name=\"fraud-info-kount\"></a>
  
    <h2>";
            // line 13
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["KOUNT Antifraud screening result"]), "html", null, true);
            echo "</h2>
  
    ";
            // line 15
            if ($this->getAttribute(($context["this"] ?? null), "getKountErrors", [], "method")) {
                // line 16
                echo "      ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getKountErrors", [], "method"));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 17
                    echo "        <div class=\"alert alert-danger\">
          <strong>";
                    // line 18
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Error"]), "html", null, true);
                    echo "!</strong>
            ";
                    // line 19
                    echo $context["error"];
                    echo "
        </div>
      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 22
                echo "    ";
            }
            // line 23
            echo "  
    ";
            // line 24
            if ($this->getAttribute(($context["this"] ?? null), "getKountMessage", [], "method")) {
                // line 25
                echo "      <p class=\"lead\">
        ";
                // line 26
                echo $this->getAttribute(($context["this"] ?? null), "getKountMessage", [], "method");
                echo ". ";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Score"]), "html", null, true);
                echo ":
        <span class=\"lead ";
                // line 27
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getKountScoreClass", [], "method"), "html", null, true);
                echo "\">
          ";
                // line 28
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getKountScore", [], "method"), "html", null, true);
                echo "
        </span>
      </p>
    ";
            }
            // line 32
            echo "  
    ";
            // line 33
            if ($this->getAttribute(($context["this"] ?? null), "getKountTransactionId", [], "method")) {
                // line 34
                echo "      <p>";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Transaction ID"]), "html", null, true);
                echo ": ";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getKountTransactionId", [], "method"), "html", null, true);
                echo "</p>
    ";
            }
            // line 36
            echo "  
    ";
            // line 37
            if ($this->getAttribute(($context["this"] ?? null), "getKountRules", [], "method")) {
                // line 38
                echo "  
      <h3>";
                // line 39
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Rules triggered"]), "html", null, true);
                echo ":</h3>
  
      <ul class=\"kount-result-lines\">
        ";
                // line 42
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getKountRules", [], "method"));
                foreach ($context['_seq'] as $context["title"] => $context["value"]) {
                    // line 43
                    echo "          <li>";
                    echo $context["value"];
                    echo "</li>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['title'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "      </ul>
  
    ";
            }
            // line 48
            echo "  
  </div>
";
        }
        // line 51
        echo "
<br/>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/admin/modules/CDev/XPaymentsConnector/order/fraud_status/kount.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 51,  131 => 48,  126 => 45,  117 => 43,  113 => 42,  107 => 39,  104 => 38,  102 => 37,  99 => 36,  91 => 34,  89 => 33,  86 => 32,  79 => 28,  75 => 27,  69 => 26,  66 => 25,  64 => 24,  61 => 23,  58 => 22,  49 => 19,  45 => 18,  42 => 17,  37 => 16,  35 => 15,  30 => 13,  24 => 9,  22 => 8,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/admin/modules/CDev/XPaymentsConnector/order/fraud_status/kount.twig", "");
    }
}
