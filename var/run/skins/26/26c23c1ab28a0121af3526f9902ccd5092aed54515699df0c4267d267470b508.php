<?php

/* /home/kirill/domen/xcart/skins/customer/items_list/product/parts/common.button-buy-selected.twig */
class __TwigTemplate_e8426f191a413df8bcadddc02159e44016742fa9e4e9d3f5cc39a7b57e6d231d extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "<!--widget class=\"\\XLite\\View\\Button\\Submit\" style=\"product-buy-selected\" label=\"Buy selected\" /-->
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/items_list/product/parts/common.button-buy-selected.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/items_list/product/parts/common.button-buy-selected.twig", "");
    }
}
