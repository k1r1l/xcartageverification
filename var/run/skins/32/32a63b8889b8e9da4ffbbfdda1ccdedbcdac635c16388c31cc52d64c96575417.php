<?php

/* banner_rotation/body.twig */
class __TwigTemplate_0aeb7ec118293d29c21fba7c63ff2a3078740696cd502c2835d9a3f3015b73ed extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "
<div id=\"banner-rotation-widget\" class=\"carousel slide banner-carousel lazy-load\">
  ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "displayCommentedData", [0 => $this->getAttribute(($context["this"] ?? null), "getCommentedData", [], "method")], "method"), "html", null, true);
        echo "

  <!-- Indicators -->
  ";
        // line 9
        if ($this->getAttribute(($context["this"] ?? null), "isRotationEnabled", [], "method")) {
            // line 10
            echo "    <ol class=\"carousel-indicators\">
      ";
            // line 11
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getImages", [], "method"));
            foreach ($context['_seq'] as $context["i"] => $context["image"]) {
                // line 12
                echo "        <li data-target=\"#banner-rotation-widget\" data-slide-to=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\"></li>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['i'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 14
            echo "    </ol>
  ";
        }
        // line 16
        echo "
  <div class=\"carousel-inner not-initialized\" role=\"listbox\">
    ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getImages", [], "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 19
            echo "      <a ";
            if ($this->getAttribute($this->getAttribute($context["image"], "bannerRotationSlide", []), "getFrontLink", [], "method")) {
                echo " href=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["image"], "bannerRotationSlide", []), "getFrontLink", [], "method"), "html", null, true);
                echo "\"";
            }
            echo " class=\"item\">
        <img ";
            // line 20
            if ($this->getAttribute(($context["this"] ?? null), "getBlurredImageData", [0 => $context["image"]], "method")) {
                echo "style=\"background: url(";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getBlurredImageData", [0 => $context["image"]], "method"), "html", null, true);
                echo "); background-size: cover\" ";
            }
            echo " src=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["image"], "getFrontURL", [], "method"), "html", null, true);
            echo "\" alt=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["image"], "getAlt", [], "method"), "html", null, true);
            echo "\" >
      </a>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "banner_rotation/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 23,  68 => 20,  59 => 19,  55 => 18,  51 => 16,  47 => 14,  38 => 12,  34 => 11,  31 => 10,  29 => 9,  23 => 6,  19 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "banner_rotation/body.twig", "/home/kirill/domen/xcart/skins/customer/banner_rotation/body.twig");
    }
}
