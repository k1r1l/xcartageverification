<?php

/* /home/kirill/domen/xcart/skins/common/order/invoice/parts/bottom.address.shipping.twig */
class __TwigTemplate_d5bb8434a1c15b2f5092b111bedacb0d75b87478c94043c92ea7f0a95d3a8500 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "<strong class=\"title\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Shipping address"]), "html", null, true);
        echo "</strong>

<ul class=\"address-section shipping-address-section\">
  ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getAddressSectionData", [0 => $this->getAttribute(($context["this"] ?? null), "saddress", [])], "method"));
        foreach ($context['_seq'] as $context["idx"] => $context["field"]) {
            // line 10
            echo "    <li class=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["field"], "css_class", []), "html", null, true);
            echo " address-field\">
      <span class=\"address-title\">";
            // line 11
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), [$this->getAttribute($context["field"], "title", [])]), "html", null, true);
            echo ":</span>
      <span class=\"address-field\">";
            // line 12
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["field"], "value", []), "html", null, true);
            echo "</span>
      <span class=\"address-comma\">,</span>
    </li>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['idx'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "</ul>
";
        // line 17
        if ($this->getAttribute(($context["this"] ?? null), "isDisplayShippingEmail", [], "method")) {
            // line 18
            echo "  <p class=\"email\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["E-mail"]), "html", null, true);
            echo ": <a href=\"mailto:";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["this"] ?? null), "order", []), "profile", []), "email", []), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["this"] ?? null), "order", []), "profile", []), "email", []), "html", null, true);
            echo "</a></p>
";
        }
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/common/order/invoice/parts/bottom.address.shipping.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 18,  52 => 17,  49 => 16,  39 => 12,  35 => 11,  30 => 10,  26 => 9,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/common/order/invoice/parts/bottom.address.shipping.twig", "");
    }
}
