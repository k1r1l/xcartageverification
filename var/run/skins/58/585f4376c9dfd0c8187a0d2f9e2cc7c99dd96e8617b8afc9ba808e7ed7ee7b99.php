<?php

/* modules/CDev/Paypal/banner/homePage/aboveProducts.twig */
class __TwigTemplate_284f7bf4df477608e5f5e7fab9a973a58365f8ce6fddcb6a48fc20c70996decf extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\Module\\CDev\\Paypal\\View\\Banner", "page" => "homePage", "position" => "A"]]), "html", null, true);
    }

    public function getTemplateName()
    {
        return "modules/CDev/Paypal/banner/homePage/aboveProducts.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modules/CDev/Paypal/banner/homePage/aboveProducts.twig", "/home/kirill/domen/xcart/skins/customer/modules/CDev/Paypal/banner/homePage/aboveProducts.twig");
    }
}
