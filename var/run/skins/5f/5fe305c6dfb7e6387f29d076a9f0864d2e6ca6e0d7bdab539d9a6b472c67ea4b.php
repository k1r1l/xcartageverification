<?php

/* /home/kirill/domen/xcart/skins/customer/modules/XC/NextPreviousProduct/product/parts/body.cookie_data.twig */
class __TwigTemplate_c17c99db79d901cb42d5e9c486876307c7171e8ea9978f81738f30766114bd52 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
<div style=\"display: none;\" class=\"next-previous-cookie-data\" data-xc-product-id=\"";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "item", []), "getProductId", [], "method"), "html", null, true);
        echo "\"    data-xc-next-previous=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "dataString", []), "html", null, true);
        echo "\"></div>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/XC/NextPreviousProduct/product/parts/body.cookie_data.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/XC/NextPreviousProduct/product/parts/body.cookie_data.twig", "");
    }
}
