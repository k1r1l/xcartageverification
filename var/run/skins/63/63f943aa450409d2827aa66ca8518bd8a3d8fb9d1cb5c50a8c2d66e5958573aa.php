<?php

/* /home/kirill/domen/xcart/skins/customer/signin/parts/login_form/email.twig */
class __TwigTemplate_30bb68d1ab67bddb22c6ebbe8d2a0da772d349b3aa39c9f337144d0bb75a3778 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "<tr><td class=\"email-field\">
  ";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "XLite\\View\\FormField\\Input\\Text\\Email", "value" => $this->getAttribute(($context["this"] ?? null), "login", []), "required" => "true", "fieldName" => "login", "placeholder" => call_user_func_array($this->env->getFunction('t')->getCallable(), ["Email"]), "label" => call_user_func_array($this->env->getFunction('t')->getCallable(), ["Email"]), "fieldOnly" => "true", "fieldId" => "login-email"]]), "html", null, true);
        echo "
</td></tr>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/signin/parts/login_form/email.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/signin/parts/login_form/email.twig", "");
    }
}
