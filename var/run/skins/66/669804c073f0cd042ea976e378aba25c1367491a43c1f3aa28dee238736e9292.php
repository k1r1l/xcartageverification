<?php

/* header/parts/meta_mobile-capable.twig */
class __TwigTemplate_1d55d364822231556f579dc2a646ec7d9ae1c57ce47f3613bf606b9ba3cd1032 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
<meta name=\"apple-mobile-web-app-capable\"   content=\"yes\" />
<meta name=\"mobile-web-app-capable\"         content=\"yes\" />";
    }

    public function getTemplateName()
    {
        return "header/parts/meta_mobile-capable.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "header/parts/meta_mobile-capable.twig", "/home/kirill/domen/xcart/skins/admin/header/parts/meta_mobile-capable.twig");
    }
}
