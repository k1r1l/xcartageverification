<?php

/* /home/kirill/domen/xcart/skins/mail/admin/modules/CDev/XPaymentsConnector/fraud_check.twig */
class __TwigTemplate_cb78e48bd7e190643bf7449d1efec29ee07c02ffaf12c39d93f16e1e789bf9de extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "order", []), "getFraudCheckData", [], "method")) {
            // line 7
            echo "<table>
  ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["this"] ?? null), "order", []), "getFraudCheckData", [], "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
                // line 9
                echo "    ";
                if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "order", []), "getFraudCheckData", [], "method")) {
                    // line 10
                    echo "      <tr>
  
        <td colspan=\"3\" style=\"background: #f9f9f9; padding: 15px;\">
  
          <h2 style=\"font-weight: normal; font-size: 24px; margin: 18px 0;\">";
                    // line 14
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), [$this->getAttribute($context["data"], "service", [])]), "html", null, true);
                    echo "</h2>
  
          ";
                    // line 16
                    if ($this->getAttribute($context["data"], "errors", [])) {
                        // line 17
                        echo "            ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["data"], "getErrorsList", [], "method"));
                        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                            // line 18
                            echo "              <div style=\"";
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getKountErrorStyle", [], "method"), "html", null, true);
                            echo "\">
                <strong>";
                            // line 19
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Error"]), "html", null, true);
                            echo "!</strong>
                  ";
                            // line 20
                            echo $context["error"];
                            echo "
              </div>
            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 23
                        echo "          ";
                    }
                    // line 24
                    echo "  
          ";
                    // line 25
                    if ($this->getAttribute($context["data"], "warnings", [])) {
                        // line 26
                        echo "            ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["data"], "getWarningList", [], "method"));
                        foreach ($context['_seq'] as $context["_key"] => $context["warning"]) {
                            // line 27
                            echo "              <div style=\"font-size: 15px; line-height: 20px;\">
                <strong>";
                            // line 28
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Warning"]), "html", null, true);
                            echo "!</strong>
                  ";
                            // line 29
                            echo $context["warning"];
                            echo "
              </div>
            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['warning'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 32
                        echo "          ";
                    }
                    // line 33
                    echo "  
          ";
                    // line 34
                    if ($this->getAttribute($context["data"], "getDisplayMessage", [], "method")) {
                        // line 35
                        echo "            <p style=\"font-size: 15px; line-height: 20px;\">
              ";
                        // line 36
                        echo $this->getAttribute($context["data"], "getDisplayMessage", [], "method");
                        echo ".
    
              ";
                        // line 38
                        if ($this->getAttribute($context["data"], "score", [])) {
                            // line 39
                            echo "                ";
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Score"]), "html", null, true);
                            echo ":
                <span class=\"lead ";
                            // line 40
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["data"], "scoreClass", []), "html", null, true);
                            echo "\">";
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["data"], "score", []), "html", null, true);
                            echo "</span>
              ";
                        }
                        // line 42
                        echo "            </p>
          ";
                    }
                    // line 44
                    echo "  
          ";
                    // line 45
                    if ($this->getAttribute($context["data"], "serviceTransactionId", [])) {
                        // line 46
                        echo "            <p style=\"font-size: 15px; line-height: 20px;\">
    
              ";
                        // line 48
                        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Transaction ID"]), "html", null, true);
                        echo ":
    
              ";
                        // line 50
                        if ($this->getAttribute($context["data"], "url", [])) {
                            // line 51
                            echo "                <a href=\"";
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["data"], "url", []), "html", null, true);
                            echo "\" target=\"_blank\">";
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["data"], "serviceTransactionId", []), "html", null, true);
                            echo "</a>
              ";
                        } else {
                            // line 53
                            echo "                ";
                            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["data"], "serviceTransactionId", []), "html", null, true);
                            echo "
              ";
                        }
                        // line 55
                        echo "    
            </p>
          ";
                    }
                    // line 58
                    echo "  
          ";
                    // line 59
                    if ($this->getAttribute($context["data"], "rules", [])) {
                        // line 60
                        echo "  
            <h3 style=\"line-height: 50px; font-size: 18px; font-weight: normal;\">";
                        // line 61
                        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Rules triggered"]), "html", null, true);
                        echo ":</h3>
  
            <ul class=\"kount-result-lines\">
              ";
                        // line 64
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["data"], "getRulesList", [], "method"));
                        foreach ($context['_seq'] as $context["_key"] => $context["rule"]) {
                            // line 65
                            echo "                <li style=\"font-size: 15px; line-height: 20px;\">";
                            echo $context["rule"];
                            echo "</li>
              ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rule'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 67
                        echo "            </ul>
          ";
                    }
                    // line 69
                    echo "  
        </td>
  
      </tr>
    ";
                }
                // line 74
                echo "  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "</table>
";
        }
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/mail/admin/modules/CDev/XPaymentsConnector/fraud_check.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 75,  203 => 74,  196 => 69,  192 => 67,  183 => 65,  179 => 64,  173 => 61,  170 => 60,  168 => 59,  165 => 58,  160 => 55,  154 => 53,  146 => 51,  144 => 50,  139 => 48,  135 => 46,  133 => 45,  130 => 44,  126 => 42,  119 => 40,  114 => 39,  112 => 38,  107 => 36,  104 => 35,  102 => 34,  99 => 33,  96 => 32,  87 => 29,  83 => 28,  80 => 27,  75 => 26,  73 => 25,  70 => 24,  67 => 23,  58 => 20,  54 => 19,  49 => 18,  44 => 17,  42 => 16,  37 => 14,  31 => 10,  28 => 9,  24 => 8,  21 => 7,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/mail/admin/modules/CDev/XPaymentsConnector/fraud_check.twig", "");
    }
}
