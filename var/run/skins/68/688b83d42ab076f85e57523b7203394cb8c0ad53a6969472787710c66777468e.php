<?php

/* /home/kirill/domen/xcart/skins/customer/modules/XC/MailChimp/model/profile/subscriptions_list.twig */
class __TwigTemplate_7c254a46c38197790f65ba6ed054f92cfc2c2887e05557526c498a27084d1b83 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 8
        echo "
";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\Module\\XC\\MailChimp\\View\\ItemsList\\Subscriptions\\RegisterSubscriptions", "profile" => $this->getAttribute(($context["this"] ?? null), "getProfile", [], "method")]]), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/XC/MailChimp/model/profile/subscriptions_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 9,  19 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/XC/MailChimp/model/profile/subscriptions_list.twig", "");
    }
}
