<?php

/* header/parts/noindex.twig */
class __TwigTemplate_c0dc35a0236605a216ad01eed0da1e9fbcfd1b77a9a7aa9cd0948943d0a05c14 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
<meta name=\"robots\" content=\"noindex, nofollow\" />
";
    }

    public function getTemplateName()
    {
        return "header/parts/noindex.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "header/parts/noindex.twig", "/home/kirill/domen/xcart/skins/admin/header/parts/noindex.twig");
    }
}
