<?php

/* /home/kirill/domen/xcart/skins/customer/modules/XC/Reviews/review/parts/field.email.twig */
class __TwigTemplate_b262bb55bd034b357026786e581eecdeb47cdb53bb0285aaf30b78f0a62b1044 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
<div class=\"form-item clearfix\">
  <div class=\"email-label\">
    <label for=\"email\">
      ";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Your email"]), "html", null, true);
        echo "
    </label>
  </div>

  <div class=\"email-value\">
    <span>";
        // line 15
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "review", []), "email", []), "html", null, true);
        echo "</span>
  </div>

  <div class=\"tooltip-main\">
      <i class=\"tooltip-caption fa fa-question-circle help-icon\"></i>
      <div class=\"help-text\" style=\"display: none;\">";
        // line 20
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["We use this email in case we need additional information on your review. We do not use it for any kind of mailing lists or spam subscriptions"]), "html", null, true);
        echo "</div>
  </div>
  <div class=\"clear\"></div>
</div>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/XC/Reviews/review/parts/field.email.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 20,  33 => 15,  25 => 10,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/XC/Reviews/review/parts/field.email.twig", "");
    }
}
