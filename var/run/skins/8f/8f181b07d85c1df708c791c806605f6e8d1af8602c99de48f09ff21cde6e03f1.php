<?php

/* /home/kirill/domen/xcart/skins/admin/images_settings/parts/resize_quality.twig */
class __TwigTemplate_509b88c251d6a93c35e6f958e76a1199ee7469f1634c18e568d32c9962cc9a71 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\View\\FormField\\Input\\Text\\Integer", "fieldName" => "resize_quality", "value" => $this->getAttribute(($context["this"] ?? null), "getResizeQuality", [], "method"), "label" => "Image quality after resize", "min" => 1, "max" => 100]]), "html", null, true);
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/admin/images_settings/parts/resize_quality.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/admin/images_settings/parts/resize_quality.twig", "");
    }
}
