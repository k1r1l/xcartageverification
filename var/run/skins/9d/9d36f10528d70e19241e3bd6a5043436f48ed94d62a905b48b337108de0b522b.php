<?php

/* /home/kirill/domen/xcart/skins/admin/modules/CDev/PINCodes/popup/backstock_status_change_notification/pin_codes_notice.twig */
class __TwigTemplate_7d923e25e524cdc69ae06e1c79b9ec652d422afdc61b8ec95d05a6fd253be282 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
<div class=\"pin-codes-notice\">
  ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["The order also contains items with missing PIN codes; you will be prompted to enter the PIN codes under item names."]), "html", null, true);
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/admin/modules/CDev/PINCodes/popup/backstock_status_change_notification/pin_codes_notice.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/admin/modules/CDev/PINCodes/popup/backstock_status_change_notification/pin_codes_notice.twig", "");
    }
}
