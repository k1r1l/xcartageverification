<?php

/* /home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/saved_cards.header.twig */
class __TwigTemplate_a56eba1e3082aa799db5351c56d5d6dfd663f323b82322ee490dffd353e89542 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 8
        echo "
<p class=\"saved-cards-description\">  ";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Cards which can be used for your future orders in this shop. No real credit cards were saved, only special token on the side of the payment processor, that can be used in this store only."]), "html", null, true);
        echo "
</p>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/saved_cards.header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 9,  19 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/saved_cards.header.twig", "");
    }
}
