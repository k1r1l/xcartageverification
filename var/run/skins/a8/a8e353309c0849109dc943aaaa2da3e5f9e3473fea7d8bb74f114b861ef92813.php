<?php

/* /home/kirill/domen/xcart/skins/admin/modules/XC/CanadaPost/products_return/parts/form.notes.twig */
class __TwigTemplate_e7d70339a53339cc6277342cc1e13ed7b37a4206b181ac1afe830b385f7a47f0 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
";
        // line 7
        if ($this->getAttribute(($context["this"] ?? null), "isCustomerNotesVisible", [], "method")) {
            // line 8
            echo "  <div class=\"customer-notes\">
    
    <h2>";
            // line 10
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Customer note"]), "html", null, true);
            echo "</h2>
    
    <div class=\"note\">";
            // line 12
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "productsReturn", []), "getNotes", [], "method"), "html", null, true);
            echo "</div>
  
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/admin/modules/XC/CanadaPost/products_return/parts/form.notes.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 12,  28 => 10,  24 => 8,  22 => 7,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/admin/modules/XC/CanadaPost/products_return/parts/form.notes.twig", "");
    }
}
