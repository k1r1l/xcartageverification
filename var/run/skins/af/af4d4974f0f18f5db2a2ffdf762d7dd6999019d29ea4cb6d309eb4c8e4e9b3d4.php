<?php

/* /home/kirill/domen/xcart/skins/admin/modules/CDev/XPaymentsConnector/account/add_new_card.address.twig */
class __TwigTemplate_db13265234e3b661016d3add9559d84786f04d7c84df8b99dcf490d25c3b7cd7 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo "
";
        // line 8
        if ($this->getAttribute(($context["this"] ?? null), "getAddressList", [], "method")) {
            // line 9
            echo "
  ";
            // line 10
            $this->startForm("\\XLite\\Module\\CDev\\XPaymentsConnector\\View\\Form\\ZeroAuthAddress");            // line 11
            echo "
    <div class=\"zero-auth-address\">

      <strong>";
            // line 14
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Billing address"]), "html", null, true);
            echo ":</strong>

      ";
            // line 16
            if ($this->getAttribute(($context["this"] ?? null), "isSingleAddress", [], "method")) {
                // line 17
                echo "        <div class=\"single\">
          ";
                // line 18
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getSingleAddress", [], "method"), "html", null, true);
                echo "
        </div>
      ";
            } else {
                // line 21
                echo "        <select name=\"address_id\" value=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getAddressId", [], "method"), "html", null, true);
                echo "\" onchange=\"javascript: shadeIframe(); this.form.submit();\">
          ";
                // line 22
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getAddressList", [], "method"));
                foreach ($context['_seq'] as $context["addressId"] => $context["address"]) {
                    // line 23
                    echo "            <option value=\"";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["addressId"], "html", null, true);
                    echo "\" ";
                    if (($context["addressId"] == $this->getAttribute(($context["this"] ?? null), "getAddressId", [], "method"))) {
                        echo "selected=\"selected\"";
                    }
                    echo ">";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["address"], "html", null, true);
                    echo "</option>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['addressId'], $context['address'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 25
                echo "        </select>
      ";
            }
            // line 27
            echo "
      ";
            // line 28
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\Module\\CDev\\XPaymentsConnector\\View\\Button\\AddAddress", "label" => "New address", "profileId" => $this->getAttribute(($context["this"] ?? null), "getCustomerProfileId", [], "method")]]), "html", null, true);
            echo "

    </div>
  ";
            $this->endForm();            // line 31
            echo " 

";
        } else {
            // line 34
            echo "
  <div class=\"alert alert-danger add-new-card-error\">
    <strong class=\"important-label\">";
            // line 36
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Warning"]), "html", null, true);
            echo "!</strong>
    ";
            // line 37
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["No billing address is defined. To add new card please "]), "html", null, true);
            echo "
    ";
            // line 38
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\Module\\CDev\\XPaymentsConnector\\View\\Button\\AddAddress", "label" => "enter address for this profile", "profileId" => $this->getAttribute(($context["this"] ?? null), "getCustomerProfileId", [], "method")]]), "html", null, true);
            echo "
  </div>

";
        }
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/admin/modules/CDev/XPaymentsConnector/account/add_new_card.address.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 38,  99 => 37,  95 => 36,  91 => 34,  86 => 31,  80 => 28,  77 => 27,  73 => 25,  58 => 23,  54 => 22,  49 => 21,  43 => 18,  40 => 17,  38 => 16,  33 => 14,  28 => 11,  27 => 10,  24 => 9,  22 => 8,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/admin/modules/CDev/XPaymentsConnector/account/add_new_card.address.twig", "");
    }
}
