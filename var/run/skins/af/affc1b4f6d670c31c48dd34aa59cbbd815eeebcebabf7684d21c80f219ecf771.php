<?php

/* /home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/invoice/invoice.twig */
class __TwigTemplate_1848842cca9101a347c9934da8e79dbe4555c867c7f6fbb4e7702cae416140f2 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo "  ";
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "order", []), "getCCData", [], "method")) {
            // line 8
            echo "    <div class=\"xpc-card-box\">
  
      <strong>";
            // line 10
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Used credit cards"]), "html", null, true);
            echo "</strong>
      ";
            // line 11
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["this"] ?? null), "order", []), "getCCData", [], "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["cc"]) {
                // line 12
                echo "        <div class=\"xpc-card\">
          <span class=\"card-icon-container\">
            <span class=\"xpc-card-type ";
                // line 14
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "card_type_css", []), "html", null, true);
                echo "\">
              <img src=\"skins/customer/images/spacer.gif\" alt=\"";
                // line 15
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "card_type", []), "html", null, true);
                echo "\" />
            </span>
          </span>
          ";
                // line 18
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "card_number", []), "html", null, true);
                echo "
          ";
                // line 19
                if ($this->getAttribute($context["cc"], "expire", [])) {
                    // line 20
                    echo "            <span>(";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["cc"], "expire", []), "html", null, true);
                    echo ")</span>
          ";
                }
                // line 22
                echo "        </div>
        <br />
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cc'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "  
  </div>
  ";
        }
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/invoice/invoice.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 25,  60 => 22,  54 => 20,  52 => 19,  48 => 18,  42 => 15,  38 => 14,  34 => 12,  30 => 11,  26 => 10,  22 => 8,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/invoice/invoice.twig", "");
    }
}
