<?php

/* /home/kirill/domen/xcart/skins/admin/modules/CDev/XPaymentsConnector/order/transactions/body.twig */
class __TwigTemplate_e76180f0f888920d094731da7cfd013f1ef51d39c5a10956a600897409a7c8e9 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo "
";
        // line 8
        if ($this->getAttribute(($context["this"] ?? null), "getXpcTransactions", [], "method")) {
            // line 9
            echo "  <div class=\"xpc-transactions line-3\">
    ";
            // line 10
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "XLite\\Module\\CDev\\XPaymentsConnector\\View\\ItemsList\\Model\\Order\\Transactions"]]), "html", null, true);
            echo "
  </div>
";
        }
        // line 13
        echo "
<br/>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/admin/modules/CDev/XPaymentsConnector/order/transactions/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 13,  27 => 10,  24 => 9,  22 => 8,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/admin/modules/CDev/XPaymentsConnector/order/transactions/body.twig", "");
    }
}
