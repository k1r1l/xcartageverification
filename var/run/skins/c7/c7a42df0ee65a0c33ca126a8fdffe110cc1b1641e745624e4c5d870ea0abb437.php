<?php

/* /home/kirill/domen/xcart/skins/admin/order/packing_slip/parts/footer/footer.additional.twig */
class __TwigTemplate_aeb478da39db61748a9153470e2f28eee83aa318b0baddd374462330fcee7f9a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "<div class=\"thank-you\">";
        echo call_user_func_array($this->env->getFunction('t')->getCallable(), ["Thank you for your order FOOTER"]);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/admin/order/packing_slip/parts/footer/footer.additional.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/admin/order/packing_slip/parts/footer/footer.additional.twig", "");
    }
}
