<?php

/* modules/TrueMachine/AgeVerification/page/age_verification/body.twig */
class __TwigTemplate_7221cbee4698df347edfa4eac64ae86307009aac45f8b11798b5a81e6a22e8d4 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "<!-- The Modal -->
<div id=\"ageVerificationPopup\" class=\"age-verification-popup\"
     style=\"background-color: rgba(0,0,0,";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getBackgroundOpacity", [], "method"), "html", null, true);
        echo ");
     ";
        // line 7
        if (((($this->getAttribute(($context["this"] ?? null), "getAgeVerification", [], "method") != "1") && $this->getAttribute(($context["this"] ?? null), "getPopupIsEnabled", [], "method")) && ($this->getAttribute(($context["this"] ?? null), "isAdmin", [], "method") == false))) {
            echo " display: block; ";
        } else {
            echo " display: none; ";
        }
        echo "\">
    <!-- Modal content -->
    <div id=\"age-verification-popup-content\" class=\"age-verification-popup-content\"
         style=\"width: ";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getWidth", [], "method"), "html", null, true);
        echo "px; height: ";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getHeight", [], "method"), "html", null, true);
        echo "px; background-color: ";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getBackgroundColor", [], "method"), "html", null, true);
        echo "\">
        <h1 id=\"age-verification-popup-content-title\" class=\"title\">";
        // line 11
        echo $this->getAttribute(($context["this"] ?? null), "getTitle", [], "method");
        echo "</h1>
        <div class=\"age-label\">
            <p id=\"age-verification-popup-content-label-text\" class=\"age-label-text\">";
        // line 13
        echo $this->getAttribute(($context["this"] ?? null), "getMinAge", [], "method");
        echo "+</p>
        </div>
        <div id=\"age-verification-popup-content-description\" class=\"description\">
            ";
        // line 16
        if (($this->getAttribute(($context["this"] ?? null), "getAgeVerification", [], "method") == "0")) {
            echo " ";
            echo $this->getAttribute(($context["this"] ?? null), "getDenyMessage", [], "method");
            echo " ";
        } else {
            echo " ";
            echo $this->getAttribute(($context["this"] ?? null), "getDescription", [], "method");
            echo " ";
        }
        // line 17
        echo "        </div>
        <input type=\"hidden\" name=\"target\" value=\"age_verification\">
        <input type=\"hidden\" name=\"action\" value=\"send\">

        ";
        // line 21
        if ($this->getAttribute(($context["this"] ?? null), "getPopupType", [], "method")) {
            // line 22
            echo "            ";
            if (($this->getAttribute(($context["this"] ?? null), "getAgeVerification", [], "method") == "0")) {
                // line 23
                echo "                <div id=\"age-verification-popup-content-container-settings\" class=\"btn-container\"
                     style=\"display:block;\">
                    <button id=\"age-verification-popup-content-settings-btn-away\"
                            class=\"btn regular-button submit btn-away\"
                            type=\"submit\" name=\"btn_away\" value=\"1\"
                            style=\"display: inline-block\">";
                // line 28
                echo $this->getAttribute(($context["this"] ?? null), "getAwayButtonText", [], "method");
                echo "</button>
                    <div id=\"age-verification-popup-content-settings-container-input\" style=\"display:none;\">
                        <input id=\"age-verification-popup-content-settings-age\" class=\"input-age\" type=\"text\" name=\"age\"
                               placeholder=\"Your age\">
                    </div>
                    <button id=\"age-verification-popup-content-settings-btn-success\"
                            class=\"btn regular-button submit btn-success\" type=\"submit\" name=\"btn_success\"
                            value=\"1\" style=\"display:none;\">";
                // line 35
                echo $this->getAttribute(($context["this"] ?? null), "getSuccessButtonText", [], "method");
                echo "</button>
                </div>
                <div id=\"age-verification-popup-content-container-no-settings\" class=\"btn-container\"
                     style=\"display:none;\">
                    <button id=\"age-verification-popup-content-no-settings-btn-away\"
                            class=\"btn regular-button submit btn-away\"
                            type=\"submit\" name=\"btn_away\" value=\"1\">";
                // line 41
                echo $this->getAttribute(($context["this"] ?? null), "getAwayButtonText", [], "method");
                echo "</button>
                    <button id=\"age-verification-popup-content-no-settings-btn-failure\"
                            class=\"btn regular-button submit btn-failure\" type=\"submit\" name=\"btn_failure\"
                            value=\"1\">";
                // line 44
                echo $this->getAttribute(($context["this"] ?? null), "getFailureButtonText", [], "method");
                echo "</button>
                    <button id=\"age-verification-popup-content-no-settings-btn-success\"
                            class=\"btn regular-button submit btn-success\" type=\"submit\" name=\"btn_success\"
                            value=\"1\">";
                // line 47
                echo $this->getAttribute(($context["this"] ?? null), "getSuccessButtonText", [], "method");
                echo "</button>
                </div>
            ";
            } else {
                // line 50
                echo "                <div id=\"age-verification-popup-content-container-settings\" class=\"btn-container\"
                     style=\"display:block;\">
                    <button id=\"age-verification-popup-content-settings-btn-away\"
                            class=\"btn regular-button submit btn-away\"
                            type=\"submit\" name=\"btn_away\" value=\"1\"
                            style=\"display: none\">";
                // line 55
                echo $this->getAttribute(($context["this"] ?? null), "getAwayButtonText", [], "method");
                echo "</button>
                    <div id=\"age-verification-popup-content-settings-container-input\" style=\"display:block;\">
                        <input id=\"age-verification-popup-content-settings-age\" class=\"input-age\" type=\"text\" name=\"age\"
                               placeholder=\"Your age\">
                    </div>
                    <button id=\"age-verification-popup-content-settings-btn-success\"
                            class=\"btn regular-button submit btn-success\" type=\"submit\" name=\"btn_success\"
                            value=\"1\" style=\"display:inline-block;\">";
                // line 62
                echo $this->getAttribute(($context["this"] ?? null), "getSuccessButtonText", [], "method");
                echo "</button>
                </div>
                <div id=\"age-verification-popup-content-container-no-settings\" class=\"btn-container\"
                     style=\"display:none;\">
                    <button id=\"age-verification-popup-content-no-settings-btn-away\"
                            class=\"btn regular-button submit btn-away\"
                            type=\"submit\" name=\"btn_away\" value=\"1\">";
                // line 68
                echo $this->getAttribute(($context["this"] ?? null), "getAwayButtonText", [], "method");
                echo "</button>
                    <button id=\"age-verification-popup-content-no-settings-btn-failure\"
                            class=\"btn regular-button submit btn-failure\" type=\"submit\" name=\"btn_failure\"
                            value=\"1\">";
                // line 71
                echo $this->getAttribute(($context["this"] ?? null), "getFailureButtonText", [], "method");
                echo "</button>
                    <button id=\"age-verification-popup-content-no-settings-btn-success\"
                            class=\"btn regular-button submit btn-success\" type=\"submit\" name=\"btn_success\"
                            value=\"1\">";
                // line 74
                echo $this->getAttribute(($context["this"] ?? null), "getSuccessButtonText", [], "method");
                echo "</button>
                </div>
            ";
            }
            // line 77
            echo "        ";
        } else {
            // line 78
            echo "            ";
            if (($this->getAttribute(($context["this"] ?? null), "getAgeVerification", [], "method") == "0")) {
                // line 79
                echo "                <div id=\"age-verification-popup-content-container-settings\" class=\"btn-container\" style=\"display: none\">
                    <button id=\"age-verification-popup-content-settings-btn-away\" class=\"btn regular-button submit btn-away\"
                            type=\"submit\" name=\"btn_away\" value=\"1\" style=\"display: inline-block\">";
                // line 81
                echo $this->getAttribute(($context["this"] ?? null), "getAwayButtonText", [], "method");
                echo "</button>
                    <div id=\"age-verification-popup-content-settings-container-input\">
                        <input id=\"age-verification-popup-content-settings-age\" class=\"input-age\" type=\"text\" name=\"age\"
                               placeholder=\"Your age\">
                    </div>
                    <button id=\"age-verification-popup-content-settings-btn-success\"
                            class=\"btn regular-button submit btn-success\" type=\"submit\" name=\"btn_success\"
                            value=\"1\" style=\"display: none\">";
                // line 88
                echo $this->getAttribute(($context["this"] ?? null), "getSuccessButtonText", [], "method");
                echo "</button>
                </div>
                <div id=\"age-verification-popup-content-container-no-settings\" class=\"btn-container\" style=\"display:block;\">
                    <button id=\"age-verification-popup-content-no-settings-btn-away\" class=\"btn regular-button submit btn-away\"
                            type=\"submit\" name=\"btn_away\" value=\"1\" style=\"display: inline-block\">";
                // line 92
                echo $this->getAttribute(($context["this"] ?? null), "getAwayButtonText", [], "method");
                echo "</button>
                    <button id=\"age-verification-popup-content-no-settings-btn-failure\"
                            class=\"btn regular-button submit btn-failure\" type=\"submit\" name=\"btn_failure\"
                            value=\"1\" style=\"display:none;\">";
                // line 95
                echo $this->getAttribute(($context["this"] ?? null), "getFailureButtonText", [], "method");
                echo "</button>
                    <button id=\"age-verification-popup-content-no-settings-btn-success\"
                            class=\"btn regular-button submit btn-success\" type=\"submit\" name=\"btn_success\"
                            value=\"1\" style=\"display: none\">";
                // line 98
                echo $this->getAttribute(($context["this"] ?? null), "getSuccessButtonText", [], "method");
                echo "</button>
                </div>
            ";
            } else {
                // line 101
                echo "                <div id=\"age-verification-popup-content-container-settings\" class=\"btn-container\" style=\"display:none;\">
                    <button id=\"age-verification-popup-content-settings-btn-away\" class=\"btn regular-button submit btn-away\"
                            type=\"submit\" name=\"btn_away\" value=\"1\" style=\"display:none;\">";
                // line 103
                echo $this->getAttribute(($context["this"] ?? null), "getAwayButtonText", [], "method");
                echo "</button>
                    <div id=\"age-verification-popup-content-settings-container-input\">
                        <input id=\"age-verification-popup-content-settings-age\" class=\"input-age\" type=\"text\" name=\"age\"
                               placeholder=\"Your age\">
                    </div>
                    <button id=\"age-verification-popup-content-settings-btn-success\"
                            class=\"btn regular-button submit btn-success\" type=\"submit\" name=\"btn_success\"
                            value=\"1\">";
                // line 110
                echo $this->getAttribute(($context["this"] ?? null), "getSuccessButtonText", [], "method");
                echo "</button>
                </div>
                <div id=\"age-verification-popup-content-container-no-settings\" class=\"btn-container\" style=\"display:block;\">
                    <button id=\"age-verification-popup-content-no-settings-btn-away\" class=\"btn regular-button submit btn-away\"
                            type=\"submit\" name=\"btn_away\" value=\"1\" style=\"display:none;\">";
                // line 114
                echo $this->getAttribute(($context["this"] ?? null), "getAwayButtonText", [], "method");
                echo "</button>
                    <button id=\"age-verification-popup-content-no-settings-btn-failure\"
                            class=\"btn regular-button submit btn-failure\" type=\"submit\" name=\"btn_failure\"
                            value=\"1\" style=\"display:inline-block;\">";
                // line 117
                echo $this->getAttribute(($context["this"] ?? null), "getFailureButtonText", [], "method");
                echo "</button>
                    <button id=\"age-verification-popup-content-no-settings-btn-success\"
                            class=\"btn regular-button submit btn-success\" type=\"submit\" name=\"btn_success\"
                            value=\"1\" style=\"display:inline-block;\">";
                // line 120
                echo $this->getAttribute(($context["this"] ?? null), "getSuccessButtonText", [], "method");
                echo "</button>
                </div>
            ";
            }
            // line 123
            echo "        ";
        }
        // line 124
        echo "    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/TrueMachine/AgeVerification/page/age_verification/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  252 => 124,  249 => 123,  243 => 120,  237 => 117,  231 => 114,  224 => 110,  214 => 103,  210 => 101,  204 => 98,  198 => 95,  192 => 92,  185 => 88,  175 => 81,  171 => 79,  168 => 78,  165 => 77,  159 => 74,  153 => 71,  147 => 68,  138 => 62,  128 => 55,  121 => 50,  115 => 47,  109 => 44,  103 => 41,  94 => 35,  84 => 28,  77 => 23,  74 => 22,  72 => 21,  66 => 17,  56 => 16,  50 => 13,  45 => 11,  37 => 10,  27 => 7,  23 => 6,  19 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modules/TrueMachine/AgeVerification/page/age_verification/body.twig", "/home/kirill/domen/xcart/skins/customer/modules/TrueMachine/AgeVerification/page/age_verification/body.twig");
    }
}
