<?php

/* /home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/add_new_card.form.twig */
class __TwigTemplate_3ebbd4829e8628857d68236d169f5e02f9c0903750a06b7580c0a9bdebf9b369 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo "
<div id=\"add_new_card_iframe_container\">

  ";
        // line 10
        if ($this->getAttribute(($context["this"] ?? null), "getAddressList", [], "method")) {
            // line 11
            echo "    <iframe data-src=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "buildUrl", [0 => "add_new_card", 1 => "xpc_iframe"], "method"), "html", null, true);
            echo "\" width=\"300\" height=\"100%\" border=\"0\" style=\"border: 0px\" id=\"add_new_card_iframe\"></iframe>
    <div class=\"clearfix\"></div>
    <input type=\"button\" value=\"";
            // line 13
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Save credit card"]), "html", null, true);
            echo "\" id=\"submit-button\" class=\"btn regular-button regular-main-button submit add-new-card-button\" />
  ";
        }
        // line 15
        echo "
  <a class=\"back-to-cards\" href=\"";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('url')->getCallable(), [$this->env, $context, "saved_cards"]), "html", null, true);
        echo "\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Back to credit cards"]), "html", null, true);
        echo "</a>

</div>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/add_new_card.form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 16,  37 => 15,  32 => 13,  26 => 11,  24 => 10,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/add_new_card.form.twig", "");
    }
}
