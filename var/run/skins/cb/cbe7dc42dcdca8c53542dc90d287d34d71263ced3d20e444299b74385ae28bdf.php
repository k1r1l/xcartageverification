<?php

/* menu/marketplace/modules.twig */
class __TwigTemplate_9be34cdf8d02362af970566ec12e122f75a03c7cace8eb9048eba1a3c970bfc4 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "<li ";
        echo $this->getAttribute(($context["this"] ?? null), "printTagAttributes", [0 => $this->getAttribute(($context["this"] ?? null), "getNodeTagAttributes", [], "method")], "method");
        echo ">
  ";
        // line 5
        if ($this->getAttribute(($context["this"] ?? null), "getHeaderURL", [], "method")) {
            // line 6
            echo "    <a href=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getHeaderURL", [], "method"), "html", null, true);
            echo "\">
      <div class=\"notification-header\">
        ";
            // line 8
            echo $this->getAttribute(($context["this"] ?? null), "getIcon", [], "method");
            echo "
        <span>";
            // line 9
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getHeader", [], "method"), "html", null, true);
            echo "</span>
        ";
            // line 10
            if ($this->getAttribute(($context["this"] ?? null), "getCounter", [], "method")) {
                // line 11
                echo "          <div class=\"counter\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getCounter", [], "method"), "html", null, true);
                echo "</div>
        ";
            }
            // line 13
            echo "      </div>
    </a>
  ";
        } else {
            // line 16
            echo "    <div class=\"notification-header\">
      ";
            // line 17
            echo $this->getAttribute(($context["this"] ?? null), "getIcon", [], "method");
            echo "
      <span>";
            // line 18
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getHeader", [], "method"), "html", null, true);
            echo "</span>
      ";
            // line 19
            if ($this->getAttribute(($context["this"] ?? null), "getCounter", [], "method")) {
                // line 20
                echo "        <div class=\"counter\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getCounter", [], "method"), "html", null, true);
                echo "</div>
      ";
            }
            // line 22
            echo "    </div>
  ";
        }
        // line 24
        echo "
  <ul>
    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getMessages", [], "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 27
            echo "      <li>
        <a href=\"";
            // line 28
            echo $this->getAttribute($context["message"], "link", []);
            echo "\"";
            if ($this->getAttribute($context["message"], "external", [])) {
                echo " target=\"_blank\"";
            }
            echo ">
          <div class=\"message\">
            <img src=\"";
            // line 30
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["message"], "image", []), "html", null, true);
            echo "\" class=\"addon-icon\" alt=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["message"], "title", []), "html", null, true);
            echo "\"/>
            <div class=\"information\">
              <div class=\"title\">";
            // line 32
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["message"], "title", []), "html", null, true);
            echo "</div>
              <div class=\"description\">";
            // line 33
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["message"], "description", []), "html", null, true);
            echo "</div>
            </div>
          </div>
        </a>
      </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "  </ul>
</li>
";
    }

    public function getTemplateName()
    {
        return "menu/marketplace/modules.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 39,  107 => 33,  103 => 32,  96 => 30,  87 => 28,  84 => 27,  80 => 26,  76 => 24,  72 => 22,  66 => 20,  64 => 19,  60 => 18,  56 => 17,  53 => 16,  48 => 13,  42 => 11,  40 => 10,  36 => 9,  32 => 8,  26 => 6,  24 => 5,  19 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "menu/marketplace/modules.twig", "/home/kirill/domen/xcart/skins/admin/menu/marketplace/modules.twig");
    }
}
