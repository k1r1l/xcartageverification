<?php

/* /home/kirill/domen/xcart/skins/admin/order/page/parts/action.shipping_status.twig */
class __TwigTemplate_1b708c78f38ea8c4bf3f7e8ae044210df3546988792b751159242d9e3b0b9af8 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
";
        // line 7
        ob_start();
        // line 8
        echo "  <div class=\"status shipping-status\">
    ";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\View\\FormField\\Select\\OrderStatus\\Shipping", "label" => "Shipping status", "fieldName" => "shippingStatus", "order" => $this->getAttribute(($context["this"] ?? null), "order", []), "attributes" => $this->getAttribute(($context["this"] ?? null), "getOrderStatusAttributes", [0 => "shipping"], "method")]]), "html", null, true);
        echo "
    ";
        // line 10
        if ($this->getAttribute(($context["this"] ?? null), "isDisplayBackorderWarning", [], "method")) {
            // line 11
            echo "      ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "XLite\\View\\Tooltip", "text" => $this->getAttribute(($context["this"] ?? null), "getBackorderWarningText", [], "method"), "isImageTag" => "true", "className" => "warning-icon", "imageClass" => "fa fa-exclamation-triangle"]]), "html", null, true);
            echo "
    ";
        }
        // line 13
        echo "  </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/admin/order/page/parts/action.shipping_status.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 13,  33 => 11,  31 => 10,  27 => 9,  24 => 8,  22 => 7,  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/admin/order/page/parts/action.shipping_status.twig", "");
    }
}
