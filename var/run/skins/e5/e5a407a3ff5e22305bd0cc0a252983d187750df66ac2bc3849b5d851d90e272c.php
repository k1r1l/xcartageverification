<?php

/* /home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/checkout/script.twig */
class __TwigTemplate_da107a9598bc5673939dae9177f9b45af4a3973132b24a4fdd354cdd230066db extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 8
        echo "<script type=\"text/javascript\">
var xpcPaymentIds = [];
";
        // line 10
        if ($this->getAttribute(($context["this"] ?? null), "getXpcPaymentIds", [], "method")) {
            // line 11
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["this"] ?? null), "getXpcPaymentIds", [], "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["id"]) {
                echo "xpcPaymentIds[";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["id"], "html", null, true);
                echo "] = '";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $context["id"], "html", null, true);
                echo "';";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['id'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "
var xpcSavedCardPaymentId = '";
            // line 13
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getXpcSavedCardPaymentId", [], "method"), "html", null, true);
            echo "';
var xpcBillingAddressId = '";
            // line 14
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getXpcBillingAddressId", [], "method"), "html", null, true);
            echo "'; 
var xpcUseIframe = '";
            // line 15
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "isUseIframe", [], "method"), "html", null, true);
            echo "';
var currentPaymentId = ";
            // line 16
            if ($this->getAttribute(($context["this"] ?? null), "isCheckoutReady", [], "method")) {
                echo "'";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getPaymentId", [], "method"), "html", null, true);
                echo "'";
            } else {
                echo "false";
            }
            echo ";
";
        } else {
            // line 18
            echo "var xpcSavedCardPaymentId = '0';
var xpcBillingAddressId = '0'; 
var xpcUseIframe = '0';
var currentPaymentId = false;
";
        }
        // line 23
        echo "</script>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/checkout/script.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 23,  64 => 18,  53 => 16,  49 => 15,  45 => 14,  41 => 13,  38 => 12,  25 => 11,  23 => 10,  19 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/checkout/script.twig", "");
    }
}
