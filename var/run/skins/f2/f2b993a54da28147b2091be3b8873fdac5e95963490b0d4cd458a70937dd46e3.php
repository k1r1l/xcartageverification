<?php

/* /home/kirill/domen/xcart/skins/customer/product/search/parts/substring.button.twig */
class __TwigTemplate_7f850e11f7aa7ad63dbe55bcc34eed99db6260604dfe827eb8d35a82b6c0226d extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo "
<td class=\"submit-button\">
  ";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "\\XLite\\View\\Button\\Submit", "label" => "Search", "style" => "search-form-submit"]]), "html", null, true);
        echo "
</td>
";
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/product/search/parts/substring.button.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 9,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/product/search/parts/substring.button.twig", "");
    }
}
