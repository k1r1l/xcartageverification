<?php

/* /home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/add_new_card.header.twig */
class __TwigTemplate_c0741c822877a20501b332b687916051eb0b60ef7c3ef37c9cfa455ed91e6367 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 7
        echo "
";
        // line 8
        if ($this->getAttribute(($context["this"] ?? null), "getAmount", [], "method")) {
            // line 9
            echo "  <div class=\"alert alert-warning add-new-card-header\" role=\"alert\">
    <strong class=\"important-label\">";
            // line 10
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["Important!"]), "html", null, true);
            echo "</strong>
    ";
            // line 11
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["We will authorize"]), "html", null, true);
            echo "
    <strong class=\"highlight-label\">";
            // line 12
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), [$this->env, $context, [0 => "XLite\\View\\Surcharge", "surcharge" => $this->getAttribute(($context["this"] ?? null), "getAmount", [], "method")]]), "html", null, true);
            echo "</strong>
    ";
            // line 13
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["on your credit card in order to attach this credit card to your account. The amount will be released back to your card after a while."]), "html", null, true);
            echo "
    ";
            // line 14
            if ($this->getAttribute(($context["this"] ?? null), "getDescription", [], "method")) {
                // line 15
                echo "      <span>
        ";
                // line 16
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), ["The transaction will be marked as"]), "html", null, true);
                echo "
        <strong class=\"highlight-label\">&ldquo;";
                // line 17
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute(($context["this"] ?? null), "getDescription", [], "method"), "html", null, true);
                echo "&rdquo;</strong>.
      </span>
    ";
            }
            // line 20
            echo "  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/add_new_card.header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 20,  52 => 17,  48 => 16,  45 => 15,  43 => 14,  39 => 13,  35 => 12,  31 => 11,  27 => 10,  24 => 9,  22 => 8,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/home/kirill/domen/xcart/skins/customer/modules/CDev/XPaymentsConnector/account/add_new_card.header.twig", "");
    }
}
