<?php

/* header/parts/ie.twig */
class __TwigTemplate_6c217d5f42c520f274c421582c165c5cc9c1e259310a3bc1794ba2437476bc31 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\" />
";
    }

    public function getTemplateName()
    {
        return "header/parts/ie.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "header/parts/ie.twig", "/home/kirill/domen/xcart/skins/admin/header/parts/ie.twig");
    }
}
